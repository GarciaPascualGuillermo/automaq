<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Customer, Name, Status, Customer_Since 
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('customer',50)->unique();
            $table->string('name',100);
            $table->string('status',50)->nullable();
            $table->datetime('customer_since')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
