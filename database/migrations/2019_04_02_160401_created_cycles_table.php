<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycles', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->uuid('id')->primary();

            // $table->uuid('process_id')->unsigned()->nullable();
            // $table->foreign('process_id')->references('id')->on('processes')->onDelete('cascade');

            // $table->bigInteger('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->string('process_status');
            $table->string('message');

            $table->timestamp('message_date');
            // $table->timestamp('ended_at')->nullable();
            $table->timestamps();

        });
        DB::statement('ALTER TABLE cycles ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cycles');
    }
}
