<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkcenterLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workcenter_logs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('workcenter_id')->unsigned();
            $table->foreign('workcenter_id')->references('id')->on('workcenters')->onDelete('cascade');

            $table->string('description');
            $table->string('ip')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workcenter_logs');
    }
}
