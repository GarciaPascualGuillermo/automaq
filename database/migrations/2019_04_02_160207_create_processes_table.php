<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->uuid('id')->primary();


            $table->uuid('job_id')->unsigned()->nullable();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->uuid('operation_id')->unsigned()->nullable();
            $table->foreign('operation_id')->references('id')->on('operations')->onDelete('cascade');

            $table->uuid('capeando_id')->unsigned()->nullable();
            $table->foreign('capeando_id')->references('id')->on('operations')->onDelete('cascade');

            $table->bigInteger('workcenter_id')->unsigned()->nullable();
            $table->foreign('workcenter_id')->references('id')->on('workcenters')->onDelete('cascade');

            $table->bigInteger('ajustador_ia_id')->unsigned()->nullable();
            $table->foreign('ajustador_ia_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_fa_id')->unsigned()->nullable();
            $table->foreign('ajustador_fa_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_il_id')->unsigned()->nullable();
            $table->foreign('ajustador_il_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_ial_id')->unsigned()->nullable();
            $table->foreign('ajustador_ial_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_pm_id')->unsigned()->nullable();
            $table->foreign('ajustador_pm_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('status', ["ih","fh","ia","fa","il","ial","ls","ln","pr","pm","pmp","pfh","pfm","cr"])->nullable();
            
            $table->timestamp('ih_created_at')->nullable();
            $table->timestamp('fh_created_at')->nullable();
            $table->timestamp('ia_created_at')->nullable();
            $table->timestamp('fa_created_at')->nullable();
            $table->timestamp('il_created_at')->nullable();
            $table->timestamp('ial_created_at')->nullable();
            $table->timestamp('ls_created_at')->nullable();
            $table->timestamp('ln_created_at')->nullable();
            $table->timestamp('pr_created_at')->nullable();
            $table->timestamp('pm_created_at')->nullable();
            $table->timestamp('pmp_created_at')->nullable();
            $table->timestamp('pfh_created_at')->nullable();
            $table->timestamp('pfm_created_at')->nullable();
            $table->timestamp('cr_created_at')->nullable();
            
            $table->integer('order')->unsigned()->nullable();
            $table->integer('scraps')->nullable();
            
            $table->string('operation_service',100)->nullable();
            $table->string('run_method')->unsigned()->nullable();
            $table->decimal('standar_time',10,2)->unsigned()->nullable();
            
            $table->integer('quantity')->default(0);

            $table->integer('total_piece')->default(0);
            $table->boolean('active')->default(false);
            
            // $table->string("");
            $table->timestamps();
        });
        DB::statement('ALTER TABLE processes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes');
    }
}
