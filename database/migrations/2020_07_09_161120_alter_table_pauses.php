<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePauses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pauses', function (Blueprint $table) {
            $table->string('type')->after('motivo')->nullable();
            $table->string('concept')->after('motivo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pauses', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('concept');
        });
    }
}
