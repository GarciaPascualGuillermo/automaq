<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedWorkcentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workcenters', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('num_machine',10);
            $table->string('num_serie_tunerd')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('section')->nullable();
            $table->string('work_center')->nullable();
            $table->string('department')->nullable();

            $table->boolean('support')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workcenters');
    }
}
