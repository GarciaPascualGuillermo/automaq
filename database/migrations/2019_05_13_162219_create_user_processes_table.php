<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_processes', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->uuid('id')->primary();

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->uuid('process_id')->unsigned()->nullable();
            $table->foreign('process_id')->references('id')->on('processes')->onDelete('cascade');

            $table->integer('start')->unsigned();
            $table->integer('end')->unsigned()->nullable();
            $table->date('start_at')->nullable();
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('log_out')->default(false);
            $table->boolean('capeando')->default(false);


            $table->timestamps();
        });
        DB::statement('ALTER TABLE user_processes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_processes');
    }
}
