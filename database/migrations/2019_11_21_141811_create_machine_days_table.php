<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachineDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_days', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->bigInteger('workcenter_id')->unsigned();
            $table->foreign('workcenter_id')->references('id')->on('workcenters')->onDelete('cascade');

            $table->date('day')->index();

            $table->decimal('ajuste',10,2)->default(0);
            $table->decimal('herramentaje',10,2)->default(0);
            $table->decimal('liberacion',10,2)->default(0);
            $table->decimal('autoliberacion',10,2)->default(0);
            $table->decimal('produccion',10,2)->default(0);
            $table->decimal('mantenimiento',10,2)->default(0);
            $table->decimal('pausa_otros',10,2)->default(0);
            $table->decimal('sinuso',10,2)->default(0);
            $table->decimal('setup',10,2)->default(0);

            $table->timestamps();
        });
        DB::statement('ALTER TABLE setup ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_days');
    }
}
