<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStatusColumnProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processes', function (Blueprint $table) {
            // $table->enum('status', ["ih","fh","ia","fa","il","ial","ls","ln","pr","pm","pmp","pfh","pfm","cr","crp"])->nullable()->change();
            DB::statement('BEGIN;');
            DB::statement('alter table processes drop constraint processes_status_check;');
            DB::statement("alter table processes add CONSTRAINT processes_status_check check (status IN ('ih','fh','ia','fa','il','ial','ls','ln','pr','pm','pmp','pfh','pfm','cr','crp'));");
            DB::statement("COMMIT;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processes', function (Blueprint $table) {
            //
        });
    }
}
