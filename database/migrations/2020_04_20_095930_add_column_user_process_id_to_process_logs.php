<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserProcessIdToProcessLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('process_logs', function (Blueprint $table) {

            $table->uuid('user_process_id')->after('parent_id')->nullable();
            $table->foreign('user_process_id')->references('id')->on('user_processes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('process_logs', function (Blueprint $table) {

            $table->dropColumn('user_process_id');

        });
    }
}
