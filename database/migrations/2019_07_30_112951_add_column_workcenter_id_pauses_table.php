<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnWorkcenterIdPausesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pauses', function (Blueprint $table) {
            $table->bigInteger('workcenter_id')->unsigned()->nullable();
            $table->foreign('workcenter_id')->references('id')->on('workcenters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pauses', function (Blueprint $table) {
            $table->dropForeign(['workcenter_id']);
            $table->dropColumn("workcenter_id");
        });
    }
}
