<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexesSetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setup', function (Blueprint $table) {
            $table->index(['ih_created_at']);
        });
        Schema::table('user_processes', function (Blueprint $table) {
            $table->index(['status_start']);
        });
        Schema::table('pauses', function (Blueprint $table) {
            $table->index(['motivo']);
            $table->index(['started_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setup', function (Blueprint $table) {
            $table->dropIndex(['ih_created_at']);
        });
        Schema::table('user_processes', function (Blueprint $table) {
            $table->dropIndex(['status_start']);
        });
        Schema::table('pauses', function (Blueprint $table) {
            $table->dropIndex(['motivo']);
            $table->dropIndex(['started_at']);
        });
    }
}
