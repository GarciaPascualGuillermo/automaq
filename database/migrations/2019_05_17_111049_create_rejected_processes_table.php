<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectedProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rejected_processes', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('process_id')->unsigned();
            $table->foreign('process_id')->references('id')->on('processes')->onDelete('cascade');

            $table->string('comment')->nullable();

            $table->timestamp('started_at');
            $table->timestamp('ended_at');


            $table->timestamps();
        });
        DB::statement('ALTER TABLE user_processes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rejected_processes');
    }
}
