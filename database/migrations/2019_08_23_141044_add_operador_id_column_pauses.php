<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOperadorIdColumnPauses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pauses', function (Blueprint $table) {
            $table->bigInteger('operador_id')->unsigned()->nullable();
            $table->foreign('operador_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pauses', function (Blueprint $table) {
            $table->dropColumn(['operador_id']);
        });
    }
}
