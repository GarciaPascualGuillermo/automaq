<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            // $table->bigIncrements('id')->unsigned();
            $table->uuid('id')->primary();

            $table->bigInteger('piece_id')->unsigned()->nullable();
            $table->foreign('piece_id')->references('id')->on('pieces')->onDelete('cascade');

            $table->string('job_operation',100)->nullable();
            $table->string('job',100)->nullable();
            $table->string('work_center',100)->nullable();
            $table->string('operation_service',100)->nullable();
            $table->string('run_method')->unsigned()->nullable();
            $table->decimal('standar_time',10,2)->unsigned()->nullable();
            $table->integer('sequence')->unsigned()->nullable();
            $table->string('description')->nullable();

            $table->timestamps();
        });
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
