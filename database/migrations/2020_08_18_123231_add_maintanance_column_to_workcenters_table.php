<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaintananceColumnToWorkcentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workcenters', function (Blueprint $table) {
            $table->boolean('on_maintenance')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workcenters', function (Blueprint $table) {
            $table->dropColumn('on_maintenance');
        });
    }
}
