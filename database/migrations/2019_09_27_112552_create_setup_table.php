<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('process_id')->unsigned();
            $table->foreign('process_id')->references('id')->on('processes')->onDelete('cascade');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('workcenter_id')->unsigned();
            $table->foreign('workcenter_id')->references('id')->on('workcenters')->onDelete('cascade');

            $table->bigInteger('ajustador_ia_id')->unsigned()->nullable();
            $table->foreign('ajustador_ia_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_fa_id')->unsigned()->nullable();
            $table->foreign('ajustador_fa_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_il_id')->unsigned()->nullable();
            $table->foreign('ajustador_il_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('ajustador_ial_id')->unsigned()->nullable();
            $table->foreign('ajustador_ial_id')->references('id')->on('users')->onDelete('cascade');

            // $table->enum('status', ["ih","fh","ia","fa","il","ial","ls","ln","pr","pm","pmp","pfh","pfm","cr"])->nullable();
            
            $table->timestamp('ih_created_at')->nullable();
            $table->timestamp('fh_created_at')->nullable();
            $table->timestamp('ia_created_at')->nullable();
            $table->timestamp('fa_created_at')->nullable();
            $table->timestamp('il_created_at')->nullable();
            $table->timestamp('ial_created_at')->nullable();
            $table->timestamp('ls_created_at')->nullable();
            $table->timestamp('ln_created_at')->nullable();

            $table->timestamps();
            $table->boolean('active')->default(true);
        });
        DB::statement('ALTER TABLE setup ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup');
    }
}
