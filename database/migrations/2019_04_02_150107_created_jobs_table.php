<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->uuid('id')->primary();
            

            $table->string('num_job');
            $table->string('num_part')->nullable();
            $table->integer('quantity')->default(0);
            $table->date('delivered_at');
            $table->date('release_date')->nullable();

            $table->timestamps();
        });
        DB::statement('ALTER TABLE jobs ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
