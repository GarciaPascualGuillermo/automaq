<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunmSetupIdRejectedProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rejected_processes', function (Blueprint $table) {
            $table->uuid('setup_id')->unsigned()->nullable();
            $table->foreign('setup_id')->references('id')->on('setup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rejected_processes', function (Blueprint $table) {
            $table->dropColumn(['setup_id']);
        });
    }
}
