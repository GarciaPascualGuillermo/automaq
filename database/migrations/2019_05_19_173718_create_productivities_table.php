<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productivities', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('user_process_id')->unsigned()->nullable();
            $table->foreign('user_process_id')->references('id')->on('user_processes')->onDelete('cascade');

            $table->decimal('percentage',6,3);
            $table->date('percentage_date');

            $table->timestamps();
        });
        DB::statement('ALTER TABLE user_processes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productivities');
    }
}
