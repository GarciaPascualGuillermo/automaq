<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_processes', function (Blueprint $table) {
            // $table->index(['log_out', 'start_time','end_time']);
            $table->index(['log_out']);
            $table->index(['start_time']);
            $table->index(['end_time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_processes', function (Blueprint $table) {
            // $table->dropIndex(['log_out', 'start_time','end_time']);
            $table->dropIndex(['log_out']);
            $table->dropIndex(['start_time']);
            $table->dropIndex(['end_time']);
        });
    }
}
