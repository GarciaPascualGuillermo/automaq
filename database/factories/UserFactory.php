<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'idemployee'=>'0'.$faker->unique()->randomNumber($nbDigits = 3), 
        'username' => $faker->unique()->username,
        'email_verified_at' => now(),
        'password' => 'hola123,',
        'mealtime'=>"11:00:00",
        'remember_token' => Str::random(10),
    ];
});
