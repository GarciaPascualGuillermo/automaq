<?php

use App\Job;
use App\Role;
use App\Client;
use App\Process;
use App\Workcenter;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles[]=Role::create([
        	'name'  =>  'ceo',
        	'description'   =>  'CEO',
        ]);
        $roles[]=Role::create([
        	'name'  =>  'gte.admin',
        	'description'   =>  'Gerente administración',
        ]);
        $roles[]=Role::create([
        	'name'  =>  'gte.operacion',
        	'description'   =>  'Gerente de operaciones',
        ]);
        $roles[]=Role::create([
            'name'  =>  'jefe.produccion',
            'description'   =>  'Jefe de producción',
        ]);
        $roles[]=Role::create([
            'name'  =>  'sup.produccion',
            'description'   =>  'Supervisor de producción',
        ]);
        $roles[]=Role::create([
            'name'  =>  'sup.linea',
            'description'   =>  'Supervisor de Línea',
        ]);
        $roles[]=Role::create([
            'name'  =>  'ajustador',
            'description'   =>  'Ajustador',
        ]);
        $roles[]=Role::create([
            'name'  =>  'operador',
            'description'   =>  'Operador',
        ]);
        $roles[]=Role::create([
            'name'  =>  'sistemas',
            'description'   =>  'Sistemas',
        ]);
        $roles[]=Role::create([
            'name'  =>  'planeacion',
            'description'   =>  'Planeación',
        ]);
        $roles[]=Role::create([
            'name'  =>  'cuenta',
            'description'   =>  'Encargados de cuenta',
        ]);
        $roles[]=Role::create([
            'name'  =>  'gte.calidad',
            'description'   =>  'Gerente y jefe de calidad',
        ]);
        $roles[]=Role::create([
            'name'  =>  'insp.calidad',
            'description'   =>  'Inspección de caliadad',
        ]);
        $roles[]=Role::create([
            'name'  =>  'jefe.mantenimiento',
            'description'   =>  'Jefe de mantenimiento',
        ]);
        $roles[]=Role::create([
            'name'  =>  'tec.mantenimiento',
            'description'   =>  'Técnico de mantenimiento',
        ]);
        $roles[]=Role::create([
            'name'  =>  'dep.mantenimiento',
            'description'   =>  'Departamento mantenimiento',
        ]);
        $roles[]=Role::create([
            'name'  =>  'dep.caliadad',
            'description'   =>  'Departamento de calidad',
        ]);

        if (App::environment('local')) {
            foreach ($roles as $role) {
                $users = factory(App\User::class, 1)
                    ->create()
                    ->each(function ($user) use ($role) {
                        $user->roles()->attach($role);
                    });

            }
        }
    }
}
