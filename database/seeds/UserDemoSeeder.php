<?php

use App\Role;
use Illuminate\Database\Seeder;

class UserDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles=Role::all();
        if (App::environment('local')) {
            foreach ($roles as $role) {
                $users = factory(App\User::class, 1)
                    ->create()
                    ->each(function ($user) use ($role) {
                        $user->roles()->attach($role);
                    });

            }
        }
    }
}
