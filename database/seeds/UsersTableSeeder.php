<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('roles')->delete();
        $role=Role::create([
            'name'  =>  'root',
            'description'   =>  'Administrator root del sitio',
        ]);

        $user=$role->users()->create([
            'name'=>'Administrator',
            'firstname' =>'Root',
            'username'  =>  'root.admin',
            'email_verified_at' => now(),
            'idemployee' => "000",
            'password'  =>  'hola123,',
            'remember_token' => Str::random(10),
            'email'     =>  'admin@tunerd.mx',
        ]);
    }
}
