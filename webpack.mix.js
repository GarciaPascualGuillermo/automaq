const mix = require('laravel-mix');

mix
   .js('resources/js/operadores/app.js', 'public/js/operadores')
   .js('resources/js/login/app.js', 'public/js/login')
   .js('resources/js/jobs/asignados/app.js', 'public/js/jobs/asignados')
   .js('resources/js/jobs/JobsAssign.js', 'public/js/jobs')
   .js('resources/js/jobs/app.js', 'public/js/jobs')
   .js('resources/js/indicadores/app.js', 'public/js/indicadores')
   .js('resources/js/solicitudes_control/app.js', 'public/js/solicitudes_control')



   // .sass('resources/sass/operadores/app.scss', 'public/css/operadores')
   // .sass('resources/sass/login/app.scss', 'public/css/login')
   // .sass('resources/sass/jobs/app.scss', 'public/css/jobs')

   // .sass('resources/sass/operadores/operador.scss', 'public/css')
   // .sass('resources/sass/operadores/setup.scss', 'public/css')
    //    usuarios
   // .sass('resources/sass/usuarios/app.scss', 'public/css/usuarios')
    //    Parametros
   // .sass('resources/sass/parametros/app.scss', 'public/css/parametros')
    //    Centro de Trabajo
   // .sass('resources/sass/centro_trabajo/app.scss', 'public/css/centro_trabajo')

   // .sass('resources/sass/operadores/setup/app.scss', 'public/css/operadores/setup')

   // .sass('resources/sass/indicadores/app.scss', 'public/css/indicadores')

   // .sass('resources/sass/solicitudes_control/app.scss','public/css/solicitudes_control')

   // .sass('resources/sass/productividad/app.scss','public/css/productividad')

   .js('resources/js/schedule/app.js', 'js/schedule')

    .sourceMaps()
    .version()

   ;
