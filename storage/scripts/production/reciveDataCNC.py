#!/usr/bin/python3.6

# Python 3.5,3.6,3.7
# require
# python-devel
# pip
# pip install
## pip install setproctitle requests pyserial
import serial
from time import strftime
import json
import requests
import sys
import os
import setproctitle

idProcess = os.getpid()
print (idProcess)

setproctitle.setproctitle("TuNerdCNC")

path = "./"

def logs(data="",error=False):
	logs_path="./logs/"
	if not os.path.exists(logs_path):
		os.mkdir(logs_path)
	if error:
		name = "reciveDataCNCError"+strftime("%Y%m%d")+".log"
	else:
		name = "reciveDataCNC"+strftime("%Y%m%d")+".log"
	archivo = logs_path+name
	f=open(archivo,'a')
	f.write("\n"+strftime("%Y-%m-%d %H:%M:%S")+"\t"+data)
	f.close()

def save_data(data):
	data = data.replace(b'\xb3',b'|').replace(b'\xc4', b'').replace(b'\xda', b'').replace(b'\xbf', b'').replace(b'\xc3', b'').replace(b'\xb4', b'').replace(b'\xc5', b'').replace(b'\xa2', b'o').replace(b'\xa1', b'i').replace(b'\xa3', b'u').replace(b'\xc0', b'').replace(b'\xc1', b'').replace(b'\xd9', b'').replace(b'\xc2', b'').replace(b'\x82', b'e').replace(b'\xa4', b'n').replace(b'\xa0', b'a').replace(b'\x14',b'').replace(b'\x12',b'')
	try:
		logs(str(data.decode().strip()))
		sendData(data.decode().strip())
		print(data.decode().strip())
	except UnicodeDecodeError:
		logs("UnicodeDecodeError"+str(data))
		sendData(data)
		print(data)


def isS(data):
	data = data.replace(b'\xb3',b'|').replace(b'\xc4', b'').replace(b'\xda', b'').replace(b'\xbf', b'').replace(b'\xc3', b'').replace(b'\xb4', b'').replace(b'\xc5', b'').replace(b'\xa2', b'o').replace(b'\xa1', b'i').replace(b'\xa3', b'u').replace(b'\xc0', b'').replace(b'\xc1', b'').replace(b'\xd9', b'').replace(b'\xc2', b'').replace(b'\x82', b'e').replace(b'\xa4', b'n').replace(b'\xa0', b'a').replace(b'\x14',b'').replace(b'\x12',b'')
	try:
		if data.decode().strip()=='s' or data.decode().strip()=='S':
			return True
		# print("-- "+data.decode().strip())
	except UnicodeDecodeError:
			#print(data)
		if data=='s' or data == 'S':
			return True
	return False
def isOne(data):
	data = data.replace(b'\xb3',b'|').replace(b'\xc4', b'').replace(b'\xda', b'').replace(b'\xbf', b'').replace(b'\xc3', b'').replace(b'\xb4', b'').replace(b'\xc5', b'').replace(b'\xa2', b'o').replace(b'\xa1', b'i').replace(b'\xa3', b'u').replace(b'\xc0', b'').replace(b'\xc1', b'').replace(b'\xd9', b'').replace(b'\xc2', b'').replace(b'\x82', b'e').replace(b'\xa4', b'n').replace(b'\xa0', b'a').replace(b'\x14',b'').replace(b'\x12',b'')
	try:
		if data.decode().strip()=='1':
			return True
		# print("-- "+data.decode().strip())
	except UnicodeDecodeError:
			#print(data)
		if data=='1':
			return True
	return False

def sendData(data=""):
	#VAR
	host="tucontrol.tecmaq.local"
	uri="/api/recive"
	url="https://"+host+uri
	cachain="./ca-chain.cert.pem"
	#DATA
	data = {'ct-name':'CT-009','ct-data':data,'ct-timestamp':strftime("%Y-%m-%d %H:%M:%S")}
	#data_json = json.dumps(data)

	#payload = {'json_payload': data_json, 'apikey':'CT-09'}
	payload = {'json_payload': data, 'apikey':'CT-09'}

	headers = {'user-agent': 'CT-009','content-type': 'application/json'}
	logs("[url] "+url)
	logs("[url data] "+str(payload))
	try:
		r = requests.post(url, data=json.dumps(payload),verify=cachain,headers=headers)
		print (r.content,r,r.status_code)
		logs("content: "+str(r.content)+"status: "+str(r.status_code))
	except Exception as e:
		logs("Error")
		logs("Error",True)
		logs (str(e),True)
		logs(str(json.dumps(payload)),True)
		logs("",True)
	except ConnectionError as e:
		logs("Error to connect server")
		logs("Error to connect server",True)
		logs(str(json.dumps(payload)),True)
		logs (str(e),True)
		logs(error=True);

s = serial.Serial(
	port = '/dev/ttyS0',
	baudrate = 9600,
	bytesize = 7,
	parity = serial.PARITY_EVEN,
	stopbits = 1
	)
try:
	#import serial.tools.list_ports
	#myports = [tuple(p) for p in list(serial.tools.list_ports.comports())]
	#print (myports)
	print('antes de abrir')
	s.open
	# Clean Buffer
	s.flushInput()
	s.setDTR()

	# State port
	logs("Port: " + s.name)
	logs("State: "+ str(s.isOpen()))

	print('despues')
	# Do while the port is open
	while s.isOpen():
		# Read
		caracter=s.read()
		print (caracter)
		if isS(caracter) or isOne(caracter):
			serial_data=s.read(4)
			print (caracter+serial_data)
			save_data(caracter+serial_data)
		# serial_data = s.readline()
		# # Store
		# save_data(serial_data)

except KeyboardInterrupt:
	# quit
	s.close()
	print("End "+str(idProcess)+"\n")
	sys.exit()