#!/bin/bash
if [ $# -eq 0 ]; then
	#echo "$0: [with_password 1|0]"
	pass=0;
fi
if [ $# -eq 1 ]; then
	if [ $1 == "--help" ] || [ $1 == '-h' ]; then
		echo "$0: [with_password 1|0]"
		exit 1
	fi
	pass=1;
fi
echo "Prepare the directory"
HOME_CA="/opt/sites"
if [ ! -d "$HOME_CA/ca" ]; then
	mkdir $HOME_CA/ca
	sudo semanage fcontext -a -t httpd_sys_content_t "$HOME_CA/ca(/.*)?"
	sudo restorecon -vFR $HOME_CA
fi
cd $HOME_CA/ca/
if [ ! -d "certs" ] || [ ! -d "crl" ] || 
[ ! -d "newcerts" ] || [ ! -d "private" ] ; then
	mkdir certs crl newcerts private
fi
chmod 700 private/
touch index.txt
echo 1000 > serial
echo "Prepare the configuration file"
echo '
[ ca ]
# `man ca`
default_ca = CA_default
[ CA_default ]
# Directory and file locations.
dir               = '$HOME_CA'/ca
certs             = $dir/certs
crl_dir           = $dir/crl
new_certs_dir     = $dir/newcerts
database          = $dir/index.txt
serial            = $dir/serial
RANDFILE          = $dir/private/.rand

# The root key and root certificate.
private_key       = $dir/private/ca.key.pem
certificate       = $dir/certs/ca.cert.pem

# For certificate revocation lists.
crlnumber         = $dir/crlnumber
crl               = $dir/crl/ca.crl.pem
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 375
preserve          = no
policy            = policy_strict

[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of `man ca`.
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the `req` tool (`man req`).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = MX
stateOrProvinceName_default     = Mexico
localityName_default            = CDMX
0.organizationName_default      = Localhost
#organizationalUnitName_default =
#emailAddress_default           =

[ v3_ca ]
# Extensions for a typical CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:0
keyUsage = critical, digitalSignature, cRLSign, keyCertSign


[ usr_cert ]
# Extensions for client certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = 127.0.0.1

[ crl_ext ]
# Extension for CRLs (`man x509v3_config`).
authorityKeyIdentifier=keyid:always

[ ocsp ]
# Extension for OCSP signing certificates (`man ocsp`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning
' > $HOME_CA/ca/openssl.cnf

echo "reate the root key"
cd $HOME_CA/ca
# Enter pass phrase for ca.key.pem: secretpassword
# Verifying - Enter pass phrase for ca.key.pem: secretpassword

openssl genrsa -out private/ca.key.pem 4096
chmod 400 private/ca.key.pem
if [ $pass -eq 1 ]; then
	echo "entro"
	openssl rsa -in private/ca.key.pem -out private/ca.key.pem
fi

openssl req -config openssl.cnf \
	-key private/ca.key.pem \
	-new -x509 -days 18250 -sha256 -extensions v3_ca \
	-out certs/ca.cert.pem

chmod 444 certs/ca.cert.pem
echo "Verify the root certificate"
#Verify the root certificate
openssl x509 -noout -text -in certs/ca.cert.pem
