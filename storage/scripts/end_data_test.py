#!/usr/bin/python3.6

# Python 3.5,3.6,3.7
# require
# python-devel
# pip
# pip install
## pip install setproctitle requests pyserial 
import serial
from time import strftime
import json
import requests
import sys
import os
import setproctitle


idProcess = os.getpid()
print (idProcess)

setproctitle.setproctitle("TuNerdCNC")

path = "./"

s = serial.Serial(
	port = '/dev/ttyS1',
	baudrate = 9600,
	bytesize = 7,
	parity = serial.PARITY_EVEN,
	stopbits = 1
	)
try:

	print('antes de abrir')
	s.open
	# Clean Buffer
	s.flushInput()
	s.setDTR()
          
	# State port
	print("Port: " + s.name)
	print("State: "+ str(s.isOpen()))
	
	print('despues')
	print("Enviando.... Prueba de COM")
	s.write(b"Prueba de COM")

except KeyboardInterrupt:
    # quit
	s.close()
	print("End "+str(idProcess)+"\n")
	sys.exit()
