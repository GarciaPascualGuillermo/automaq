#!/bin/bash
function usage {
	echo "usage: $0 usr_cert|server_cert name_cert [without_password 1|0]";
	echo "	 example: "
	echo "	   $0 usr_cert localhost 1"
	exit 1;
}
pass=0;
if [ $# -lt 1 ]; then
	usage;
fi

if [ $# -ge 2 ]; then
	if ([ "$1" == "--help" ] || [ "$1" == '-h' ]) || 
	([ "$1" != "usr_cert" ] && [ "$1" != "server_cert" ]) 
	then
		usage;
	fi
	type_cert="$1";
	name_cert="$2";
	if [ $# -eq 3 ]; then
		pass=1;
	fi
fi
echo "Sign $type_cert certificates";
echo "create  a key";

HOME_CA="/opt/sites"

cd $HOME_CA/ca
openssl genrsa \
      -out intermediate/private/$name_cert.key.pem 4096
chmod 400 intermediate/private/$name_cert.key.pem
if [ $pass -eq 1 ]; then
	echo "entro"
	openssl rsa -in intermediate/private/$name_cert.key.pem -out intermediate/private/$name_cert.key.pem
fi

echo "create a certificate"
cd $HOME_CA/ca
openssl req -config intermediate/openssl.cnf \
      -key intermediate/private/$name_cert.key.pem \
      -new -sha256 -out intermediate/csr/$name_cert.csr.pem

cd $HOME_CA/ca
openssl ca -config intermediate/openssl.cnf \
      -extensions $type_cert -days 18250 -notext -md sha256 \
      -in intermediate/csr/$name_cert.csr.pem \
      -out intermediate/certs/$name_cert.cert.pem
chmod 444 intermediate/certs/$name_cert.cert.pem

echo "verify the certificate"
openssl x509 -noout -text \
    -in intermediate/certs/$name_cert.cert.pem

openssl verify -CAfile intermediate/certs/ca-chain.cert.pem \
	intermediate/certs/$name_cert.cert.pem
