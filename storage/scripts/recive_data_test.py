#!/usr/bin/python3.6

# Python 3.5,3.6,3.7
# require
# python-devel
# pip
# pip install
## pip install setproctitle requests pyserial 
import serial
from time import strftime
import json
import requests
import sys
import os
import setproctitle


idProcess = os.getpid()
print (idProcess)

setproctitle.setproctitle("TuNerdCNC")

path = "./"
def save_data(data):
	data = data.replace(b'\xb3',b'|').replace(b'\xc4', b'').replace(b'\xda', b'').replace(b'\xbf', b'').replace(b'\xc3', b'').replace(b'\xb4', b'').replace(b'\xc5', b'').replace(b'\xa2', b'o').replace(b'\xa1', b'i').replace(b'\xa3', b'u').replace(b'\xc0', b'').replace(b'\xc1', b'').replace(b'\xd9', b'').replace(b'\xc2', b'').replace(b'\x82', b'e').replace(b'\xa4', b'n').replace(b'\xa0', b'a').replace(b'\x14',b'').replace(b'\x12',b'')
	try:
		print("-- "+data.decode().strip())
	except UnicodeDecodeError:
		print(data)

def isS(data):
	data = data.replace(b'\xb3',b'|').replace(b'\xc4', b'').replace(b'\xda', b'').replace(b'\xbf', b'').replace(b'\xc3', b'').replace(b'\xb4', b'').replace(b'\xc5', b'').replace(b'\xa2', b'o').replace(b'\xa1', b'i').replace(b'\xa3', b'u').replace(b'\xc0', b'').replace(b'\xc1', b'').replace(b'\xd9', b'').replace(b'\xc2', b'').replace(b'\x82', b'e').replace(b'\xa4', b'n').replace(b'\xa0', b'a').replace(b'\x14',b'').replace(b'\x12',b'')
	try:
		if data.decode().strip()=='s' or data.decode().strip()=='S':
			return True
		# print("-- "+data.decode().strip())
	except UnicodeDecodeError:
			#print(data)
		if data=='s' or data == 'S':
			return True
	return False
def isOne(data):
	data = data.replace(b'\xb3',b'|').replace(b'\xc4', b'').replace(b'\xda', b'').replace(b'\xbf', b'').replace(b'\xc3', b'').replace(b'\xb4', b'').replace(b'\xc5', b'').replace(b'\xa2', b'o').replace(b'\xa1', b'i').replace(b'\xa3', b'u').replace(b'\xc0', b'').replace(b'\xc1', b'').replace(b'\xd9', b'').replace(b'\xc2', b'').replace(b'\x82', b'e').replace(b'\xa4', b'n').replace(b'\xa0', b'a').replace(b'\x14',b'').replace(b'\x12',b'')
	try:
		if data.decode().strip()=='1':
			return True
		# print("-- "+data.decode().strip())
	except UnicodeDecodeError:
			#print(data)
		if data=='1':
			return True
	return False


s = serial.Serial(
	port = '/dev/ttyS0',
	baudrate = 9600,
	bytesize = 7,
	parity = serial.PARITY_EVEN,
	stopbits = 1
	)
try:
	print('antes de abrir')
	s.open
	# Clean Buffer
	s.flushInput()
	s.setDTR()

	# State port
	print("Port: " + s.name)
	print("State: "+ str(s.isOpen()))

	print('despues')
	while s.isOpen():
		# Read
		caracter=s.read()
		print (caracter)
		if isS(caracter) or isOne(caracter):
			serial_data=s.read(4)
			print (caracter+serial_data)
			save_data(caracter+serial_data)
		#serial_data = s.readline()
		#print (serial_data)
		# Store

except KeyboardInterrupt:
	# quit
	s.close()
	print("End "+str(idProcess)+"\n")
	sys.exit()
