#!/bin/bash
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
sudo yum install -y python36u python36u-libs python36u-devel python36u-pip python-devel python-pip ntp
yum groupinstall "Development Tools" -y
firewall-cmd --add-service=ntp --permanent
firewall-cmd --reload
systemctl start ntpd
systemctl enable ntpd
systemctl status ntpd
ntpq -p
date -R

python3.6 -m pip install setproctitle requests pyserial

