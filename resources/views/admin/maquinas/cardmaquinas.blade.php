<div class="col">
	<div class="card shadow">
		<div class="container-fluid" style="border: none; background: #8597AF;">
			<div class="row">
				<div class="col text-center mt-3" style="border: none; background: #8597AF;">
					<p class="text-white" style="margin-bottom: 0; font-size: 30px; line-height:1.5rem;">{{$ct}}</p>
					<p class="font-regular text-white" style="font-size: 10px;">P-300L-PH10 </p> 
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<p class="font-light color-gray text-center" style="font-size: 13px;">Uso de la máquina</p>
				</div>
			</div>
			<div class="row">
				<div class="col-5 col-md-5 col-sm-5 col-lg-5 col-xl-5 font-regular" style="padding: 0; margin-left: 10px" >
					<div style="font-size: .5rem;"><i class="fa fa-square" aria-hidden="true" style="color: #C0DE6C"></i> PROD. 
						<div class="text-center" style="font-size: .5rem;"> 65%</div>
					</div>
					<div style="font-size: .5rem;"><i class="fa fa-square" aria-hidden="true" style="color: #DADDE5"></i> SIN USO 
						<div class="text-center" style="font-size: .5rem;">35%</div>
					</div>
					<div style="font-size: .5rem;"><i class="fa fa-square" aria-hidden="true" style="color: #FEDA30"></i> SETUP 
						<div class="text-center" style="font-size: .5rem;">25%</div>
					</div>
				</div>
				<div class="col-6" id="prueba{{$x}}" style="height: 5rem;">
					<canvas id="myChart{{$x}}" width="100px" height="100px"></canvas>
				</div>
				
			</div>
		</div>
		<div class="container-fluid" style="border-top: solid 1px gray; margin-bottom: 0">
			<div class="row">
				<div class="col">
					<p class="font-light text-left" style="margin-bottom: 0; font-size: 13px;">Eficiencia:</p>
					<p class="font-light color-heavy lh-05 text-left" style="font-size: 8px;">JOB EN CURSO</p>
				</div>
				<div class="col">
					<p class="font-regular text-right fs-18"><b>70%</b></p>
				</div>
			</div>
		</div>
	</div>
</div>
		
{{-- Card con el canvas --}}

{{-- <div class="col card shadow" style="height: 220px; margin-left: 20px;">
	<div class="row" style="justify-content: center; background: #8597AF">
		<div class="col">
			<p size="5" class="font-medium lh-05" style="color: white">CT-007</p>
		</div>
	</div>
	<div class="row" style="background: #8597AF">
		<div class="col">
			<p size="1" class="font-regular" style="color: white; line-height: 1">P-300L-PH10</p> 
		</div>
	</div>
	<div class="row" style="height: 50%; border-bottom: 1px solid #8597AF">
		<canvas id="myChart"></canvas> 
		<div class="row" style="height: 20px;width: 20px; background-color: blue"></div>
		<div class="row" style="height: 20px; background-color: yellow"></div> 
	</div> 
	<div class="row" style="height: 20%; ">
		<font size="3">Eficiencia:</font>
		<font size="1">JOB EN CURSO</font>
	</div>
</div> --}}