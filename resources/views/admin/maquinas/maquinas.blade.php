@extends('admin.jobs.layouts.app')

@section('content')
	<div class="container-fluid mt-2 mb-2">
		<div class="row text-left ml-1"> 
			<div class="col color-black font-light col-md-5">
				<font size="3">28 de marzo 2019</font>
			</div>
		</div>
	</div>				  
	<div class="container-fluid" style="background: #E6E8F1;">
		<div class="row mb-3">
			@for($x=0; $x<5; $x++)
				@include('admin.maquinas.cardmaquinas',['ct'=>'CT-007','x'=>$x])
			@endfor
			
		</div>
		<div class="row">
			@for($x=5; $x<10; $x++)
				@include('admin.maquinas.cardmaquinas',['ct'=>'CT-007','x'=>$x])
			@endfor 
		</div>
		<!-- <div>
   			 <canvas id="myChart" style="max-width: 500px; max-height: 500px"></canvas>
   			 
   			 <canvas id="bar-chart" style="max-width: 500px; max-height: 500px"></canvas>
   			
  		</div> -->
	</div>


@endsection
@section('script')

<script >
	window.ctx=new Array();
	window.chart=new Array();
	var dib = function(indice){
		$("#myChart"+indice).width($("#prueba"+indice).width());
		$("#myChart"+indice).height( ($("#prueba"+indice).height() - 10 )+ 'px' );
		if ( window.ctx[indice] == undefined ){
			window.ctx[indice] = document.getElementById('myChart'+indice).getContext('2d');
		}else{
			window.chart[indice].destroy();
		}
		window.chart[indice] = new Chart(window.ctx[indice], {
		    type: 'doughnut',
		    data:{
			datasets: [{
				data: [60,30,10],
				backgroundColor: ['#C0DE6C','#DADDE5','#FEDA30'],
				showInLegend: true,
				label: 'Uso de la maquina'}],
				labels: ['produccion','sin uso','setup']},
		    options: {
		    	style:{
		    		width:'100px',
		    		height:'100px'
		    	},
		    	layout: {
					padding: {
						left: 0,
						right: 0,
						top: 0,
						bottom: 0
					}
				},
		    	responsive: false,
		    	legend: {
		   			position: 'top',
		   			display: false
		   		},
		   		tooltips: {
		            enabled: false
		        },
		   		scales: {
		   			fontSize: 5
		   		},
		    	cutoutPercentage: 80
		    }
		});
	}
	@for($x=0;$x<10;$x++)
		dib({{$x}});
	@endfor
	$( window ).resize(function() {
		for (var i = 0; i < window.ctx.length; i++) {
			console.log("[resize i]",i);
			dib(i);
		}
	});

</script>
@endsection