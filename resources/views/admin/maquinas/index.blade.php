@extends('admin.maquinas.layouts.app')
@section('head')
	{{-- <meta http-equiv="refresh" content="300"> --}}
@endsection
@section('content')

    <div class="container">

        <h5 class="mt-4">Rango de fecha</h5>

        <form action="/maquinas" method="GET">
            <input type="hidden" name="section" value="{{ request('section') }}">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                        <input type="text" value="{{ request('from') != '' ? request('from') : now()->format('m/d/Y') }}" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="from" placeholder="Desde"/>
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                        <input type="text" value="{{ request('to') != '' ? request('to') : now()->format('m/d/Y') }}" class="form-control datetimepicker-input" placeholder="Hasta" name="to" data-target="#datetimepicker2"/>
                        <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-2">Filtrar</button>
        </form>

    </div>


    <maquina-asignar-component
        :from="{{ json_encode(request('from')) }}"
        :to="{{ json_encode(request('to')) }}"
        :section="{{ json_encode(request('section')) }}"
     />

@endsection

@section('script')
    <!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />


    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'L'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'L'
            });
        });
    </script>
@endsection
