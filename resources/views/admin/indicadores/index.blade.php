@extends('admin.indicadores.layouts.app')
@section('head')
	<meta http-equiv="refresh" content="300">
@endsection
@section('content')
	<div class="container-fluid " style="background: #E6E8F1;">
		<div class="row" style="margin-left: 10px;margin-right: 10px;margin-top: 10px;">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem;">
						<div class="row">
							<div class="col-12">
								<b class="font-light fs-18"> Uso de las máquinas </b>
							</div>
						</div>
						<hr>
						<div class="row text-center" style="width: 103%; height: 86%;">
							<div class="col-4 " id="" style="height: 100%;border-right: 1px solid grey">
								<div class="row" style="height: 15%;">
									<div class="col-5 font-light" style="font-size: .6rem; padding: 0"><i class="fa fa-square" aria-hidden="true" style="color: #AAF55B"></i> PRODUCCION
										<div>  {{ $production_mensual }}Hrs</div>
									</div>
									<div class="col-4 font-light" style="font-size: .6rem; padding: 0"><i class="fa fa-square" aria-hidden="true" style="color: #DADAE5"></i> SIN USO
										<div> {{ $sin_uso_mensual }} Hrs</div></div>
									<div class="col-3 font-light" style="font-size: .6rem; padding: 0"><i class="fa fa-square" aria-hidden="true" style="color: #F3F000"></i> SETUP
										<div> {{ $setup_mensual }} Hrs</div>
									</div>
								</div>
								<div class="row" style="height: 10%; justify-content: center;">
									<div class="font-regular" style="color: grey;"> {{ $now->format('M Y') }}</div>
								</div>
								<div class="row" style="height: 65%" id="row_chart1">
									<canvas id="chart1"></canvas>
								</div>
								<div class="row text-center" style="height: 10%; font-size: 10px">
									<div class="col-6" style="padding: 0">
										<a size="1" class="color-heavy text-right font-regular {{ request()->input('section',"")=='S' ? 'text-primary':'' }}" href="{{route('historial_semana.maquinas')}}">Historial por semana</a>
									</div>
									<div class="col-6" style="padding: 0">
										<a size="1" class="color-heavy text-right font-regular" href="{{route('historial_dia.maquinas')}}">Historial por día</a>
									</div>
								</div>
							</div>
							<div class="col-8" id="prueba" style="height: 100%;">
								<div class="row" style="height: 10%; justify-content: center;">
									<div class="font-regular" style="color: grey;"> Semanal</div>
								</div>
								<div id="bb" class="row" style="height: 90%">
									<canvas id="chart"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		<div class="row mt-4" style="margin-left: 10px;margin-right: 10px;">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem;">
						<div class="row">
							<div class="col-12">
								<b class="font-light fs-18"> Productividad de los operadores </b>
							</div>
						</div>
						<hr>
						<div class="row text-center" style="width: 103%; height: 86%;">
							<div class="col-4" id="" style="height: 100%;border-right: 1px solid grey">
								<div class="row" style="height: 15%; justify-content: center;">
									<div class="font-regular" style="color: grey;"> {{ $now->format('M Y') }} </div>
								</div>
								@php $promedio=round($user_processes->avg('productivity'),2) @endphp
								<div class="row" style="height: 35%">
									<div class="col">
										<p class="porcent1" style="color: #0A55C0"> {{ $promedio }}% </p>
									</div>
								</div>
								<div class="row" style="height: 40%">
									<div class="col">	
										<div class="round-xlarge progress-bar-white1">
											<div class="round-xlarge progress-container1" style="width:{{ intval($user_processes->avg('productivity')) }}%" id="barra">{{ intval($user_processes->avg('productivity')) }}%</div>
										</div>
									</div>
								</div>
								<div class="row text-center" style="height: 10%;">
									<div class="col-6" style="padding: 0; font-size: 10px">
										<a class="color-heavy text-right font-regular" href="{{route('historial_semana.operadores')}}">Historial por semana</a>
									</div>
									<div class="col-6" style="padding: 0">
										<font size="1" class="color-heavy text-right font-regular"></font>
									</div>
								</div>
							</div>
							<div class="col-8" id="prueba" style="height: 100%;">
								<div class="row" style="height: 10%; justify-content: center;">
									<div class="font-regular" style="color: grey;"> Semanal</div>
								</div>
								<div id="aa" class="row" style="height: 100%">
									<canvas id="bar-chart"></canvas>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 dist">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem; ">
						<div class="row">
							<div class="col">
								<b class="font-light fs-18"> Status de Jobs </b>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 20%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 font-light">
										<div class="letrafin">Por asignar:</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="col letrafin color-heavy text-right font-regular"> <b>{{ $process->where('workcenter_id',null)->where('status',null)->count() }}</b> </div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 20%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 font-light ">
										<div class="letrafin">En curso:</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="col letrafin color-heavy text-right font-regular"> <b>{{ $process->whereNotIn('status',['cr',null])->count() }}</b> </div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 20%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 font-light">
										<div class="letrafin">Finalizados:</div>
										<div class="font-light color-heavy text-left" style="font-size: 12px;">ÚLTIMOS 7 DÍAS</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="col letrafin color-heavy text-right font-regular"> <b>{{ $process->where('status','cr')->count() }}</b> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
	{{-- {{ dd( $label->toJson() ) }} --}}
@endsection
@section('script')

<script >
	// $.ajax({
	// 	headers: {
	// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 	},
		// url: "{{--route('configurations.change')--}}",
	// 	type: "POST",
	// 	success: function(data) {

			$("#chart1").width($("#row_chart1").width());
			$("#chart1").height($("#row_chart1").height());
			var ctx1 = document.getElementById('chart1');
			var myChart1 = new Chart(ctx1, {
				type: 'doughnut',
				data:{
				datasets: [{
					data: [{{ $production_mensual }},{{ $sin_uso_mensual }},{{ $setup_mensual }}],
					backgroundColor: ['#C0DE6C','#DADDE5','#FEDA30'],
					showInLegend: true,
					label: 'Uso de la maquina'}],
					labels: ['','','']},
				options: {
					style:{
						width:'100px',
						height:'100px'
					},
					layout: {
						padding: {
							left: 0,
							right: 0,
							top: 0,
							bottom: 0
						}
					},
					responsive: false,
					legend: {
						position: 'top',
						display: false
					},
					tooltips: {
						enabled: false
					},
					title: {
						display: false,
						text: 'Abril 2019'
					},
					scales: {
						fontSize: 5
					},
					cutoutPercentage: 80
				}
			});
	// 	},
	// 	error: function() {
	// 				console.log("No se ha podido obtener la información");
	// 	}
	// });
	


	// $.ajax({
	// 	headers: {
	// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 	},
	// 	url: "{{--route('configurations.change')--}}",
	// 	type: "POST",
	// 	success: function(data) {
			$("#chart").width($("#bb").width());
			$("#chart").height( ($("#bb").height())+ 'px' );
			

			Chart.plugins.register({
				//PARA MOSTRAR DATOS ARRIBA DE LAS BARRAS
				beforeRender: function(chart) {
					if (chart.config.options.showAllTooltips) {
						// create an array of tooltips
						// we can't use the chart tooltip because there is only one tooltip per chart
						chart.pluginTooltips = [];
						chart.config.data.datasets.forEach(function(dataset, i) {
							chart.getDatasetMeta(i).data.forEach(function(sector, j) {
								chart.pluginTooltips.push(
									new Chart.Tooltip(
										{
											_chart: chart.chart,
											_chartInstance: chart,
											_data: chart.data,
											_options: chart.options.tooltips,
											_active: [sector]
										},
										chart
									)
								);
							});
						});

						// turn off normal tooltips
						chart.options.tooltips.enabled = false;
					}
				},
				afterDraw: function(chart, easing) {
					if (chart.config.options.showAllTooltips) {
						// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
						if (!chart.allTooltipsOnce) {
							if (easing !== 1) return;
							chart.allTooltipsOnce = true;
						}

						// turn on tooltips
						chart.options.tooltips.enabled = true;
						Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
							tooltip.initialize();
							tooltip._options.bodyFontFamily = "'font-light', sans-serif"
							tooltip._options.displayColors = false;
							tooltip._options.bodyFontSize = tooltip._chart.height * 0.05;
							tooltip._options.yPadding = 5;
							tooltip._options.xPadding = 5;
							
							tooltip.update();
							// we don't actually need this since we are not animating tooltips
							tooltip.pivot();
							tooltip.transition(easing).draw();
						});
						chart.options.tooltips.enabled = false;
					}
				}
			});

			var label= {!! $label->toJson() !!};

			var ctx = document.getElementById('chart');
			var myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: label,
					datasets: [
					{
						label: 'Low',
						data: {!! $sin_uso_semanal->toJSon() !!},
						backgroundColor: '#DADDE5',
					},
					{
						label: 'Moderate',
						data: {!! $produccion_semanal->toJson() !!},
						backgroundColor: '#C0DE6C',
					},
					{
						label: 'High',
						data: {!! $setup_semanal->toJson() !!},
						backgroundColor: '#FEDA30',
					}]
				},
				options: {
					aspectRatio:1,
					scales: {
						xAxes: [{ stacked: true,
							ticks: {
								fontSize: 10,

							},
						}],
						yAxes: [{ stacked: true,
							ticks: {
								fontSize: 12,
								fontFamily: "font-light",
								callback: function (value) {
									return value + 'Hrs';
								}
							}
						}]
					},
					
					legend: { 
						display: false },
					title: {
						display: false,
						text: 'holis'
					},
				}
			});
	// 	},
	// 	error: function() {
	// 				console.log("No se ha podido obtener la información");
	// 	}
	// });


	// $.ajax({
	// 	headers: {
	// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 	},
	// 	url: "{{--route('configurations.change')--}}",
	// 	type: "POST",
	// 	success: function(data) {

			$("#bar-chart").width($("#aa").width());
			$("#bar-chart").height( ($("#aa").height())+ 'px' );
			new Chart(document.getElementById("bar-chart"), {
				type: 'bar',
				data: {
					labels: label,
					datasets: [
						{
							label: "",
							backgroundColor: ["#0A55C0", "#0A55C0","#0A55C0","#0A55C0","#0A55C0","#0A55C0","#0A55C0"],
							data:{{ $productividad_semanal->toJson() }}
						}
					]
				},
				options: {
					legend: { 
						labels:{
							defaultFontFamily:"font-light",
							defaultFontSize:6,
						},
						display: false ,
					},
					aspectRatio:1,
					intersect:false,
					title: {
						display: false,
						text: 'holi2'
					},
					scales: {
						xAxes: [{
							display: true,
							ticks: {
								fontSize: 10,
							},
							scaleLabel: {
								display: true
							}
						}],
						yAxes: [{
							ticks: {
								fontSize: 12,
								min: 0,
								max: 100,
								callback: function (value) {
									return value + '%';
								}
							}
						}]
					},
					showAllTooltips: true,
					responsive:true,
					animation: {
						animateScale: true,
						animateRotate: true
					},
					tooltips: {
						// mode: 'x',
						enabled: false,
						// intersect:false,
						yAlign: 'bottom',
						xAlign: 'center',
						xPadding: 25,
						yPadding: 15,
						xPadding: 45,
						titleAlign: 'center',
						footerAlign: 'center',
						bodyAlign: 'center',
						callbacks: {
							title: function(tooltipItems, data) {
								return "";
							},
							label: function(tooltipItem, data) {
								var label = data.datasets[tooltipItem.datasetIndex].label || '';

								if (label) {
									label += ': ';
								}

								label += Math.round(tooltipItem.yLabel * 100) / 100;
								label += "%";
								return label;
							}
						},
						
					},
				}
			});
	// 	},
	// 	error: function() {
	// 				console.log("No se ha podido obtener la información");
	// 	}
	// });

</script>
@endsection