@extends('admin.indicadores.layouts.app')
@section('head')
    {{-- <meta http-equiv="refresh" content="300"> --}}
    <link rel="stylesheet" href="{{ asset('js/vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    <style>
        #chart {
            height: 100%!important;
        }
        #bar-chart {
            height: 100%!important;
        }
    </style>    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" /> --}}

@endsection
@section('content')
	<div class="container-fluid " style="background: #E6E8F1;">
		<div class="row" style="margin-left: 10px;margin-right: 10px;margin-top: 10px;">
			<div class="col-12 col-md-12 col-md-12 col-lg-12 col-xl-12">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem;">
						<div class="row">
							<div class="col-4">
								<b class="font-light fs-18"> Uso de las máquinas </b>
							</div>
							<div class="row">
                            	<div class="col-md-8">

                                    <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
	                                    <input type="text" value="{{ isset($day) ? $day : '' }}" class="form-control datetimepicker-input test-of-class test" data-target="#datetimepicker3" name="day" id ="day" placeholder="Fecha"/>
	                                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
	                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                                    </div>
	                                    <button type="button" class="button-blue btn-sm" id="btnFiltrar" style="height: 60%; margin-left: 10px;margin-top: 5px;">FILTRAR</button>
	                                </div>

	                            </div>
	                        </div>
						</div>
						<hr>
						<div class="row text-center" style="width: 103%; height: 86%;">
							<div class="col-4 " id="" style="height: 100%;border-right: 1px solid grey">
								<div class="row" style="height: 15%;">
									<div class="col-5 font-light" style="font-size: .6rem; padding: 0"><i class="fa fa-square" aria-hidden="true" style="color: #558b2f"></i> PRODUCCION
										<div id="prod_men">  Hrs</div>
									</div>
									<div class="col-4 font-light" style="font-size: .6rem; padding: 0"><i class="fa fa-square" aria-hidden="true" style="color: #DADAE5"></i> SIN USO
										<div id="sinuso_men">  Hrs</div></div>
									<div class="col-3 font-light" style="font-size: .6rem; padding: 0"><i class="fa fa-square" aria-hidden="true" style="color: #F88628"></i> SETUP
										<div id="setup_men">  Hrs</div>
									</div>
								</div>
								<div class="row" style="height: 10%; justify-content: center;">
									<div class="font-regular" id="now" style="color: grey;"> {{ $now->format('M Y') }}</div>
								</div>
								<div class="row" style="height: 65%" id="row_chart1">
									<canvas id="chart1"></canvas>
								</div>
								<div class="row text-center" style="height: 10%; font-size: 10px">
									<div class="col-6" id="div_semanal" style="padding: 0">
										<a size="1" class="color-heavy text-right font-regular {{ request()->input('section',"")=='S' ? 'text-primary':'' }}" href="{{route('historial_semana.maquinas')}}">Historial por semana</a>
									</div>
									<div class="col-6"id="div_diario" style="padding: 0">
										<a size="1" class="color-heavy text-right font-regular" href="{{route('historial_dia.maquinas')}}">Historial por día</a>
									</div>
								</div>
							</div>
							<div class="col-8" id="prueba" style="height: 100%;">
								<div class="row" style="height: 10%; justify-content: center;">
									<div class="font-regular" id="div_fecha" style="color: grey;"> Semanal</div>
								</div>
								<div id="bb" class="row" style="height: 90%">
									<canvas id="chart"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!--	
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 dist">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem;">
						<div class="row" style="height: 15%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row" style="margin-left: 0px">
									{{-- <b class="font-light fs-18"> Tiempo por fase </b> --}}
									<b class="font-light text-left fs-18" >Tiempo por fase
									<div class="font-light color-heavy text-left" id="div_tiempo" style="font-size: 12px;">ÚLTIMOS 7 DÍAS</div>
									</b>
								</div>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 85%">
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" style="padding-right: 0px;">
								<b class="color-heavy font-light fs-18"> FASE</b>
								<div class="font-light letrafase"> Herramentaje:</div>
								<div class="font-light letrafase"> Ajuste:</div>
								<div class="font-light letrafase"> Liberación:</div>
								<div class="font-light letrafase"> Auto-liberación:</div>
								<div class="font-light letrafase"> Producción:</div>
								<div class="font-light letrafase"> Pausa por mant:</div>
								<div class="font-light letrafase"> Pausa otros:</div>
								<div class="font-light letrafase"> Sin uso:</div>
							</div>
							<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center" style="padding-left: 0px" >
								<b class="color-heavy font-light fs-18 "> HRS </b>
								<div class="color-heavy " id="data_herramentaje">  </div>
								<div class="color-heavy " id="data_ajuste">  </div>
								<div class="color-heavy " id="data_liberacion">  </div>
								<div class="color-heavy " id="data_autoliberacion">  </div>
								<div class="color-heavy " id="data_produccion">  </div>
								<div class="color-heavy " id="data_mantenimiento">  </div>
								<div class="color-heavy " id="data_pausa_otros">  </div>
								<div class="color-heavy " id="data_sinuso">  </div>
							</div>
							<div class="col-3 text-center">
								<div class="color-heavy font-light fs-18"> % </div>
								<div class="color-heavy" id="porcentaje_herramentaje">  </div>
								<div class="color-heavy" id="porcentaje_ajuste">  </div>
								<div class="color-heavy" id="porcentaje_liberacion">  </div>
								<div class="color-heavy" id="porcentaje_autoliberacion">  </div>
								<div class="color-heavy" id="porcentaje_produccion">  </div>
								<div class="color-heavy" id="porcentaje_mantenimiento">  </div>
								<div class="color-heavy" id="porcentaje_pausa_otros">  </div>
								<div class="color-heavy" id="porcentaje_sinuso">  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			-->
		</div>
		<div class="row mt-4" style="margin-left: 10px;margin-right: 10px;">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem;">
						<div class="row">
							<div class="col-12">
								<b class="font-light fs-18"> Productividad de los operadores </b>
							</div>
						</div>
						<hr>
						<div class="row text-center" style="width: 103%; height: 86%;">
							<div class="col-4" id="" style="height: 100%;border-right: 1px solid grey">
								<div class="row" style="height: 15%; justify-content: center;">
									<div class="font-regular" style="color: grey;"> {{ $now->format('M Y') }} </div>
								</div>
								<div class="row" style="height: 35%">
									<div class="col">
										<p class="porcent1" id="porcent1" style="color: #0A55C0"> % </p>
									</div>
								</div>
								<div class="row" style="height: 40%">
									<div class="col">
										<div class="round-xlarge progress-bar-white1">
											<div class="round-xlarge progress-container1" id="barra">%</div>
										</div>
									</div>
								</div>
								<div class="row text-center" style="height: 10%;">
									<div class="col-6" id="div_semanal2" style="padding: 0; font-size: 10px">
										<a class="color-heavy text-right font-regular" href="{{route('historial_semana.operadores')}}">Historial por semana</a>
									</div>
									<div class="col-6" style="padding: 0; font-size: 10px">
										<a class="color-heavy text-right font-regular" href="{{route('historial_dia.operadores')}}">Historial por dia</a>
									</div>
								</div>
							</div>
							<div class="col-8" id="prueba" style="height: 100%;">
								<div class="row" id="div_semanal3" style="height: 10%; justify-content: center;">
									<div class="font-regular" style="color: grey;"> Semanal</div>
								</div>
								<div id="aa" class="row" style="height: 100%">
									<canvas id="bar-chart"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 dist">
				<div class=" card card-size shadow">
					<div class="card-body" style="height: 18rem; ">
						<div class="row">
							<div class="col">
								<b class="font-light fs-18"> Status de Jobs </b>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 20%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 font-light">
										<div class="letrafin">Por asignar:</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="col letrafin color-heavy text-right font-regular" id="div_porasignar"> <b>{{ $porasignar }}</b> </div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 20%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 font-light ">
										<div class="letrafin">En curso:</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="col letrafin color-heavy text-right font-regular" id="div_enproceso"> <b>{{ $enproceso }}</b> </div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="row" style="height: 20%">
							<div class="col col-sm col-md col-lg col-xl">
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 font-light">
										<div class="letrafin">Finalizados:</div>
										<div class="font-light color-heavy text-left"  id="div_ultimos" style="font-size: 12px;">ÚLTIMOS 7 DÍAS</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="col letrafin color-heavy text-right font-regular" id="div_terminados"> <b>{{ $terminados }}</b> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- {{ dd( $label->toJson() ) }} --}}
@endsection
@section('script')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

<script>
    var myChart, myChart2, myChart3;
	$(function () {
        $('#datetimepicker3').datetimepicker({format: 'DD/MM/YYYY'});
    });
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{route('indicadores.semanal_mensual')}}",
		type: "POST",
		data: { day: $("#day").val()
	    },
		beforeSend: function(){
		   // console.log("cargando datos");
		},
		success: function(data) {
			// console.log("[data semanal]",data);
			$("#chart").width($("#bb").width());
			$("#chart").height( '100%' );
			Chart.plugins.register({
				//PARA MOSTRAR DATOS ARRIBA DE LAS BARRAS
				beforeRender: function(chart) {
					if (chart.config.options.showAllTooltips) {
						// create an array of tooltips
						// we can't use the chart tooltip because there is only one tooltip per chart
						chart.pluginTooltips = [];
						chart.config.data.datasets.forEach(function(dataset, i) {
							chart.getDatasetMeta(i).data.forEach(function(sector, j) {
								chart.pluginTooltips.push(
									new Chart.Tooltip(
										{
											_chart: chart.chart,
											_chartInstance: chart,
											_data: chart.data,
											_options: chart.options.tooltips,
											_active: [sector]
										},
										chart
									)
								);
							});
						});

						// turn off normal tooltips
						chart.options.tooltips.enabled = false;
					}
				},
				afterDraw: function(chart, easing) {
					if (chart.config.options.showAllTooltips) {
						// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
						if (!chart.allTooltipsOnce) {
							if (easing !== 1) return;
							chart.allTooltipsOnce = true;
						}

						// turn on tooltips
						chart.options.tooltips.enabled = true;
						Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
							tooltip.initialize();
							tooltip._options.bodyFontFamily = "'font-light', sans-serif"
							tooltip._options.displayColors = false;
							tooltip._options.bodyFontSize = tooltip._chart.height * 0.05;
							tooltip._options.yPadding = 7;
							tooltip._options.xPadding = 7;

							tooltip.update();
							// we don't actually need this since we are not animating tooltips
							tooltip.pivot();
							tooltip.transition(easing).draw();
						});
						chart.options.tooltips.enabled = false;
					}
				}
			});

			var label= data.label;
            $('#chart').height('100%');
            var ctx = document.getElementById('chart');
			myChart2 = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: label,
					datasets: [
				
					{
						label: 'Producción',
						data: data.data_semanal.prod_semanal,
						backgroundColor: '#558b2f',
					},				
					{
						label: 'Setup',
						data: data.data_semanal.setup_semanal,
						backgroundColor: '#F88628',
					},
					{
						label: 'Sin uso',
						data: data.data_semanal.sinuso_semanal,
						backgroundColor: '#DADDE5',
					}]
				},
				options: {
					aspectRatio:1,
					scales: {
						xAxes: [{ stacked: true,
							ticks: {
								fontSize: 10,

							},
						}],
						yAxes: [{ stacked: true,
							ticks: {
								fontSize: 12,
								fontFamily: "font-light",
								callback: function (value) {
									return value + 'Hrs';
								}
							}
						}]
					},
					// animation: {
					// 	duration: 500,
					// 	easing: "easeOutQuart",
					// 	onComplete: function () {
					// 		var ctx = this.chart.ctx;
					// 		ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
					// 		ctx.textAlign = 'center';
					// 		ctx.textBaseline = 'bottom';

					// 		this.data.datasets.forEach(function (dataset) {
					// 			for (var i = 0; i < dataset.data.length; i++) {
					// 				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
					// 					scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
					// 				ctx.fillStyle = '#FF0042';
					// 				var y_pos = model.y + 25;
					// 				// Make sure data value does not get overflown and hidden
					// 				// when the bar's value is too close to max value of scale
					// 				// Note: The y value is reverse, it counts from top down
					// 				if ((scale_max - model.y) / scale_max >= 2.93)
					// 					y_pos = model.y + 40;
					// 				ctx.fillText(dataset.data[i], model.x, y_pos);
					// 			}
					// 		});
					// 	}
					// },

					legend: {
						display: false },
					title: {
						display: false,
						text: 'Semanal'
					},
				}
			});
			/*
			   Mensual
			*/
			$("#chart1").width($("#row_chart1").width());
			$("#chart1").height($("#row_chart1").height());
			var ctx1 = document.getElementById('chart1');
			myChart1 = new Chart(ctx1, {
				type: 'doughnut',
				data:{
				datasets: [{
					data: data.data_mensual,
					backgroundColor: ['#558b2f','#DADDE5','#F88628'],
					showInLegend: true,
					label: 'Uso de la maquina'}],
					labels: ['','','']},
				options: {
					style:{
						width:'100px',
						height:'100px'
					},
					layout: {
						padding: {
							left: 0,
							right: 0,
							top: 0,
							bottom: 0
						}
					},

					responsive: false,
					legend: {
						position: 'top',
						display: false
					},
					tooltips: {
						enabled: false
					},
					title: {
						display: false,
						text: 'Abril 2019'
					},
					scales: {
						fontSize: 5
					},
					cutoutPercentage: 80

				}
			});
			$("#prod_men").empty();
			$("#prod_men").text(data.data_mensual[0]+' HRS');
			$("#sinuso_men").empty();
			$("#sinuso_men").text(data.data_mensual[1]+' HRS');
			$("#setup_men").empty();
			$("#setup_men").text(data.data_mensual[2]+' HRS');
		},
		error: function() {
			console.log("No se ha podido obtener la información,semanal");
		},
		complete: function(){
			// console.log("datos cargados");
		},
	});
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{route('indicadores.semanal')}}",
		type: "POST",
		beforeSend: function(){
			// console.log("[tiempo fase] cargando datos");
		},
	}).done(function(data){
		// console.log(data);
		$.each(data.porcentajes,function(name,hours){
			// console.log("#porcentaje_"+name +" => "+hours);
			$("#porcentaje_"+name).empty();
			$("#porcentaje_"+name).text(hours);
		});
		$.each(data.horas,function(name,hours){
			// console.log("#data_"+name +" => "+hours);
			$("#data_"+name).empty();
			$("#data_"+name).text(hours);
		});
	}).fail(function() {
		console.log( "error" );
	});


	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{route('indicadores.productividad')}}",
		type: "POST",
		beforeSend: function(){
		   //console.log("[tiempo fase] cargando datos");
		},
    })
    .done(function(data) {

		$("#barra").empty();
		$("#barra").text(data.prom+" %");
		$("#porcent1").empty();
        $("#porcent1").text(data.prom+" %");
        var prom = 0;
        if (data.prom > 100) {
            prom = 100;
        } else {
            prom = data.prom;
        }
        $("#barra").css('width', `${prom}%`);

		$("#bar-chart").width($("#aa").width());
        $("#bar-chart").height( '100%' );

		myChart3 = new Chart(document.getElementById("bar-chart"), {
			type: 'bar',
			data: {
				labels: data.label,
				datasets: [
					{
						label: "",
						backgroundColor: ["#0A55C0", "#0A55C0","#0A55C0","#0A55C0","#0A55C0","#0A55C0","#0A55C0"],
						data:data.productivity
					}
				]
			},
			options: {
				legend: {
					labels:{
						defaultFontFamily:"font-light",
						defaultFontSize:6,
					},
					display: false ,
				},
				aspectRatio:1,
				intersect:false,
				title: {
					display: false,
					text: 'Semanal'
				},
				scales: {
					xAxes: [{
						display: true,
						ticks: {
							fontSize: 10,
						},
						scaleLabel: {
							display: true
						}
					}],
					yAxes: [{
						ticks: {
							fontSize: 12,
							min: 0,
							max: 100,
							callback: function (value) {
								return value + '%';
							}
						}
					}]
				},
				showAllTooltips: false,
				responsive:true,
				animation: {
					duration: 500,
					easing: "easeOutQuart",
					onComplete: function () {
						var ctx = this.chart.ctx;
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';

						this.data.datasets.forEach(function (dataset) {
							for (var i = 0; i < dataset.data.length; i++) {
								var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
									scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
								ctx.fillStyle = '#E16B1D';
								var y_pos = model.y - 5;
								// Make sure data value does not get overflown and hidden
								// when the bar's value is too close to max value of scale
								// Note: The y value is reverse, it counts from top down
								if ((scale_max - model.y) / scale_max >= 0.93)
									y_pos = model.y + 20;
								ctx.fillText(dataset.data[i], model.x, y_pos);
							}
						});
					}
				},
				tooltips: {
					// mode: 'x',
					enabled: false,
					// intersect:false,
					yAlign: 'bottom',
					xAlign: 'center',
					xPadding: 25,
					yPadding: 15,
					xPadding: 45,
					titleAlign: 'center',
					footerAlign: 'center',
					bodyAlign: 'center',
					callbacks: {
						title: function(tooltipItems, data) {
							return "";
						},
						label: function(tooltipItem, data) {
							var label = data.datasets[tooltipItem.datasetIndex].label || '';

							if (label) {
								label += ': ';
							}

							label += Math.round(tooltipItem.yLabel * 100) / 100;
							label += "%";
							return label;
						}
					},

				},
			}
		});

	}).fail(function() {
		console.log( "error" );
	});

	$("#btnFiltrar").click(function() {

        // Peticion para grafica de barras uso de maquinas
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('indicadores.semanal_mensual') }}",
			type: "POST",
			data: {
                day: $("#day").val()
		    },
			success: function(data) {

				if ($("#day").val() == '') {
					setTimeout(function(){
						location.reload();
					}, 0);
                }

				$("#div_semanal").addClass("d-none");
				$("#div_diario").addClass("d-none");
				$("#div_fecha")[0].innerHTML = '';
				$("#div_tiempo")[0].innerHTML = $("#day").val();
				$("#div_porasignar")[0].innerHTML = data.data_jobs.porasignar;
				$("#div_enproceso")[0].innerHTML = data.data_jobs.enproceso;
				$("#div_terminados")[0].innerHTML = data.data_jobs.terminados;
				$("#chart").width($("#bb").width());
				$("#chart").height( '100%' );

                // Grafica de barras uso de maquinas
				var label = data.label;
                var ctx = document.getElementById('chart');
                $("#chart").height( '100%' );
                myChart2.destroy();
				myChart2 = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: label,
                        datasets: [
                        {
                            label: 'Sin uso',
                            data: data.data_day.sinuso_day,
                            backgroundColor: '#DADDE5',
                        },
                        {
                            label: 'Producción',
                            data: data.data_day.prod_day,
                            backgroundColor: '#558b2f',
                        },
                        {
                            label: 'Setup',
                            data: data.data_day.setup_day,
                            backgroundColor: '#F88628',
                        }]
                    },
                    options: {
                        aspectRatio:1,
                        scales: {
                            xAxes: [
                                {
                                stacked: true,
                                ticks: {
                                    fontSize: 10,
                                },
                            }],
                            yAxes: [{ stacked: true,
                                ticks: {
                                    fontSize: 12,
                                    fontFamily: "font-light",
                                    callback: function (value) {
                                        return (value/2) + 'Hrs';
                                    }
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        title: {
                            display: false,
                            text: 'Dia'
                        }
                    }
                });

                $("#chart1").width($("#row_chart1").width());
                $("#chart1").height('100%');


                // Grafica de tiempos tipo dona
                var ctx1 = document.getElementById('chart1');
                myChart1.destroy();
                myChart1 = new Chart(ctx1, {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            data: data.data_diario,
                            backgroundColor: ['#558b2f','#DADDE5','#F88628'],
                            showInLegend: true,
                            label: 'Uso de la maquina'
                        }],
                        labels: ['','','']
                    },
                    options: {
                        style:{
                            width:'100px',
                            height:'100px'
                        },
                        layout: {
                            padding: {
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0
                            }
                        },
                        responsive: false,
                        legend: {
                            position: 'top',
                            display: false
                        },
                        tooltips: {
                            enabled: false
                        },
                        title: {
                            display: false,
                            text: 'Abril 2019'
                        },
                        scales: {
                            fontSize: 5
                        },
                        cutoutPercentage: 80

                    }
                });
                //$("#prod_men").empty();
                $("#prod_men")[0].innerHTML = data.data_diario[0]+' HRS';
                //$("#sinuso_men").empty();
                $("#sinuso_men")[0].innerHTML = data.data_diario[1]+' HRS';
                //$("#setup_men").empty();
                $("#setup_men")[0].innerHTML = data.data_diario[2]+' HRS';
            }

        });

        // Tiempos en horas para grafica de uso de maquinas
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('indicadores.semanal')}}",
            type: "POST",
            data: { day: $("#day").val()},
            beforeSend: function(){
                // console.log("[tiempo fase] cargando datos");
            },
        })
        .done(function(data){
            $.each(data.porcentajes,function(name,hours){
                // console.log("#porcentaje_"+name +" => "+hours);
                $("#porcentaje_"+name).empty();
                $("#porcentaje_"+name).text(hours);
            });
            $.each(data.horas,function(name,hours){
                // console.log("#data_"+name +" => "+hours);
                $("#data_"+name).empty();
                $("#data_"+name).text(hours);
            });
        });

        // Productividad de los operadores
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('indicadores.productividad') }}",
            type: "POST",
            data: { day: $("#day").val()},
        })
        .done(function(data) {

            console.log(data)

            $("#div_semanal2").addClass("d-none");
            $("#div_semanal3")[0].innerHTML = '';
            $("#div_ultimos")[0].innerHTML = '';
            $("#barra").empty();
            $("#barra").text(data.prom+" %");
            $("#porcent1").empty();
            $("#porcent1").text(data.prom + " %");

            if (data.prom > 100) {
                prom = 100;
            } else {
                prom = data.prom;
            }

            $("#barra").css('width', `${prom}%`);

            $("#bar-chart").width($("#aa").width());
            $("#bar-chart").height( '100%' );

            myChart3.destroy();

            myChart3 = new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                    labels: data.label,
                    datasets: [
                        {
                            label: "",
                            backgroundColor: ["#0A55C0", "#0A55C0","#0A55C0","#0A55C0","#0A55C0","#0A55C0","#0A55C0"],
                            data: data.prom
                        }
                    ]
                },
                options: {
                    legend: {
                        labels:{
                            defaultFontFamily:"font-light",
                            defaultFontSize:6,
                        },
                        display: false ,
                    },
                    aspectRatio:1,
                    intersect:false,
                    title: {
                        display: false,
                        text: 'Semanal'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                fontSize: 10,
                            },
                            scaleLabel: {
                                display: true
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 12,
                                min: 0,
                                max: 200,
                                callback: function (value) {
                                    return value + '%';
                                }
                            }
                        }]
                    },
                    showAllTooltips: false,
                    responsive:true,
                    animation: {
                        duration: 500,
                        easing: "easeOutQuart",
                        onComplete: function () {

                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                                    ctx.fillStyle = '#E16B1D';
                                    var y_pos = model.y - 5;
                                    // Make sure data value does not get overflown and hidden
                                    // when the bar's value is too close to max value of scale
                                    // Note: The y value is reverse, it counts from top down
                                    if ((scale_max - model.y) / scale_max >= 0.93)
                                        y_pos = model.y + 20;

                                    ctx.fillText(dataset.data[i], model.x, y_pos);
                                }
                            });

                        }
                    },
                    tooltips: {
                        enabled: false,
                        yAlign: 'bottom',
                        xAlign: 'center',
                        xPadding: 25,
                        yPadding: 15,
                        xPadding: 45,
                        titleAlign: 'center',
                        footerAlign: 'center',
                        bodyAlign: 'center',
                        callbacks: {
                            title: function(tooltipItems, data) {
                                return "";
                            },
                            label: function(tooltipItem, data) {
                                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                if (label) {
                                    label += ': ';
                                }

                                label += Math.round(tooltipItem.yLabel * 100) / 100;
                                label += "%";
                                return label;
                            }
                        }
                    },
                }
            });


        });

	});

</script>
@endsection
