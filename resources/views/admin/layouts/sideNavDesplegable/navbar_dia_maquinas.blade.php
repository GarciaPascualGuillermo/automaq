<nav id="nav_h" class="navbar fixed-top navbar-expand-md navbar-light navbar-laravel close-nav" style="">
	<img src="/assets/jobs/close_sidebar.png" id="arrow_right" class="arrow" >
	<img src="/assets/jobs/open_sidebar.png" id="arrow_left" class="arrow">
	<ul class="navbar-nav justify-content-end" style="width: 100%;">
		@if(auth()->user()->hasRoles(['ceo','root']))
			<li class="nav-item dropleft ">
				<a href="{{route('mensaje_global.index')}}"><img src="/img/mensaje.png" style="width: 27px;height: 27px; margin-top: 10px;margin-right: 18px;"></a>
			</li>
		@endif
		<li class="nav-item dropleft ">
			<a target="_blank" href="{{asset('/assets/jobs/ManualPlataforma.pdf')}}"><img src="/assets/jobs/ayuda.png" style="width: 27px;height: 27px; margin-top: 10px;margin-right: 18px;"></a>
		</li>

		<li class="nav-item dropleft">
			

			<a class="nav-link" data-toggle="dropdown"  id="nomenclatura" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				<img src="/assets/jobs/nomenclatura_inactivo.png"  style="width: 30px;height: 30px;">
			</a>
			<div class="dropdown-menu" aria-labelledby="nomenclatura">
				<a class="dropdown-item disabled" href="#">Nomenclatura</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item disabled" href="#">
					<i class="fa fa-circle" aria-hidden="true" style="color: orange"></i> Herramentaje-Ajuste</a>
				<a class="dropdown-item disabled" href="#">
					<i class="fa fa-circle" aria-hidden="true" style="color: yellow"></i> Liberación</a>
				<a class="dropdown-item disabled" href="#">
					<i class="fa fa-circle" aria-hidden="true" style="color: green"></i> Producción</a>
				<a class="dropdown-item disabled" href="#">
					<i class="fa fa-circle" aria-hidden="true" style="color: red"></i> Pausa</a>
					<a class="dropdown-item disabled" href="#">
					<i class="fa fa-circle" aria-hidden="true" style="color: purple"></i> Mantenimiento</a>

			</div>
		</li>
		{{-- <li class="nav-item">
			<a class="nav-link" href="#">
				<img src="/assets/jobs/notificaciones_inactivo.png" style="width: 30px;height: 30px; margin-left: 10px">
			</a>
		</li> --}}
		@if(!auth()->user()->hasRoles(['ceo']))
		<li class="nav-item dropleft ">
			<a class="nav-link" data-toggle="dropdown"  id="nomenclatura" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				<img src="/assets/jobs/settings_inactivo.png" style="width: 30px;height: 30px; margin-left: 10px">
			</a>
			<div class="dropdown-menu" aria-labelledby="nomenclatura">
				<a class="dropdown-item disabled" href="#">Ajustes</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="{{route('usuarios.index')}}">
					<i class="fa fa-arrow-circle-right" aria-hidden="true" style="color: blue"></i> Usuarios</a>
				<a class="dropdown-item" href="{{route('centro_trabajo.index')}}">
					<i class="fa fa-arrow-circle-right" aria-hidden="true" style="color: blue"></i> Centro de Trabajo</a>
				<a class="dropdown-item" href="{{route('parametros.index')}}">
					<i class="fa fa-arrow-circle-right" aria-hidden="true" style="color: blue"></i> Parámetros</a>
			</div>
		</li>
		@endif
	</ul>
</nav>