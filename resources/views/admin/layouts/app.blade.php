<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/operadores/app.css')."?v=".str_random(2) }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
    {{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
    @yield('style')
</head>
<body>
    <div id="app">
        @include('admin.layouts.partials.sidebar')
        <div id="" class="content-nav open-nav">
            <nav id="nav_h" class="navbar fixed-top navbar-expand-md navbar-light navbar-laravel open-nav">
                <div class="container-fluid">
                    {{-- <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a> --}}
                    <ul class="container-fluid nav justify-content-center" style="font-size: 18px; ">
                      <li class="nav-item">
                        <div class=" color-gray nav-link font-regular">
                            {{ __('Centro de Trabajo:') }}
                        </div>
                      </li>
                        <li class="nav-item">
                        <div class="nav-link color-heavy font-weight-bold">
                            {{__($workcenter->num_machine)}}
                        </div>
                      </li>
                      <li class=" nav-item">
                        <div class="color-gray nav-link font-regular">
                            {{__('Modelo de la Máquina:')}}
                        </div>
                      </li>
                      <li class="nav-item">
                        <div class="color-heavy nav-link font-weight-bold">
                            {{__($workcenter->work_center)}}
                        </div>
                      </li>
                    </ul>
                </div>
            </nav>

            <main class="py-3 background-main-index">

                @yield('content')

            </main>
        </div>
        @yield('modal')
       <modal-mensaje></modal-mensaje>
    </div>

    <script>
        window.Auth = window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'url_logout'=>route('logout'),
            'vue'=>[
                'operador'=>[
                    'enabled'=>true,
                    'setup'=>[
                        'enabled'=>false,
                        'espera'=>[
                            'enabled'=>false,
                        ],
                        'espera'=>[
                            'enabled'=>false,
                        ],
                        'autoliberacion'=>[
                            'enabled'=>true,
                            'liberada'=>[
                                'enabled'=>true
                            ],
                            'rechazada'=>[
                                'enabled'=>false
                            ]
                        ],
                    ],
                    'pausa'=>[
                        'enabled'=> true,
                        'mantenimiento'=>[
                            'soporte'=>[
                                'enabled'=>false,
                            ],
                            'enabled'=>false,
                        ],
                        'herramienta'=>[
                            'enabled'=>false,
                        ],
                        'material'=>[
                            'enabled'=>false,
                        ]

                    ],

                    'capeando'=>[
                        'enabled'=>true,
                        'validar'=>[
                            'enabled'=>false,
                        ],
                        'enabled'=>true,
                    ],
                ],
                'sidebar'=>[
                    'enabled'=>true,
                    'finalizar'=>false,
                    'dibujo'=>[
                        'enabled'=>true,
                    ],
                    'mot'=>[
                        'enabled'=>true,
                    ],
                    'plan'=>[
                        'enabled'=>true,
                    ],
                    'terminar'=>[
                        'enabled'=>true,
                        'scraps'=>[
                            'confirmar'=>false,
                            'resultado'=>[
                                'bueno'=>false,
                                'regular'=>false,
                                'malo'=>false,
                            ],
                            'total'=>0,
                        ],
                    ],
                ],
                'workcenter'=>$workcenter,
                'job'=>\App\Job::find(session('job_id')),
                'process'=>\App\Process::with(["job"])
                    ->with(['capeando'])
                    ->with(['ajustador_ia'])
                    ->with(['user_processes'=>function ($q_up) {
                        $q_up->whereDate('start_at',date('Y-m-d'))
                            ->where('active',true)
                            ->where('id',session('user_process_id'))
                            ->orderBy('created_at','DESC')
                            ;
                    }])
                    ->with(['operation'=>function ($q_o) {
                        $q_o->with(['piece'=>function ($q_p) {
                            $q_p->with(['client']);
                        }]);
                    }])->where('id',session('process_id'))->first(),
                'pieceCount'=>0,
                'pieceTotal'=>0,
                'parametros'=>\App\Parameter::all()->pluck('value','name'),
            ],
        ]) !!};
    </script>

    <!-- Scripts -->
    <script src="{{ asset('js/operadores/app.js') . "?v=" . str_random(3) }}"></script>
    <script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
    @yield('script')
</body>
</html>


{{-- https://github.com/cretueusebiu/laravel-web-push-demo --}}
{{-- https://github.com/laravel-notification-channels/webpush --}}
{{-- http://laravel-notification-channels.com/ --}}
