@extends('admin.schedule.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <schedule-list :initial_schedules="{{ json_encode($schedules) }}"></schedule-list>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ mix('js/schedule/app.js') }}"></script>
@endsection
