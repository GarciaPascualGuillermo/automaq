<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}">

	@yield('style')
</head>
<body>
	<div id="app">

        @include('admin.jobs.layouts.partials.sidebar_active')

		<div id="main-content" class="content-nav close-nav">

            @include('admin.layouts.sideNavDesplegable.navbar')

            @yield('sidebar')

            @yield('content')

        </div>

		@yield('modal')
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
		]) !!};
    </script>

	<!-- Scripts -->
	@yield('scripts')
</body>
</html>
