<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="refresh" content="18000">
	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->
	<script src="{{ asset('js/login/app.js')."?v=".str_random(3) }}" defer></script>

	<!-- Styles -->
	<link href="{{ asset('css/login/app.css')."?v=".str_random(3) }}" rel="stylesheet">
</head>
<body>
	<div class="container valign-wrapper contenedor-login">
		<div class="no-valing-wrapper">
			<div class="row card-deck">
				<div class="card valign-wrapper" style="border: none;background: transparent;">
						<div class="container valign-wrapper">
							<div class="no-valing-wrapper text-justifity text-white">
								<h3 class="mb-3 font-regular">Bienvenido a TECMAQ</h3>
								<p class="font-regular">
									La <b>calidad</b> es nuestra imagen, </p>
								<p class="font-regular">
									el <b>compromiso</b> nuestra norma y
								</p>
								<p class="font-regular">
									el <b>costo</b> nuestra ética.
								</p>
							</div>
						</div>
				</div>
				<div class="card bg-light" style="margin-right: 90px; margin-left: 90px;">
					<div class="img-container align-self-center">
						<img src="{{ asset('img/logo tecmaq.png') }}" class="my-1 mt-3 align-self-center img-fluid" alt="...">
					</div>
					<div class="card-body text-center login-form">
                        @if($wc->isNotEmpty())
						<h5 class="card-title font-regular mb-4 ct">
								{{ $wc->first()->num_machine }}
                        </h5>
                            @if ($wc->first()->on_maintenance)
                            <div class="text-center">
                                <h4>
                                    <span class="badge badge-danger">{{__('En petición de mantenimiento')}}</span>
                                </h4>
                            </div>
                            @endif
                        @endif
						<h6 class="card-subtitle mb-3 text-muted font-light text-left mt-2">Introduzca su número de empleado y contraseña</h6>
							<form autocomplete="off" action="{{ route('operador.login') }}" method="POST">
								@csrf
								<div class="input-group form-group mb-4">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-user" aria-hidden="true" style="color: #0c54c4;"></i></span>
									</div>
									<input autocomplete="ÑÖcompletes" type="text" name="noempleado" class="form-control form-control-nbl font-light {{ $errors->has('noempleado')? ' is-invalid' : ''  }}" placeholder="Número de usuario" aria-label="Número de usuario" aria-describedby="basic-addon1" id="empleado" autofocus="" required="required" value="{{ old('noempleado') }}">
									<div class="invalid-feedback" id="usernameFail" >
										@if ($errors->has('noempleado'))
											{{ $errors->first('noempleado') }}
										@endif
									</div>
								</div>
								<div class="input-group mb-4">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true" style="color: #0c54c4;"></i></span>
									</div>
									<input autocomplete="ÑÖcompletes" type="password" name="password" class="form-control form-control-nbl font-light {{ $errors->has('password')? ' is-invalid' : ''  }}" placeholder="Contraseña" aria-label="Contraseña" aria-describedby="basic-addon1" id="password" required="required">
									<div class="invalid-feedback" id="passwordFail">
										@if ($errors->has('password'))
											{{ $errors->first('password') }}
										@endif
									</div>
								</div>
								<div class="input-group mb-4">
									<button type="submit" class="btn btn-primary btn-block font-light">INGRESAR</button>
								</div>
							</form>
							<div class="card-text my-1" style="font-size: .65em;">
								<div class="row">
									<div class="col">
										Desarrollado por
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <img src="{{ asset('logo_sidebar.png') }}" width="70px" alt="" class="">
                                    </div>
                                </div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		// setTimeout(function (e) {
		// 	window.location.reload();
        // }, {{ config('session.lifetime') }} * 60 * 1000 );
	</script>
</body>
</html>
