@extends('admin.layouts.app')
@section('style')
	{{-- <link href="{{ asset('css/setup.css')."?v=".str_random(2) }}" rel="stylesheet"> --}}
	<link href="{{ asset('css/operadores/setup/app.css') }}" rel="stylesheet">
@endsection

@section('content')
<setup-index-component :url_send_notification="{{ json_encode(route('notification.operador.herramentaje')) }}"></setup-index-component>
@endsection
