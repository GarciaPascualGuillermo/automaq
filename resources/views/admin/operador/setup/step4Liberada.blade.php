@extends('admin.layouts.app')
@section('style')
	{{-- <link href="{{ asset('css/setup.css')."?v=".str_random(2) }}" rel="stylesheet"> --}}
	<link href="{{ asset('css/operadores/setup/app.css')."?v=".str_random(2) }}" rel="stylesheet">
@endsection
@section('content')
<setup4-liberada-component 
	:url_login_ajustador="{{ json_encode(route('operador.setup.loginajustador')) }}"
	:url_iniciaproduccion="{{ json_encode(route('operador.setup.step4.iniciaproduccion')) }}"
	>	</setup4-liberada-component>
@endsection
