@extends('admin.layouts.app')
@section('style')
	{{-- <link href="{{ asset('css/setup.css')."?v=".str_random(2) }}" rel="stylesheet"> --}}
	<link href="{{ asset('css/operadores/setup/app.css')."?v=".str_random(2) }}" rel="stylesheet">
@endsection
@section('content')
<setup3calidad-component :url_login_ajustador="{{ json_encode(route('operador.setup.loginajustador')) }}">	</setup3calidad-component>
@endsection
