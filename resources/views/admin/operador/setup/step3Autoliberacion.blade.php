@extends('admin.layouts.app')
@section('style')
	{{-- <link href="{{ asset('css/setup.css')."?v=".str_random(2) }}" rel="stylesheet"> --}}
	<link href="{{ asset('css/operadores/setup/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	
@endsection
@section('content')
<setup3autoliberacion-component :url_autoliberacion="{{ json_encode(route('operador.setup.step3.autoliberacion')) }}">	</setup3autoliberacion-component>
@endsection
