@extends('admin.layouts.app')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/operador.css') }}">
@endsection
@section('content')

	<operador-component :url_pausa_motivo="{{ json_encode(route('operador.pausa.motivo')) }}"></operador-component>

@endsection
@section('modal')
	{{-- @include('admin.operador.modal.resultadosmalo') --}}
	{{-- <capeando-component></capeando-component> --}}
	{{-- <pausa-component></pausa-component> --}}
@endsection
@section('script')
	<script>
		// $("#scraps").modal('show');
	</script>
@endsection
