<form method="POST" action="{{ $route }}"  enctype="multipart/form-data" role="form">
	{{ csrf_field() }}
	@if($errors->count())
		{{-- {{ dd($errors) }} --}}
	@endif
	<div class="row">
		<div class="col-8">
			<div class="form-group row" style="margin-top: 20px">
				<label for="inputName" class="col-3 col-form-label">Nombre</label>
				<div class="col-9">
					<input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="nombre" placeholder="Nombre" value="{{$user->name??old('name')}}">
					@if ($errors->has('name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row" style="margin-top: 20px">
				<label for="firstName" class="col-3 col-form-label">Primer apellido</label>
				<div class="col-9">
					<input type="text" name="firstname" class="form-control {{ $errors->has('firstname') ? ' is-invalid' : '' }}" id="nombre" placeholder="Nombre" value="{{$user->firstname ?? old('firstname')}}">
					@if ($errors->has('firstname'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('firstname') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row" style="margin-top: 20px">
				<label for="firstName" class="col-3 col-form-label">Email</label>
				<div class="col-9">
					<input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="correo" placeholder="Email" value="{{$user->email??old('email')}}">
					@if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group row">
				<label for="idemployee" class="col-3 col-form-label">{{ __('idemployee') }}</label>

				<div class="col-9">
					<input id="idemployee" type="text" class="form-control{{ $errors->has('idemployee') ? ' is-invalid' : '' }}" name="idemployee" value="{{ $user->idemployee??old('idemployee') }}"  placeholder="id de empleado">

					@if ($errors->has('idemployee'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('idemployee') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row" style="margin-top: 20px">
				<label for="inputPassword" class="col-3 col-form-label">Password</label>
				<div class="col-9">
					<input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="inputPassword" placeholder="Password">
					@if ($errors->has('password'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row" style="margin-top: 20px">
				<label for="ConfirmPassword" class="col-3 col-form-label">Confirm Password</label>
				<div class="col-9">
					<input type="password" name="password_confirmation" class="form-control" id="ConfirmPassword" placeholder="Password">
				</div>
			</div>

			<div class="form-group row" style="margin-top: 20px">
				<label for="inputPassword3" class="col-3 col-form-label">Turno A</label>
				<div class="col-9">
					<div class="form-group">
						{{ Form::select("mealtime",['11:00:00'=>'11:00:00','11:30:00'=>'11:30:00','12:00:00'=>'12:00:00','12:30:00'=>'12:30:00','12:45:00'=>'12:45:00'],$user->mealtime??old('mealtime'),['class'=>'form-control']) }}
					</div>
					@if ($errors->has('mealtime'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('mealtime') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group row" style="margin-top: 20px">
				<label for="inputPassword3" class="col-3 col-form-label">Turno B</label>
				<div class="col-9">
					<div class="form-group">
						{{ Form::select("mealtime2",['19:00:00'=>'19:00:00','19:30:00'=>'19:30:00','20:00:00'=>'20:00:00'],$user->mealtime2??old('mealtime2'),['class'=>'form-control']) }}
					</div>
					@if ($errors->has('mealtime2'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('mealtime2') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group row" style="margin-top: 20px">
				<label for="inputPassword3" class="col-3 col-form-label">Rol</label>
				<div class="col-9">
					<div class="form-group">
						{{ Form::select("roles[]",$roles->pluck('description','id'),$user->roles->pluck('id'),['class'=>'form-control','multiple'=>'multiple']) }}

					</div>
				</div>
			</div>
			<div class="form-group row" style="margin-top: 20px;">
				<label for="inputPassword3" class="col-3 col-form-label">Máquinas</label>
				<div class="col-9">
					<div class="form-group" >
                        {{
                            Form::select(
                                "workcenters[]",
                                $workcenters->pluck('num_machine', 'id'),
                                $user->workcenters->pluck('id'),
                                ['class' => 'form-control', 'multiple' => 'multiple', 'style' => 'height: 158px;']
                            )
                        }}
					</div>
				</div>
			</div>
		</div>
		<div class="col-4">
			<div>
				<div class="col-4" style="text-align: center;">
					<div class="card" style="width: 210%; margin-bottom: 10px">
						<img src="{{ $user->photo?'/photos/'.$user->photo:'/assets/operador/empty_profile.png' }}" style="width: 100%">
					</div>
					<input type="file" name="photo" class="" id="inputPassword3" placeholder="Cargar Foto">
				</div>
			</div>
		</div>
	</div>
	<div class="form-group row" style="margin-top: 20px">
		<div class="col-8" style="">
			<button type="submit" class="btn btn-primary">{{$user->id?'Actualizar':'Crear'}}</button>
		</div>
	</div>
</form>
