@extends('admin.usuarios.layouts.app')

@section('content')
    <div class="container-fluid">
        @include( 'admin.usuarios.form', ['user' => $user, 'route'=>route('usuarios.update', $user->id)] )
    </div>
@endsection
