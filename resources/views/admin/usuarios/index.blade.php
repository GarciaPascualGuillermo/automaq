@extends('admin.usuarios.layouts.app')

@section('content')
<div class="container-fluid" >
	<div class="row">
		<div class="col-6 color-black font-light col-md-5">
			<b style="">Usuarios</b>
		</div>
		<div class="col-6" style="text-align: right;">
			<a href="{{ route('usuarios.crear') }}"><i class="fa fa-plus" aria-hidden="true" ></i> Agregar Usuario</a>
		</div>
	</div>

    <div class="row" style="width: 95%; margin-left: 25px; margin-top: 25px">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body py-3">

                    @if(session('message'))
                        <div class="col-12">
                            <div class="alert alert-danger">
                                {{ session('message') }}
                            </div>
                        </div>
                    @endif
                    <table class="table table-bordered datatables text-center">
                        <thead class="thead-dark">
                            <tr>
                                <th>Id de empleado</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Roles</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                                <tr class="table-secondary">
                                    <td>{{ $user->idemployee }}</td>
                                    <td>{{ $user->name." ".$user->firstname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->roles->pluck('description')->implode(', ') }}</td>
                                    <td>
                                        <a href="{{ route('usuarios.editar',$user->id) }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: black"></i></a>
                                        <form method="POST" action="{{ route('usuarios.destroy',$user->id) }}" class="delete-user-form" style="display: inline-block;">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="button" class="delete-user" style="background-color: transparent;border: none"><i class="fa fa-user-times" aria-hidden="true"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">
                                        <h3>No hay usuarios registrados.</h3>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
	</div>
</div>
@endsection

@section('script')

    <script>

        $('.delete-user').click(function () {
            let choice = confirm('¿Está seguro de que desea eliminar este usuario?');
            if (choice) {
                choice = confirm('Confirme esta accion nuevamente');
                if (choice) {
                    $(this).parents('.delete-user-form').submit();
                }
            }
        });

        $('.datatables').DataTable();

    </script>

@endsection
