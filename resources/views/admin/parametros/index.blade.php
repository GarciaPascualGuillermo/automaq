@extends('admin.parametros.layouts.app')
@section('content')
<form method="POST" action="{{ route('parametros.store') }}"  enctype="multipart/form-data" role="form">
	{{ csrf_field() }}
	<div class="container-fluid">
		<div class="row" style="margin-left: 10px; margin-top: 10px">
			<div class="col-12 color-black font-light">
				<h2 style="">Parámetros</h2> 
				<h4 class="color-black font-light" style="margin-top: 10px; "> Porcentaje productividad
				</h4>
				<div class="row" style="margin-left: 15px; margin-top: 5px">
					<div class="">Verde
						{{-- <input class="" type="" name="" placeholder="Porcentaje">% &nbsp&nbsp  Amarillo <input type="" name="" placeholder="Porcentaje">% --}}
						<input type="text" name="verde" class="{{ $errors->has('verde') ? ' is-invalid' : '' }}" id="verde" placeholder="Porcentaje" value="{{$parameter->where('name','verde')->first()->value??old('verde')}}">
						@if ($errors->has('verde'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('verde') }}</strong>
							</span>
						@endif
						&nbsp&nbsp  Amarillo <input type="text" name="amarillo" class="{{ $errors->has('amarillo') ? ' is-invalid' : '' }}" id="amarillo" placeholder="Porcentaje" value="{{$parameter->where('name','amarillo')->first()->value??old('amarillo')}}">
						@if ($errors->has('amarillo'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('amarillo') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<h4 class="color-black font-light" style="margin-top: 15px; "> Grados de calidad
					
				</h4>
				
				<div class="color-black font-light" style="margin-top: 15px;margin-left: 15px "> Porcentaje de grado de calidad del día
					<input type="text" name="Pro_ca_dia" class="{{ $errors->has('Pro_ca_dia') ? ' is-invalid' : '' }}" id="Pro_ca_dia" placeholder="Porcentaje" value="{{$parameter->where('name','Pro_ca_dia')->first()->value??old('Pro_ca_dia')}}">
					@if ($errors->has('Pro_ca_dia'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('Pro_ca_dia') }}</strong>
						</span>
					@endif
				</div>
			</div>
		</div>
		
		<div class="row">
			<button style="margin-left: 40px; margin-top: 20px" type="submit" class="btn btn-secondary"> Actualizar</button>
		</div>
		
	</div>
</form>



@endsection
@section('script')

<script >


</script>
@endsection