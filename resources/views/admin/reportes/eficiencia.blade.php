<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }} - Reporte por Job</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1; overflow-x: auto; max-width: 100%">
				<div class="container-fluid" style="height: 100%;">
					<div class="row" style="height: 25px;">
						<div class="offset-9 offset-sm-9 offset-md-9 offset-lg-9 offset-xl-9 col-3 col-ms-3 col-md-3 col-lg-3 col-xl-3 color-heavy " style="text-align: right;">
							{{-- <a href=''><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a> --}}
						</div>
					</div>
					<div class="row" style="height: 40px;">
						<div class="col-12">
							<div class="page-header">
								<h1>
									{{Form::open(['route' => 'eficiencia.index','method' =>'GET','class' => 'form-inline' ])}}
										<div class="form-group">
											{{Form::text('num_job', $num_job,['class' => 'form-control', 'placeholder' => 'Job'])}}
										</div>
										<div class="form-group">
											{{Form::text('part_number', $part_number,['class' => 'form-control', 'placeholder' => 'No. de parte'])}}
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-md btn-secondary ml-1{{-- input-group-text --}}" id="num_job">	<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</div>
									{{Form::close()}}
								</h1>
							</div>
						</div>
					</div>
					<div class="" style="margin-left: 10px; text-align: center; overflow-x: auto; overflow-y: auto;max-width: 100%;max-height: calc( 100% - 65px);height: calc( 100% - 65px);" >
						
						@foreach($processes_operation as $processes)
							<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="13" style="border: hidden;">Eficiencia por operador</th>
							</tr>
							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 200px;">Operador</th>
								<th style="background-color: #878EAF; min-width: 100px;">CT</th>
								<th style="background-color: #878EAF; min-width: 100px;">Job</th>
								<th style="background-color: #878EAF; min-width: 100px;">OP</th>
								<th style="background-color: #878EAF; min-width: 200px;">Parte</th>
								<th style="background-color: #878EAF; min-width: 100px;">QYT</th>
								<th style="background-color: #878EAF; min-width: 100px;">REG</th>
								<th style="background-color: #878EAF; min-width: 100px;">Hora</th>
								<th style="background-color: #878EAF; min-width: 100px;">Cant</th>
								<th style="background-color: #878EAF; min-width: 100px;">Scrapt</th>
								<th style="background-color: #878EAF; min-width: 100px;">TO</th>
								<th style="background-color: #878EAF; min-width: 100px;">TS</th>
								<th style="background-color: #878EAF; min-width: 100px;">EF</th>
							</tr>
								@foreach($processes as $process)
									@foreach($processes_operador[$process->id] as $operador)
										<tr class="color-heavy" style=" font-size: 10px;">
											<td>{{ $operador->up_u_idemployee }} - {{ $operador->up_u_name }} {{ $operador->up_u_firstname }}</td>
											<td>{{ $process->num_machine }}</td>
											<td>{{ $process->num_job}}</td>
											<td>{{ $process->operation }}</td>
											<td>{{ $part_number}}</td>
											<td>{{ $process->piece}}</td>
											<td>{{ $process->status}}</td>
											<td>{{ $process->ih_created_at->toTimeString() }}</td>
											<td>{{ $operador->end - $operador->start }}</td>
											<td>{{ $operador->scraps }}</td>
											<td></td>
											<td></td>
											<td>{{ intval($operador->productivity) }}%</td>
										</tr>
									@endforeach
								@endforeach
								
							</table>
						@endforeach

						

						
					</div>
				</div>
			</main>
		</div>
	</div>
	@yield('modal')
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
</body>
</html>