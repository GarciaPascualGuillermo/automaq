<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }} - Reporte por Job</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1; overflow-x: auto; max-width: 100%">
				<div class="container-fluid" style="height: 100%;">
					<div class="row" style="height: 25px;">
						<div class="offset-9 offset-sm-9 offset-md-9 offset-lg-9 offset-xl-9 col-3 col-ms-3 col-md-3 col-lg-3 col-xl-3 color-heavy " style="text-align: right;">
							{{-- <a href=''><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a> --}}
						</div>
					</div>
					<div class="row" style="height: 40px;">
						<div class="col-12">
							<div class="page-header">
								<h1>
									{{Form::open(['route' => 'reporte.index','method' =>'GET','class' => 'form-inline' ])}}
										<div class="form-group">
											{{Form::text('num_job', $num_job,['class' => 'form-control', 'placeholder' => 'Job'])}}
										</div>
										<div class="form-group">
											{{Form::text('part_number', $part_number,['class' => 'form-control', 'placeholder' => 'No. de parte'])}}
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-md btn-secondary ml-1{{-- input-group-text --}}" id="num_job">	<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</div>
									{{Form::close()}}
								</h1>
							</div>
						</div>
					</div>
					<div class="" style="margin-left: 10px; text-align: center; overflow-x: auto; overflow-y: auto;max-width: 100%;max-height: calc( 100% - 65px);height: calc( 100% - 65px);" >
						@foreach($processes_operation as $processes)
							<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
								<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
									<th colspan="14" style="height: 35px; width: 40px; border: hidden;">Avance de job desglosado por operación</th>
								</tr>
								<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
									<th colspan="14" style="height: 14px; width: 40px; border: hidden;font-size: 12px;">
										{{$num_job}}/{{$part_number}}
									</th>
								</tr>
								<tr style=" font-size: 10px; color: white; height: 35px;">
									<th style="background-color: #878EAF; min-width: 100px;">Num</th>
									<th style="background-color: #878EAF; min-width: 100px;">Nombre</th>
									<th style="background-color: #878EAF; min-width: 100px;">Apellido</th>
									<th style="background-color: #878EAF; min-width: 100px;">CT</th>
									<th style="background-color: #878EAF; min-width: 100px;">OP</th>
									<th style="background-color: #878EAF; min-width: 100px;">Proceso</th>
									<th style="background-color: #878EAF; min-width: 200px;">Fecha</th>
									<th style="background-color: #878EAF; min-width: 100px;">Hora</th>
									<th style="background-color: #878EAF; min-width: 100px;">Efi</th>
									<th style="background-color: #878EAF; min-width: 100px;">Tiempo</th>
									<th style="background-color: #878EAF; min-width: 100px;">Cant</th>
									<th style="background-color: #878EAF; min-width: 100px;">CantNo</th>
									<th style="background-color: #878EAF; min-width: 100px;">Paro</th>
									<th style="background-color: #878EAF; min-width: 100px;">Comentarios</th>
								</tr>
								@foreach($processes as $process)
									<tr class="color-heavy" style=" font-size: 10px;">
										<td>
											<input type="hidden" name="" value="{{$process->id}}">
											{{ $process->a_ia_idemployee }}
										</td>
										<td>{{ $process->a_ia_name }}</td>
										<td>{{ $process->a_ia_firstname }}</td>
										<td>{{ $process->num_machine }}</td>
										<td>{{ $process->operation }}</td>
										<td>{{ 'ih' }}</td>
										<td>{{ $process->ih_created_at->toDateString() }}</td>
										<td>{{ $process->ih_created_at->toTimeString() }}</td>
										<td></td>
										@php
											$Total_horas=$process->ih_created_at->diffInSeconds($process->fh_created_at);
											$horas= intval((($Total_horas)/60)/60);
											$min= intval(($Total_horas - (($horas*60)*60))/60); 
										@endphp
										@if(!is_null($process->fh_created_at ))
											<td>{{$horas}}:{{$min}} hrs</td>
										@else 
											<td>--</td>
										@endif

										<td>{{$process->qua}}</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr class="color-heavy" style=" font-size: 10px;">
										<td>{{ $process->a_fa_idemployee }}</td>
										<td>{{ $process->a_fa_name }}</td>
										<td>{{ $process->a_fa_firstname }}</td>
										<td>{{ $process->num_machine }}</td>
										<td>{{ $process->operation }}</td>
										<td>{{ 'fa' }}</td>
										<td>{{ $process->fh_created_at->toDateString() }}</td>
										<td>{{ $process->fh_created_at->toTimeString() }}</td>
										<td></td>
										@php
											$Total_horas=$process->ia_created_at->diffInSeconds($process->fa_created_at);
											$horas= intval((($Total_horas)/60)/60);
											$min= intval(($Total_horas - (($horas*60)*60))/60); 
										@endphp
										@if(!is_null($process->fa_created_at ))
											<td>{{$horas}}:{{$min}} hrs</td>
										@else 
											<td>--</td>
										@endif
										<td>{{$process->qua}}</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									@if(!is_null($process->a_il_idemployee))
										<tr class="color-heavy" style=" font-size: 10px;">
											<td>{{ $process->a_il_idemployee }}</td>
											<td>{{ $process->a_il_name }}</td>
											<td>{{ $process->a_il_firstname }}</td>
											<td>{{ $process->num_machine }}</td>
											<td>{{ $process->operation }}</td>
											<td>{{ 'il' }}</td>
											<td>{{ $process->il_created_at->toDateString() }}</td>
											<td>{{ $process->il_created_at->toTimeString() }}</td>
											<td></td>
											@php
											$Total_horas=$process->il_created_at->diffInSeconds($process->ls_created_at);
											$horas= intval((($Total_horas)/60)/60);
											$min= intval(($Total_horas - (($horas*60)*60))/60); 
											@endphp
											@if(!is_null($process->ls_created_at ))
												<td>{{$horas}}:{{$min}} hrs</td>
											@else 
												<td>--</td>
											@endif
											<td>{{$process->qua}}</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									@elseif(!is_null($process->a_ial_idemployee))
										<tr class="color-heavy" style=" font-size: 10px;">
											<td>{{ $process->a_ial_idemployee }}</td>
											<td>{{ $process->a_ial_name }}</td>
											<td>{{ $process->a_ial_firstname }}</td>
											<td>{{ $process->num_machine }}</td>
											<td>{{ $process->operation }}</td>
											<td>{{ 'ial' }}</td>
											<td>{{ $process->ial_created_at->toDateString() }}</td>
											<td>{{ $process->ial_created_at->toTimeString() }}</td>
											<td></td>
											@php
											$Total_horas=$process->ial_created_at->diffInSeconds($process->ls_created_at);
											$horas= intval((($Total_horas)/60)/60);
											$min= intval(($Total_horas - (($horas*60)*60))/60); 
											@endphp
											@if(!is_null($process->ls_created_at ))
												<td>{{$horas}}:{{$min}} hrs</td>
											@else 
												<td>--</td>
											@endif
											<td></td>
											<td>{{$process->qua}}</td>
											<td></td>
											<td></td>
										</tr>
									@endif
									@foreach($processes_operador[$process->id] as $operador)
										<tr class="color-heavy" style=" font-size: 10px;">
											<td>{{ $operador->up_u_idemployee }}</td>
											<td>{{ $operador->up_u_name }}</td>
											<td>{{ $operador->up_u_firstname }}</td>
											<td>{{ $process->num_machine }}</td>
											<td>{{ $process->operation }}</td>
											<td>{{ $operador->status_start."- ".$operador->status_end }}</td>
											<td>{{ $process->fh_created_at->toDateString() }}</td>
											<td>{{ $process->fh_created_at->toTimeString() }}</td>
											<td>{{ intval($operador->productivity) }}%</td>
											@php
												$Total_horas=$operador->start_time->diffInSeconds($operador->end_time);
												$horas= intval((($Total_horas)/60)/60);
												$min= intval(($Total_horas - (($horas*60)*60))/60); 
											@endphp
											@if(!is_null($operador->end_time ))
												<td>{{$horas}}:{{$min}} hrs</td>
											@else 
												<td></td>
											@endif
											<td>{{ $process->qua }}</td>
											<td>{{ $operador->end - $operador->start }}</td>
											<td></td>
											<td></td>
										</tr>
									@endforeach
								@endforeach
								
							</table>
						@endforeach
						@foreach($processes_operation as $processes)
							<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="13" style="border: hidden;">Eficiencia por operador</th>
							</tr>
							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 200px;">Operador</th>
								<th style="background-color: #878EAF; min-width: 100px;">CT</th>
								<th style="background-color: #878EAF; min-width: 100px;">Job</th>
								<th style="background-color: #878EAF; min-width: 100px;">OP</th>
								<th style="background-color: #878EAF; min-width: 200px;">Parte</th>
								<th style="background-color: #878EAF; min-width: 100px;">QYT</th>
								<th style="background-color: #878EAF; min-width: 100px;">REG</th>
								<th style="background-color: #878EAF; min-width: 100px;">Hora</th>
								<th style="background-color: #878EAF; min-width: 100px;">Cant</th>
								<th style="background-color: #878EAF; min-width: 100px;">Scrapt</th>
								<th style="background-color: #878EAF; min-width: 100px;">TO</th>
								<th style="background-color: #878EAF; min-width: 100px;">TS</th>
								<th style="background-color: #878EAF; min-width: 100px;">EF</th>
							</tr>
								@foreach($processes as $process)
									@foreach($processes_operador[$process->id] as $operador)
										<tr class="color-heavy" style=" font-size: 10px;">
											<td>{{ $operador->up_u_idemployee }} - {{ $operador->up_u_name }} {{ $operador->up_u_firstname }}</td>
											<td>{{ $process->num_machine }}</td>
											<td>{{ $process->num_job}}</td>
											<td>{{ $process->operation }}</td>
											<td>{{ $part_number}}</td>
											<td>{{ $process->piece}}</td>
											<td>{{ $process->status}}</td>
											<td>{{ $process->ih_created_at->toTimeString() }}</td>
											<td>{{ $operador->end - $operador->start }}</td>
											<td>{{ $operador->scraps }}</td>
											<td></td>
											<td></td>
											<td>{{ intval($operador->productivity) }}%</td>
										</tr>
									@endforeach
								@endforeach
								
							</table>
						@endforeach
						@foreach($processes_operation as $processes)

							<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
								<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
									<th colspan="5" style="border: hidden;">Setup a la primera</th>
								</tr>
								<tr style=" font-size: 10px; color: white; height: 35px;">
									<th style="background-color: #878EAF; min-width: 70px;">Num</th>
									<th style="background-color: #878EAF; min-width: 80px;">Name</th>
									<th style="background-color: #878EAF; min-width: 80px;">First Name</th>
									<th style="background-color: #878EAF; min-width: 80px;">#Setup</th>
									<th style="background-color: #878EAF; min-width: 80px;">#Setup 1st</th>
								</tr>
								@if(isset($processes))
									@foreach($processes as $process)
										@foreach($processes_setup[$process->id] as $setup)
											<tr class="color-heavy" style=" font-size: 10px;">
												<td>{{ $setup->up_u_idemployee }}</td>
												<td>{{ $setup->up_u_name }} </td>
												<td>{{ $setup->up_u_firstname }}</td>
												<td></td>
												<td>1</td>
											</tr>
										@endforeach
									@endforeach
								@endif
							</table>
						@endforeach
					</div>
				</div>
			</main>
		</div>
	</div>
	@yield('modal')
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
</body>
</html>