@extends('admin.layouts.app')
@section('content')
	<div class="container container-fluid-x ">
		<div class="row">
			<div class="col-6 col-sm-6 col-md-6 col-xl-6 col-lg-6">
				<p class="text-justify font-light">
					Por favor presione el número o letra correspondiente.
				</p>
			</div>
			<div class="col-6 col-sm-6 col-md-6 col-xl-6 col-lg-6 color-heavy" align="right">
				<img src="/img/C.png" style="width: 45px;height: 45px;font-size: 18px;" >
				 Capeando 
			</div>
		</div>
		<div id="status" class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card">
					<div class="card-header fs-20" style="background-color: #E6E8F1;">
						Status
					</div>
					<div class="card-body card-body-py" >
						<div class="row">
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 py-4 text-center border-right">
								<div class="valign-wrapper background-img setup-inactivo" style="">
									<h2 style="width: 100%;" class="text-center color-clear">
										<img src="/img/1.png" style="width: 45px;height: 45px;font-size: 18px;">Setup
									</h2>
								</div>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 py-4 text-center border-right">
								<div class="valign-wrapper background-img produccion-inactivo" style="">
									<h2 style="width: 100%;" class="text-center color-clear">
										<img src="/img/2.png" style="width: 45px;height: 45px;font-size: 18px;">Producción</h2>
								</div>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 py-4 text-center border-right">
								<div class="valign-wrapper background-img pausa-inactivo" style="">
									<h2 style="width: 100%;" class="text-center color-clear">
										<img src="/img/0.png" style="width: 45px;height: 45px;font-size: 18px;">Pausa</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-5 container-grid">
			<div class="col-md-6">
				<div class=" card green-card card-size">
					<div class="card-body">
						<p> Productividad </p>
						<p class="porcent"> 84% </p>
						  <div class="round-xlarge progress-bar-white">
						  	<div class="round-xlarge progress-container" style="width:84%" id="barra">84%</div>
                          </div>
                          <hr />
                          <div class="px-4 my-1 text-left font-light" style="display: block;width: 100%;">
                           <p>Tiempo de Ciclo: </p>
					</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card white-card card-size">
					<div class="card-body">
						<p> Avance de la Orden </p>
					    <div class="row">
					  		<div class="col single-chart">
                        	<svg viewBox="0 0 36 36" class="circular-chart blue">
                        		<path class="circle-bg"
                        		d="M18 2.0845
                                   a 15.9155 15.9155 0 0 1 0 31.831
                                   a 15.9155 15.9155 0 0 1 0 -31.831"
                                />
                                <path class="circle"
                                stroke-dasharray="75, 100"
                                d="M18 2.0845
                                  a 15.9155 15.9155 0 0 1 0 31.831
                                  a 15.9155 15.9155 0 0 1 0 -31.831"
                                 />
                                <text x="18" y="20.35" class="percentage">75%</text>
                            </svg>
                        </div>
					    </div>
                      <hr />
                      <div class="row">
                      	<div class="col">
                      		<p>Cantidad de Piezas:</p> 
                      	</div>
                      	<div class="col">
                      		<p class="color-heavy font-regular"><b>12</b>
                      			<span class="font-light">de</span>
                      			<b>24</b></p>
                      	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
{{-- 	@include('admin.productores.popup.pausa') --}}
@endsection
{{-- 
@yield('modal') --}}