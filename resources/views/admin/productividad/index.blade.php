<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/productividad/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	@yield('style')
</head>
<body style="background-color:  #E6E8F1">
	<div id="app" class="container-fluid" style=" height: 100%;">
		<div class="row" style="margin-top: 30px; margin-right: 20px; margin-left: 20px;">
			<div class="col-3 valign-wrapper">
				<img class="" src="/assets/operador/logo_tecmaq.png" style="height: auto !important; width: 80%!important">
			</div>
			<div class="col-6 valign-wrapper">
				<div class="font-light fs-18 text center " style="font-size: 85px;"> Ranking de Productividad<b>-Operadores</b> </div>
			</div>
			<div class="col-3 valign-wrapper " style="font-size: 75px;">
				<div class="no-valign-wrapper font-light text-center">
					<b>{{$date}}</b>
				</div>
			</div>
		</div>
		<div class="row" style="margin-right: 20px; margin-left: 20px;">
			<div class="col-12">
				<div class="container-fluid">
					@foreach($user_prom as $user_id => $prom)
						@php
							$user=App\User::find($user_id);
						@endphp
						@if($count >= $inicio)
							<div class="row">
								<div class="card col-12 shadow px-5" style="margin-top: 60px; font-size: 180px;">
									<div class="row">
										<div class="col-1">
											<div class="color-heavy font-regular "><b>{{ $count++ }}</b></div>
										</div>
										<div class="col-8">
											<div class="font-regular ">{{ $user->name }} {{ $user->firstname }}</div>
										</div>
										<div class="col-3">
											<div class="color-heavy font-regular text-right" ><b>{{ number_format($prom['prom'],2) }} %</b></div>
										</div>
									</div>
								</div>
							</div>
						@else
							@php $count++; @endphp
						@endif
						@php
							if ($count > $limite ) break;
						@endphp
					@endforeach

				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<script>

		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
		]) !!};
	</script>
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>
	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
</body>
</html>
