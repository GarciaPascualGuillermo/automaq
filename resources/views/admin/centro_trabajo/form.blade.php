<form method="POST" action="{{ $route }}"  enctype="multipart/form-data" role="form">
	{{ csrf_field() }}
		<div class="row">
			<div class="col-6">
				<div class="form-group row" style="margin-top: 25px">
					<label for="maquina" class="col-3 col-form-label">No. de Máquina</label>
					<div class="col-6">
						<input type="text" name="num_machine" class="form-control {{ $errors->has('num_machine') ? ' is-invalid' : '' }}" id="numero_maquina" placeholder="Número" value="{{$workcenter->num_machine??old('num_machine')}}">
						@if ($errors->has('num_machine'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('num_machine') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group row" style="margin-top: 25px">
					<label for="serie" class="col-3 col-form-label">No. Serie Tu Nerd</label>
					<div class="col-6">
						<input type="text" name="num_serie_tunerd" class="form-control {{ $errors->has('num_serie_tunerd') ? ' is-invalid' : '' }}" id="numero_serie" placeholder="Número" value="{{$workcenter->num_serie_tunerd??old('num_serie_tunerd')}}">
						@if ($errors->has('num_serie_tunerd'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('num_serie_tunerd') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group row" style="margin-top: 25px">
					<label for="ip" class="col-3 col-form-label">IP</label>
					<div class="col-6">
						<input type="text" name="ip_address" class="form-control {{ $errors->has('ip_address') ? ' is-invalid' : '' }}" id="ip" placeholder="Número" value="{{$workcenter->ip_address??old('ip_address')}}">
						@if ($errors->has('ip_address'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('ip_address') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group row" style="margin-top: 25px">
					<label for="marca" class="col-3 col-form-label">Marca</label>
					<div class="col-6">
						<input type="text" name="brand" class="form-control {{ $errors->has('brand') ? ' is-invalid' : '' }}" id="ip" placeholder="Marca" value="{{$workcenter->brand??old('brand')}}">
						@if ($errors->has('brand'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('brand') }}</strong>
							</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group row" style="margin-top: 25px">
					<label for="modelo" class="col-3 col-form-label">Modelo</label>
					<div class="col-6">
						<input type="text" name="model" class="form-control {{ $errors->has('model') ? ' is-invalid' : '' }}" id="ip" placeholder="Modelo" value="{{$workcenter->model??old('model')}}">
						@if ($errors->has('model'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('model') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group row" style="margin-top: 25px">
					<label for="seccion" class="col-3 col-form-label">Sección</label>
					<div class="col-6">
						{{	 Form::select(
								'section',	#name
								['SE'=>'Seguetas','TC'=>'Tornos chicos','TV'=>'Tornos verticales','TP'=>'Tornos Petroleros','5E'=>'5 ejes','CM'=>'Centro de Maquinado'], # option
								$workcenter->section??old('section'), #value
								#opciones
								[ 
									'class'=>$errors->has('section') ? ' is-invalid form-control' : 'form-control',
									'placeholder'=>'Sección',
									'id'=>'seccion'
								] 
							) 
						}}
						@if ($errors->has('section'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('section') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group row" style="margin-top: 25px">
					<label for="ct" class="col-3 col-form-label">Centro de Trabajo</label>
					<div class="col-6">
						<input type="text" name="work_center" class="form-control {{ $errors->has('work_center') ? ' is-invalid' : '' }}" id="ip" placeholder="Sección" value="{{$workcenter->work_center??old('work_center')}}">
						@if ($errors->has('work_center'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('work_center') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group row" style="margin-top: 25px">
					<label for="departamento" class="col-3 col-form-label">Departamento</label>
					<div class="col-6">
						<input type="text" name="department" class="form-control {{ $errors->has('department') ? ' is-invalid' : '' }}" id="ip" placeholder="Departamento" value="{{$workcenter->department??old('department')}}">
						@if ($errors->has('department'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('department') }}</strong>
							</span>
						@endif
					</div>
				</div>
			</div>
			
		</div>
		<div class="form-group row" style="margin-top: 20px">
			<div class="col-8" style="">
				<button type="submit" class="btn btn-primary">{{$workcenter->id?'Actualizar':'Crear'}}</button>
			</div>
		</div>
	</form>