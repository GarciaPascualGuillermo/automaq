@extends('admin.centro_trabajo.layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-6 color-black font-light col-md-5">
			<b style="">Centro de Trabajo</b>
		</div>
		<div class="col-6" style="text-align: right;">
			<a href="{{ route('centro_trabajo.crear') }}"><i class="fa fa-plus" aria-hidden="true" ></i> Agregar CT</a>
		</div>
	</div>

    <div class="row" style="width: 95%; margin-left: 25px; margin-top: 25px">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">

                    @if(session('message'))
                        <div class="col-12">
                            <div class="alert alert-danger">
                                {{ session('message') }}
                            </div>
                        </div>
                    @endif
                    <table class="table table-hover table-bordered datatables text-center" style="">
                        <thead class="thead-dark">
                            <tr>
                                <th>No. Máquina</th>
                                <th>IP</th>
                                <th>Sección</th>
                                <th>Centro de trabajo</th>
                                <th>Departamento</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($workcenters as $workcenter)
                                <tr class="table-secondary">
                                    <td>{{ $workcenter->num_machine }}</td>
                                    <td>{{ $workcenter->ip_address }}</td>
                                    <td>{{ $workcenter->section }} </td>
                                    <td>{{ $workcenter->work_center }}</td>
                                    <td>{{ $workcenter->department }}</td>
                                    <td>
                                        <a href="{{ route('centro_trabajo.editar', $workcenter->id) }}" ><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: black"></i></a>
                                        <form method="POST" action="{{ route('centro_trabajo.destroy',$workcenter->id) }}" style="display: inline-block;" class="form-delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="button", style="background-color: transparent;border: none;" class="btn-delete"><i class="fa fa-times-circle-o" aria-hidden="true" style="color: black"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <li>No hay centros de trabajo registrados</li>
                            @endforelse
                        </tbody>
                    </table>

                </div>
            </div>


        </div>

	</div>
</div>

@endsection
@section('script')

<script>
    $('.datatables').DataTable();

    $('.btn-delete').click(function () {
            let choice = confirm('¿Está seguro de que desea eliminar este centro de trabajo?');
            if (choice) {
                choice = confirm('Confirme esta accion nuevamente');
                if (choice) {
                    $(this).parents('.form-delete').submit();
                }
            }
        });
</script>

@endsection
