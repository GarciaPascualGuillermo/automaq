<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	@yield('head')
	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.jobs.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1;">
				<div class="container-fluid">
					<div class="row " style="border-bottom: 1px solid #898989;margin-left: 5px; margin-top: -20px; margin-right: 5px">
						<a href="{{ route('supervisores.index') }}?section=  " class="col btn btn-link font-light {{ request()->input('section',"")=='' ? 'text-primary':'' }} " style="color: #6E6E6E;">Todas</a>
						<a href="{{ route('supervisores.index') }}?section=S " class="col btn btn-link font-light {{ request()->input('section',"")=='S' ? 'text-primary':'' }} " style="color: #6E6E6E;">Seguetas</a>
						<a href="{{ route('supervisores.index') }}?section=TC" class="col btn btn-link font-light {{ request()->input('section',"")=='TC' ? 'text-primary':'' }} " style="color: #6E6E6E;">Tornos chicos</a>
						<a href="{{ route('supervisores.index') }}?section=TV" class="col btn btn-link font-light {{ request()->input('section',"")=='TV' ? 'text-primary':'' }} " style="color: #6E6E6E;">Tornos verticales</a>
						<a href="{{ route('supervisores.index') }}?section=TP" class="col btn btn-link font-light {{ request()->input('section',"")=='TP' ? 'text-primary':'' }} " style="color: #6E6E6E;">Tornos petroleros</a>
						<a href="{{ route('supervisores.index') }}?section=5E" class="col btn btn-link font-light {{ request()->input('section',"")=='5E' ? 'text-primary':'' }} " style="color: #6E6E6E;">5 ejes</a>
						<a href="{{ route('supervisores.index') }}?section=CM" class="col btn btn-link font-light {{ request()->input('section',"")=='CM' ? 'text-primary':'' }} " style="color: #6E6E6E;">Centros de maquinado</a>
					</div>
				</div>
					@yield('content')
			</main>
		</div>
		@yield('modal')
		<modal-mensaje></modal-mensaje>
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
	@yield('js')
</body>
</html>
