@extends('admin.supervisores.app')
@section('head')
    <meta http-equiv="refresh" content="300">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endsection
@section('content')

    <div class="container-fluid" style="margin-top: 10px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <h3>Filtrar por dia</h3>
                        <form action="">

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                        <input type="text" value="{{ request('day') }}" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="day" placeholder="Seleccione un dia"/>
                                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary">Filtrar</button>
                            <a href="javascript:window.open('{{ route('supervisores.export.pdf') }}?day={{ request('day') }}' , 'ventana1', 'scrollbars=NO')" class="btn btn-secundary">Export PDF</a>
                            <a href="{{ route('supervisores.export.excel') }}?day={{ request('day') }}" class="btn btn-success">Export CSV (Excel)</a>
                            <a href="{{ route('supervisores.index') }}" class="btn btn-info">Reset filtro</a>
                        </form>


                    </div>
                </div>
            </div>
        </div>
		<div class="row" style="text-align: center;">
			<div class="col color-black font-light col-md-5">
				<font size="3">Jobs en proceso</font>
				{{-- <font size="4" class="col color-heavy text-right font-regular"> {{ this.proceso }} </font> --}}
			</div>
			<div class="col color-black font-light col-md-7">
				<font size="3">Status</font>
				{{-- <font size="4" class="col color-heavy text-right font-regular"> {{ this.programados }} </font> --}}
			</div>
		</div>
	</div>
	<div class="container-fluid" style="background: #E6E8F1; ">

        @foreach($workcenters as $workcenter)


            @php
				$process = $workcenter->processes->first();
            @endphp

            <div class="row" style="margin-top: 10px;">

                <div class="col-4" style="height: 160px;">
                  {{-- Workcenter Card  --}}
					<div class="card shadow" style="max-width: 420px;height: 150px; background: #8597AF;min-width: 420px;">
						<div class="row no-gutters" style="height: 140px;">
							<div class="col-md-4 text-center valign-wrapper">
								<div class="no-valign-wrapper">
									<font size="5" class="text-white">{{ $workcenter->num_machine }}</font><br>
									<font size="1" class="font-light text-white">{{$workcenter->model}}</font>
									<div class="input-search input-group font-light text-center" style="display: block;">
										<span class="input-group-text icon font-light input-search" id="basic-addon1" style="background-color: white; color: #8597AF; font-size: 10px;display: inline-block;">
											{{$workcenter->day}} DÍAS |  {{round($workcenter->hour,0)}} HORAS
										</span>
									</div>
								</div>
							</div>
							<div class="" style="border-left: 1px solid gray; height: 107%;"></div>
							<div class="col-md-8" style="margin-left: -1px;">
								<div class="card-body" style="padding: 0.2rem;">

                                    <div class="addJobCardFull">
                                        <div class="col-6 valign-wrapper" style="padding-left: 2px">
                                            <div class="no-valign-wrapper text-right">
                                                {{-- Card ejecutandose en el workcenter  --}}
                                                @if(!is_null($workcenter->processes->first()))
                                                    @include('admin.supervisores.cards.cnc-job-full', [
                                                        'process' => $workcenter->processes->first()
                                                    ])
                                                @endif
                                            </div>
                                        </div>
                                    </div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card col shadow" style="width: 60px; height: 140px;margin-left:  25px; margin-right: 5px">
					<div class="row" style="height: 45%; border-bottom: 1px solid #8597AF; text-align: center;">
						<div class="col" style="">


							@if(is_null($process) || is_null($process->status))
								<img src="/assets/supervisores/1.png" style="width: 100%;height: 70px">
                            @endif

                            @if(is_null($process) || is_null($process->status))
								<img src="/assets/supervisores/1.png" style="width: 100%;height: 70px">
							@endif
							@if(!is_null($process) && ($process->status=='ih' || $process->status=='fh' ))
								<img src="/assets/supervisores/2.png" style="width: 100%;height: 70px">
							@endif
							@if(!is_null($process) && ($process->status=='ia' || $process->status=='fa'))
								<img src="/assets/supervisores/3.png" style="width: 100%;height: 70px">
							@endif
							@if(!is_null($process) && ($process->status=='il' || $process->status=='ls' || $process->status=='ln'|| $process->status=='ial'))
								<img src="/assets/supervisores/4.png" style="width: 100%;height: 70px">
							@endif
							@if(!is_null($process) && ($process->status=='pr' || $process->status=='cr' || $process->status=='pm' || $process->status=='pmp' || $process->status=='pfh' || $process->status=='pfm' ))
								<img src="/assets/supervisores/5.png" style="width: 100%;height: 70px">
							@endif
						</div>

					</div>
					@if(is_null($process))
					<div class="row" style="height: 60%; overflow-y: hidden; ">
						<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
							<div style="margin-top: 10px">

							</div>
						</div>
						<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
							<div style="margin-top: 10px">

							</div>
						</div>
						<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
							<div style="margin-top: 10px">

							</div>
						</div>
						<div class="col" style="text-align: center;">
							<div class="" style="">

							</div>
						</div>
					</div>
					@else

					<div class="row" style="height: 60%; overflow-y: hidden;">
						<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
							@if(!is_null($process) && ($process->status=='ih' || $process->status=='fh'|| $process->status=='ia' || $process->status=='ial' || $process->status=='fa' || $process->status=='il' || $process->status=='ls' || $process->status=='ln'|| $process->status=='pr' || $process->status=='cr' || $process->status=='pm' || $process->status=='pmp' || $process->status=='pfh' || $process->status=='pfm'))

								<div style="margin-top: 10px">
									<font size="2" class="color-heavy font-light" >
										@php
										$user= $process->user_processes->where('active',true)->first();
										if (!is_null($user)) $user=$user->user;
										@endphp

										@if(!is_null($user))
											{{$user->idemployee}} {{$user->name}}
										@endif
									</font><br>
									<font size="2" class="color-heavy font-light" ><i class="fa fa-clock-o " aria-hidden="true"></i>&nbsp{{$workcenter['total_herramentaje']}} min</font>

								</div>
							@endif
						</div>
						<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
							@if(!is_null($process) && ($process->status=='ia' || $process->status=='ial'|| $process->status=='fa' || $process->status=='il' || $process->status=='ls' || $process->status=='ln' || $process->status=='pr' || $process->status=='cr' || $process->status=='pm' || $process->status=='pmp' || $process->status=='pfh' || $process->status=='pfm'))

								<div style="margin-top: 10px">
									<font size="2" class="color-heavy font-light" >
										@if(!is_null($process->ajustador_ia))
										{{$process->ajustador_ia->idemployee}} {{$process->ajustador_ia->name}}
										@endif
									</font><br>

									<font size="2" class="color-heavy font-light" ><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp{{$workcenter['total_ajuste']}} min</font>
								</div>
							@endif
						</div>
						<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
							@if(!is_null($process) && ($process->status=='il' || $process->status=='ial'|| $process->status=='ls' || $process->status=='ln' || $process->status=='pr' || $process->status=='cr' || $process->status=='pm' || $process->status=='pmp' || $process->status=='pfh' || $process->status=='pfm'))

								<div style="margin-top: 10px">
									<font size="2" class="color-heavy font-light" >
										@if(!is_null($process->ajustador_il))
										{{$process->ajustador_il->idemployee}} {{$process->ajustador_il->name}}
										@elseif (!is_null($process->ajustador_ial))
										{{$process->ajustador_ial->idemployee}} {{$process->ajustador_ial->name}}
										@endif
									</font><br>
									<font size="2" class="color-heavy font-light" ><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp{{$workcenter['total_liberacion']}} min</font>
								</div>
							@endif
						</div>
						<div class="col" style="text-align: center;">
							<div class="" style="">
								@if(!is_null($process) && ($process->status=='pr' || $process->status=='cr' || $process->status=='pm' || $process->status=='pmp' || $process->status=='pfh' || $process->status=='pfm'))

									@php
                                    $user = $process->user_processes->where('active',true)->first();
                                    $productivity = productivity($process)['productividad'];
									@endphp
									<font size="2" class="color-heavy font-medium" >

                                    @if(!is_null($user))
										{{$user->idemployee}} {{$user->name}}
									@endif

									</font><br>
                                    <font size="2" class="color-heavy font-medium" ><i class="fa fa-clock-o" aria-hidden="true"></i>
                                        &nbsp{{$workcenter['total_produccion']}} min

                                        {{ $productivity }}%
                                    </font>
									<div class="row ">
										<div class="col-8">
											<div class="progress" style="height: 10px; width: 100%; margin-top: 5px; padding-right: 0; padding-left: 0">
												<div class="progress-bar progress-bar-striped bg-success" role="progressbar" aria-valuenow="{{$workcenter['progreso']}}"aria-valuemin="0" aria-valuemax="100" style="width: {{$workcenter['progreso']}}%">
												</div>
											</div>
                                        </div>

                                        @php
                                            if ($process) {
                                                $piece = getCycles($process)['pieceTotal'];
                                            }
                                        @endphp

										<div class="col-4" style="color: green">{{ isset($piece) ? $piece : 0 }}</div>
									</div>
								@endif
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		@endforeach
	</div>

@endsection
@section('js')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

	<script>

		@foreach($workcenters as $workcenter)
			@php $process=$workcenter->processes->where('active',true)->first() @endphp
			@if(!is_null($process))
				process_first=@php echo json_encode($process); @endphp;
				axios.post("operador/cycles",process_first)
				.then(
					(response) => {
						var data=response.data;
						// console.log(data);
						$(".value_circle_{{ $process->id }}").attr('stroke-dasharray',data.piecePercent+",100");
						$("#text_circle_{{ $process->id }}").empty();
						$("#text_circle_{{ $process->id }}").append(data.piecePercent+"%");
					}
				).catch(error => {
					console.log("error",error);
				});
			@endif
		@endforeach
	</script>
@endsection
