<div id="cardCNC_{{$process->id}}" class="card jobs-card color-gray font-light shadow card_jobs" style="width: 17rem;
        height: 122px;">
	<div class="card-body valign-wrapper" style="min-height: 100%;padding: 0 1rem 0 1rem; font-size: 10px">
		<div class="no-valign-wrapper">
			<div class="row " style="margin-top: 2px" >
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="">
					<div class="row fs-18">
						<div class="col font-regular text-left" style="color: black; font-size: 12px">
							<b>Job:</b>
						</div>
						<div class="col font-regular color-heavy text-right" style="font-size: 12px">
							<b>{{ $process->job->num_job }}</b>
						</div>
						<div class="col" style="text-align: center; padding-right: 0.1px; color: black; font-size: 14px">
							@if($process->status == 'pr')
								<div class="rounded-pill" style="background-color: green; ">{{strtoupper($process->status) }}</div>
							@elseif($process->status == 'ls' || $process->status == 'il' || $process->status == 'ial' || $process->status == 'ln')
								<div class="rounded-pill" style="background-color: yellow; ">{{strtoupper($process->status)  }}</div>
							@elseif($process->status == 'pfh' || $process->status == 'pfm')
								<div class="rounded-pill" style="background-color: red; ">{{strtoupper($process->status) }}</div>
							@elseif($process->status == 'ih' || $process->status == 'fh' || $process->status == 'ia' || $process->status == 'fa')
								<div class="rounded-pill" style="background-color: orange; ">{{strtoupper($process->status) }}</div>
							@elseif($process->status == 'pm' || $process->status == 'pmp')
								<div class="rounded-pill" style="background-color: purple; ">{{strtoupper($process->status)  }} </div>
							@endif
						</div>
					</div>
				</div>
				
			</div>
			<div class="row font-regular" style="height: 98px">
				<div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9" style="padding-right: 4px">
					<hr style="margin-top: 0.1rem; margin-bottom: 0.1rem;">
					<div class="row">
						<div class="col font-light color-heavy text-left" style="word-break: break-all;">
							#{{$process->operation->piece->part_number}}
						</div>
					</div>

					<div class="row">
						<div class="col font-light color-gray text-left">
							Operación:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ $process->operation->operation_service.'-'.substr($process->operation->description, 0,3) }}
						</div>	
					</div>

					<div class="row">
						<div class="col-8 font-light color-gray text-left">
							Cantidad piezas:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ $process->job->quantity }}
						</div>
					</div>
					<div class="row">
						<div class="col-8 font-light color-gray text-left">
							Tiempo de ciclo:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ $process->operation->standar_time }}
						</div>
					</div>
					<div class="row">
						<div class="col font-light color-gray text-left">
							Cliente:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ str_limit($process->operation->piece->client->customer,8) }}
						</div>	
					</div>
				</div>
				<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3" style="margin-left: -20px;">
					<hr style="margin-top: 0.1rem; margin-bottom: 0.1rem;">
					<div class="single-chart" style="width: 5rem;">
						<svg viewBox="0 0 36 36" class="circular-chart blue">
							<path class="circle-bg"
							d="M18 2.0845
							   a 15.9155 15.9155 0 0 1 0 31.831
							   a 15.9155 15.9155 0 0 1 0 -31.831"
							/>
							<path class="circle value_circle_{{ $process->id }}"
							stroke-dasharray="0, 100"
							d="M18 2.0845
							  a 15.9155 15.9155 0 0 1 0 31.831
							  a 15.9155 15.9155 0 0 1 0 -31.831"
							 />
							<text id="text_circle_{{ $process->id }}" i x="18" y="20.35" class="percentage">0%</text>
						</svg>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-white text-center shadow background-color-heavy">
		<div class="row">
			<div class="col-4" style="padding-right: 0px; padding-left: 10px;">
				<i class="fa fa-calendar fs-18" aria-hidden="true" style="font-size:12px;"></i>&nbsp
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{{ $process->job->delivered_at->format('d m') }}</b></div>
			</div>
			<div class="col-2" style="padding-right: 0px; padding-left: 10px;">
				<i class="fa fa-clock-o" aria-hidden="true" style="font-size:12px;"></i>&nbsp
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{{ round((($process->job->quantity)*($process->operation->standar_time))/60),0}}</b></div>
			</div>
			<div class="col-6" style="padding-right: 0px; padding-left: 10px;">
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{!! $process->operation->work_center!!}</b></div>
			</div>
		</div>
	</div>
</div>