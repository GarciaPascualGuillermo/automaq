<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }} - Calidad</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1; max-width: 100%">
				<div class="container-fluid" style="height: 100%;">
					<div class="row" style="height: 25px;">
						<div class="offset-9 offset-sm-9 offset-md-9 offset-lg-9 offset-xl-9 col-3 col-ms-3 col-md-3 col-lg-3 col-xl-3 color-heavy " style="text-align: right;">
							<button type="button" class="btn btn-link" id="btnExportar"><i class="fa fa-external-link" aria-hidden="true" > Exportar</i></button>
							{{-- <a id="exportar" href='{{route("export.calidad")}}?page={{ request()->input('page',1) }}'><i class="fa fa-external-link" aria-hidden="true" > Exportar</i></a> --}}
						</div>
					</div>
					<div class="row" style="height: 40px;">
						<div class="col-12">
							<div class="page-header">
								<h1>
									{{Form::open(['route' => 'historial.calidad','method' =>'GET','class' => 'form-inline','action' => 'ExportController@calidad'])}}
										<div class="form-group">
											{{Form::text('num_job', $num_job,['class' => 'form-control', 'placeholder' => 'Job','id' => 'num_job'])}}
										</div>
										<div class="form-group">
											{{Form::text('part_number', $part_number,['class' => 'form-control', 'placeholder' => 'No. de parte','id' => 'part_number'])}}
										</div>
										<div class="form-group">
											{{Form::text('inspector', $inspector,['class' => 'form-control', 'placeholder' => 'Inspector','id' => 'inspector'])}}
										</div>
										<div class="form-group">
											{{Form::text('fecha', $fecha,['class' => 'form-control', 'placeholder' => 'Fecha (YYYY-mm-dd)','id' => 'fecha'])}}
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-md btn-secondary ml-1{{-- input-group-text --}}" id="num_job">	<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</div>
									{{Form::close()}}
								</h1>
							</div>
						</div>
					</div>

					<div class="" style="margin-left: 10px; text-align: center; overflow-x: auto; overflow-y: auto;max-width: 100%;max-height: calc( 100% - 65px);height: calc( 100% - 65px);" >
						<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="3" style="height: 35px; width: 40px; border: hidden;">Información de la pieza</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Inicio liberación</th>
								<th style="min-width: 20px"></th>
								<th colspan="3" style="border: hidden;">Fin de liberación</th>
								<th style="min-width: 20px"></th>
								<th style="border: hidden;">Tiempo <br>total</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Responsables</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;"></th>
							</tr>
							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 80px;">Número <br>de job</th>
								<th style="background-color: #878EAF; min-width: 80px;">Número <br>de parte</th>
								<th style="background-color: #878EAF; min-width: 80px;">Operación</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Hora</th>
								<th style="background-color: #878EAF; min-width: 80px;">Fecha</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Hora</th>
								<th style="background-color: #878EAF; min-width: 80px;">Fecha</th>
								<th style="background-color: #878EAF; min-width: 80px;">Resultado</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF;min-width: 70px;">Horas/<br>minutos</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 280px;">Ajustador</th>
								<th style="background-color: #878EAF; min-width: 280px;">Inspector de Calidad</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 280px;">Comentarios</th>
							</tr>
							@foreach($process as $proces)
								@foreach($proces->rejected_processes as $proces1)
									<tr class="color-heavy" style=" font-size: 10px;">
										<td>{{$proces->job->num_job}}</td>
										<td>{{$proces->operation->piece->part_number}}</td>
										<td>{{$proces->operation->operation_service}}</td>
										<td style="background:  #E6E8F1;"></td>
										<td>{{$proces1->started_at->format('H:i').' hrs'}}</td>
										<td>{{$proces1->started_at->format('d-m-Y')}}</td>
										<td style="background:  #E6E8F1;"></td>
										<td>{{$proces1->ended_at->format('H:i').' hrs'}}</td>
										<td>{{$proces1->ended_at->format('d-m-Y')}}</td>
										<td style="background: red"><i class="fa fa-times" aria-hidden="true" style="color: white"></i></td>
										<td style="background:  #E6E8F1;"></td>
										@php
											$Total_horas=$proces1->started_at->diffInSeconds($proces1->ended_at);
											$horas= intval((($Total_horas)/60)/60);
											$min= intval(($Total_horas - (($horas*60)*60))/60); 
										@endphp
										<td>{{$horas}}:{{$min}} hrs</td>
										<td style="background:  #E6E8F1;"></td>
										@if(!is_null($proces->ajustador_fa))
											<td>{{$proces->ajustador_fa->idemployee .' '.$proces->ajustador_fa->name .' '.$proces->ajustador_fa->firstname}}</td>
										@else 
											<td>--</td>
										@endif
										@if(!is_null($proces->ajustador_il))
											<td>{{$proces->ajustador_il->idemployee .' '.$proces->ajustador_il->name .' '.$proces->ajustador_il->firstname}}</td>
										@elseif(!is_null($proces->ajustador_ial))
											<td>
												{{$proces->ajustador_ial->idemployee .' '.$proces->ajustador_ial->name .' '.$proces->ajustador_ial->firstname}}
											</td>
										@else
											<td>--</td>											
										@endif
										<td style="background:  #E6E8F1;"></td>
										@if(!is_null($proces->pauses))
											<td> {{$proces1->comment}}</td>
										@else
											<td>--</td>
										@endif
									</tr>
								@endforeach
								<tr class="color-heavy" style=" font-size: 10px;">
									<td>{{$proces->job->num_job}}</td>
									<td>{{$proces->operation->piece->part_number}}</td>
									<td>{{$proces->operation->operation_service}}</td>
									<td style="background:  #E6E8F1;"></td>
									@if(!is_null($proces->il_created_at))
										<td>{{$proces->il_created_at->format('H:i').' hrs'}}</td>
										<td>{{$proces->il_created_at->format('d-m-Y')}}</td>
									@elseif(!is_null($proces->ial_created_at))
										<td>{{$proces->ial_created_at->format('H:i').' hrs'}}</td>
										<td>{{$proces->ial_created_at->format('d-m-Y')}}</td>
									@else
										<td>--</td>
										<td>--</td>
									@endif
									<td style="background:  #E6E8F1;"></td>
									<td>{{$proces->ls_created_at->format('H:i').' hrs'}}</td>
									<td>{{$proces->ls_created_at->format('d-m-Y')}}</td>
									<td style="background: green"><i class="fa fa-check" aria-hidden="true" style="color: white"></i></td>
									<td style="background:  #E6E8F1;"></td>
									@php
										if (!is_null($proces->il_created_at)) {
											$Total_horas=$proces->il_created_at->diffInSeconds($proces->ls_created_at);
										}elseif(!is_null($proces->ial_created_at)){
											$Total_horas=$proces->ial_created_at->diffInSeconds($proces->ls_created_at);
										}else{
											$Total_horas=0;
										}
										$horas= intval((($Total_horas)/60)/60);
										$min= intval(($Total_horas - (($horas*60)*60))/60); 
									@endphp
									<td>{{$horas}}:{{$min}} hrs</td>
									<td style="background:  #E6E8F1;"></td>
									@if(!is_null($proces->ajustador_fa))
										<td>{{$proces->ajustador_fa->idemployee .' '.$proces->ajustador_fa->name .' '.$proces->ajustador_fa->firstname}}</td>
									@else <td>--</td>
									@endif
									@if(!is_null($proces->ajustador_il_id))
										<td>{{$proces->inspector}}</td>
									@elseif(!is_null($proces->ajustador_ial_id))
										<td>
											{{$proces->inspector_ail}}
										</td>
									@else
										<td></td>
									@endif
									<td style="background:  #E6E8F1;"></td>
									<td>--</td>
								</tr>
							@endforeach
						</table>
						{{ $process->links() }}
					</div>
				</div>
			</main>
		</div>
		<modal-mensaje></modal-mensaje>
	</div>
	@yield('modal')
	</div>

	<script>

		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	<script type="text/javascript">
		$('#btnExportar').click(function(event){
			event.preventDefault();
			envio();
		});

		function envio(){
			var num_job = $('#num_job').val();
			var part_number = $('#part_number').val();
			var inspector = $('#inspector').val();
			var fecha = $('#fecha').val();
			window.location.href = "{{route('export.calidad')}}?num_job="+num_job+"&part_number="+part_number+"&inspector="+inspector+"&fecha="+fecha;
		};
	</script>
	@yield('script')
</body>
</html>