<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	<link rel="stylesheet" href="{{ asset('js/vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" /> --}}
	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1; overflow-x: auto; max-width: 100%">
				<div class="container-fluid" style="">
					<form action="{{ route('historial.mantenimiento') }}" method="GET">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <input type="text" name="keyword_workcenter" value="{{ isset($keyword_workcenter) ? $keyword_workcenter : '' }}" class="form-control" placeholder="Centro de Trabajo" aria-label="Filtrar (Workcenter)" aria-describedby="Filtrado del historial de mantenimiento">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <input type="text" name="keyword_user" value="{{ isset($keyword_user) ? $keyword_user : '' }}" class="form-control" placeholder="Operador" aria-label="Filtrar (Usuario de Mantenimiento)" aria-describedby="Filtrado del historial de Mantenimiento">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                                    <input type="text" value="{{ isset($day) ? $day : '' }}" class="form-control datetimepicker-input" data-target="#datetimepicker3" name="day" placeholder="Dia"/>
                                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <h5>Rango de fecha (Opcional)</h5>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" value="{{ isset($from) ? $from : '' }}" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="from" placeholder="Desde"/>
                                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                    <input type="text" value="{{ isset($to) ? $to : '' }}" class="form-control datetimepicker-input" placeholder="Hasta" name="to" data-target="#datetimepicker2"/>
                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary mt-2">Filtrar</button>
                    </form>
					<div class="color-heavy" style="text-align: right;">
						<a href={{route("export.mantenimiento")}}><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a>
					</div>
					<div class="" style="margin-left: 10px; text-align: center;" >
						<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="2" style="height: 35px; width: 40px; border: hidden;">Información de <br>la máquina</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Inicio de <br>mantenimiento</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Fin de <br>mantenimiento</th>
								<th style="min-width: 20px"></th>
								<th style="border: hidden;">Tiempo <br>total</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Responsables</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;"></th>
							</tr>
							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 50px;">Número <br>de CT</th>
								<th style="background-color: #878EAF; min-width: 100px;">Modelo</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Hora</th>
								<th style="background-color: #878EAF; min-width: 80px;">Fecha</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Hora</th>
								<th style="background-color: #878EAF; min-width: 80px;">Fecha</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF;min-width: 70px;">Horas/<br>minutos</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 280px;">Operador</th>
								<th style="background-color: #878EAF; min-width: 280px;">Mantenimiento</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 280px;">Comentarios</th>
							</tr>
							@foreach($pauses as $pause)
							<tr class="color-heavy" style=" font-size: 10px;">
								<td>{{$pause->num_machine}}</td>
								
								<td>{{$pause->model}}</td>
							
								<td style="background:  #E6E8F1;"></td>
								<td>{{$pause->started_at->format('H:i').' hrs'}}</td>
								<td>{{$pause->started_at->format('d-m-Y')}}</td>
								<td style="background:  #E6E8F1;"></td>
								<td>{{$pause->ended_at->format('H:i').' hrs'}}</td>
							
								<td>{{$pause->ended_at->format('d-m-Y')}}</td>
								
								<td style="background:  #E6E8F1;"></td>
								@php
									$Total_horas=$pause->started_at->diffInSeconds($pause->ended_at);
									$horas= intval((($Total_horas)/60)/60);
									$min= intval(($Total_horas - (($horas*60)*60))/60); 
								@endphp
								<td>{{$horas}}:{{$min}} hrs</td>
								<td style="background:  #E6E8F1;"></td>
								
								@if(!is_null($pause->operador_id))
									<td>{{$pause->operador->idemployee .' '.$pause->operador->name .' '.$pause->operador->firstname}} </td>
								@elseif(!is_null($pause->process->user_processes->first()->user))
									<td>{{$pause->process->user_processes->first()->user->idemployee .' '.$pause->process->user_processes->first()->user->name .' '.$pause->process->user_processes->first()->user->firstname}}</td>
								@else <td>--</td>
								@endif
								
								<td> {{ $pause->user_mantenimiento }}</td>
								
								<td style="background:  #E6E8F1;"></td>

								<td> {{$pause->comments}}</td>
								
							</tr>
							@endforeach
						</table>
					    {{$pauses->links()}}
					</div>
				</div>
			</main>
		</div>
		<modal-mensaje></modal-mensaje>
	</div>
	@yield('modal')
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />


    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker();
            $('#datetimepicker2').datetimepicker();
            $('#datetimepicker3').datetimepicker({format: 'MM/DD/YYYY'});
        });
    </script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
</body>
</html>