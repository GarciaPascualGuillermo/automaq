<div id="sidebar_open" class="sidenav active" style="width: 200px">
	<div class="logo" style="height: 55px">
		<img src="/assets/operador/logo_tecmaq.png" style="width: 100%; height: 100%;">
	</div>
	<div class="sidenav_content">
		<div class="container-fluid sidenav_usuario">
			<div class="row">
				<div class="col">
					<div class="img-usr">
						<img src="/assets/operador/empty_profile.png" class="rounded-circle" style="padding: 0%;">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col text-center font-regular color-heavy">
					{{ auth()->user()->name }}
				</div>
			</div>
			<div class="row">
				<div class="col font-light color-gray">
					No. Usuario: <span class="font-light color-heavy">{{ auth()->user()->idemployee }}</span>
				</div>
			</div>
		</div>

		<div class="sidenav_menu pt-3">
			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas']))
				<div class="px-4 my-2 color-heavy font-regular " href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" style="border-bottom: 3px" >

					<a href="{{route('maquinas.index')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em; hover: none"><img src="/assets/jobs/maquinas.png" style="width: 25px; height: 25px"> Máquinas </a>

					{{-- <img src="/assets/jobs/maquinas.png" style="width: 25px; height: 25px;"> Máquinas --}}
					<i class="fa fa-angle-down" aria-hidden="true"></i>
						<ul class="collapse list-unstyled" id="homeSubmenu">
							@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas']))
								<li>
									<a href="{{route('historial_semana.maquinas')}}" class="px-4 my-2 font-regular" style="color: blue">Historial por semana</a>
								</li>
							@endif
							@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas']))
								<li>
									<a href="{{route('historial_dia.maquinas')}}" class="px-4 my-2 font-regular" style="color: blue">Historial por dia</a>
								</li>
							@endif
						</ul>
				</div>
			@endif
			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas','planeacion','cuenta','sup.linea','ajustador','jefe.mantenimiento']))
				<div class="px-4 my-2 color-heavy font-regular" href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false">
					<a href="{{route('jobs.index')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em; hover: none"><img src="/assets/jobs/jobs.png" style="width: 25px; height: 25px"> Jobs </a>

					<i class="fa fa-angle-down" aria-hidden="true"></i>

						<ul class="collapse list-unstyled " id="homeSubmenu1">
							@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas','planeacion','cuenta']))
							<li>
								<a href="{{route('historial.jobs')}}" class="px-4 my-2 font-regular" style="color: blue">Historial</a>
							</li>
							@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sistemas']))
							<li>
								<a href="{{route('historial.jobs.eliminados')}}" class="px-4 my-2 font-regular" style="color: blue">Historial Jobs Eliminados</a>
							</li>
							@endif
							@endif
						</ul>
				</div>
			@endif
			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas']))
				<div class="px-4 my-2 color-heavy font-regular">
						<a href="{{route('supervisores.index')}}?section=" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em; hover: none"><img src="/assets/jobs/supervisores.png" style="width: 25px; height: 25px"> Supervisores </a>
				</div>
			@endif
			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion','gte.admin','sup.produccion','sup.linea','sistemas','gte.calidad']))

				<div class="px-4 my-2 color-heavy font-regular " href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" >
					<a href="{{route('calidad.liberacion')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em; hover: none"><img src="/assets/jobs/calidad.png" style="width: 25px; height: 25px"> Calidad </a>

					<i class="fa fa-angle-down" aria-hidden="true"></i>
						<ul class="collapse list-unstyled " id="homeSubmenu3">
							@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','gte.admin','jefe.produccion','sup.produccion','sistemas','gte.calidad']))
								<li>
									<a href="{{ route('reporteador', ['admin.calidad.index']) }}" class="px-4 my-2 font-regular" style="color: blue">Historial</a>
								</li>
							@endif
							{{-- @if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion','sup.produccion','sup.linea','gte.calidad']))
								<li>
									<a href="{{route('calidad.liberacion')}}" class="px-4 my-2 font-regular" style="color: blue">Liberar</a>
								</li>
							@endif --}}
						</ul>
				</div>
			@endif
			@if(auth()->user()->hasRoles(['root','ceo','gte.admin','gte.operacion','sup.produccion','jefe.produccion','sistemas','jefe.mantenimiento','tec.mantenimiento']))
				<div class="px-4 my-2 color-heavy font-regular " href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" >
					<a href="{{route('mantenimiento.index')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em; hover: none"><img src="/assets/jobs/mantenimiento.png" style="width: 25px; height: 25px"> Mantenimiento </a><i class="fa fa-angle-down" aria-hidden="true"></i>

						<ul class="collapse list-unstyled " id="homeSubmenu4">
							@if(auth()->user()->hasRoles(['root','ceo','gte.admin','gte.operacion','sup.produccion','jefe.produccion','sistemas','jefe.mantenimiento','tec.mantenimiento']))
								<li>
									<a href="{{ route('reporteador', ['admin.mantenimiento.index']) }}" class="px-4 my-2 font-regular" style="color: blue">Historial</a>
								</li>
							@endif
							{{-- @if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','jefe.mantenimiento','tec.mantenimiento']))
								<li>
									<a href="{{route('mantenimiento.index')}}" class="px-4 my-2 font-regular" style="color: blue">En espera</a>
								</li>
							@endif --}}
						</ul>
				</div>
			@endif
			@if(auth()->user()->hasRoles(['root','ceo','gte.admin','gte.operacion','jefe.produccion','sistemas']))
				<div class="px-4 my-2 color-heavy font-regular">
					<a href="{{route('indicadores.index')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em"><img src="/assets/jobs/indicadores.png" style="width: 25px; height: 25px"> Indicadores </a>
				</div>
			@endif

		</div>

		<div class="sidenav_copyrigth text-center font-light">
			<div class=" px-4 my-2  color-heavy font-regular">
				<a class="dropdown-item" href="{{ route('logout') }}" style="padding: 0;"
				   onclick="event.preventDefault();
				                 document.getElementById('logout-form').submit();">

				    <img src="/assets/jobs/log out.png" style="width: 20%; margin-left: -50px ">
				</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				    @csrf
				</form>

				<p style="margin-left: 40px; margin-top: -25px">Log out</p>
			</div>
			Desarrollado por
			<img src="{{ asset('HTDEVS50px.png') }}" style="height: 29px; width: 38px;"/>
		</div>
	</div>
</div>



 <div id="sidebar_close" class="sidenav sidebar_small">
 	<div class="logo">
 		<img src="/assets/jobs/logo_sidebar_cerrado.png" style="width: 100%; height: 100%;">
 	</div>
 	<div class="sidenav_content">
 		<div class="container-fluid sidenav_usuario border-bottom">
 			<div class="row">
 				<div class="col without-padding">
 					<div class="img-usr-close without-padding">
 						<img src="/assets/operador/empty_profile.png" class="rounded-circle without-padding">
 					</div>
 				</div>
 			</div>
 			<div class="row">
 				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 font-light color-gray text-center">
 					No. Usuario: <span class="font-light color-heavy">{{ auth()->user()->idemployee }}</span>
 				</div>
 			</div>
 		</div>

 		<div class="pt-3" style="padding-top: 0rem !important">
 			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas']))
	 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom" href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
	 				<a href="{{route('maquinas.index')}}" style="padding: 0; text-decoration: none" aria-expanded="false" aria-haspopup="true" ><img src="/assets/jobs/maquinas.png" class="img-close">
	 			</div></a>
 			@endif
 			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas','planeacion','cuenta','sup.linea','ajustador','jefe.mantenimiento']))
	 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom" href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false">
	 				<a href="{{route('jobs.index')}}" style="padding: 0"><img src="/assets/jobs/jobs.png"class="img-close">
 				</div></a>
 			@endif
 			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','sup.produccion','jefe.produccion','sistemas']))
	 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom">
	 				<a href="{{route('supervisores.index')}}?section=" style="padding: 0"><img src="/assets/jobs/supervisores.png"class="img-close">
	 			</div></a>
 			@endif
 			@if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion','gte.admin','sup.produccion','sup.linea','sistemas','gte.calidad']))
	 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom " href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" >
	 				<a href="{{route('historial.calidad')}}" style="padding: 0"><img src="/assets/jobs/calidad.png"class="img-close">
	 			</div></a>
 			@endif
 			@if(auth()->user()->hasRoles(['root','ceo','gte.admin','gte.operacion','sup.produccion','jefe.produccion','sistemas','jefe.mantenimiento','tec.mantenimiento']))
	 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom " href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" >
	 				<a href="{{route('historial.mantenimiento')}}" style="padding: 0"><img src="/assets/jobs/mantenimiento.png"class="img-close">
	 			</div></a>
 			@endif
 			@if(auth()->user()->hasRoles(['root','ceo','gte.admin','gte.operacion','jefe.produccion','sistemas']))
	 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom">
	 				<a href="{{route('indicadores.index')}}" style="padding: 0"><img src="/assets/jobs/indicadores.png"class="img-close">
	 			</div></a>
 			@endif
 		</div>
 		<div class="sidenav_copyrigth text-center font-light">
			<div class=" px-4 my-2  color-heavy font-regular">
					<a class="dropdown-item" href="{{ route('logout') }}" style="padding: 0;"
					   onclick="event.preventDefault();
					                 document.getElementById('logout-form').submit();">

					    <img src="/assets/jobs/log out.png" class="img-close">
					</a>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					    @csrf
					</form>

			</div>
 			Desarrollado por
 			<img src="{{ asset('HTDEVS50px.png') }}" style="height: 29px; width: 38px;"/>
 		</div>
 	</div>
 </div>
