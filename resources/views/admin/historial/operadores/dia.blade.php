<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	
	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1;overflow-x: auto; max-width: 100%">
				<div class="container-fluid">
					<div class="font-regular" style="margin-left: 10px; margin-top: 20px;">
						Productividad Operadores / Historial por día
						
					</div>
					<div class="row">
						<div class="col-6">
							<div class="page-header">
								<h1>
									{{Form::open(['route' => 'historial_semana.operadores','method' =>'GET','class' => 'form-inline','action' => 'ExportController@operadores' ])}}
										<div class="form-group">
											{{Form::text('name', $name, ['class' => 'form-control', 'placeholder' => 'Nombre','id' => 'name'])}}
										</div>
										<div class="form-group">
											<button type="submit" class="input-group-text" id="name2"><i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</div>
									{{Form::close()}}
								</h1>
							</div>
						</div>
						<div class="color-heavy col-6" style="text-align: right;">
							<button id="exportar" type="button" class="btn btn-link" ><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></button>
							{{-- <a href="" id="exportar"><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a> --}}
						</div>
					</div>
					{{ $users->appends(request()->input())->links() }}
					<table border="1" style="margin-left: 20px; margin-top: 30px; text-align: center; background-color: white">
						<tr style="background-color: #878EAF; font-size: 10px; color: white">
							
							<th style="background-color: #E6E8F1; border: hidden; "></th>
							@foreach($users_headers as $user)
							<th style="background:  #E6E8F1; border: hidden;"></th>
							<th style="min-width: 70px">{{ $user }}</th>
							@endforeach

						</tr>
						<tr style="background-color: #E7EBFA">
							<td style="min-width: 150px;">Promedio</td>
							@foreach ($users_headers as $user)
								<td></td>
								<td>{{ intval($users_content[$user]->avg()) }} %</td>
							@endforeach
						</tr>
						@php $i=0; @endphp
						@while($limit->lessThanOrEqualTo($week_start))
							<tr>
								<td>{{$week_start->format('d-').mes_esp2($week_start->format('m')) }} - {{$week_start->endOfDay()->subDay()->format('d-').mes_esp2($week_start->endOfDay()->subDay()->format('m'))}}</td>
								@foreach($users_headers as $user)
									{{-- {{dd($users_content[$user])}} --}}
									<td></td>
									<td> {{ $users_content[$user][$i] }} % </td>
								@endforeach
							</tr>
							@php
								$week_start->startOfDay()->subDay();
								$i++;
							@endphp
						@endwhile
						
					</table>
				</div>
			</main>
		</div>
		@yield('modal')
		<modal-mensaje></modal-mensaje>
	</div>

	<script>
		
		

		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
		
		
		
			
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	<script type="text/javascript">

		$('#exportar').click(function(event){
			event.preventDefault();
			envio();
		});

		function envio(){
			var name = $('#name').val();
			console.log(name);
			window.location.href = "{{route('export.operadres')}}?name="+name;

			console.log("HOLA123");
		};
	</script>
	@yield('script')
</body>
</html>