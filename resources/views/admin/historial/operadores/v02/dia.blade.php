<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> --}}

	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1;overflow-x: auto; max-width: 100%">
                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">

                            <div class="row" style="height: 25px;">
                                <div class="col-9 col-ms-9 col-md-9 col-lg-9 col-xl-9">
                                    Productividad Operadores / Historial por día <span>(Ultimos 6 meses)</span>
                                </div>
                            </div>

                            <table class="table datatables text-center">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Nombre</th>
                                        <th>Productvidad</th>
                                        <th>Job</th>
                                        <th>Piezas</th>
                                        <th>Scraps</th>
                                        <th>Total Sesión</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user_processes_by_date as $user_process)
                                    <tr>
                                        <td class="table-dark">
                                            {{ Carbon\Carbon::parse($user_process->created_at)->format('d-M-Y') }}
                                        </td>
                                        <td>{{ $user_process->user->name }} {{ $user_process->user->firstname }}</td>
                                        <td>{{ $user_process->productivity }} %</td>
                                        <td>{{ $user_process->process->job->num_job }}</td>
                                        <td>{{ $user_process->conformed_pieces }}</td>
                                        <td>{{ $user_process->scraps }}</td>
                                        <td>{{ $user_process->session_time }}</td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>

				</div>
			</main>
		</div>
		@yield('modal')
		<modal-mensaje></modal-mensaje>
    </div>


	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js') }}"></script>

	<script>

        $('.datatables').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ],
            ordering: false
        });

		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};



	</script>
</body>
</html>
