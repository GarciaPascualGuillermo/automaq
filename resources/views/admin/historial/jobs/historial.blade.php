<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">


	@yield('style')
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1; overflow-x: auto; max-width: 100%">
				<div class="container-fluid" style="">
                    <form action="{{ route('historial.jobs') }}" method="GET">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <input type="text" name="keyword_job_num" value="{{ isset($keyword_job_num) ? $keyword_job_num : '' }}" class="form-control" placeholder="Numero de job" aria-label="Filtrar (Numero de job)" aria-describedby="Filtrado del historial de Job">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <input type="text" name="keyword_job_part" value="{{ isset($keyword_job_part) ? $keyword_job_part : '' }}" class="form-control" placeholder="Numero de parte" aria-label="Filtrar (Numero de parte)" aria-describedby="Filtrado del historial de Job">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <h5>Rango de fecha (Opcional)</h5>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" value="{{ isset($from) ? $from : '' }}" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="from" placeholder="Desde"/>
                                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                    <input type="text" value="{{ isset($to) ? $to : '' }}" class="form-control datetimepicker-input" placeholder="Hasta" name="to" data-target="#datetimepicker2"/>
                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary mt-2">Filtrar</button>
                    </form>

					<div class="color-heavy" style="text-align: right;">
						<a href={{route("export.jobs")}}><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a>
                    </div>

					<div class="" style="margin-left: 10px; text-align: center; " >
						<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="9" style="height: 35px; width: 40px; border: hidden;">Información del job</th>
								<th style="min-width: 20px"></th>
								<th colspan="5" style="border: hidden;">Duración de la operación</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Herramentaje</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Ajuste</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Liberación / auto-liberación</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Producción</th>
								<th style="min-width: 20px"></th>
								<th colspan="2" style="border: hidden;">Pausa por mantenimiento</th>
							</tr>


							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de job</th>
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de parte</th>
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de operación</th>
								<th style="background-color: #878EAF; min-width: 100px;">Nombre <br>de la operación</th>
								<th style="background-color: #878EAF; min-width: 100px;">Cantidad <br>de piezas</th>
								<th style="background-color: #878EAF; min-width: 100px;">Total <br>de piezas</th>
								<th style="background-color: #878EAF; min-width: 100px;">Promedio <br>de ciclo</th>
								<th style="background-color: #878EAF; min-width: 200px;">Cliente</th>
								<th style="background-color: #878EAF; min-width: 100px;">Fecha de<br>entrega</th>
								<!-- ----- -->
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Tiempo total</th>
								<th style="background-color: #878EAF; min-width: 80px;">Hora de inicio</th>
								<th style="background-color: #878EAF; min-width: 80px;">Fecha de inicio</th>
								<th style="background-color: #878EAF; min-width: 80px;">Hora término</th>
								<th style="background-color: #878EAF; min-width: 80px;">Fecha término</th>
								<!-- ----- -->
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Duración</th>
								<th style="background-color: #878EAF; min-width: 180px;">Responsable</th>
								<!-- ----- -->
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Duración</th>
								<th style="background-color: #878EAF; min-width: 180px;">Responsable</th>
								<!-- ----- -->
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Duración</th>
								<th style="background-color: #878EAF; min-width: 180px;">Responsable</th>
								<!-- ----- -->
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Duración</th>
								<th style="background-color: #878EAF; min-width: 180px;">Responsable</th>
								<!-- ----- -->
								<th style="background:  #E6E8F1;"></th>
								<th style="background-color: #878EAF; min-width: 70px;">Duración</th>
								<th style="background-color: #878EAF; min-width: 180px;">Responsable</th>
							</tr>
							@foreach($process->where('ih_created_at','!=',null) as $proces)
								{{-- {{ dd($proces->job) }} --}}
								<tr class="color-heavy" style=" font-size: 10px;" style="{{ !is_null($proces->delete_at) ? 'background-color: red;':'' }}">
									{{-- INFORMACION DEL JOB --}}
									<td>{{$proces->num_job}}</td>
									<td>{{$proces->operation->piece->part_number}}</td>
									<td>{{$proces->operation->operation_service}}</td>
									<td>{!!$proces->operation->description!!}</td>
									<td>{{$proces->piece}} </td>
									<td>{{$proces->job->quantity}}</td>
									<td>{{$proces->time_cycle}}</td>
									<td>{{$proces->operation->piece->client->name}}</td>
									<td>{{$proces->delivered_at->format('d-m-Y')}}</td>
									<td style="background:  #E6E8F1;"></td>

									{{-- DURACION DE LA OPERACION --}}
									@php
										$Total_horas=$proces->ih_created_at->diffInSeconds($proces->cr_created_at);
										$horas= intval((($Total_horas)/60)/60);
										$min= intval(($Total_horas - (($horas*60)*60))/60);
									@endphp
									<td>{{$horas}}:{{$min}} hrs</td>
									<td>{{$proces->ih_created_at->format('H:i').' hrs'}}</td>
									<td>{{$proces->ih_created_at->format('d-m-Y')}}</td>
									@if (!is_null($proces->cr_created_at))
										<td>{{$proces->cr_created_at->format('H:i').' hrs'}}</td>
										<td>{{$proces->cr_created_at->format('d-m-Y')}}</td>
									@else
										<td></td>
									@endif


									<td style="background:  #E6E8F1;"></td>

									{{-- HERRAMENTAJE --}}
									@php
										$Total_horasH=$proces->ih_created_at->diffInSeconds($proces->fh_created_at);
										$horasH= intval((($Total_horasH)/60)/60);
										$minH= intval(($Total_horasH - (($horasH*60)*60))/60);
									@endphp
									<td>{{$horasH}}:{{$minH}} hrs</td>
									@if(!is_null($proces->user_processes->first()) && !is_null($proces->user_processes->first()->user))
										<td>{{$proces->user_processes->first()->user->idemployee .' '.$proces->user_processes->first()->user->name .' '.$proces->user_processes->first()->user->firstname}}</td>
									@else
										<td>--</td>
									@endif
									<td style="background:  #E6E8F1;"></td>

									{{-- AJUSTE --}}
									@php
										if (!is_null($proces->ia_created_at) && !is_null($proces->fa_created_at)) {
											$Total_horasA=$proces->ia_created_at->diffInSeconds($proces->fa_created_at);
											$horasA= intval((($Total_horasA)/60)/60);
											$minA= intval(($Total_horasA - (($horasA*60)*60))/60);
										}
										else
											$horasA=0;
											$minA=0;
									@endphp
									<td>{{$horasA}}:{{$minA}} hrs</td>
									@if(!is_null($proces->ajustador_ia))
										<td>{{$proces->ajustador_ia->idemployee .' '.$proces->ajustador_ia->name .' '.$proces->ajustador_ia->firstname}}</td>
									@else
										<td>--</td>
									@endif
									<td style="background:  #E6E8F1;"></td>

									{{-- LIBERACION/AUTOLIBERAION --}}
									@php
										if (!is_null($proces->ia_created_at) && !is_null($proces->ls_created_at)) {
											$Total_horasL=$proces->ia_created_at->diffInSeconds($proces->ls_created_at);
											$horasL= intval((($Total_horasL)/60)/60);
											$minL= intval(($Total_horasL - (($horasL*60)*60))/60);
										}
										elseif (!is_null($proces->ial_created_at) && !is_null($proces->ls_created_at)) {
											$Total_horasL=$proces->ial_created_at->diffInSeconds($proces->ls_created_at);
											$horasL= intval((($Total_horasL)/60)/60);
											$minL= intval(($Total_horasL - (($horasL*60)*60))/60);
										}else{
											$horasL=0;
											$minL=0;
										}
									@endphp
									<td>{{$horasL}}:{{$minL}} hrs</td>

									@if(!is_null($proces->ajustador_il))
										<td> {{$proces->ajustador_il->idemployee .' '.$proces->ajustador_il->name .' '.$proces->ajustador_il->firstname}} </td>
									@elseif (!is_null($proces->ajustador_ial))
										<td>{{$proces->ajustador_ial->idemployee .' '.$proces->ajustador_ial->name .' '.$proces->ajustador_ial->firstname}} </td>
									@else
										{{-- {{ ($proces) }} --}}
										<td>--</td>
									@endif

									{{-- PRODUCCION --}}
									@php
										if (!is_null($proces->pr_created_at) && !is_null($proces->cr_created_at)) {
											$Total_horasP=$proces->pr_created_at->diffInSeconds($proces->cr_created_at);
											$horasP= intval((($Total_horasP)/60)/60);
											$minP= intval(($Total_horasP - (($horasP*60)*60))/60);
										}
										else{
											$horasP=0;
											$minP=0;
										}
									@endphp
									<td style="background:  #E6E8F1;"></td>
									<td>{{$horasP}}:{{$minP}} hrs</td>
									@if(!is_null($proces->user_processes->first()) && !is_null($proces->user_processes->first()->user))
										<td>{{$proces->user_processes->first()->user->idemployee .' '.$proces->user_processes->first()->user->name .' '.$proces->user_processes->first()->user->firstname}}</td>
									@else
										<td>--</td>
									@endif



									{{-- PAUSA POR MANTENIMIENTO  --}}
									@php
										if (!is_null($proces->pauses->first()) && !is_null($proces->pauses->first())) {
											$Total_horasM=$proces->pauses->first()->started_at->diffInSeconds($proces->pauses->first()->ended_at);
											$horasM= intval((($Total_horasM)/60)/60);
											$minM= intval(($Total_horasM - (($horasM*60)*60))/60);
										}
										else{
											$horasM=0;
											$minM=0;
										}
									@endphp

									<td style="background:  #E6E8F1;"></td>
									<td>{{$horasM}}:{{$minM}} hrs</td>
									@if(!is_null($proces->ajustador_pm))
										<td> {{$proces->ajustador_pm->idemployee .' '.$proces->ajustador_pm->name .' '.$proces->ajustador_pm->firstname}} </td>
									@else
										<td>--</td>
									@endif
									<td></td>
								</tr>
							@endforeach
						</table>
						{{ $process->links() }}
					</div>
				</div>
			</main>
		</div>
		<modal-mensaje></modal-mensaje>
	</div>
	@yield('modal')
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
    <script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />


    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker();
            $('#datetimepicker2').datetimepicker();
        });
    </script>


	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
</body>
</html>
