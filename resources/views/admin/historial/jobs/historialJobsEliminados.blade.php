<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	@yield('style')
	<style type="text/css">
		.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link{
		    border: 1px solid transparent;
		    border-top-left-radius: 0.25rem;
		    border-top-right-radius: 0.25rem;
		    background-color: transparent;
		    text-decoration: underline;
		}
		ul{
			width: 100%;
		}
	</style>
</head>
<body>
	<div id="app">
		@include('admin.historial.layouts.partials.sidebar_active')
		<div id="main-content" class="content-nav close-nav">
			@include('admin.layouts.sideNavDesplegable.navbar')
			@yield('sidebar')
			<main class="py-4" style="background:  #E6E8F1; overflow-x: auto; max-width: 100%">
				<div class="row" style="border-bottom: 1px solid rgb(137, 137, 137); margin-left: 5px; margin-top: -20px; margin-right: 5px;">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item col btn btn-link font-light text-primary ">
					    	<a class="nav-link active col btn btn-link font-light text-primary " id="home-tab" data-toggle="tab" href="#eliminados" role="tab" aria-controls="home" aria-selected="true" style="color: rgb(110, 110, 110);">Jobs Eliminados</a>
					  	</li>
					  	<li class="nav-item col btn btn-link font-light text-primary ">
						    <a class="nav-link col btn btn-link font-light text-primary " id="profile-tab" data-toggle="tab" href="#terminados" role="tab" aria-controls="profile" aria-selected="false" style="color: rgb(110, 110, 110);">Jobs Terminados</a>
						</li>
					</ul>
				</div>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane active fade show" id="eliminados" role="tabpanel" aria-labelledby="home-tab">
					    <table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="9" style="height: 35px; width: 40px; border: hidden;">Información del job Eliminado</th>
								<th style="min-width: 20px"></th>
							</tr>
							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de job</th>
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de parte</th>
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de operación</th>
								<th style="background-color: #878EAF; min-width: 100px;">Nombre <br>de la operación</th>
								<th style="background-color: #878EAF; min-width: 100px;">Total <br>de piezas</th>
								<th style="background-color: #878EAF; min-width: 100px;">Nombre <br>del cliente</th>
								<th style="background-color: #878EAF; min-width: 100px;">Fecha <br>de eliminación</th>
								<th style="background-color: #878EAF; min-width: 200px;">Restaurar</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background:  #E6E8F1;"></th>
							</tr>
							@foreach($process as $key => $proces)
								{{-- {{ dd($proces->job) }} --}}
								<tr class="color-heavy" style=" font-size: 10px;" style="{{ !is_null($proces->delete_at) ? 'background-color: red;':'' }}">
									{{-- INFORMACION DEL JOB ELIMINADO--}}
									<td hidden="hidden">{{$proces->id}}</td>
									<td>{{$proces->num_job}}</td>
									<td>{{$operations[$key]->piece->part_number}}</td>
									<td>{{$proces->operation->operation_service}}</td>
									<td>{!!$proces->operation->description!!}</td>
									<td>{{$proces->job->quantity}}</td>
									<td>{{$proces->operation->piece->client->name}}</td>
									<td>{{$proces->deleted_at->format('Y-m-d')}}</td>
									<td><button class="restaurar" type="button">Restaurar</button></td>
									<td style="background:  #E6E8F1;"></td>
									<td style="background:  #E6E8F1;"></td>
							    </tr>
							@endforeach
						</table>
						{{$process->links()}}
					</div>
					<div class="tab-pane fade" id="terminados" role="tabpanel" aria-labelledby="profile-tab">
						<table border="1" style="margin-left: 10px; margin-top: 15px; text-align: center; background-color: white; ">
							<tr class="font-regular" style="background:  #E6E8F1; border: hidden;">
								<th colspan="9" style="height: 35px; width: 40px; border: hidden;">Información del job Terminado</th>
								<th style="min-width: 20px"></th>
							</tr>
							<tr style=" font-size: 10px; color: white; height: 35px;">
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de job</th>
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de parte</th>
								<th style="background-color: #878EAF; min-width: 100px;">Número <br>de operación</th>
								<th style="background-color: #878EAF; min-width: 100px;">Nombre <br>de la operación</th>
								<th style="background-color: #878EAF; min-width: 100px;">Total <br>de piezas</th>
								<th style="background-color: #878EAF; min-width: 100px;">Nombre <br>del cliente</th>
								<th style="background-color: #878EAF; min-width: 100px;">Fecha <br>de eliminación</th>
								<th style="background-color: #878EAF; min-width: 200px;">Restaurar</th>
								<th style="background:  #E6E8F1;"></th>
								<th style="background:  #E6E8F1;"></th>
							</tr>
							@foreach($process_fin as $key => $process)
								{{-- {{ dd($proces->job) }} --}}
								<tr class="color-heavy" style=" font-size: 10px;" style="{{ !is_null($process->created_at) ? 'background-color: red;':'' }}">
									{{-- INFORMACION DEL JOB TERMINADO--}}
									<td hidden="hidden">{{$process->id}}</td>
									<td>{{$process->job->num_job}}</td>
									<td>{{$process->operation->piece->part_number}}</td>
									<td>{{$process->operation->operation_service}}</td>
									<td>{!!$process->operation->description!!}</td>
									<td>{{$process->quantity}}</td>
									<td>{{$process->operation->piece->client->name}}</td>
                                    <td>{{ $process->cr_created_at->format('Y-m-d') }}</td>
                                    <td>
                                        <form action="{{ route('historial.jobs.restaurados_t') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $process->id }}">
                                            {{ method_field('PUT') }}
                                            <button type="submit" class="restaurar_t">Restaurar</button>
                                        </form>
                                    </td>
									<td style="background:  #E6E8F1;"></td>
								    <td style="background:  #E6E8F1;"></td>
							    </tr>
							@endforeach
						</table>
						{{$process_fin->links()}}
					</div>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="Actualizar" tabindex="-1" role="dialog" aria-labelledby="ActualizarTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content" style="align-items: center;">
							<div class="modal-header">
								<h5 class="modal-title" id="ActualizarTitle">RESTAURANDO</i>
								</h5>
							</div>
							<div class="modal-body">
								<div class="row col-12">
									<img src="/img/loading_v2.gif" style="height: 235px;width: 300px;">
								</div>

							</div>
						</div>
					</div>
				</div>
			</main>
		</div>
		<modal-mensaje></modal-mensaje>
	</div>
	@yield('modal')
	</div>

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'jobs'=>[
				'workcenters'=>[],
				'cardSelect'=>[],
				'workcenterSelect'=>[],
				'workcenterSelect_proccess_key'=>null,
			],
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/jobs/app.js')."?v=".str_random(3) }}"></script>

	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	<script>
		/*$(".page-link").click(function(){
			var id = this.parentNode.parentNode.parentNode.getAttribute("id");
			if (id == 'terminados') {
				var div_eliminados = this.parentNode.parentNode.parentNode.parentNode.firstChild.getAttribute("id");
				var div_terminados = this.parentNode.parentNode.parentNode.parentNode.firstChild.parentNode.lastChild.getAttribute("id");
				var li_eliminados = this.parentNode.parentNode.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.firstChild.getAttribute("id");
				var li_terminados = this.parentNode.parentNode.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.firstChild.parentNode.lastChild.getAttribute("id");
				alert(div_eliminados);
				alert(li_eliminados);
				alert(li_terminados);
				alert(div_terminados);
				setTimeout(function(){
				    $(document).ready(function(){
				        div_eliminados.removeClass('active');
					    li_eliminados.removeClass('active');
					    div_terminados.addClass('active');
					    li_terminados.addClass('active');
					});
				},0);
			}
		});*/
	    $(".restaurar").click(function(){
	    	$("#Actualizar").modal('show');
	    	var id = this.parentNode.parentNode.firstChild.innerHTML;
	    	axios.post("restaurados",{'id':id})
	    	.then(
				(response) => {
				    $("#Actualizar").modal('hide');
					setTimeout(function(){
						location.reload();
					},2000);
				}
			).catch(error => {
				console.log(error,error.response,error.response.data.message);
			});
	    });
	    $(".restaurar_t").click(function(){
	        $("#Actualizar").modal('show');
	    	/*var id = this.parentNode.parentNode.firstChild.innerHTML;
	    	axios.post("restaurados_t",{'id':id})
	    	.then(
				(response) => {
				    $("#Actualizar").modal('hide');
					setTimeout(function(){
						location.reload();
					},2000);
				}
			).catch(error => {
				console.log(error,error.response,error.response.data.message);
			});*/
	    });
	</script>
	@yield('script')
</body>
</html>
