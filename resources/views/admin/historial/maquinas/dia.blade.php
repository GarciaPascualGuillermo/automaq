@extends('admin.historial.layouts.appdia')

@section('content')
<div class="container-fluid" style="">
	<div class="" style="margin-left: 10px; margin-top: 20px; ">
		<b class="font-light text-left fs-18" >Uso de máquinas / Historial por día
		<div class="font-light color-heavy text-left" style="font-size: 12px;">INFORMACIÓN PRESENTADA EN HORAS
			<div class="color-heavy" style="text-align: right;">
				<a href={{route("export.maquinas_dias")}}><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a>
			</div>
		</div>
		</b>				
	
		<div class="row">
			<table border="1" style="margin-left: 20px; margin-top: 30px; text-align: center; background-color: white">
				<tr style="background-color: #878EAF; font-size: 15px; color: white">
					@foreach($workcenters as $workcenter)
					<th style="background:  #E6E8F1;border: hidden;"></th>
					<th colspan="3">{{ $workcenter->num_machine }}</th>
					@endforeach	
				</tr>
				<tr style="background-color: #878EAF; font-size: 15px; color: white">
					@for ($i = 0; $i < $workcenters->count(); $i++)
					<td style="background:  #E6E8F1; border: hidden;"></td>
					<td style="min-width: 50px">Setup</td>
					<td style="min-width: 50px">Prod</td>
					<td style="min-width: 50px">Sin U</td>
					@endfor
				</tr>
				<tr style="background-color: #E7EBFA">
					<td style="min-width: 110px;">Promedio</td>
					@foreach($workcenters as $wc)
						@php
							$day_tecmaq=15; # 15 horas laborales
							$semana_tecmaq=6;
							$segundos=60;
							$minutos=60;

							$pausa=$setup=$produccion=$sinuso=0;
							$count_setup=$count_produccion=$count_sinuso=0;
							$week_in_second=(($day_tecmaq*$semana_tecmaq*$num_semanas)*$minutos)*$segundos;
							// if ($wc->num_machine=='CT-38')  dd($processes->toArray());

							if ($wc->processes->count() >0) {
								$count_produccion=$wc->processes->count();

								$processes=$wc->processes;
								$user_processes=$processes->pluck('user_processes')->collapse();
								$pauses=$processes->pluck('pauses')->collapse();
								// dd($pauses);
								foreach ($processes as $process) {
									$setup+=getTime($process->ih_created_at,$process->fh_created_at,true);
									// if ($wc->num_machine=='CT-38')  dd( $process,getTime($process->ih_created_at,$process->fh_created_at,true) );

									if (!is_null($process->ia_created_at)) {
										$setup+=getTime($process->ia_created_at,$process->fa_created_at,true);
									}
									if (!is_null($process->il_created_at)) {
										$setup+=getTime($process->il_created_at,$process->ls_created_at,true);
									}elseif(!is_null($process->ial_created_at)){
										$setup+=getTime($process->ial_created_at,$process->ls_created_at,true);
									}

								}
								foreach ($user_processes as $u_p) {
									$produccion+=getTime($u_p->start_time,$u_p->end_time,true);
								}
								// foreach ($pauses as $p) {
								// 	$pausa+=$p->started_at->diffInSeconds($p->ended_at);
								// }
								$sinuso=($week_in_second-$setup-$produccion+$pausa);
								$sinuso=intval((($sinuso/*/$count_produccion*/)/$segundos)/$minutos);
								$setup=intval((($setup/*/$count_produccion*/)/$segundos)/$minutos);
								$produccion=intval((($produccion/*/$count_produccion*/)/$segundos)/$minutos);
							}
						@endphp
						<td>{{ $setup }}</td>
						<td>{{ $produccion }}</td>
						<td>{{ $sinuso }} </td>
						<td></td>
					@endforeach
				</tr>
				@while($now->gte($limit_start))
					<tr>
						<td>{{ $now->day." ".mes_esp2($now->month) }}</td>
						@foreach($workcenters as $wc)
							@php
								$day_tecmaq=15; # 15 horas laborales
								$semana_tecmaq=6;
								$segundos=60;
								$minutos=60;

								$pausa=$setup=$produccion=$sinuso=0;
								$count_setup=$count_produccion=$count_sinuso=0;
								$week_in_second=(($day_tecmaq*$semana_tecmaq)*$minutos)*$segundos;
								// if ($wc->num_machine=='CT-38')  dd($processes);
								if ($wc->processes->count() >0) {
									$count_produccion=$wc->processes->count();
									$processes=$wc->processes->where('cr_created_at','>=',$now)->where('cr_created_at','<=',$weekend);
									$user_processes=$processes->pluck('user_processes')->collapse();
									$pauses=$processes->pluck('pauses')->collapse();
									foreach ($processes as $process) {
										$setup+=getTime($process->ih_created_at,$process->fh_created_at,true);
										// if ($wc->num_machine=='CT-38')  dd( $process,getTime($process->ih_created_at,$process->fh_created_at,true) );

										if (!is_null($process->ia_created_at)) {
											$setup+=getTime($process->ia_created_at,$process->fa_created_at,true);
										}
										if (!is_null($process->il_created_at)) {
											$setup+=getTime($process->il_created_at,$process->ls_created_at,true);
										}elseif(!is_null($process->ial_created_at)){
											$setup+=getTime($process->ial_created_at,$process->ls_created_at,true);
										}

									}
									foreach ($user_processes as $u_p) {
										$produccion+=getTime($u_p->start_time,$u_p->end_time,true);
									}
									// foreach ($pauses as $p) {
									// 	$pausa+=$p->started_at->diffInSeconds($p->ended_at);
									// }
									$sinuso=($week_in_second-$setup-$produccion+$pausa);

									$sinuso=intval((($sinuso/*/$count_produccion*/)/$segundos)/$minutos);
									$setup=intval((($setup/*/$count_produccion*/)/$segundos)/$minutos);
									$produccion=intval((($produccion/*/$count_produccion*/)/$segundos)/$minutos);
								}
							@endphp
							<td>{{ $setup }}</td>
							<td>{{ $produccion }}</td>
							<td>{{ $sinuso }} </td>
							<td></td>
						@endforeach
						@php 
							$now->subDay(); 
							$weekend->subDay(); 
						@endphp
					</tr>
				@endwhile
			</table>
			
		</div>
	</div>




</div>

@endsection
@section('script')

<script >

</script>
@endsection