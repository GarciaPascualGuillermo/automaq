@extends('admin.historial.layouts.app')

@section('content')
<div class="container-fluid">
	<div class="" style="margin-left: 10px; margin-top: 20px; ">
		<b class="font-light text-left fs-18" >Uso de máquinas / Historial por semana
		<div class="font-light color-heavy text-left" style="font-size: 12px;">INFORMACIÓN PRESENTADA EN HORAS
			<div class="color-heavy" style="text-align: right;">
				<a href={{route("export.maquina_semanas")."?section=".request()->input("section")}}><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a>
			</div>
		</div>
		</b>				
	
	
		<div class="row">
			<table border="1" style="margin-left: 20px; margin-top: 30px; text-align: center; background-color: white">
				<tr style="background-color: #878EAF; font-size: 15px; color: white">
					@foreach($workcenters as $workcenter)
					<th style="background:  #E6E8F1; border: hidden;"></th>
					<th colspan="3">{{ $workcenter->num_machine }}</th>
					@endforeach	
				</tr>
				<tr style="background-color: #878EAF; font-size: 15px; color: white">
				@for ($i = 0; $i < $workcenters->count(); $i++)
					<td style="background:  #E6E8F1; border: hidden;"></td>
					<td style="min-width: 50px">Setup</td>
					<td style="min-width: 50px">Prod</td>
					<td style="min-width: 50px">Sin U</td>
				@endfor
				</tr>
				
				<tr style="background-color: #E7EBFA">
					<td style="min-width: 160px;">Promedio</td>
					@foreach($workcenters as $wc)
						<td>{{ intval(round($times[$wc->num_machine]->avg('setup')/3600,1)) }}</td>
						<td>{{ intval(round($times[$wc->num_machine]->avg('produccion')/3600,1)) }}</td>
						<td>{{ intval(round($times[$wc->num_machine]->avg('sinuso')/3600,1)) }}</td>
						<td></td>
					@endforeach
				</tr>
				@php $i=0; @endphp
				@while($limit->lessThanOrEqualTo($week_start))
					<tr>
						<td> {{ $week_start->format('d ')." ".mes_esp2($week_start->month)." - ".$week_start->endOfWeek()->subDay()->day." ".mes_esp2($week_start->month) }} </td>
							@foreach($workcenters as $wc)
								<td>{{ intval(round($times[$wc->num_machine][$i]["setup"]/3600,1)) }}</td>
								<td>{{ intval(round($times[$wc->num_machine][$i]["produccion"]/3600,1)) }}</td>
								<td>{{ intval(round($times[$wc->num_machine][$i]["sinuso"]/3600,1)) }}</td>
								<td></td>
							@endforeach
					</tr>
					@php 
						$week_start->subWeek()->startOfWeek();
						$i++;
					@endphp
				@endwhile
			</table>
		</div>
	</div>
</div>


@endsection
@section('script')

<script >

</script>
@endsection