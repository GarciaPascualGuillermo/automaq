@extends('admin.historial.layouts.appdia')

@section('content')
<div class="container-fluid">
	<div class="" style="margin-left: 10px; margin-top: 20px; ">
		<b class="font-light text-left fs-18" >Uso de máquinas / Historial por día {{-- <span>(Ultimos 6 meses)</span> --}}
		<div class="font-light color-heavy text-left" style="font-size: 12px;">INFORMACIÓN PRESENTADA EN HORAS
			<div class="color-heavy" style="text-align: right;">
				<a href={{route("export.maquina_semanas")."?section=".request()->input("section")}}><i class="fa fa-external-link" aria-hidden="true"> Exportar</i></a>
			</div>
		</div>
		</b>				
	
	
		<div class="row">
			<table border="1" style="margin-left: 20px; margin-top: 30px; text-align: center; background-color: white">
				<tr style="background-color: #878EAF; font-size: 15px; color: white">
					@foreach($workcenters as $workcenter)
					<th style="background:  #E6E8F1; border: hidden;"></th>
					<th colspan="3">{{ $workcenter }}</th>
					@endforeach	
				</tr>
				<tr style="background-color: #878EAF; font-size: 15px; color: white">
				@for ($i = 0; $i < $workcenters->count(); $i++)
					<td style="background:  #E6E8F1; border: hidden;"></td>
					<td style="min-width: 50px">Setup</td>
					<td style="min-width: 50px">Prod</td>
					<td style="min-width: 50px">Sin U</td>
				@endfor
				</tr>
				
				<tr style="background-color: #E7EBFA">
					<td style="min-width: 160px;">Promedio</td>
					@foreach($workcenters as $wc)
						@php
							$wc_tmp=$machine_days->where('num_machine',$wc);
						@endphp
						<td>{{ intval($wc_tmp->avg('setup')/3600) }}</td>
						<td>{{ intval($wc_tmp->avg('produccion')/3600) }}</td>
						<td>{{ intval($wc_tmp->avg('sinuso')/3600) }}</td>
						<td></td>
					@endforeach
				</tr>
				@while($limit->lessThanOrEqualTo($now))
					@if ($now->dayOfWeek===Illuminate\Support\Carbon::SUNDAY)
					@php
						$now->startOfDay()->subday();
						continue;
					@endphp
					@endif
					<tr>
						<td> {{ $now->format('d ')." ".mes_esp2($now->month) }} </td>
							@foreach($workcenters as $wc)
								@php
									$wc_tmp=$machine_days->where('num_machine',$wc)->where('day',$now);
								@endphp
								@if($wc_tmp->isNotEmpty())
									@php 
										$wc_tmp = $wc_tmp->first(); 
									@endphp
									<td>{{ intval($wc_tmp->setup/3600) }}</td>
									<td>{{ intval($wc_tmp->produccion/3600) }}</td>
									<td>{{ intval($wc_tmp->sinuso/3600) }}</td>
								@else
									<td></td>
									<td></td>
									<td></td>
								@endif
								<td></td>
							@endforeach
					</tr>
					@php 
						$now->subDay();
					@endphp
				@endwhile
			</table>
		</div>
	</div>
</div>


@endsection
@section('script')

<script >

</script>
@endsection