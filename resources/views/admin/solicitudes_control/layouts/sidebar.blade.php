<div id="sidebar_open" class="sidenav active" style="width: 400px">
	<div class="logo" style="height: 130px">
		<img src="/assets/operador/logo_tecmaq.png" style="width: 100%; height: 100%;">
	</div>
	<div class="sidenav_content">
		<div class="container-fluid sidenav_usuario">
			<div class="row">
				<div class="col">
					<div class="img-usr">
						<img src="/assets/solicitudes_control/dpto de calidad.png" class="rounded-circle" style="padding: 0%;">
					</div>		
				</div>
			</div>
			<div class="row">
				<div class="col text-center font-regular color-heavy" style="font-size: 35px">
					Departamento de calidad
				</div>
			</div>
			
		</div>
					
		<div class="sidenav_menu pt-3">
			<div class="px-4 my-2 color-heavy font-regular " href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" style="border-bottom: 3px; font-size: 31px;" >
				<img src="/assets/jobs/maquinas.png" style="width: 80px; height: 80px;"> Máquinas
				<i class="fa fa-angle-down" aria-hidden="true"></i>
					<ul class="collapse list-unstyled" id="homeSubmenu" >
						<li>
							<a href="{{route('historial_semana.maquinas')}}" class="px-4 my-2 font-regular" style="color: blue; font-size: 25px">Historial por semana</a>
						</li>
						<li>
							<a href="{{route('historial_dia.maquinas')}}" class="px-4 my-2 font-regular" style="color: blue; font-size: 25px">Historial por dia</a>
						</li>
					</ul>
			</div>
			<div class="px-4 my-2 color-heavy font-regular" href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" style="font-size: 31px;">
				<img src="/assets/jobs/jobs.png" style="width: 80px; height: 80px"> Jobs
				<i class="fa fa-angle-down" aria-hidden="true"></i>

					<ul class="collapse list-unstyled " id="homeSubmenu1">
						<li>
							<a href="{{route('historial.jobs')}}" class="px-4 my-2 font-regular" style="color: blue; font-size: 25px">Historial</a>
						</li>
						<li>
							<a href="{{route('historial.jobs.eliminados')}}" class="px-4 my-2 font-regular" style="color: blue">Historial Jobs Eliminados</a>
						</li>
					</ul>
			</div>
			<div class="px-4 my-2 color-heavy font-regular" style="font-size: 31px;">
				<a href="{{route('maquinas.index')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em"><img src="/assets/jobs/supervisores.png" style="width: 80px; height: 80px"> Supervisores </a> 
			</div>
			<div class="px-4 my-2 color-heavy font-regular" href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" style="font-size: 31px;">
				<img src="/assets/jobs/operadores.png" style="width: 80px; height: 80px"> Operadores
				<i class="fa fa-angle-down" aria-hidden="true"></i>

				<ul class="collapse list-unstyled " id="homeSubmenu2">
					<li>
						<a href="{{route('historial_semana.operadores')}}" class="px-4 my-2 font-regular" style="color: blue; font-size: 25px">Historial por semana</a>
					</li>
				</ul>
			</div>
			<div class="px-4 my-2 color-heavy font-regular " href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" style="font-size: 31px;">
				<img src="/assets/jobs/calidad.png" style="width: 80px; height: 80px"> Calidad
				<i class="fa fa-angle-down" aria-hidden="true"></i>

				<ul class="collapse list-unstyled " id="homeSubmenu3">
					<li>
						<a href="{{route('historial.calidad')}}" class="px-4 my-2 font-regular" style="color: blue; font-size: 25px">Historial</a>
					</li>
				</ul>
			</div>
			<div class="px-4 my-2 color-heavy font-regular " href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" style="font-size: 31px;">
				<img src="/assets/jobs/mantenimiento.png" style="width: 80px; height: 80px"> Mantenimiento<i class="fa fa-angle-down" aria-hidden="true"></i>

				<ul class="collapse list-unstyled " id="homeSubmenu4">
					<li>
						<a href="{{route('historial.mantenimiento')}}" class="px-4 my-2 font-regular" style="color: blue; font-size: 25px">Historial</a>
					</li>
				</ul>
			</div>
			<div class="px-4 my-2 color-heavy font-regular" style="font-size: 31px;">
				<a href="{{route('indicadores.index')}}" style="padding: 0; display: inline-block;color: #0c54c4; font-size: 1em"><img src="/assets/jobs/indicadores.png" style="width: 80px; height: 80px"> Indicadores </a> 
			</div>
		</div>
		
		<div class="sidenav_copyrigth text-center font-light" style="font-size: 31px;"> 
			<div class=" px-4 my-2  color-heavy font-regular">
				<a class="dropdown-item" href="{{ route('logout') }}" style="padding: 0;" 
				   onclick="event.preventDefault();
				                 document.getElementById('logout-form').submit();">
				    
				    <img src="/assets/jobs/log out.png" style="width: 25%;">
				</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				    @csrf
				</form>
				
				<p style="margin-left: 40px; margin-top: -15px">Log out</p>
			</div>
			Desarrollada por 
			<img src="{{ asset('logo_sidebar.png') }}" style="height: 32px; width: 51px;"/>
		</div>
	</div>		
</div>

<div id="sidebar_close" class="sidenav sidebar_small">
 	<div class="logo">
 		<img src="/assets/jobs/logo_sidebar_cerrado.png" style="width: 100%; height: 100%;">
 	</div>
 	<div class="sidenav_content">
 		<div class="container-fluid sidenav_usuario border-bottom">
 			<div class="row">
 				<div class="col without-padding">
 					<div class="img-usr-close without-padding">
 						<img src="/assets/solicitudes_control/dpto de calidad.png" class="rounded-circle without-padding">
 					</div>		
 				</div>
 			</div>
 		</div>
 					
 		<div class="pt-3">
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom" href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
 				<a href="{{route('historial_semana.maquinas')}}" style="padding: 0; text-decoration: none" aria-expanded="false" aria-haspopup="true" ><img src="/assets/jobs/maquinas.png" class="img-close">
 			</div></a>
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom" href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false">
 				<a href="{{route('historial.jobs')}}" style="padding: 0"><img src="/assets/jobs/jobs.png"class="img-close"> 
 			</div></a>
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom">
 				<a href="{{route('maquinas.index')}}" style="padding: 0"><img src="/assets/jobs/supervisores.png"class="img-close"> 
 			</div></a>
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom" href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" >
 				<a href="{{route('historial_semana.operadores')}}" style="padding: 0"><img src="/assets/jobs/operadores.png"class="img-close"> 
 			</div></a>
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom " href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" >
 				<a href="{{route('historial.calidad')}}" style="padding: 0"><img src="/assets/jobs/calidad.png"class="img-close"> 
 			</div></a>
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom " href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" >
 				<a href="{{route('historial.mantenimiento')}}" style="padding: 0"><img src="/assets/jobs/mantenimiento.png"class="img-close">
 			</div></a>
 			<div class="px-4 pb-2 pt-2 color-heavy border-bottom">
 				<a href="{{route('indicadores.index')}}" style="padding: 0"><img src="/assets/jobs/indicadores.png"class="img-close"> 
 			</div></a>
 		</div>
 		<div class="sidenav_copyrigth text-center font-light" style="font-size: 16px"> 
			<div class=" px-4 my-2  color-heavy font-regular">
					<a class="dropdown-item" href="{{ route('logout') }}" style="padding: 0;" 
					   onclick="event.preventDefault();
					                 document.getElementById('logout-form').submit();">
					    
					    <img src="/assets/jobs/log out.png" class="img-close">
					</a>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					    @csrf
					</form>
				
			</div>
 			Desarrollado por 
 			<img src="{{ asset('logo_sidebar.png') }}" style="height: 29px; width: 38px;"/>
 		</div>
 	</div>		
</div>
