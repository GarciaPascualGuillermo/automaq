<nav id="nav_h" class="navbar fixed-top navbar-expand-md navbar-light navbar-laravel close-nav" style="">
	<img src="/assets/jobs/close_sidebar.png" id="arrow_right" class="arrow" >
	<img src="/assets/jobs/open_sidebar.png" id="arrow_left" class="arrow">
	<ul class="navbar-nav justify-content-end" style="width: 100%;">
		<li class="nav-item dropleft ">
	        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-md-offset-12 font-regular text-right navbar-asignados">
    	        {{ __('Pausa por mantenimiento') }}
            </div>
		</li>
	</ul>
</nav>