<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
</head>
   <body>
   <div id="app" class="background-main-index" style="padding-left: 80px;">
      @include('admin.historial.layouts.partials.sidebar_active')
      <div id="main-content" class="content-nav close-nav">
         @include('admin.solicitudes_control.layouts.navbar.navbar')
      @yield('content')
      <modal-mensaje></modal-mensaje>   
         
      </div>
      
   </div>


     {{--  <div id="app" class="background-main-index" style="padding-left: 80px;">
            <nav id="nav_h" class=" navbar navbar-expand-md navbar-light navbar-laravel " style="font-size:18px; height: 58px;">
               <img src="/assets/jobs/close_sidebar.png" id="arrow_right" class="arrow" >
               <img src="/assets/jobs/open_sidebar.png" id="arrow_left" class="arrow">
               <div class="row" style="width: 100%;">
                  <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                     <img class="img-tecmaq" src="/assets/operador/logo_tecmaq.png" style="max-width: 30%;">
                  </div>
                  <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 col-md-offset-6 font-regular text-right navbar-asignados">
                     {{ __('Pausa por mantenimiento') }}
                  </div>
               </div>
             </nav>  
         
         @yield('content')
         <modal-mensaje></modal-mensaje>
      </div> --}}
      <script src="{{ asset('js/solicitudes_control/app.js')."?v=".str_random(3) }}"></script>
      <script>
         window.Auth = window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'url_logout'=>route('logout'),
         ]) !!};
      </script>
      @yield('data_json')
   </body>
</html>