<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/solicitudes_control/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
	{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
	@yield('style')
</head>
<body>

	<div id="app">
 		@include('admin.solicitudes_control.layouts.sidebar')

		<div id="main-content" class="content-nav close-nav" style="padding-top: 100px;">
			<nav id="nav_h" class="navbar fixed-top navbar-expand-md navbar-light navbar-laravel close-nav" style="">
				<img src="/assets/jobs/close_sidebar.png" id="arrow_right" class="arrow" >
				<img src="/assets/jobs/open_sidebar.png" id="arrow_left" class="arrow">
				<ul class="navbar-nav justify-content-end" style="width: 100%; padding-right: 10px">
					<li class="nav-item dropleft">
						<a class="nav-link" data-toggle="dropdown"  id="nomenclatura" href="#" role="button" aria-haspopup="true" aria-expanded="false">
							<img src="/assets/jobs/nomenclatura_inactivo.png"  style="width: 65px;height: 65px;">
						</a>
						<div class="dropdown-menu" aria-labelledby="nomenclatura" style="font-size: 40px">
							<a class="dropdown-item" href="#">Nomenclatura</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<i class="fa fa-circle" aria-hidden="true" style="color: orange"></i> Herramentaje-Ajuste</a>
							<a class="dropdown-item" href="#">
								<i class="fa fa-circle" aria-hidden="true" style="color: yellow"></i> Liberación</a>
							<a class="dropdown-item" href="#">
								<i class="fa fa-circle" aria-hidden="true" style="color: green"></i> Producción</a>
							<a class="dropdown-item" href="#">
								<i class="fa fa-circle" aria-hidden="true" style="color: red"></i> Pausa</a>

						</div>
					</li>
					{{-- <li class="nav-item">
						<a class="nav-link" href="#">
							<img src="/assets/jobs/notificaciones_inactivo.png" style="width: 30px;height: 30px; margin-left: 10px">
						</a>
					</li> --}}
					<li class="nav-item dropleft " >
						<a class="nav-link" data-toggle="dropdown"  id="nomenclatura" href="#" role="button" aria-haspopup="true" aria-expanded="false">
							<img src="/assets/jobs/settings_inactivo.png" style="width: 65px;height: 65px; margin-left: 60px">
						</a>
						<div class="dropdown-menu" aria-labelledby="nomenclatura" style="font-size: 40px">
							<a class="dropdown-item disabled" href="#">Ajustes</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{route('usuarios.index')}}">
								<i class="fa fa-arrow-circle-right" aria-hidden="true" style="color: blue"></i> Usuarios</a>
							<a class="dropdown-item" href="{{route('centro_trabajo.index')}}">
								<i class="fa fa-arrow-circle-right" aria-hidden="true" style="color: blue"></i> Centro de Trabajo</a>
							<a class="dropdown-item" href="{{route('parametros.index')}}">
								<i class="fa fa-arrow-circle-right" aria-hidden="true" style="color: blue"></i> Parametros</a>
						</div>
					</li>
				</ul>
			</nav>
			<div class="container-fluid" style="background: #E6E8F1;min-height: 100%;">
				@yield('content')
			</div>
		</div>
		<modal-mensaje></modal-mensaje>
	</div>
	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
		]) !!};
	</script>

	<!-- Scripts -->
	<script src="{{ asset('js/solicitudes_control/app.js')."?v=".str_random(3) }}"></script>
	<script src="{{ asset('plugins/@fortawesome/fontawesome-free/js/fontawesome.min.js') }}"></script>
	@yield('script')
</body>
</html>
