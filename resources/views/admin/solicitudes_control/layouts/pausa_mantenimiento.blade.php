<!DOCTYPE>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
</head>
<body>
   <div id="app" class="background-main-index">
      <nav class=" navbar navbar-expand-md navbar-light navbar-laravel" style="font-size:18px; height: 58px;">
         <div class="container-fluid">
            <div class="row" style="width:100%">
               <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <img class="img-tecmaq" src="/assets/operador/logo_tecmaq.png" style="max-width: 30%;">
               </div>
               <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 col-md-offset-6 font-regular text-right navbar-asignados">
                  {{ __('Pausa por mantenimiento') }}
               </div>
            </div>
         </div>
      </nav>  
      @include('admin.solicitudes_control.pausa_mantenimiento') 
      <modal-mensaje></modal-mensaje>
   </div>
  <script src="{{ asset('js/solicitudes_control/app.js')."?v=".str_random(3) }}"></script>
</body>
</html>