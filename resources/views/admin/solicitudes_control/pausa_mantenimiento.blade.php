<div class="container-fluid mt-2 mb-2">
	<div class="row text-left ml-1"> 
		<div class="col-md-7 color-black font-light">
			<p class="fs-20">En proceso</p>
		</div>
		<div class="col-md-5 color-black font-light">
			<p class="fs-20">En espera</p>
		</div>
	</div>
</div>				  
<div class="container-fluid" style="background: #E6E8F1;">
	<div class="row mb-3">
		@for($x=0; $x<5; $x++)
			@include('admin.solicitudes_control.cards.cardpausa',['ct'=>'CT-001','x'=>$x])
		@endfor
		
	</div>
	<div class="row">
		@for($x=5; $x<10; $x++)
			@include('admin.solicitudes_control.cards.cardpausa',['ct'=>'CT-001','x'=>$x])
		@endfor 
	</div>
</div>