@extends('admin.jobs.layouts.asignados')
@section('content')
	<nav class=" navbar navbar-expand-md navbar-light navbar-laravel" style="min-height: 55px;">
		<div class="content-nav">
			<div class="container-fluid">
				<div class="row">
					<div class="col" style="position: relative;">
						<img class="img-tecmaq" src="/assets/operador/logo_tecmaq.png" style="height:100%;max-height: 55px;width: auto;">
					</div>
					<div class="col  color-gray font-regular navbar-asignados">
						{{ __('Centro de Trabajo:') }}
						<span class="col color-heavy  font-weight-bold">{{ $wc->first()->num_machine }}</span>
					</div>
					<div class="col color-gray font-regular navbar-asignados">
						{{__('Modelo de la Máquina:')}}
						<span class="col color-heavy  font-weight-bold">{{ $wc->first()->model }}</span>
                    </div>
                    @if ($wc->first()->on_maintenance)
                    <div class="col color-gray font-regular navbar-asignados">
                        <h3>
                            <span class="badge badge-danger">
                                {{__('En petición de mantenimiento')}}
                            </span>
                        </h3>
					</div>
                    @endif
				</div>
			</div>
		</div>
    </nav>

    <div id="app" class="pt-2" style="min-height: 80%;height: calc(100% - 55px - 0.5rem); padding: 0px 15px;">

        @if (request()->user()->hasRoles(['tec.mantenimiento', 'dep.mantenimiento', 'jefe.mantenimiento']))
            <mantenimiento-espera-component :initial_workcenter="{{ json_encode($wc->first()) }}"></mantenimiento-espera-component>
        @endif

        <div class="row valign-wrapper" style="height: 10%;">
            <div class="no-valign-wrapper">
                <div class="col-12 text-center color-gray text-center fs-16 font-regular">
                    <h3>Asignado</h3>
                </div>
            </div>
        </div>
        <div class="row " style="height: 40%;">
            <div class="col-6 valign-wrapper">
                <div class="no-valign-wrapper text-right">
                    @if($wc->first()->charge)
                        @include('admin.jobs.asignados.cards.CncJobSinCarga')
                    @elseif(!is_null($p_first))
                        @include('admin.jobs.asignados.cards.cnc-job-full',['process'=>$p_first])
                    @else
                        @include('admin.jobs.asignados.cards.CncJobSinCarga')
                    @endif
                </div>
            </div>
            <div class="col-6 valign-wrapper">
                <div class="no-valign-wrapper">
                    <div class="card background-main-index">
                        <div class="card-body valign-wrapper" style="">
                            <div class="no-valign-wrapper">
                                <p class="lh-05 fs-18 font-light color-gray">Presiona "Enter" para empezar</p>
                                <p class="lh-05 fs-18 font-light color-gray">a trabajar este Job.</p>


                                <div class="spinner-border loading" role="status" style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>

                            </div>
                        </div>
                        <div style="margin-top: 10%;">
                            <img src="/assets/operador/enter.png" width="125px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 40%;">
            <div class="col-12 px-3 valign-wrapper" style="">
                <div class="no-valign-wrapper  justify-content-center align-self-center" style="display: flex;overflow-x: auto;overflow-y: hidden;min-height: 200px;padding-top: 0.7rem;">
                    @php $count=0; @endphp
                    @foreach($processes as $process)
                        @if(!$wc->first()->charge)
                            @if($process->id === $p_first->id) @php continue; @endphp @endif
                        @endif
                        @php $count++; @endphp
                        <div class="" style="display: inline-block;margin: 0px .4rem;">
                            @include('admin.jobs.asignados.cards.cncJob',['process'=>$process])
                        </div>
                    @endforeach
                    @while($count<5)
                        <div class="" style="display: inline-block;margin: 0px .4rem;">
                            @include('admin.jobs.asignados.cards.cncClean')
                        </div>
                        @php $count++; @endphp
                    @endwhile
                </div>
            </div>
        </div>

        <confirmar-asignado-component></confirmar-asignado-component>



        <!-- Modal Loader desasignar -->
        <div class="modal fade" id="loader-modal" role="dialog" tabindex="0">
            <div class="modal-dialog modal-dialog-centered">
                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="container-fluid" style="margin color: grey; overflow: auto;" >
                            <div>
                                <div class="row" >
                                    <div class="col text-center">
                                        <div class="spinner-border" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                        <h3 class="my-3">Saliendo...</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <span>
                <img src="/assets/operador/T.png" style="width: 40px"> Terminar
            </span>
            @if (auth()->user()->hasRoles(['ajustador', 'root']))
            <span>
                <img src="/assets/operador/S.png" style="width: 40px"> Sistema
            </span>
            @endif
        </div>

        <form id="logout-form" action="{{ route('operador.logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>

@endsection
@section('script')

    <script>
        var change = false;
    </script>

    @if (session()->has('message'))
        <script>
            alert("{{ session('message') }}");
        </script>
    @endif

    @if (!request()->user()->hasRoles(['jefe.mantenimiento','tec.mantenimiento']))
	<script>

        var onSubmit = false;

		var processes=JSON.parse('@php echo json_encode($processes->toArray()); @endphp');
		//var primero = processes.shift();
		var key = 0;

		function select_card(process_key) {
			$(".card_jobs").removeClass('card_selected');
			$("#cardCNC_"+processes[process_key].id).addClass('card_selected');
        }

		@if($processes->isNotEmpty())
		process_first= @php echo json_encode($processes->first()->toArray()); @endphp;
		axios.post("/operador/cycles",process_first)
		.then(
			(response) => {
				var data=response.data;
				// console.log(data);
				$(".value_circle").attr('stroke-dasharray',data.piecePercent+",100");
				$("#text_circle").empty();
				$("#text_circle").append(data.piecePercent+"%");

			}
		).catch(error => {
			console.log("error",error);
		});
        @endif

		if (processes.length) {
			select_card(key);
			document.addEventListener("keydown", (e) => {
                console.log(e.keyCode);
				if (Laravel.vue.asignar.enabled ) {
					switch(e.keyCode){
						//ArrowLeft
						case 37:
							@if(true)
								key--;
								if (key<0) {
									key=processes.length-1;
								}
								select_card(key);
							@endif
							break;
						//ArrowRight
						case 39:
							@if(true)
								key++;
								if (processes.length==key) {
									key=0;
								}
								select_card(key);
							@endif
							break;
						case 84:

                            if (onSubmit) {
                                break;
                            }

                            onSubmit = true;

                            $("#loader-modal").modal('show');

                            axios.post("/operador/logout")
                            .then(
                                response => {
                                    console.log(response);
                                    var data=response.data;
                                    window.location.href="/";
                                },

                            ).catch(error => {
                                console.log('Error', error)
                                onSubmit = false;
                            });

                            break;
						// ENTER
						case 13:
							if (key == 0) {
                                if (onSubmit) {
                                    break;
                                }
                                onSubmit = true;
                                $('.loading').show();
								axios.post( "{{route('jobs.asignados.selecciona')}}" ,{process_id:processes[key].id})
								.then((response) => {
                                    var process=response.data
                                    window.location.href="{{ route('operador.setup.index') }}?job_id="+process.job_id;
                                })
                                .catch((error) => {
                                    console.log("Error de la peticion: ",error);
                                    onSubmit = false;
                                    $('.loading').hide();
                                    alert('Ha ocurrido un error, intente de nuevo');
								});

							}else{

								$("#confirmarAsignado").modal('show');
								Laravel.process = processes[key];

							}
							break;
					}
				}
			});
		}else{
			document.addEventListener("keydown", (e) => {
                if (Laravel.vue.asignar.enabled) {
                    switch(e.keyCode) {
                        case 84:

                            if (onSubmit) {
                                break;
                            }

                            onSubmit = true;

                            $("#loader-modal").modal('show');

                            axios.post("/operador/logout")
                            .then(
                                response => {
                                    var data=response.data;
                                    window.location.href="/";
                                },

                            ).catch(error => {
                                console.log('Error', error)
                                onSubmit = false;
                            });

                            break;
                    }
                }
			});
        }

        @if (auth()->user()->hasRoles(['ajustador', 'root', 'sup.linea', 'sup.produccion']))

            document.addEventListener("keydown", (e) => {
                if (e.keyCode == 83 && Laravel.vue.asignar.enabled) {
                    window.location.href = "/jobs";
                }
            });

        @endif

        if (processes.length > 0) {

            window.Echo.private(`process-${processes[0].id}`).listen('DesasignarEvent', (e) => {

                var isUserAccept = confirm('El job que se estaba ejecutando en este workcenter, ha vuelto a la cola');

                axios.post('{{ route("jobs.asignados.readed") }}', {
                    process_id: processes[0].id
                })
                .then(response => {
                    console.log(response)
                })
                .catch(error => {
                    console.log(error)
                })
                .then(response => {
                    document.addEventListener("keydown", (e) => {
                        e.preventDefault();
                        e.stopPropagation();
                    });
                    window.location.href = '/flush-session';
                });

            });

        }

    </script>
    @else
        @if (!$wc->first()->on_maintenance)
            <script>

                document.addEventListener("keydown", (e) => {
                    switch (e.keyCode) {
                        case 84:
                            window.location.href = '/logout';
                        break;
                        default:
                            alert('No hay ningun mantenimiento disponible en este centro de trabajo')
                            break;
                    }
                });

            </script>
        @endif
    @endif

    @if (session()->has('info'))
        <script>
            alert("{{ session()->get('info') }}");
        </script>
    @endif

@endsection


