<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/jobs/app.css')."?v=".str_random(2) }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/@fortawesome/fontawesome-free/css/fontawesome.min.css') }}">
</head>
<body class="background-main-index">
   {{-- @include('admin.jobs.asignados') --}}
   @yield('content')

	<script>
		window.Auth = window.Laravel = {!! json_encode([
			'user' => Auth::user(),
			'csrfToken' => csrf_token(),
			'vapidPublicKey' => config('webpush.vapid.public_key'),
			'vue'=>[
				'asignar'=>[
					'enabled'=>true
				],
			],
			'process'=>[],
			'wc'=>$wc->first(),
		]) !!};
	</script>
	<!-- Scripts -->
	<script src="{{ asset('js/jobs/asignados/app.js')."?v=".str_random(3) }}"></script>
	@yield('script')
</body>
</html>
