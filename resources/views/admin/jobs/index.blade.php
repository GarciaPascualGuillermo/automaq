@extends('admin.jobs.layouts.app')
@section('content')
	<job-asignar-component
		:url_process="{{json_encode(route('api.processes'))}}"
		:url_workcenters="{{json_encode(route('api.workcenters'))}}"
		:url_addjobstoworkcenter="{{json_encode(route('api.addJobsToWorkcenter'))}}"
		:url_getjobofwc="{{json_encode(route('api.GetJobOfWC'))}}"
		:section="{{json_encode( request()->input('section','') )}}"
        :url_jobs="{{json_encode(route('jobs.index'))}}"
	></job-asignar-component>
@endsection

@if (auth()->user()->hasRoles(['root', 'ceo', 'gte.operacion', 'sistemas']))

    @section('script')
        <script>

            $(document).ready(function () {

                $( "#sortable" ).sortable({
                    axis: "y",
                    start: function(event, ui) {
                        ui.item.startPos = ui.item.index();
                    },
                    stop: function(event, ui) {

                        var workcenter_id = $(ui.item).attr('id');
                        axios.post('workcenter/reorder', {
                            startPosition: ui.item.startPos,
                            endPosition: ui.item.index(),
                            workcenter_id: workcenter_id
                        })
                        .then(response => {
                            console.log(response);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                    }
                });

            });

        </script>
    @endsection

@endif
