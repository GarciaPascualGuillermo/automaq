@extends('admin.jobs.layouts.app')

@section('content')
	<div class="container-fluid" style="margin-top: 10px">
		<div class="row" style="text-align: center;"> 
			<div class="col color-black font-light col-md-5">
				<font size="3">Jobs en proceso</font>
			</div>
			<div class="col color-black font-light col-md-7">
				<font size="3">Status</font>
			</div>
		</div>
	</div>				  
	<div class="container-fluid" style="background: #E6E8F1; overflow-y: auto;">
		<div class="row" style="margin-top: 10px;">
			
			<div class="card col-4 shadow" style="height: 140px; margin-left: 20px; background: #8597AF;">
				<div class="row" style="height: 100%; align-items: center;">
					<div class="col-5" style="text-align: center; ">
						<font size="5" class="font-medium" style="color: white">CT-001</font><br>
						<font size="1" class="font-regular" style="color: white; line-height: 1">P-300L-PH10></font>
						<div class="contenedor" style="margin-top: 5px">
							<img src="/assets/jobs/barra_horas1.png" style="width: 90%">
							<div class="centrado" style="color: #8597AF">4 DÍAS | 3 HORAS</div>
						</div>
					</div>
					<div class="vl" style="border-left: 1px solid gray; height: 100%;"></div>
					<div class="col-7" style="width: 20px; height: 20px" >
						
					</div>
				</div>
			</div>
			
			<div class="card col shadow" style="width: 60px; height: 140px;margin-left:  25px; margin-right: 5px">
				<div class="row" style="height: 45%; border-bottom: 1px solid #8597AF; text-align: center;">
					<div class="col" style="">
						<img src="/assets/supervisores/5.png" style="width: 100%;height: 90%">
					</div>
				</div>
				<div class="row" style="height: 60%; ">
					<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
						<div style="margin-top: 10px">
							<font size="2" class="color-heavy font-light" >142 Carlos Vera</font><br>
							<font size="2" class="color-heavy font-light" ><i class="fa fa-clock-o " aria-hidden="true"></i>120 min</font>
						</div>
					</div>
					<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
						<div style="margin-top: 10px">
							<font size="2" class="color-heavy font-light" >087 Mario Martinez</font><br>
							<font size="2" class="color-heavy font-light" ><i class="fa fa-clock-o" aria-hidden="true"></i>87 min</font>
						</div>
					</div>
					<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
						<div style="margin-top: 10px">
							<font size="2" class="color-heavy font-light" >025 Juan Lopez</font><br>
							<font size="2" class="color-heavy font-light" ><i class="fa fa-clock-o" aria-hidden="true"></i>127 min</font>
						</div>
					</div>
					<div class="col" style="text-align: center;">
						<div class="" style="">
							<font size="2" class="color-heavy font-medium" >106 Miguel Lopez</font><br>
							<font size="2" class="color-heavy font-medium" ><i class="fa fa-clock-o" aria-hidden="true"></i>42 min</font>
							<div class="progress" style="height: 10px; width: 75%; margin-top: 5px">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"aria-valuemin="0" aria-valuemax="100" style="width:84%">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row" style="margin-top: 25px;">
			<div class="card col-4 shadow" style="height: 140px; margin-left: 20px; background: #8597AF;">
				<div class="row" style="height: 100%; align-items: center;">
					<div class="col-5" style="text-align: center; margin-top: 10px;">
						<font size="5" class="font-medium" style="color: white">CT-001</font><br>
						<font size="1" class="font-regular" style="color: white; line-height: 1">P-300L-PH10></font>
						<div class="contenedor" style="margin-top: 5px">
							<img src="/assets/jobs/barra_horas1.png" style="width: 90%">
							<div class="centrado" style="color: #8597AF">4 DÍAS | 3 HORAS</div>
						</div>
					</div>
					<div class="vl" style="border-left: 1px solid gray; height: 100%;"></div>
					<div class="col-6 text-center" style="border-width: 1px; border-style: dashed ; color: white; margin-left: 15px; ">
						<img src="/assets/jobs/checar_status.png" style="width: 50px;height: 50px; margin-top: 30px;" >
						<p class="font-light" style="color: white; text-align: center;">Checar status</p>
					</div>
				</div>
			</div>
			<div class="card col shadow" style="width: 60px; height: 140px;margin-left:  25px; margin-right: 5px">
				<div class="row" style="height: 45%; border-bottom: 1px solid #8597AF; text-align: center;">
					<div class="col" style="">
						<img src="/assets/supervisores/1.png" style="width: 100%;height: 90%">
					</div>
				</div>
				<div class="row" style="height: 60%; ">
					<div class="col" style="border-right: 1px solid #8597AF"></div>
					<div class="col" style="border-right: 1px solid #8597AF"></div>
					<div class="col" style="border-right: 1px solid #8597AF"></div>
					<div class="col" style=""></div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 25px;">
			<div class="card col-4 shadow" style="height: 140px; margin-left: 20px; background: #8597AF;">
				<div class="row" style="height: 100%; align-items: center;" >
					<div class="col-5" style="text-align: center; margin-top: 10px;">
						<font size="5" class="font-medium" style="color: white">CT-001</font><br>
						<font size="1" class="font-regular" style="color: white; line-height: 1">P-300L-PH10></font>
						<div class="contenedor" style="margin-top: 5px">
							<img src="/assets/jobs/barra_horas1.png" style="width: 90%">
							<div class="centrado" style="color: #8597AF">4 DÍAS | 3 HORAS</div>
						</div>
					</div>
					<div class="vl" style="border-left: 1px solid gray; height: 100%;"></div>
					<div class="col-7" style="width: 20px; height: 20px" >
						
					</div>
				</div>
			</div>
			<div class="card col shadow" style="width: 60px; height: 140px;margin-left:  25px; margin-right: 5px">
				<div class="row" style="height: 45%; border-bottom: 1px solid #8597AF; text-align: center;">
					<div class="col" style="">
						<img src="/assets/supervisores/2.png" style="width: 100%;height: 90%">
					</div>
				</div>
				<div class="row" style="height: 60%; ">
					<div class="col" style="border-right: 1px solid #8597AF; text-align: center;">
						<div style="margin-top: 10px">
							<font size="2" class="color-heavy font-medium" >142 Carlos Vera</font><br>
							<font size="2" class="color-heavy font-medium" ><i class="fa fa-clock-o" aria-hidden="true"></i>120 min</font>
						</div>
					</div>
					<div class="col" style="border-right: 1px solid #8597AF"></div>
					<div class="col" style="border-right: 1px solid #8597AF"></div>
					<div class="col" style=""></div>
				</div>
			</div>
			
		</div>
	</div>	
@endsection