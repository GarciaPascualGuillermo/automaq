@extends('admin.jobs.layouts.app')

@section('content')
	<div class="container-fluid mt-2 mb-2">
		<div class="row text-left ml-1"> 
			<div class="col color-black font-light col-md-5">
				<font size="3">28 de marzo 2019</font>
			</div>
		</div>
	</div>				  
	<div class="container" style="background: #E6E8F1;">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#correct_info">
		  Launch demo modal
		</button>
		@include('admin.jobs.modal.correct_info')
		{{-- <div class="row mb-3">
			@for($x=0; $x<5; $x++)
			@include('admin.maquinas.cardmaquinas',['ct'=>'CT-007'])
			@endfor
			
		</div>
		<div class="row">
			@for($x=0; $x<5; $x++)
			@include('admin.maquinas.cardmaquinas',['ct'=>'CT-007'])
			@endfor
		</div> --}}
		<!-- <div>
   			 <canvas id="myChart" style="max-width: 500px; max-height: 500px"></canvas>
   			 
   			<canvas id="bar-chart" style="max-width: 500px; max-height: 500px"></canvas>
   			
  		</div> -->


	</div>

<!-- <script src="chart.js"></script> -->
@endsection
@section('script')
<script type="text/javascript" src="{{asset('plugins/canvasjs/canvasjs.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/canvasjs/canvasjs.react.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/canvasjs/jquery.canvasjs.min.js')}}"></script>
<script >

// var ctx = document.getElementById('myChart').getContext('2d');
// var chart = new Chart(ctx, {
//     type: 'doughnut',
//     data:{
// 	datasets: [{
// 		data: [60,30,10],
// 		backgroundColor: ['green','grey','yellow'],
// 		showInLegend: true,
// 		label: 'Uso de la maquina'}],
// 		labels: [Productividad]},
//     options: {
//     	responsive: true,
//     	legend: {
//    			position: 'left'
//    		},
//    		scales: {
//    			fontSize: 5
//    		},
//     	cutoutPercentage: 80
//     }
// });


// Bar chart
// new Chart(document.getElementById("bar-chart"), {
//     type: 'bar',
//     data: {
//       labels: ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes","Sabado","Domingo"],
//       datasets: [
//         {
//           label: "Semanal",
//           backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#c45850","#c45850"],
//           data: [60,40,34,78,43,50,45]
//         }
//       ]
//     },
//     options: {
//       legend: { display: false },
//       title: {
//         display: true,
//         text: 'Semanal'
//       }
//     }
// });

/*-----------------------------------------------*/
// var value = 60;
// var value1 = 30;
// var value2 = 10;
// var data = {
//   labels: [
//     "My val",
//     ""
//   ],
//   datasets: [
//     {
//       data: [value, 100-value, 100-value1, 100-value2],
//       backgroundColor: [
//         "#FF6384",
//         "#AAAAAA",
//         "#FF5555",
//         "#AAAAAA",
//         "#FF4444",
//         "#AAAAAA"
//       ],
//       hoverBackgroundColor: [
//         "#FF6384",
//         "#AAAAAA"
//       ],
//       hoverBorderColor: [
//         "#FF6384",
//         "#ffffff"
//       ]
//     }]
// };

// var myChart = new Chart(document.getElementById('myChart1'), {
//   type: 'doughnut',
//   data: data,
//   options: {
//   	responsive: true,
//     legend: {
//       display: false
//     },
//     cutoutPercentage: 80,
//     tooltips: {
//     	filter: function(item, data) {
//         var label = data.labels[item.index];
//         if (label) return item;
//       }
//     }
//   }
// });

// textCenter(value);

// function textCenter(val) {
//   Chart.pluginService.register({
//     beforeDraw: function(chart) {
//       var width = chart.chart.width,
//           height = chart.chart.height,
//           ctx = chart.chart.ctx;

//       ctx.restore();
//       var fontSize = (height / 114).toFixed(2);
//       ctx.font = fontSize + "em sans-serif";
//       ctx.textBaseline = "middle";

//       var text = val+"%",
//           textX = Math.round((width - ctx.measureText(text).width) / 2),
//           textY = height / 2;

//       ctx.fillText(text, textX, textY);
//       ctx.save();
//     }
//   });
// }

/*------------------------------------*/
// var options = {
// 	animationEnabled: true,
// 	title: {
// 		text: "Uso de la máquina"
// 	},
// 	data: [{
// 		type: "doughnut",
// 		innerRadius: "80%",
// 		showInLegend: false,
// 		legendText: "{label}",
// 		indexLabel: "{label}: #percent%",
// 		dataPoints: [
// 			{ label: "PRODUCCIÓN", y: 60 },
// 			{ label: "SIN USO", y: 30 },
// 			{ label: "SETUP", y: 10 }
			
// 		]
// 	}]
// };
// $("#chartContainer").CanvasJSChart(options);
</script>
@endsection