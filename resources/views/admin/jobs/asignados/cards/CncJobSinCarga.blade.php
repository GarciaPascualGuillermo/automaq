<div id="cardCNC_" class="card jobs-card color-gray font-light shadow card_jobs" style="">
	<div class="card-body valign-wrapper" style="min-height: 100%;padding: 0 1rem 0 1rem;">
		<div class="no-valign-wrapper">
			<div class="row mt-2">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="row fs-18">
						<div class="col font-regular text-left" style="color: black;">
							<b>Job:</b>
						</div>
						<div class="col font-regular color-heavy text-right " >
							<b>SIN CARGA</b>
						</div>
					</div>
				</div>
				{{-- <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-right color-gray" >
					<i class="fa fa-ellipsis-v fs-20" aria-hidden="true" style="color: black;"></i>
				</div> --}}
			</div>
			<div class="row font-regular">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<hr>
					<div class="row">
						<div class="col font-light color-gray text-left">
							#Parte
						</div>
						<div class="col font-light color-heavy text-right">
							S/C
						</div>
					</div>

					<div class="row">
						<div class="col font-light color-gray text-left">
							Operación:
						</div>
						<div class="col font-light color-heavy text-right">
							S/C
						</div>	
					</div>

					<div class="row">
						<div class="col-8 font-light color-gray text-left">
							Cantidad piezas:
						</div>
						<div class="col font-light color-heavy text-right">
							S/C
						</div>
					</div>
					<div class="row">
						<div class="col-8 font-light color-gray text-left">
							Tiempo de ciclo:
						</div>
						<div class="col font-light color-heavy text-right">
							S/C
						</div>
					</div>
					<div class="row">
						<div class="col font-light color-gray text-left">
							Cliente:
						</div>
						<div class="col font-light color-heavy text-right">
							S/C
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-white text-center shadow background-color-heavy">
	  <div class="row">
	  	<div class="col">
	  		<i class="fa fa-calendar fs-18" aria-hidden="true"></i>
	  		
	  	</div>
	  	<div class="col">
	  		{{-- <i class="fa fa-clock-o" aria-hidden="true" style="font-size:20px;"></i> --}}
	  		{{-- <a href="#" class="btn text-white"><b class="fs-13">10 HRS</b></a> --}}
	  	</div>
	  </div>
	</div>
</div>
