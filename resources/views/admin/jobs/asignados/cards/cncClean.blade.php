
<div id="" class="card jobs-card-sm color-gray font-light shadow" style="height: 150px;background: rgba(255,255,255,.6);">
	<div class="card-body valign-wrapper" style="margin-top:0px;margin-bottom:0px;padding: .2rem 1rem;">
		<div class="no-valign-wrapper">
			<div class="row" style="visibility: hidden;">
				<div class="col">
					<div class="row fs-14" style="font-size: 13px;border-bottom: 1px solid #e8e8e8;">
						<div class="col font-regular text-left" style="color: black;">
							<b>Job:</b>
						</div>
						<div class="col font-regular color-heavy text-right">
							<b></b>
						</div>
						<div class="col-1" style="padding-right: 0px; padding-left: 0px;">
							<i class="fa fa-ellipsis-v fs-20" aria-hidden="true" style="color: black;"></i>
						</div>
					</div>
					<div class="row font-regular fs-12">
						<div class="col">
							<div class="row">
								<div class="col font-light color-gray text-left">
									#Parte
								</div>
								<div class="col font-light color-heavy text-right">

								</div>
							</div>

							<div class="row">
								<div class="col font-light color-gray text-left">
									Operacion:
								</div>
								<div class="col font-light color-heavy text-right">

								</div>
							</div>

							<div class="row">
								<div class="col-8 font-light color-gray text-left">
									Cantidad piezas:
								</div>
								<div class="col font-light color-heavy text-right">

								</div>
							</div>
							<div class="row">
								<div class="col-8 font-light color-gray text-left">
									Tiempo de ciclo:
								</div>
								<div class="col font-light color-heavy text-right">

								</div>
							</div>
							<div class="row">
								<div class="col font-light color-gray text-left">
									Cliente:
								</div>
								<div class="col font-light color-heavy text-right">

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-white text-center shadow background-color-heavy" style="visibility: hidden;">
	  <div class="row" style="">
	  	<div class="col">
	  		<i class="fa fa-calendar fs-16" aria-hidden="true"></i>
	  		<a href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;"><b class="fs-13"></b></a>
	  	</div>
	  	<div class="col">
	  		{{-- <i class="fa fa-clock-o" aria-hidden="true" style="font-size:20px;"></i> --}}
	  		{{-- <a href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;"><b class="fs-13">40 HRS</b></a> --}}
	  	</div>
	  </div>
	</div>
</div>
