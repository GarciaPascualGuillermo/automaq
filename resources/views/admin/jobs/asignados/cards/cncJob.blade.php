<div id="cardCNC_{{$process->id}}" class="card jobs-card-sm color-gray font-light shadow card_jobs" style="height: 150px;">
	<div class="card-body valign-wrapper" style="margin-top:0px;margin-bottom:0px;padding: .2rem 1rem;">
		<div class="no-valign-wrapper">
			<div class="row">
				<div class="col">
					<div class="row fs-14" style="font-size: 13px;border-bottom: 1px solid #e8e8e8;">
						<div class="col font-regular text-left" style="color: black;">
							<b>Job:</b>
						</div>
						<div class="col font-regular color-heavy text-right">
							<b>{{ $process->job->num_job }}</b>
						</div>
						<div class="col-1" style="padding-right: 0px; padding-left: 0px;">
							<i class="fa fa-ellipsis-v fs-20" aria-hidden="true" style="color: black;"></i>
						</div>
					</div>
					<div class="row font-regular fs-12">
						<div class="col">
							<div class="row" style="font-size: 12px; word-break: break-all;">
								<div class="col-12 font-light color-heavy text-justify">
									#{{$process->operation->piece->part_number}}
								</div>
							</div>

							<div class="row">
								<div class="col font-light color-gray text-left">
									Operacion:
								</div>
								<div class="col font-light color-heavy text-right">
									{{ $process->operation->operation_service.'-'.substr($process->operation->description, 0,3) }}
								</div>
							</div>

							<div class="row">
								<div class="col-8 font-light color-gray text-left">
									Cantidad piezas:
								</div>
								<div class="col font-light color-heavy text-right">
									{{ $process->quantity > 0 ? $process->quantity : $process->job->quantity }}
								</div>
							</div>
							<div class="row">
								<div class="col-8 font-light color-gray text-left">
									Tiempo de ciclo:
								</div>
								<div class="col font-light color-heavy text-right">
									{{ $process->operation->standar_time }}
								</div>
							</div>
							<div class="row">
								<div class="col font-light color-gray text-left">
									Cliente:
								</div>
								<div class="col font-light color-heavy text-right">
									{{ str_limit($process->operation->piece->client->customer,8) }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-white text-center shadow background-color-heavy">
		<div class="row">
			<div class="col-4" style="padding-right: 0px; padding-left: 12px;">
				<i class="fa fa-calendar fs-18" aria-hidden="true" style="font-size:10px;"></i>&nbsp
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{{ $process->job->delivered_at->format('d m') }}</b></div>
			</div>
			<div class="col-2" style="padding-right: 0px; padding-left: 12px;">
				<i class="fa fa-clock-o" aria-hidden="true" style="font-size:10px;"></i>&nbsp
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{{ round((($process->job->quantity)*($process->operation->standar_time))/60),0}}</b></div>
			</div>
			<div class="col-6" style="padding-right: 0px; padding-left: 12px;">
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{!!$process->operation->work_center!!}</b></div>
			</div>
		</div>
	</div>
</div>
