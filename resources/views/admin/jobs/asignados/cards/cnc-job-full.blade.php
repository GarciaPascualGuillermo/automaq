<div id="cardCNC_{{$process->id}}" class="card jobs-card color-gray font-light shadow card_jobs" style="">
	<div class="card-body valign-wrapper" style="min-height: 100%;padding: 0 1rem 0 1rem;">
		<div class="no-valign-wrapper">
			<div class="row mt-2">
				<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
					<div class="row fs-18">
						<div class="col font-regular text-left" style="color: black;">
							<b>Job:</b>
						</div>
						<div class="col font-regular color-heavy text-right">
							<b>{{ $process->job->num_job }}</b>
						</div>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-right color-gray" >
					<i class="fa fa-ellipsis-v fs-20" aria-hidden="true" style="color: black;"></i>
				</div>
			</div>
			<div class="row font-regular">
				<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
					<hr>
					<div class="row" style="font-size: 12px;" >
						<div class="col-12 font-light color-heavy text-left" style="word-break: break-all;" >
							#
							{{$process->operation->piece->part_number}}
						</div>
					</div>

					<div class="row">
						<div class="col font-light color-gray text-left">
							Operación:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ $process->operation->operation_service . '-' . htmlentities(substr($process->operation->description, 0, 3)) }}
						</div>
					</div>

					<div class="row">
						<div class="col-8 font-light color-gray text-left">
							Cantidad piezas:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ $process->quantity > 0 ? $process->quantity : $process->job->quantity }}
						</div>
					</div>
					<div class="row">
						<div class="col-8 font-light color-gray text-left">
							Tiempo de ciclo:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ $process->operation->standar_time }}
						</div>
					</div>
					<div class="row">
						<div class="col font-light color-gray text-left">
							Cliente:
						</div>
						<div class="col font-light color-heavy text-right">
							{{ str_limit($process->operation->piece->client->customer,8) }}
						</div>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="margin-left: -30px;">
					<hr>
					<div class="single-chart" style="width: 8rem;">
						<svg viewBox="0 0 36 36" class="circular-chart blue">
							<path class="circle-bg"
							d="M18 2.0845
							   a 15.9155 15.9155 0 0 1 0 31.831
							   a 15.9155 15.9155 0 0 1 0 -31.831"
							/>
							<path class="circle value_circle"
							stroke-dasharray="0, 100"
							d="M18 2.0845
							  a 15.9155 15.9155 0 0 1 0 31.831
							  a 15.9155 15.9155 0 0 1 0 -31.831"
							 />
							<text id="text_circle" i x="18" y="20.35" class="percentage">0%</text>
						</svg>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-white text-center shadow background-color-heavy">
		<div class="row">
			<div class="col-4" style="padding-right: 0px; padding-left: 12px;">
				<i class="fa fa-calendar fs-18" aria-hidden="true"></i>&nbsp
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{{ $process->job->delivered_at->format('d m') }}</b></div>
			</div>
			<div class="col-2" style="padding-right: 0px; padding-left: 12px;">
				<i class="fa fa-clock-o" aria-hidden="true" style="font-size:20px;"></i>&nbsp
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{{ round((($process->job->quantity)*($process->operation->standar_time))/60),0}}</b></div>
			</div>
			<div class="col-6" style="padding-right: 0px; padding-left: 12px;">
				<div href="#" class="btn text-white" style="margin-bottom:0px;padding: 0px;font-size: 10px"><b class="fs-13">{!!$process->operation->work_center!!}</b></div>
			</div>
		</div>
	</div>

</div>
