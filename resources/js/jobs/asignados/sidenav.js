$(document).ready(function(){
	$('#arrow_left').click(function(){
		$('arrow_left').hide();
		$('#arrow_right').show();
		$('#arrow_right').addClass("arrow-open");
		$('#sidebar_close').hide();
		$('#main-content').addClass("open-contain-padding");
		$('#sidebar_open').show(); 
	});

	$('#arrow_right').click(function(){
		$('#arrow_right').removeClass("arrow-open");
		$('#arrow_right').hide();
		$('#arrow_left').show();
		$('#sidebar_open').hide();
		$('#main-content').removeClass("open-contain-padding"); 
		$('#sidebar_close').show(); 
	});


});