<?php

namespace Tests\Unit;

use App\Process;
use App\ProcessLog;
use App\Schedule;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TurnModuleTest extends TestCase
{
    use RefreshDatabase;

    public $user;

    public function setUp(): void
    {

        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /**
     * @test
     */
    function it_saving_tunr_when_a_process_log_it_created()
    {

        Carbon::setTestNow('2020-05-05 16:00:00');

        $weekNumber = now()->weekOfYear;


        $schedule = Schedule::create([
            'year'          => 2020,
            'week_number'   => $weekNumber,
            'start_of_week' => '2020-05-05',
            'first_turn'    => 'A',
            'second_turn'   => 'B'
        ]);

        ProcessLog::create([
            'activity' => 'pr',
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('process_logs', [
            'activity' => 'pr',
            'turn' => 'B'
        ]);

    }
    /**
     * @test
     */
    function it_saving_tunr_when_a_process_log_it_created_v2()
    {

        Carbon::setTestNow('2020-05-05 08:00:00');

        $weekNumber = now()->weekOfYear;

        Schedule::create([
            'year'          => 2020,
            'week_number'   => $weekNumber,
            'start_of_week' => '2020-05-05',
            'first_turn'    => 'A',
            'second_turn'   => 'B'
        ]);

        ProcessLog::create([
            'activity' => 'crp',
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('process_logs', [
            'activity' => 'crp',
            'turn' => 'A'
        ]);

    }
    /**
     * @test
     */
    function it_saving_turn_currectly_by_cr_logs_on_middle_journel()
    {


        Carbon::setTestNow('2020-05-05 09:00:00');

        $weekNumber = now()->weekOfYear;

        Schedule::create([
            'year'          => 2020,
            'week_number'   => $weekNumber,
            'start_of_week' => '2020-05-05',
            'first_turn'    => 'A',
            'second_turn'   => 'B'
        ]);

        $log = ProcessLog::create([
            'activity' => 'pr',
            'user_id' => $this->user->id,
            'created_at' => '2020-05-05 09:00:00'
        ]);

        Carbon::setTestNow('2020-05-05 15:00:00');

        ProcessLog::create([
            'activity' => 'crp',
            'parent_id' => $log->id,
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('process_logs', [
            'activity' => 'crp',
            'turn' => 'A'
        ]);

    }
    /**
     * @test
     */
    function it_saving_turn_currectly_by_cr_logs_on_middle_journel_v2()
    {
        Carbon::setTestNow('2020-05-05 15:00:00');

        $weekNumber = now()->weekOfYear;

        Schedule::create([
            'year'          => 2020,
            'week_number'   => $weekNumber,
            'start_of_week' => '2020-05-05',
            'first_turn'    => 'B',
            'second_turn'   => 'A'
        ]);

        $log = ProcessLog::create([
            'activity' => 'pr',
            'user_id' => $this->user->id,
        ]);

        $this->assertDatabaseHas('process_logs', [
            'activity' => 'pr',
            'turn' => 'A'
        ]);

    }
}
