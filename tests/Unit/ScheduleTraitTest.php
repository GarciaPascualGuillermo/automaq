<?php

namespace Tests\Unit;

use App\Traits\Schedule;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScheduleTraitTest extends TestCase
{
    use RefreshDatabase, Schedule;

    /**
     * @test
     */
    public function the_feed_method_it_working_on()
    {

        $start = Carbon::parse('2020-08-03');

        $this->assertTrue($this->feedScheduleTable($start));

        $start = Carbon::parse('2020-08-03');

        $result = $start->weekOfYear;

        $start_of_week = $start->startOfWeek();

        $number_of_weeks_in_the_year = now()->weeksInYear;

        do {

            if ($result % 2 == 0) {
                $group_in_morning = 'B';
                $group_in_afternoon = 'A';
            } else {
                $group_in_morning = 'A';
                $group_in_afternoon = 'B';
            }

            $this->assertDatabaseHas('schedules', [
                'year'          => 2020,
                'week_number'   => $result,
                'start_of_week' => $start_of_week->format("Y-m-d"),
                'first_turn'    => $group_in_morning,
                'second_turn'   => $group_in_afternoon,
            ]);

            $result++;
            $start_of_week->addWeek();

        } while ($result <= $number_of_weeks_in_the_year);

    }
}
