<?php

namespace Tests\Feature;

use App\Schedule;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScheduleModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {

        parent::setUp();

        $user = factory(User::class)->create();

        $this->actingAs($user);

    }
    /**
     * @test
     */
    function the_use_can_alternate_groups_by_weeks()
    {
        $this->withExceptionHandling();

        $schedule = Schedule::create([
            'year'          => 2020,
            'week_number'   => 20,
            'start_of_week' => '2020-05-05',
            'first_turn'    => 'A',
            'second_turn'   => 'B'
        ]);

        $this->putJson("/admin/schedule/alternate/{$schedule->id}")
        ->assertOk()
        ->assertJson([
            'message' => 'Se ha cambiado el orden de los turnos exitosamente'
        ]);

        $this->assertDatabaseHas('schedules', [
            'year'          => 2020,
            'week_number'   => 20,
            'start_of_week' => '2020-05-05',
            'first_turn'    => 'B',
            'second_turn'   => 'A'
        ]);
    }
    /**
     * @test
     */
    function user_can_generate_more_schedules_with_a_petition()
    {
        $this->withoutExceptionHandling();

        Carbon::setTestNow('2020-08-03');

        $number_of_week = now()->weekOfYear;

        $this->postJson('/admin/schedule/generate', [
            'to' => '2020-08-13'
        ])
        ->assertStatus(201)
        ->assertJson([
            'message' => 'Semanas generadas exitosamente'
        ])
        ;

        $this->assertDatabaseHas('schedules', [
            'week_number' => 32,
            'start_of_week' => '2020-08-03',
            'first_turn' => 'B',
            'second_turn' => 'A'
        ]);

        $this->assertDatabaseHas('schedules', [
            'week_number' => 33,
            'start_of_week' => '2020-08-10',
            'first_turn' => 'A',
            'second_turn' => 'B'
        ]);

    }

    /**
     * @test
     */
    function user_can_generate_more_schedules_with_a_petition_with_long_date()
    {
        $this->withoutExceptionHandling();

        Carbon::setTestNow('2020-11-03');

        $number_of_week = now()->weekOfYear;

        $this->postJson('/admin/schedule/generate', [
            'to' => '2021-01-15'
        ])
        ->assertStatus(201)
        ->assertJson([
            'message' => 'Semanas generadas exitosamente'
        ])
        ;


        $this->assertDatabaseHas('schedules', [
            'week_number' => 2,
            'start_of_week' => '2021-01-11',
            'first_turn' => 'B',
            'second_turn' => 'A'
        ]);

    }
}
