<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','firstname','username','photo','mealtime','idemployee','mealtime2', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','email_verified_at','deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'mealtime' => 'time',
        'mealtime2' => 'time',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    // protected $with = ['roles'];

    public function setPasswordAttribute($password)
    {
        if ($password!="" && !is_null($password)) {
            $this->attributes['password']=bcrypt($password);
        }
    }

    public function role_name($role_name)
    {
        return $this->roles->where('name',$role_name)->first();
    }


    // Relaciones
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }
    public function process_ias()
    {
        return $this->hasMany(Process::class);
    }
    public function process_fas()
    {
        return $this->hasMany(Process::class);
    }
    public function process_ils()
    {
        return $this->hasMany(Process::class);
    }
    public function process_ials()
    {
        return $this->hasMany(Process::class);
    }
    public function setup_ias()
    {
        return $this->hasMany(Setup::class);
    }
    public function setup_fas()
    {
        return $this->hasMany(Setup::class);
    }
    public function setup_ils()
    {
        return $this->hasMany(Setup::class);
    }
    public function setup_ials()
    {
        return $this->hasMany(Setup::class);
    }
    public function setups()
    {
        return $this->hasMany(Setup::class);
    }
    public function user_processes()
    {
        return $this->hasMany(UserProcess::class);
    }
    public function pause_opeadores()
    {
        return $this->hasMany(Pause::class,'operador_id','id');
    }
    public function pause_ends()
    {
        return $this->hasMany(Pause::class,'user_end_id','id');
    }
    public function pauses()
    {
        return $this->hasMany(Pause::class,'user_id','id');
    }
    public function statusworkcenters()
    {
        return $this->hasMany(StatusWorkcenter::class);
    }
    public function cycles()
    {
        return $this->hasMany(Cycle::class);
    }
    public function app_logs()
    {
        return $this->hasMany(AppLog::class);
    }
    public function workcenters()
    {
        return $this->belongsToMany(Workcenter::class)->withTimestamps();
    }

    /*function*/
    public function hasRoles($roles=[])
    {
        $roles=$this->roles->pluck('name')->intersect($roles);
        if ($roles->count()) {
            return true;
        }
        return false;
    }


    public function isAdmin()
    {
        return $this->hasRoles(['root']);
    }

    //Query Scope

    public function scopeName($query, $name='')
    {
        return $query
            ->where('users.idemployee','ILIKE',"%$name%")
            ->orWhere('users.name','ILIKE',"%$name%")
            ->orWhere('users.firstname','ILIKE',"%$name%")
            // ->where(DB::raw('CONCAT(users.idemployee,\' \',users.name,\' \',users.firstname)'),'ILIKE',"%$name%")
            ;
    }


    /**
    *  Broadcast
    */
    // public function receivesBroadcastNotificationsOn()
    // {
    //     return 'users.'.$this->id;
    // }

}
