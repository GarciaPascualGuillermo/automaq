<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Workcenter extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'num_machine', 'num_serie_tunerd', 'ip_address', 'brand', 'model', 'section', 'work_center', 'department', 'support', 'charge', 'order', 'on_maintenance'
    ];

    /**
    *  RELATIONS
    */

    public function processes()
    {
    	return $this->hasMany(Process::class);
    }
    public function setups()
    {
        return $this->hasMany(Setup::class);
    }
    public function pauses()
    {
        return $this->hasMany(Pause::class);
    }
    public function statusworkcenters()
    {
        return $this->hasMany(StatusWorkcenter::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    public function workcenter_logs()
    {
        return $this->hasMany(WorkcenterLog::class);
    }
    public function machinedays()
    {
        return $this->hasMany(MachineDay::class);
    }
    public function process_logs()
    {
        return $this->hasMany(ProcessLog::class);
    }
    public function scopeForDay($query, $day)
    {
        if ($day) {
            try {
                $day = Carbon::parse($day)->format('Y-m-d');
            } catch (\Throwable $th) {
                $day = null;
            }
        } else {
            $day = null;
        }

        if ($day) {
            $query
            ->whereHas('processes', function ($subquery) use ($day) {
                $subquery
                    ->whereDate('ih_created_at', $day)
                    ->orWhereDate('ih_created_at', $day)
                    ->orWhereDate('ia_created_at', $day)
                    ->orWhereDate('fa_created_at', $day)
                    ->orWhereDate('il_created_at', $day)
                    ->orWhereDate('ial_created_at', $day)
                    ->orWhereDate('ln_created_at', $day)
                    ->orWhereDate('ls_created_at', $day)
                    ->orWhereDate('pr_created_at', $day)
                    ->orWhereDate('cr_created_at', $day)
                    ;
            }, '>', 0)
            ;
        }
    }
}
