<?php

namespace App\Listeners;

use App\Events\OperadorAmountPieces;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OperadorAmountPiecesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OperadorAmountPieces  $event
     * @return void
     */
    public function handle(OperadorAmountPieces $event)
    {
        //
    }
}
