<?php

namespace App\Http\View\Composers;

use Illuminate\Contracts\View\View;
use App\Workcenter;

class NavComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $ip=request()->ip();
        // $ip="172.31.3.155"; # CT-109
        $workcenter=Workcenter::where('ip_address',$ip)->get();

        if ($workcenter->isEmpty())  $workcenter=new Workcenter();
        else $workcenter=$workcenter->first();

        $view->with('workcenter', $workcenter);
    }

}