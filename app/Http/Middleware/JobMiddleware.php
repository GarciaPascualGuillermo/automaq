<?php

namespace App\Http\Middleware;


use App\Job;
use Closure;
use App\Process;
use Illuminate\Support\Carbon;

class JobMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();
        if($user->hasRoles(['root', 'gte.operacion','jefe.produccion','ceo','sup.produccion','sup.linea','ajustador','planeacion','cuenta','jefe.mantenimiento'])){
            return $next($request);
        }
        return abort (401);
    }
}
