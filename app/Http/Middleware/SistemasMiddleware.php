<?php

namespace App\Http\Middleware;

use Closure;

class SistemasMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if(!$user->hasRoles(['sistemas', 'root']))
            return abort (401);
        
        else
        return $next($request);
    }
}
