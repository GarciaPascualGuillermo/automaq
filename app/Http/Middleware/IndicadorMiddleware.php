<?php

namespace App\Http\Middleware;

use Closure;

class IndicadorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();
        if(!$user->hasRoles(['ceo','root','gte.admin','gte.operacion','jefe.produccion','sistemas']))
            return abort(401);

        return $next($request);
    }
}
