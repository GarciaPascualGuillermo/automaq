<?php

namespace App\Http\Middleware;

use Closure;

class DepMantenimientoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();
        if($user->hasRoles(['root', 'dep.mantenimiento','jefe.mantenimiento','tec.mantenimiento','ceo','gte.operacion','jefe.produccion','sup.produccion'])){

        return $next($request);

        }
        return abort (401);
    }
}
