<?php

namespace App\Http\Middleware;

use Closure;

class HistorialCalidadMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user->hasRoles(['root','ceo','gte.admin','gte.operacion','jefe.produccion','sup.produccion','sistemas','gte.calidad'])) 
            return $next($request);

        return abort(403);
    }
}
