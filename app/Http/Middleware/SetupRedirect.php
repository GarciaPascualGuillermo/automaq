<?php

namespace App\Http\Middleware;

use App\Events\PhaseUpdated;
use App\Job;
use Closure;
use App\Process;
use Illuminate\Support\Carbon;

class SetupRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();
        if (!$user->hasRoles(['root','operador','sup.produccion']))  return abort(401);

        $process=Process::with(['user_processes'=>function ($query_UP)use ($user){
                $query_UP->where('user_id',$user->id)
                    ->where('start_at',Carbon::now())
                    ;
            }])->where('id',session('process_id'))
            ->limit(1)
            ->get();
        if ($process->isEmpty() || $process->first()->user_processes->count()<1 ) {
            return redirect()->route('jobs.asignados.index');
        }else $process=$process->first();
        // dd($request->route()->getName(),$next,$process->status);
        $routeName=$request->route()->getName();

        $routeRedirectName='operador.setup.index';
        $routeRedirectNames=collect([
            $routeRedirectName,
            'operador.setup.step2'
        ]);
        switch ($process->status) {
            case null:
                $routeRedirectName='operador.setup.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    // 'operador.setup.step2'
                ]);
                break;
            case 'ih':
                $routeRedirectName='operador.setup.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    'operador.setup.step2proceso',
                ]);
                break;
            case 'fh':
                $routeRedirectName='operador.setup.step2';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    // 'operador.setup.step2proceso',
                ]);
                break;
            case 'ia':
                // $routeRedirectName='operador.setup.step2';
                $routeRedirectName='operador.setup.step2proceso';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    'operador.setup.step3.calidad',
                    'operador.setup.step3.autoliberacion'
                ]);
                break;
            case 'fa':
                $routeRedirectName='operador.setup.step2proceso';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    // 'operador.setup.step4.rechazada',
                    // 'operador.setup.step4.liberada'
                ]);
                break;
            /**
            *  Liberacion por calidad
            */
            case 'il':
                $routeRedirectName='operador.setup.step3.calidad';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    // 'operador.setup.step4.rechazada',
                    // 'operador.setup.step4.liberada'
                ]);
                break;
            /**
            *  Autoliberacion
            */
            case 'ial':
                $routeRedirectName='operador.setup.step3.autoliberacion';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    // 'operador.setup.step4.rechazada',
                    // 'operador.setup.step4.liberada'
                ]);
                break;
            case 'ln':
                $routeRedirectName='operador.setup.step4.rechazada';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                    // 'operador.setup.step2proceso',
                ]);
                break;
            case 'ls':
                $routeRedirectName='operador.setup.step4.liberada';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
            case 'pr':
                $routeRedirectName='operador.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
            case 'crp':
                $routeRedirectName='operador.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
            case 'pm':
                $routeRedirectName='operador.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
            case 'pmp':
                $routeRedirectName='operador.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
            case 'pfh':
                $routeRedirectName='operador.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
            case 'pfm':
                $routeRedirectName='operador.index';
                $routeRedirectNames=collect([
                    $routeRedirectName,
                ]);
                break;
        }
        if ($routeRedirectNames->contains($routeName)){
            return $next($request);
        }
        else
            return redirect()->route($routeRedirectName);
    }
}
