<?php

namespace App\Http\Middleware;

use Closure;

class OperadorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();
        if($user->hasRoles(['operador','root','sup.produccion'])){
            return $next($request);
        }
        
        return abort (401);
    
    }
}
