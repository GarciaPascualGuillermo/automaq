<?php

namespace App\Http\Middleware;

use Closure;

class HistorialMantenimientoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if(!$user->hasRoles(['root','ceo','gte.admin','gte.operacion','jefe.produccion','sup.produccion','sistemas','jefe.mantenimiento','tec.mantenimiento'])) return abort(403);
        return $next($request);
    }
}
