<?php

namespace App\Http\Middleware;

use Closure;

class DepCalidadMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();
        if($user->hasRoles(['root', 'dep.calidad','gte.calidad','insp.calidad','ceo','gte.operacion','jefe.produccion','sup.produccion','sup.linea'])){

        return $next($request);

        }
        return abort (401);
    }
}
