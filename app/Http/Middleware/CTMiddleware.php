<?php

namespace App\Http\Middleware;

use Closure;

class CTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // $job= \App\Job::whereNotNull('id')->limit(1)->first();
        // session()->put('job_id',$job->id);
        // session()->save();
        
        return $next($request);
    }
}
