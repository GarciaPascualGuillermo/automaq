<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginOperadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'noempleado' => ['required', 'numeric','min:0'],
            'password' => ['required', 'string'],
        ];
    }
    public function messages()
    {
        return[
            'noempleado.required'=>'El campo número de empleado es requerido',
            'noempleado.numeric'=>'El campo número de empleado debe ser un numero',
            'noempleado.min'=>'El campo número de empleado no debe ser un numero negativo',
            'password.required'=>'El campo contraseña es requerido',
        ];
    }
}
