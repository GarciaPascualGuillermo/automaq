<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user_id') ?? 0;

        return [
            'name' => 'required|string|min:2|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'idemployee' => 'required|string|max:255|unique:users,idemployee,'.$id,
            'password' => 'nullable|min:4|confirmed',
            'firstname' => 'nullable|string|max:255',
            'roles'=> 'required|array',
            'photo'=> 'nullable|file',
            'mealtime' => 'required|string',
            'mealtime2' => 'required|string',
        ];
    }
}
