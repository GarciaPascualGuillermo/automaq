<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WCRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id=$this->route('ct_id')??0;

        return [
            'num_machine' => 'required|string|min:1|max:255|unique:workcenters,num_machine,'.$id,
            'num_serie_tunerd' => 'string|nullable|max:255',
            'ip_address' => 'string|nullable|max:255',
            'brand' => 'string|nullable|max:255',
            'model' => 'string|nullable|max:255',
            'section'=>'required|string|min:1|max:255',
            'work_center'=>'required|string|min:1|max:255',
            'department'=>'required|string|min:1|max:255',
        ];
    }
}
