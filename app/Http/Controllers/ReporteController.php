<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Job;
use App\Role;
use App\Pause;
use App\Cycle;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ReporteController extends Controller
{
    function __construct()
	{
		$this->middleware(['auth']);
	}

    public function reporte(Request $request)
    {
        $num_job=$request->input('num_job');
    	$part_number=$request->input('part_number');
        if (is_null($num_job) && is_null($num_job)) {
            return view('admin.reportes.index')
                ->with('num_job',$num_job)
                ->with('part_number',$part_number)
                ->with('processes_operation',[])
                ->with('processes_operador',[])
                ->with('processes_setup',[])
                ;
        }
        $jobs=Job::where('num_job',$num_job)
            ->select('jobs.num_job','p.ih_created_at','op.operation_service','pi.part_number')
            ->join('processes as p','p.job_id','=','jobs.id')
            ->join('operations as op','op.id','=','p.operation_id')
            ->join('pieces as pi','pi.id','=','op.piece_id')
            ->orderBy('op.operation_service','ASC')
            ->get();
        if (is_null($part_number)) {
            $part_number=$jobs->first()->part_number;
        }


        $processes_operation=[];
        $processes_operador=[];
        $processes_setup=[];
        foreach ($jobs as $job) {
            $processes=Process::select(
                'j.num_job','processes.ih_created_at','op.operation_service as operation','pi.part_number',
                'wc.num_machine','processes.*','j.quantity as qua',
                'a_ia.idemployee as a_ia_idemployee','a_ia.name as a_ia_name','a_ia.firstname as a_ia_firstname',
                'a_fa.idemployee as a_fa_idemployee','a_fa.name as a_fa_name','a_fa.firstname as a_fa_firstname',
                'a_il.idemployee as a_il_idemployee','a_il.name as a_il_name','a_il.firstname as a_il_firstname',
                'a_ial.idemployee as a_ial_idemployee','a_ial.name as a_ial_name','a_ial.firstname as a_ial_firstname'
                // ,DB::raw("CONCAT(a_ia.idemployee,' ',a_ia.name,' ',a_ia.firstname) as ajustador_ia")
                )
                ->join('jobs as j','j.id','=','processes.job_id')
                ->join('workcenters as wc','wc.id','=','processes.workcenter_id')
                ->join('operations as op','op.id','=','processes.operation_id')
                ->join('pieces as pi','pi.id','=','op.piece_id')
                ->join('users as a_ia','a_ia.id','=','processes.ajustador_ia_id')
                ->join('users as a_fa','a_fa.id','=','processes.ajustador_fa_id')
                ->leftJoin('users as a_il','a_il.id','=','processes.ajustador_il_id')
                ->leftJoin('users as a_ial','a_ial.id','=','processes.ajustador_ial_id')
                ->where('j.num_job',$num_job)
                ->where('op.operation_service',$job->operation_service)
                ->orderBy('op.operation_service','ASC')
                ->get();
            if ($processes->isNotEmpty() && $processes->count() >= 1 ) {
                $processes_operation[]=$processes;
                foreach ($processes as $process) {
                    $user_processes = UserProcess::select(
                            'user_processes.status_start','user_processes.status_end','user_processes.start_time','user_processes.end_time',
                            'user_processes.productivity','user_processes.start','user_processes.end','user_processes.scraps',
                            'up_u.idemployee as up_u_idemployee','up_u.name as up_u_name','up_u.firstname as up_u_firstname'
                        )
                        ->join('users as up_u','up_u.id','=','user_processes.user_id')
                        ->where('process_id',$process->id)
                        ->whereIn('status_start',['ls','pr','crp'])
                        ->orderBy('user_processes.created_at','ASC')
                        ->get();
                    if ($user_processes->isNotEmpty() && $user_processes->count()>=1) $processes_operador[$process->id]=$user_processes;
                    $rejected = Process::select(
                            'processes.*'
                            // ,'r_p.aceptada','r_p.fullname'/*, DB::raw('count(r_p.aceptada) as set')*/
                            ,'up_u.idemployee as up_u_idemployee','up_u.name as up_u_name','up_u.firstname as up_u_firstname'

                        )
                        ->join('user_processes as up','up.process_id','=','processes.id')
                        ->join('users as up_u','up_u.id','=','up.user_id')
                        ->whereNotIn('status_start',['cr','pr','crp'])
                        // ->leftJoin('rejected_processes as r_p','r_p.process_id','=','processes.id')
                        ->where('processes.id',$process->id)
                        ->get();
                        ;
                    if ($rejected->isNotEmpty()) {
                        $processes_setup[$process->id]=$rejected;
                    }
                }

            }

        }

        // $processes_operation=collect($processes_operation);
        // dd($processes_operation);


    	return view('admin.reportes.index')
            ->with('num_job',$num_job)
            ->with('part_number',$part_number)
            ->with('processes_operation',$processes_operation)
            ->with('processes_operador',$processes_operador)
            ->with('processes_setup',$processes_setup)

            // ->with('process',$process)
            ;


    }
    public function eficiencia(Request $request)
    {
        $num_job=$request->input('num_job');
        $part_number=$request->input('part_number');
        if (is_null($num_job) && is_null($num_job)) {
            return view('admin.reportes.eficiencia')
                ->with('num_job',$num_job)
                ->with('part_number',$part_number)
                ->with('processes_operation',[])
                ->with('processes_operador',[])
                ->with('processes_setup',[])
                ;
        }
        $jobs=Job::where('num_job',$num_job)
            ->select('jobs.num_job','p.ih_created_at','op.operation_service','pi.part_number')
            ->join('processes as p','p.job_id','=','jobs.id')
            ->join('operations as op','op.id','=','p.operation_id')
            ->join('pieces as pi','pi.id','=','op.piece_id')
            ->orderBy('op.operation_service','ASC')
            ->get();
        if (is_null($part_number)) {
            $part_number=$jobs->first()->part_number;
        }


        $processes_operation=[];
        $processes_operador=[];
        $processes_setup=[];
        foreach ($jobs as $job) {
            $processes=Process::select(
                'j.num_job','processes.ih_created_at','op.operation_service as operation','pi.part_number',
                'wc.num_machine','processes.*','j.quantity as qua',
                'a_ia.idemployee as a_ia_idemployee','a_ia.name as a_ia_name','a_ia.firstname as a_ia_firstname',
                'a_fa.idemployee as a_fa_idemployee','a_fa.name as a_fa_name','a_fa.firstname as a_fa_firstname',
                'a_il.idemployee as a_il_idemployee','a_il.name as a_il_name','a_il.firstname as a_il_firstname',
                'a_ial.idemployee as a_ial_idemployee','a_ial.name as a_ial_name','a_ial.firstname as a_ial_firstname'
                // ,DB::raw("CONCAT(a_ia.idemployee,' ',a_ia.name,' ',a_ia.firstname) as ajustador_ia")
                )
                ->join('jobs as j','j.id','=','processes.job_id')
                ->join('workcenters as wc','wc.id','=','processes.workcenter_id')
                ->join('operations as op','op.id','=','processes.operation_id')
                ->join('pieces as pi','pi.id','=','op.piece_id')
                ->join('users as a_ia','a_ia.id','=','processes.ajustador_ia_id')
                ->join('users as a_fa','a_fa.id','=','processes.ajustador_fa_id')
                ->leftJoin('users as a_il','a_il.id','=','processes.ajustador_il_id')
                ->leftJoin('users as a_ial','a_ial.id','=','processes.ajustador_ial_id')
                ->where('j.num_job',$num_job)
                ->where('op.operation_service',$job->operation_service)
                ->orderBy('op.operation_service','ASC')
                ->get();
            if ($processes->isNotEmpty() && $processes->count() >= 1 ) {
                $processes_operation[]=$processes;
                foreach ($processes as $process) {
                    $user_processes = UserProcess::select(
                            'user_processes.status_start','user_processes.status_end','user_processes.start_time','user_processes.end_time',
                            'user_processes.productivity','user_processes.start','user_processes.end','user_processes.scraps',
                            'up_u.idemployee as up_u_idemployee','up_u.name as up_u_name','up_u.firstname as up_u_firstname'
                        )
                        ->join('users as up_u','up_u.id','=','user_processes.user_id')
                        ->where('process_id',$process->id)
                        ->whereIn('status_start',['ls','pr','crp'])
                        ->orderBy('user_processes.created_at','ASC')
                        ->get();
                    if ($user_processes->isNotEmpty() && $user_processes->count()>=1) $processes_operador[$process->id]=$user_processes;
                    $rejected = Process::select(
                            'processes.*'
                            // ,'r_p.aceptada','r_p.fullname'/*, DB::raw('count(r_p.aceptada) as set')*/
                            ,'up_u.idemployee as up_u_idemployee','up_u.name as up_u_name','up_u.firstname as up_u_firstname'

                        )
                        ->join('user_processes as up','up.process_id','=','processes.id')
                        ->join('users as up_u','up_u.id','=','up.user_id')
                        ->whereNotIn('status_start',['cr','pr','crp'])
                        // ->leftJoin('rejected_processes as r_p','r_p.process_id','=','processes.id')
                        ->where('processes.id',$process->id)
                        ->get();
                        ;
                    if ($rejected->isNotEmpty()) {
                        $processes_setup[$process->id]=$rejected;
                    }
                }

            }

        }

        // $processes_operation=collect($processes_operation);
        // dd($processes_operation);


        return view('admin.reportes.eficiencia')
            ->with('num_job',$num_job)
            ->with('part_number',$part_number)
            ->with('processes_operation',$processes_operation)
            ->with('processes_operador',$processes_operador)
            ->with('processes_setup',$processes_setup)
            ;


    }

}
