<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReciveDataCTController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function reciveData(Request $request)
    {
        $data=$request->input('json_payload');
        return $data;
    }
}
