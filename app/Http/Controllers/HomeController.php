<?php

namespace App\Http\Controllers;

use App;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('registerToken');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        dd("hh");
        // return redirect()->route('jobs.asignados');
        return $this->redirect(auth()->user());
    }
    private function redirect($user)
    {
        if ($user->hasRoles(['root'])) {
            return redirect()->route('maquinas.index');
            return redirect()->intended();
        }else if ($user->hasRoles(['sup.produccion'])) {
            // dd(!is_null(session('wc')),session('wc'));
            if (!is_null(session('wc'))) return redirect()->route('jobs.asignados.index');
            else return redirect()->route('maquinas.index');

        }if ($user->hasRoles(['operador'])) {
            return redirect()->route('jobs.asignados.index');
        }else if ($user->hasRoles(['ceo'])) {
            return redirect()->route('indicadores.index');
        }else if ($user->hasRoles(['dep.calidad'])) {
            return redirect()->route('calidad.liberacion');
        }elseif($user->hasRoles(['dep.mantenimiento'])){
            return redirect()->route('mantenimiento.index');
        }elseif ($user->hasRoles(['jefe.mantenimiento','tec.mantenimiento'])) {
            return redirect()->route('calidad.liberacion');
        }elseif ($user->hasRoles(['sistemas'])) {
            return redirect()->route('usuarios.index');
        }elseif($user->hasRoles(['gte.operacion','jefe.produccion','ceo','sup.produccion'])){
            return redirect()->route('maquinas.index');
        }
        return redirect()->route('maquinas.index');

    }
    public function reporteador(String $routeRedirect = '')
    {

        $token = Str::random(100);

        auth()->user()->update([
            'token' => $token
        ]);

        Auth::logout();

        if (App::environment('local')) {
            $url = "http://reporteador.test/open-session/{$token}/$routeRedirect";
        } else {
            $url = "http://automaq-reporteador.tecmaq.local/";
        }
        return redirect($url);

    }

}
