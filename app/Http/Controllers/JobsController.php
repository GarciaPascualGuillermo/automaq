<?php

namespace App\Http\Controllers;

use App\Events\DesasignarEvent;
use App\Events\PhaseUpdated;
use App\Job;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use App\WorkcenterLog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
// use DB;
use Illuminate\Support\Facades\DB;


class JobsController extends Controller
{
	function __construct()
	{
		$this->middleware(['auth']);
		$this->middleware(['job.middleware'])->only(['index']);
		$this->middleware(['operador.middleware'])->only(['selecciona']);

	}
	public function index(Request $request)
	{

        $workcenter_section = strtoupper($request->input("section", "S3"));

		$workCenters = WorkCenter::where('section', $workcenter_section)
			->orderBy('num_machine','ASC')
            ->where('num_machine', 'LIKE', 'CT-07')
            ->get();

        return view('admin.jobs.index', compact('workCenters'));

	}

	public function asignados(Request $request)
	{
		// if (session()->has('process_id')) {
		// 	return redirect()->route('operador.setup.index');
        // }

        if (!$request->user()->hasRoles(['operador', 'tec.mantenimiento', 'root'])) {
            abort(403);
        }

        $ip = request()->ip();

		$wc = Workcenter::select(['workcenters.*', 'processes.id as p_id', 'processes.order'])
			->join('processes','workcenters.id', '=', 'processes.workcenter_id')
			->join('jobs','processes.job_id','=','jobs.id')
			->where('ip_address', $ip)
			->where(function ($q) {
				$q->where('status', '!=', 'cr')
					->orWhereNull('status');
			})
            ->orderBy('processes.order','ASC')
            ->get();

		if ($wc->isEmpty()) {
            $wc = Workcenter::where('ip_address', $ip)->limit(1)->get() ;
        }

		if ($wc->isEmpty()) {
			abort(404);
        }

		$processes = Process::with(['operation'=>function ($q2){
					$q2->with(['piece'=>function ($q_piece){
						$q_piece->with('client');
					}]);
				}])
				->with('job')
				->orderBy('processes.order','ASC')
				->whereIn('id',$wc->pluck('p_id'))
				->limit(6)
				->get();

        $p_first = $processes->first();

        $workcenter_log = WorkcenterLog::where('ip', $wc->first()->ip_address)
        ->where('description', 'reassign')
        ->orderBy('id', 'DESC')
        ->first();

        if ($workcenter_log && $workcenter_log->description == 'reassign') {
            session()->put('info', 'El job que se estaba ejecutando en este workcenter, ha vuelto a la cola');
            $workcenter_log->update([
                'description' => 'reassign readed'
            ]);
        } else {
            session()->forget('info');
        }

        if ($wc->first()) {
            if ($wc->first()->on_maintenance)  {
                $wc->load(['pauses' => function ($query)
                {
                    $query
                        ->orderBy('created_at','desc')
                        ->whereNull('ended_at')
                        ->limit(1)
                        ;
                }]);
            }
        }

		return view('admin.jobs.asignados')
			->with('processes',$processes)
			->with('p_first',$p_first)
			->with('wc', $wc)
			;
	}
	public function selecciona(Request $request)
	{
		$process = Process::with(['user_processes' => function ($query){
		        $query->with(['cycles'/*=>function ($queryC){
		        	$queryC->where('process_status','pr');
		        }*/])
		        ->where('user_id',auth()->user()->id)
		    	// ->where('log_out',false)
		        ;
		    }])
		    ->where('id', $request->input('process_id'))
		    ->first();

        if (session()->has('process_id')) {
            return response()->json($process, 201);
        }

		$getCycles = getCycles($process);
		$total_cycles = $getCycles['pieceTotal'];
		$user_process = UserProcess::where('process_id', $process->id)
			// ->where('user_id',auth()->user()->id)
			// ->where('start_at',Carbon::now())
            ->get();

		$setup = $process->setups->where('active', true)->where('workcenter_id', $process->workcenter_id);

		if ($setup->isEmpty() ) {
			$setup = $process->setups()->create([
				'workcenter_id' => $process->workcenter_id,
				'user_id' => auth()->user()->id,
			]);
		} else {
			$setup = $setup->first();
        }

		DB::table('setup')->where('process_id', $process->id)
			->where('workcenter_id', $process->workcenter_id)
			->where('id', '!=', $setup->id)
			->update(['active' => false]);

		foreach ($user_process as $u_process) {
			$u_process->active = false;
			$u_process->save();
        }

		$user_proce = $user_process
			->where('user_id', auth()->user()->id)
			->where('start_at', date('Y-m-d') . " 00:00:00")
			->where('log_out', false)
			;
        // return [$user_proce,$user_process,auth()->user()->id];

		if ( $user_proce->isEmpty() ) {

			$user_proce = $process->user_processes()->create([
				'user_id'       => auth()->user()->id,
				'start'         => $total_cycles,
				'start_at'      => now(),
				'start_time'    => now(),
				'status_start'  => is_null($process->status) ? 'ih' : $process->status,
				'active'        => true
            ]);

		} else {

			$user_proce = UserProcess::find($user_proce->first()->id);
			$user_proce->active = true;
			// $user_proce->start=$total_cycles;
			$user_proce->save();
		}
		$wc = Workcenter::with(['processes' => function ($query){
				$query->orderBy('order','asc');
            }])
            ->where('id', $process->workcenter_id)
			->limit(1)
            ->get();

        $aumenta = true;

		foreach ($wc->first()->processes as $p) {

			if ($aumenta) {
				$p->order++;
            }

			if ($p->id === $process->id) {

				if($p->status === 'crp') {
                    $p->status = 'pr';
                }

				$p->active = true;
				$p->order = 0;
                $aumenta = false;

				if (is_null($p->status) || (!is_null($p) && $p->status == 'cr')) {
					$p->status = 'ih';
					$setup->ih_created_at = $p->ih_created_at = now();
                }

                $p->process_log()->create([
                    'activity' => $p->status,
                    'user_id' => auth()->user()->id,
                    'parent_id' => parent_id($p),
                    'user_process_id' => $user_proce->id,
                    'workcenter_id' => $p->workcenter_id
                ]);

            } else {
                $p->active = false;
            }

			$setup->save();
			$p->save();
        }

        $process->fresh();

        event(new PhaseUpdated());

		session()->forget('job_id');
		session()->put('job_id',$process->job->id);
		session()->forget('process_id');
		session()->put('process_id',$process->id);
		session()->forget('user_process_id');
		session()->put('user_process_id',$user_proce->id);
		session()->forget('setup_id');
		session()->put('setup_id',$setup->id);
		session()->save();

		return response()->json($process, 201);
	}

	public function maquinas1()
	{
		return view('admin.jobs.maquinas1');
    }
    public function desasignar(Request $request)
    {
        $request->validate([
            'process_id' => 'exists:processes,id|required'
        ]);

        $process = Process::where('id' ,$request->process_id)
        ->with(['user_processes' => function ($query) {
            $query
            ->where('active', true)
            ->where('log_out', false)
            ->orderBy('created_at', 'DESC')
            ->first();
        }])
        ->first();

        $user_process = $process->user_processes->first();

        $cycles = getCycles($process);

        DB::beginTransaction();

        try {

            if ($user_process) {

                $user_process->update([
                    'log_out' => true,
                    'active' => false,
                    'end_time' => now(),
                    'status_end' => 'crp',
                    'end' => $cycles['pieceTotal']
                ]);

            }

            $processes = $process->workcenter->processes()->orderBy('order', 'ASC')->get();

            $index = 1;

            foreach ($processes as $workcenter_process) {
                if ($workcenter_process->is($process)) {
                    continue;
                }
                $workcenter_process->update([ 'order' => $index ]);
                $index++;
            }

            $process->update([
                'status'    => null,
                'active'    => false,
                'order'     => 0
            ]);

            $process->process_log()->create([
                'activity' => 'crp',
                'comment' => 'Desasignado por el gerente',
                'user_id' => auth()->user()->id,
                'parent_id' => parent_id($process),
                'user_process_id' => $user_process ? $user_process->id : null,
                'workcenter_id' => $process->workcenter_id
            ]);

            WorkcenterLog::create([
                'workcenter_id' => $process->workcenter->id,
                'description'   =>  'reassign',
                'ip'            => $process->workcenter->ip_address
            ]);

            DB::commit();

        } catch (\Throwable $th) {

            DB::rollBack();
            return response()->json($th, 401);

        }

        event(new DesasignarEvent($process));

        return response()->json([
            'message' => 'El proceso a sido movido a la cola exitosamente'
        ]);
    }



    
    public function desasignarReaded(Request $request)
    {
        $workcenter = Process::find($request->process_id);

        $workcenter_log = WorkcenterLog::where('ip', $workcenter->first()->ip_address)
        ->where('description', 'reassign')
        ->orderBy('id', 'DESC')
        ->first();

        if ($workcenter_log && $workcenter_log->description == 'reassign') {
            // Removiendo datos de la sesión actual
            $workcenter_log->update([
                'description' => 'reassign readed'
            ]);
        }
        return response()->json([
            'msg' => 'Notificacion leida'
        ]);
    }

    public function solicitarAyuda(Request $request)
    {
        $workcenter = Process::find($request->process_id);
        $message = $request;
        $name = $request->input('process.id');

       
        Log::info('fin'.$name);
        foreach($message as $arr){

            //foreach($criterio as $arr){
            //$id_var= $arr;
            Log::info('message'.$arr[0]);
            //Log::error($id_var); 

        }
            
        //DB::insert('insert into helpsystem  values (DEFAULT,?)', [$request->process_id]);

        /*

        
        $workcenter_log = WorkcenterLog::where('ip', $workcenter->first()->ip_address)
        ->where('description', 'reassign')
        ->orderBy('id', 'DESC')
        ->first();
        

       
        if ($workcenter_log && $workcenter_log->description == 'reassign') {
            // Removiendo datos de la sesión actual
            $workcenter_log->update([
                'description' => 'reassign readed'
            ]);
        }*/
        return response()->json([
            'msg' => 'Notificacion leida'
        ]);
    }
    public function flushSession(Request $request)
    {
        $request->session()->forget('process_id');

        return redirect()->route('jobs.asignados.index')->withSuccess('Cache vaciada')->withMessage(request('message'));
    }

}
