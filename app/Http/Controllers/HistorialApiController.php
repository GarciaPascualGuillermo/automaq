<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Workcenter;
use App\MachineDay;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class HistorialApiController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
	}

	public function maquinas_semanal(Request $request)
	{
		$times = new Collection();
		$workcenter_section=strtoupper($request->input("section",""));
		$workcenters= WorkCenter::where('section','like', $workcenter_section."%")
			->where('num_machine','CT-115')
			->orderBy('section','ASC')
			->orderBy('num_machine','ASC')
			->get();
		$start=Carbon::createFromFormat('Y-m-d','2019-10-28');
		// dd(getTimeSemanalMaquinas($workcenters->first(),$start));

		$user_process = UserProcess::select("created_at")->orderBy('created_at','ASC')->limit(1)->first();
		if (is_null($user_process)) return response()->json(['msg'=>'no data'],200);
		$limit = $user_process->created_at->startOfWeek();

		foreach ($workcenters as $wc) {
			$week_start=Carbon::now()->subWeek()->startOfWeek();
			$times->put($wc->num_machine,new Collection());
			$i=0;
			while ( $limit->lessThanOrEqualTo($week_start)) {
				// if ($i==2) {
				// 	dd(getTimeSemanalMaquinas($wc,$week_start));
				// }
				$times[$wc->num_machine]->push(getTimeSemanalMaquinas($wc,$week_start));
				$week_start->subWeek();
				$i++;
			}
		}

		return $times['CT-115']->pluck('setup');
	}
	public function maquinas_dia(Request $request)
	{
		$workcenter_section=strtoupper($request->input("section","S"));
		$start=Carbon::createFromFormat('Y-m-d','2019-10-28');
		$machine_days=MachineDay::select(
				'machine_days.day','machine_days.ajuste','machine_days.herramentaje','machine_days.liberacion',
				'machine_days.autoliberacion','machine_days.produccion','machine_days.mantenimiento',
				'machine_days.pausa_otros','machine_days.sinuso','wc.num_machine'
			)
			->join('workcenters as wc','wc.id','=','machine_days.workcenter_id')
			->where('wc.section','like', $workcenter_section."%")
			->orderBy('machine_days.day','DESC')
			->orderBy('wc.order','ASC')
			->get();
		$workcenters=$machine_days->pluck('num_machine')->unique();
		$limit=Carbon::createFromFormat('Y-m-d H:i:s',$machine_days->last()->day);
		$times=new Collection();
		foreach ($workcenters as $wc) {
			$now=Carbon::now()->subDay()->startOfDay();
			while ($limit->lte($now)) {
				$machine=$machine_days->where('num_machine',$wc)->where('day',$now);
				if ($machine->isEmpty()) {
					$times->put($wc,collect(['setup'=>0,'produccion'=>0,'sinuso'=>0]));
				}else{
					$machine=$machine->first();
					$times->put($wc,collect([
						'setup' => $machine->ajuste+$machine->herramentaje+$machine->liberacion+$machine->autoliberacion,
						'produccion' => $machine->produccion,
						'sinuso' => $machine->sinuso
					]));
				}
				$now->subDay();
			}
		}

		// $setup=$ajuste+$herramentaje+$liberacion+$autoliberacion;
		return response()->json($times,200);
	}
	public function operadores_semanal(Request $request)
	{
		// $name = "";
		$name = $request->get('name');

		$users = User::select("users.*",'r.name as role_name')
			->join('role_user as r_u','r_u.user_id','=','users.id')
			->join('roles as r','r_u.role_id','=','r.id')
			// ->join('user_processes as u_p','u_p.user_id','=','users.id')
			->where('r.name','operador')
			->orderBy('users.idemployee','DESC')
			->name($name)
			->get();
		if($users->isEmpty()) return abort(404);
		$user_processes=UserProcess::whereIn('user_id',$users->pluck('id'))
			->orderBy('created_at','DESC')
			->get();
		if($user_processes->isEmpty()) return abort(404);
		$limit=Carbon::createFromFormat('Y-m-d H:i:s',UserProcess::select('created_at')->orderBy('created_at','ASC')->first()->created_at);

		$users_headers=new Collection();
		$users_content=new Collection();
		foreach ($users as $user) {
			$name=$user->idemployee." ".$user->name." ".$user->firstname;
			$week_start = Carbon::now()->startOfWeek()->subWeek();
			$users_headers->push($name);
			$users_content->put( $name , new Collection());
			while ($limit->lessThanOrEqualTo($week_start)) {
				$prom=$user_processes
					->whereBetween('created_at',[$week_start->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString()])
					->where('user_id',$user->id)
					;
				if($prom->isEmpty()) $users_content[$name]->push(0);
				else $users_content[$name]->push(round($prom->avg('productivity'),0,PHP_ROUND_HALF_DOWN));

				// return response()->json([$week_start->startOfWeek()->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString(),$prom,$prom->avg('productivity')],200);
				$week_start->startOfWeek()->subWeek();
			}
		}

		return response()->json([$users_headers,$users_content],200);
	}
	public function operadores_dia(Request $request)
	{
		// $name = "";
		$name = $request->get('name');

		$users = User::select("users.*",'r.name as role_name')
			->join('role_user as r_u','r_u.user_id','=','users.id')
			->join('roles as r','r_u.role_id','=','r.id')
			// ->join('user_processes as u_p','u_p.user_id','=','users.id')
			->where('r.name','operador')
			->orderBy('users.idemployee','DESC')
			->name($name)
			->get();
		if($users->isEmpty()) return abort(404);
		$user_processes=UserProcess::whereIn('user_id',$users->pluck('id'))
			->orderBy('created_at','DESC')
			->get();
		if($user_processes->isEmpty()) return abort(404);
		// $limit=Carbon::createFromFormat('Y-m-d H:i:s',UserProcess::select('created_at')->orderBy('created_at','ASC')->first()->created_at);
		$limit=Carbon::now()->subDay()->subMonths(6);

		$users_headers=new Collection();
		$users_content=new Collection();
		foreach ($users as $user) {
			$name=$user->idemployee." ".$user->name." ".$user->firstname;
			$day_start = Carbon::now()->subDay();
			$users_headers->push($name);
			$users_content->put( $name , new Collection());
			while ($limit->lessThanOrEqualTo($day_start)) {
				if ($day_start->dayOfWeek===Carbon::SUNDAY) {
					$day_start->startOfDay()->subday();
					continue;
				}
				$prom=$user_processes
					->where('start_at',$day_start)
					->where('user_id',$user->id)
					;
				if($prom->isEmpty()) $users_content[$name]->push(0);
				else $users_content[$name]->push(round($prom->avg('productivity'),0,PHP_ROUND_HALF_DOWN));

				// return response()->json([$week_start->startOfWeek()->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString(),$prom,$prom->avg('productivity')],200);
				$day_start->startOfDay()->subday();
			}
		}

		return response()->json([$users_headers,$users_content],200);
	}
}
