<?php

namespace App\Http\Controllers;

use DB;
use App\Role;
use App\User;
use App\Cycle;
use App\Pause;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;

class ExportController extends Controller
{
	public function maquinas_semana(Request $request)
	{
		$times = new Collection();
		$workcenter_section=strtoupper($request->input("section","S"));
		$workcenters= WorkCenter::where('section','like', $workcenter_section."%")
			->orderBy('section','ASC')
			->orderBy('num_machine','ASC')
			->get();
		$user_procecess = UserProcess::select("created_at")->orderBy('created_at','ASC')->limit(1)->first();
		if (is_null($user_procecess)) return response()->json(['msg'=>'no data'],200);
		$limit = $user_procecess->created_at->startOfWeek();

		foreach ($workcenters as $wc) {
			$week_start=Carbon::now()->subWeek()->startOfWeek();
			$times->put($wc->num_machine,new Collection());
			while ( $limit->lessThanOrEqualTo($week_start)) {
				$times[$wc->num_machine]->push(getTimeSemanalMaquinas($wc,$week_start));
				$week_start->subWeek();
			}
		}
		// return response()->json([$workcenters,$times],400);  
	   

		
		// dd($process_old,$limit_start,$limit_end,$now->gte($limit_start));

		$csv="--,";
		foreach($workcenters as $workcenter){
			$csv.=$workcenter->num_machine.',';
			$csv.=$workcenter->num_machine.',';
			$csv.=$workcenter->num_machine.',';
		}

		$csv.="\n";
		$csv.="--,";

		foreach($workcenters as $workcenter){
			$csv.='Setup,'.'Prod'.','.'Sin U'.',';
		}

		$csv.="\n";

		$csv.='PROMEDIO,';
		
		foreach($workcenters as $wc){
			$csv.=intval(round($times[$wc->num_machine]->avg('setup')/3600,1)).',';
			$csv.=intval(round($times[$wc->num_machine]->avg('produccion')/3600,1)).',';
			$csv.=intval(round($times[$wc->num_machine]->avg('sinuso')/3600,1)).',';
		}
		$csv.="\n";
		$week_start=Carbon::now()->subWeek()->startOfWeek();
		$i=0; 
		while($limit->lessThanOrEqualTo($week_start)){
			$csv.=$week_start->format('d ')." ".mes_esp2($week_start->month)." - ".$week_start->endOfWeek()->subDay()->day." ".mes_esp2($week_start->month).',';
			foreach($workcenters as $wc){
				$csv.=intval(round($times[$wc->num_machine][$i]["setup"]/3600,1)).',';
				$csv.=intval(round($times[$wc->num_machine][$i]["produccion"]/3600,1)).',';
				$csv.=intval(round($times[$wc->num_machine][$i]["sinuso"]/3600,1)).',';
			}
			
			$week_start->subWeek()->startOfWeek();
			$i++;
			$csv.="\n";
		}			
		
		
		$path="maquinas_semanas/";
		$name="maquinas_semana_".date('Ymd_His').".csv";
		// if (!File::isDirectory($path)) {
		//     File::makeDirectory($path, 0664, true);
		// }
		$remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
		$csv=strtr($csv,$remove_acents);
		Storage::disk('public')->put("$path$name", $csv);
		return Storage::disk('public')->download("$path$name");
	}
	public function maquinas_dia(Request $request)
	{
		
		$times = new Collection();
    	$workcenter_section=strtoupper($request->input("section","S"));
		$workcenters= WorkCenter::where('section','like', $workcenter_section."%")
			->orderBy('section','ASC')
			->orderBy('num_machine','ASC')
			->get();
		$user_procecess = UserProcess::select("created_at")->orderBy('created_at','ASC')->limit(1)->first();
		if (is_null($user_procecess)) return response()->json(['msg'=>'no data'],200);
		$limit = $user_procecess->created_at->startOfWeek();

		foreach ($workcenters as $wc) {
			$week_start=Carbon::now()->subDay()->startOfWeek();
			$times->put($wc->num_machine,new Collection());
			while ( $limit->lessThanOrEqualTo($week_start)) {
				$times[$wc->num_machine]->push(getTimeMaquinas($wc,$week_start));
				$week_start->subDay();
			}
		}
		

		$csv="--,";
		foreach($workcenters as $workcenter){
			$csv.=$workcenter->num_machine.',';
		}
		
		$csv.="\n";
		$csv.='PROMEDIO,';
		foreach($workcenters as $wc){
			
			$day_tecmaq=15; # 15 horas laborales
			$semana_tecmaq=6;
			$segundos=60;
			$minutos=60;

			$pausa=$setup=$produccion=$sinuso=0;
			$count_setup=$count_produccion=$count_sinuso=0;
			$week_in_second=(($day_tecmaq*$semana_tecmaq*$num_semanas)*$minutos)*$segundos;
			// if ($wc->num_machine=='CT-38')  dd($processes->toArray());

			if ($wc->processes->count() >0) {
				$count_produccion=$wc->processes->count();

				$processes=$wc->processes;
				$user_processes=$processes->pluck('user_processes')->collapse();
				$pauses=$processes->pluck('pauses')->collapse();
				// dd($pauses);
				foreach ($processes as $process) {
					$setup+=getTime($process->ih_created_at,$process->fh_created_at,true);
					// if ($wc->num_machine=='CT-38')  dd( $process,getTime($process->ih_created_at,$process->fh_created_at,true) );

					if (!is_null($process->ia_created_at)) {
						$setup+=getTime($process->ia_created_at,$process->fa_created_at,true);
					}
					if (!is_null($process->il_created_at)) {
						$setup+=getTime($process->il_created_at,$process->ls_created_at,true);
					}elseif(!is_null($process->ial_created_at)){
						$setup+=getTime($process->ial_created_at,$process->ls_created_at,true);
					}

				}
				foreach ($user_processes as $u_p) {
					$produccion+=getTime($u_p->start_time,$u_p->end_time,true);
				}
				// foreach ($pauses as $p) {
				//  $pausa+=$p->started_at->diffInSeconds($p->ended_at);
				// }
				$sinuso=($week_in_second-$setup-$produccion+$pausa);
				$sinuso=intval((($sinuso/*/$count_produccion*/)/$segundos)/$minutos);
				$setup=intval((($setup/*/$count_produccion*/)/$segundos)/$minutos);
				$produccion=intval((($produccion/*/$count_produccion*/)/$segundos)/$minutos);
			}
		
		$csv.=$setup.','.$produccion.','.$sinuso.',';
		}
				
			while($now->gte($limit_start)){
				
				$csv.=$now->day." ".mes_esp2($now->month).',';
				foreach($workcenters as $wc){
					
					$day_tecmaq=15; # 15 horas laborales
					$semana_tecmaq=6;
					$segundos=60;
					$minutos=60;

					$pausa=$setup=$produccion=$sinuso=0;
					$count_setup=$count_produccion=$count_sinuso=0;
					$week_in_second=(($day_tecmaq*$semana_tecmaq)*$minutos)*$segundos;
					// if ($wc->num_machine=='CT-38')  dd($processes);
					if ($wc->processes->count() >0) {
						$count_produccion=$wc->processes->count();
						$processes=$wc->processes->where('cr_created_at','>=',$now)->where('cr_created_at','<=',$weekend);
						$user_processes=$processes->pluck('user_processes')->collapse();
						$pauses=$processes->pluck('pauses')->collapse();
						foreach ($processes as $process) {
							$setup+=getTime($process->ih_created_at,$process->fh_created_at,true);
							// if ($wc->num_machine=='CT-38')  dd( $process,getTime($process->ih_created_at,$process->fh_created_at,true) );

							if (!is_null($process->ia_created_at)) {
								$setup+=getTime($process->ia_created_at,$process->fa_created_at,true);
							}
							if (!is_null($process->il_created_at)) {
								$setup+=getTime($process->il_created_at,$process->ls_created_at,true);
							}elseif(!is_null($process->ial_created_at)){
								$setup+=getTime($process->ial_created_at,$process->ls_created_at,true);
							}

						}
						foreach ($user_processes as $u_p) {
							$produccion+=getTime($u_p->start_time,$u_p->end_time,true);
						}
						// foreach ($pauses as $p) {
						//  $pausa+=$p->started_at->diffInSeconds($p->ended_at);
						// }
						$sinuso=($week_in_second-$setup-$produccion+$pausa);

						$sinuso=intval((($sinuso/*/$count_produccion*/)/$segundos)/$minutos);
						$setup=intval((($setup/*/$count_produccion*/)/$segundos)/$minutos);
						$produccion=intval((($produccion/*/$count_produccion*/)/$segundos)/$minutos);
					}
					
					$csv.=$setup.','.$produccion.','.$sinuso.',';
				}
				
				$now->subDay(); 
				$weekend->subDay(); 
				$csv.="\n";
				
			}
		$path="maquinas_dias/";
		$name="maquinas_dia_".date('Ymd_His').".csv";
		// if (!File::isDirectory($path)) {
		//     File::makeDirectory($path, 0664, true);
		// }
		$remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
		$csv=strtr($csv,$remove_acents);
		Storage::disk('public')->put("$path$name", $csv);
		return Storage::disk('public')->download("$path$name");
	}
	public function jobs()
	{
		
		$process= Process::select('j.num_job','j.delivered_at'
				,'processes.*', 'processes.id', 'up.user_id as operador','up.id as u_p_id')
			->join('jobs as j','j.id','=','processes.job_id')
			->join('user_processes as up','up.process_id','=','processes.id')
			->where('processes.status','cr')
			->whereNotNull('processes.ajustador_ia_id')
			->whereNotNull('processes.ajustador_fa_id')
			->whereNotNull('processes.cr_created_at')
			// ->withTrashed()
			->orderBy('num_job','DESC')
			->get();

		//CONTEO DE LAS PIEZAS
		foreach ($process as $proces) {

			//CONTEO DE LOS CICLOS
			$cycles=Cycle::where('user_process_id',$proces->u_p_id)->get();
			$process->delivered_at=Carbon::createFromFormat('Y-m-d H:i:s',$proces->delivered_at);
			$cycles_count=0;
			$time_cycle_array=[];
			$time_cycle=0;
			for ($i=0; $i < $cycles->count()-1 ; $i++) { 
				if ($cycles[$i]->message=='START' && (isset($cycles[$i+1]) && $cycles[$i+1]->message=='12345' )) {
					$cycles_count++;
					$time_cycle_array[]=$cycles[$i]->message_date->diffInSeconds($cycles[++$i]->message_date);
				}
			}
			$time_cycle_array=collect($time_cycle_array);
			$time_cycle=round($time_cycle_array->avg(),2);
			$time_cycle_min=round($time_cycle/60,2);

			if ($time_cycle_min<1) $proces->time_cycle= $time_cycle." seg";
			else $proces->time_cycle= $time_cycle_min." min";
			$proces->piece=$cycles_count;
		}

		$csv="NUMERO DE JOB,NUMERO DE PARTE,NUMERO DE OPERACION,NOMBRE DE LA OPERACION,CANTIDAD DE PIEZAS,TIEMPO DE CICLO,CLIENTE,FECHA DE ENTREGA,TIEMPO TOTAL,HORA DE INICIO,FECHA DE INICIO,HORA TERMINO,FECHA TERMINO,DURACION,RESPONSABLE,DURACION,RESPONSABLE,DURACION,RESPONSABLE,DURACION,RESPONSABLE,DURACION,RESPONSABLE\n";
		foreach($process->where('ih_created_at','!=',null) as $proces){
			
			$csv.=$proces->job->num_job.','.$proces->operation->piece->part_number.',"'.str_replace(',',' ',html_entity_decode($proces->operation->operation_service)).'",'.str_replace(',',' ',html_entity_decode($proces->operation->description)).',';
			// dd($csv);
			if ($proces->piece<=0){
				$csv.=$proces->piece.',';
			}else{
				$csv.=$proces->piece.',';
			}

			$csv.=$proces->time_cycle.','.$proces->operation->piece->client->name.','.$proces->job->delivered_at->format('d-m-Y').',';
			
			$Total_horas=$proces->ih_created_at->diffInSeconds($proces->cr_created_at);
			$horas= intval((($Total_horas)/60)/60);
			$min= intval(($Total_horas - (($horas*60)*60))/60); 
			
			$csv.=$horas.':'.$min.'hrs'.','.$proces->ih_created_at->format('H:i').' hrs'.','.$proces->ih_created_at->format('d-m-Y').','.$proces->cr_created_at->format('H:i').' hrs'.','.$proces->cr_created_at->format('d-m-Y').',';
			
			$Total_horasH=$proces->ih_created_at->diffInSeconds($proces->fh_created_at);
			$horasH= intval((($Total_horasH)/60)/60);
			$minH= intval(($Total_horasH - (($horasH*60)*60))/60); 
			
			$csv.=$horasH.':'.$minH.'hrs'.',';
			if(!is_null($proces->user_processes->first()->user)){
				$csv.=$proces->user_processes->first()->user->idemployee .' '.$proces->user_processes->first()->user->name .' '.$proces->user_processes->first()->user->firstname.',';
			}else{ 
				$csv.='--'.',';
			}
							
			if (!is_null($proces->ia_created_at) && !is_null($proces->fa_created_at)) {
				$Total_horasA=$proces->ia_created_at->diffInSeconds($proces->fa_created_at);
				$horasA= intval((($Total_horasA)/60)/60);
				$minA= intval(($Total_horasA - (($horasA*60)*60))/60); 
			}else{ 
					$horasA=0;
					$minA=0;
			}
			$csv.=$horasA.':'.$minA.'hrs'.',';
			if(!is_null($proces->ajustador_ia)){
				$csv.=$proces->ajustador_ia->idemployee .' '.$proces->ajustador_ia->name .' '.$proces->ajustador_ia->firstname.',';
			}else{
				$csv.='--'.',';
			}
						
			if (!is_null($proces->ia_created_at) && !is_null($proces->ls_created_at)) {
				$Total_horasL=$proces->ia_created_at->diffInSeconds($proces->ls_created_at);
				$horasL= intval((($Total_horasL)/60)/60);
				$minL= intval(($Total_horasL - (($horasL*60)*60))/60); 
			}
			elseif (!is_null($proces->ial_created_at) && !is_null($proces->ls_created_at)) {
				$Total_horasL=$proces->ial_created_at->diffInSeconds($proces->ls_created_at);
				$horasL= intval((($Total_horasL)/60)/60);
				$minL= intval(($Total_horasL - (($horasL*60)*60))/60); 
			}else{
				$horasL=0;
				$minL=0;
			}

			$csv.=$horasL.':'.$minL.'hrs'.',';
			if(!is_null($proces->ajustador_il)){
				$csv.=$proces->ajustador_il->idemployee .' '.$proces->ajustador_il->name .' '.$proces->ajustador_il->firstname.',';
			}elseif (!is_null($proces->ajustador_ial)){
				$csv.=$proces->ajustador_ial->idemployee .' '.$proces->ajustador_ial->name .' '.$proces->ajustador_ial->firstname.',';
			}else{
				$csv.='--'.',';
			}

			if (!is_null($proces->pr_created_at) && !is_null($proces->cr_created_at)) {
				$Total_horasP=$proces->pr_created_at->diffInSeconds($proces->cr_created_at);
				$horasP= intval((($Total_horasP)/60)/60);
				$minP= intval(($Total_horasP - (($horasP*60)*60))/60); 
			}
			else {
				$horasP=0;
				$minP=0;
			}
			
			$csv.=$horasP.':'.$minP.'hrs'.',';
			if(!is_null($proces->user_processes->first()->user)){
				$csv.=$proces->user_processes->first()->user->idemployee .' '.$proces->user_processes->first()->user->name .' '.$proces->user_processes->first()->user->firstname.',';
			}
			else{ 
				$csv.='--'.',';
			}
			
			if (!is_null($proces->pauses->first()) && !is_null($proces->pauses->first())) {
				$Total_horasM=$proces->pauses->first()->started_at->diffInSeconds($proces->pauses->first()->ended_at);
				$horasM= intval((($Total_horasM)/60)/60);
				$minM= intval(($Total_horasM - (($horasM*60)*60))/60); 
			}
			else 
				$horasM=0;
				$minM=0;
			
			$csv.=$horasM.':'.$minM.'hrs'.',';
			if(!is_null($proces->ajustador_pm)){
				$csv.=$proces->ajustador_pm->idemployee .' '.$proces->ajustador_pm->name .' '.$proces->ajustador_pm->firstname.',';
			}
			else{
				$csv.='--'.'';
			}
			$csv.="\n";
		}
		$path="jobs/";
		$name="job_".date('Ymd_His').".csv";
		// if (!File::isDirectory($path)) {
		//     File::makeDirectory($path, 0664, true);
		// }
		$remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
		$csv=strtr($csv,$remove_acents);
		Storage::disk('public')->put("$path$name", $csv);
		return Storage::disk('public')->download("$path$name");
  
	}

	public function operadores(Request $request)
	{
		$name = $request->get('name');

		$users = User::select("users.*",'r.name as role_name')
			->join('role_user as r_u','r_u.user_id','=','users.id')
			->join('roles as r','r_u.role_id','=','r.id')
			// ->join('user_processes as u_p','u_p.user_id','=','users.id')
			->where('r.name','operador')
			->orderBy('users.id','ASC')
			->name($name)
			->paginate(15);
		if($users->isEmpty()) return abort(404);
		$user_processes=UserProcess::whereIn('user_id',$users->pluck('id'))
			// ->orderBy('created_at','DESC')
			->get();
		if($user_processes->isEmpty()) return abort(404);
		$limit=Carbon::createFromFormat('Y-m-d H:i:s',UserProcess::select('created_at')->orderBy('created_at','ASC')->first()->created_at);

		$users_headers=new Collection();
		$users_content=new Collection();
		foreach ($users as $user) {
			$name_=$user->idemployee." ".$user->name." ".$user->firstname;
			$week_start = Carbon::now()->startOfWeek()->subWeek();
			$users_headers->push($name_);
			$users_content->put( $name_ , new Collection());
			while ($limit->lessThanOrEqualTo($week_start)) {
				$prom=$user_processes
					->whereBetween('created_at',[$week_start->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString()])
					->where('user_id',$user->id)
					;
				if($prom->isEmpty()) $users_content[$name_]->push(0);
				else $users_content[$name_]->push(round($prom->avg('productivity'),0,PHP_ROUND_HALF_DOWN));

				// return response()->json([$week_start->startOfWeek()->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString(),$prom,$prom->avg('productivity')],200);
				$week_start->startOfWeek()->subWeek();
			}
		}


		$csv="--,";
			foreach($users_headers as $user){
			   $csv.=$user.',';
			}
			$csv.="\n";
			$csv.='PROMEDIO,';
			foreach ($users_headers as $user){
				$csv.=intval($users_content[$user]->avg()).'%'.',';
			}
			$csv.="\n";
			$week_start=Carbon::now()->subWeek()->startOfWeek();
			$i=0; 

			while($limit->lessThanOrEqualTo($week_start)){
				$csv.=$week_start->format('d-').mes_esp2($week_start->format('m')).'-'.$week_start->endOfWeek()->subDay()->format('d-').mes_esp2($week_start->endOfWeek()->subDay()->format('m')).',';
				foreach($users_headers as $user){
					$csv.=$users_content[$user][$i].'%'.',';
				}
				$week_start->startOfWeek()->subWeek();
				$i++;
				$csv.="\n";
			}
				
			// dd($csv);
		$path="operadores/";
		$name="operador_".date('Ymd_His').".csv";
		// if (!File::isDirectory($path)) {
		//     File::makeDirectory($path, 0664, true);
		// }
		$remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
		$csv=strtr($csv,$remove_acents);
		Storage::disk('public')->put("$path$name", $csv);
		return Storage::disk('public')->download("$path$name");
		
	
	}
	public function calidad(Request $request)
	{
		$num_job = $request->get('num_job');
		$part_number = $request->get('part_number');
		$inspector = $request->get('inspector');
		$fecha = $request->get('fecha');
		// dd($num_job,$part_number,$inspector,$fecha);

		$process = Process::select('processes.*','j.num_job',
			DB::raw("CONCAT(insp_il.idemployee,' ',insp_il.name,' ',insp_il.firstname) as inspector"),
			DB::raw("CONCAT(insp_ial.idemployee,' ',insp_ial.name,' ',insp_ial.firstname) as inspector_ail")
			)
			->with(['rejected_processes'=>function ($q_rp){
				$q_rp
					->whereNotNull('started_at');
			}])
			->joinJob()
			->joinOperation()
			->joinPiece()
			->joinInspector()
			->whereNotNull('ls_created_at')
			->orderBy('j.num_job','DESC');
		if(!is_null($num_job)) $process=$process->job($num_job);
		if (!is_null($part_number)) $process=$process->Part_number($part_number);
		if (!is_null($inspector)) {
			$process=$process->where(function ($q_insp)use($inspector){
				$q_insp->where(DB::raw("CONCAT(insp_il.idemployee,' ',insp_il.name,' ',insp_il.firstname)"),'ilike',"%$inspector%")
					->orWhere(DB::raw("CONCAT(insp_ial.idemployee,' ',insp_ial.name,' ',insp_ial.firstname)"),'ilike',"%$inspector%");
			});
		}
		if (!is_null($fecha)) {
			$process=$process->where(function ($q) use ($fecha){
				$q->whereDate('il_created_at',$fecha)
					->orWhereDate('ial_created_at',$fecha);
			});
		}
		// $process=$process->paginate(100);
		$csv="NUMERO DE JOB,NUMEROS DE PARTE,OPERACION,HORA,FECHA,HORA,FECHA,RESULTADO,HORAS/MINUTOS,AJUSTADOR,INSPECTOR DE CALIDAD,COMENTARIOS\n";
		foreach($process as $proces) {
			foreach($proces->rejected_processes as $proces1){
				
				$csv.=$proces->job->num_job.','.$proces->operation->piece->part_number.','.$proces->operation->operation_service.','.$proces1->started_at->format('H:i').' hrs'.','.$proces1->started_at->format('d-m-Y').','.$proces1->ended_at->format('H:i').' hrs'.','.$proces1->ended_at->format('d-m-Y').','.'rechazado,';
				
				
				$Total_horas=$proces1->started_at->diffInSeconds($proces1->ended_at);
				$horas= intval((($Total_horas)/60)/60);
				$min= intval(($Total_horas - (($horas*60)*60))/60); 
				
				$csv.=$horas.':'.$min.'hrs'.',';
				if(!is_null($proces->ajustador_fa)){
					$csv.=$proces->ajustador_fa->idemployee .' '.$proces->ajustador_fa->name .' '.$proces->ajustador_fa->firstname.',';
				}else {
					$csv.='--'.',';
				}
				if(!is_null($proces->ajustador_il)){
					$csv.=$proces->ajustador_il->idemployee .' '.$proces->ajustador_il->name .' '.$proces->ajustador_il->firstname.',';
				}else {
					$csv.=$proces->ajustador_ial->idemployee .' '.$proces->ajustador_ial->name .' '.$proces->ajustador_ial->firstname.',';
				}
				if(!is_null($proces->pauses)){
					$csv.=str_replace(',','',html_entity_decode($proces1->comment)).',';
				}
				else{
					$csv.='--';
				}
				$csv.="\n";
						
			}
			
			$Total_horas=0;
			$csv.=$proces->job->num_job.','.$proces->operation->piece->part_number.','.$proces->operation->operation_service.',';
			if (!is_null($proces->il_created_at)) {
				$csv.=$proces->il_created_at->format('H:i').' hrs'.',';
				$csv.=$proces->il_created_at->format('d-m-Y');
				$Total_horas=$proces->il_created_at->diffInSeconds($proces->ls_created_at);
			}elseif(!is_null($proces->ial_created_at)){
				$csv.=$proces->ial_created_at->format('H:i').' hrs'.',';
				$csv.=$proces->ial_created_at->format('d-m-Y');
				$Total_horas=$proces->ial_created_at->diffInSeconds($proces->ls_created_at);
			}else{
				$csv.="--,--";
			}

			$csv.=','.$proces->ls_created_at->format('H:i').' hrs'.','.$proces->ls_created_at->format('d-m-Y').',liberado,';
			
			
			$horas= intval((($Total_horas)/60)/60);
			$min= intval(($Total_horas - (($horas*60)*60))/60); 
			
			$csv.=$horas.':'.$min.'hrs'.',';
			
			if(!is_null($proces->ajustador_fa)){
				$csv.=$proces->ajustador_fa->idemployee .' '.$proces->ajustador_fa->name .' '.$proces->ajustador_fa->firstname.',';
			}
			else{
				$csv.='--'.',';
			}
			if(!is_null($proces->ajustador_il)){
				$csv.=$proces->ajustador_il->idemployee .' '.$proces->ajustador_il->name .' '.$proces->ajustador_il->firstname.',';
			}
			else{
				$csv.='--'.',';
			} 
						
			$csv.='--';
			$csv.="\n";
			
		}
		
		$path="calidad/";
		$name="calidad_".date('Ymd_His').".csv";
		// if (!File::isDirectory($path)) {
		//     File::makeDirectory($path, 0664, true);
		// }
	   $remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
	   $csv=strtr($csv,$remove_acents);
	   Storage::disk('public')->put("$path$name", $csv);
	   return Storage::disk('public')->download("$path$name");
		
	}
	public function mantenimiento()
	{
		$pauses=Pause::select("pauses.*","j.num_job",'w.num_machine','w.model', DB::raw("CONCAT(user_mantenimiento.name,' ',user_mantenimiento.firstname) as user_mantenimiento"),'w.order' )
			->join('workcenters as w','w.id','=','pauses.workcenter_id')
			->join('processes as p','pauses.process_id','=','p.id')
			->join('users as user_mantenimiento','user_mantenimiento.id','=','pauses.user_id')
			->join('jobs as j','j.id','=','p.job_id')
			->join('operations as op','op.id','=','p.operation_id')
			->whereNotNull('ended_at')
			->where('motivo','mantenimiento')
			->orderBy('j.num_job')
			->get();
   //  	$process = Process::where('status','cr')->whereNotNull('pm_created_at')->whereNotNull('pmp_created_at')->get();

		$csv="NUMERO DE CT,MODELO,HORA,FECHA,HORA,FECHA,HORAS/MINUTOS,OPERADOR,MANTENIMIENTO,COMENTARIOS\n";  

		foreach($pauses as $pause){
		
			$csv.=$pause->num_machine.','.$pause->model.','.$pause->started_at->format('H:i').' hrs'.','.$pause->started_at->format('d-m-Y').','.$pause->ended_at->format('H:i').' hrs'.','.$pause->ended_at->format('d-m-Y').',';
			
		   
				$Total_horas=$pause->started_at->diffInSeconds($pause->ended_at);
				$horas= intval((($Total_horas)/60)/60);
				$min= intval(($Total_horas - (($horas*60)*60))/60); 
			
			$csv.=$horas.':'.$min.'hrs'.',';
		   
			
			if(!is_null($pause->operador_id)){
			   $csv.=$pause->operador->idemployee .' '.$pause->operador->name .' '.$pause->operador->firstname.',';
			}elseif(!is_null($pause->process->user_processes->first()->user)){
				$csv.=$pause->process->user_processes->first()->user->idemployee .' '.$pause->process->user_processes->first()->user->name .' '.$pause->process->user_processes->first()->user->firstname.',';
			}else{
				$csv.='--'.',';
			}
			
			$csv.=$pause->user_mantenimiento.','.$pause->comments;
			$csv.="\n";
		
		}
		$path="mantenimiento/";
		$name="mantenimiento_".date('Ymd_His').".csv";
		
		$remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
		$csv=strtr($csv,$remove_acents);
		Storage::disk('public')->put("$path$name", $csv);
		return Storage::disk('public')->download("$path$name");

   //  	foreach($process as $proces){
		
   //  		if (!is_null($proces->workcenter)){
   //  			$csv.=$proces->workcenter->num_machine.',';
   //          }
   //  		else{
   //  			$csv.='--'.',';
   //          }
			
   //  		if (!is_null($proces->workcenter)){
   //  			$csv.=$proces->workcenter->model.',';
   //          }
   //  		else{
   //  			$csv.='--'.',';
   //          }
			
			
   //  		$csv.=$proces->pauses->first()->started_at->format('H:i').' hrs'.','.$proces->pauses->first()->started_at->format('d-m-Y').',';
			
   //  		if (!is_null($proces->pauses->first() && !is_null($proces->pauses->first()->ended_at))){
   //  			$csv.=$proces->pauses->first()->ended_at->format('H:i').' hrs'.',';
   //          }
   //  		else{
   //  			$csv.='--'.',';
   //          }
			
   //  		if (!is_null($proces->pauses->first() && !is_null($proces->pauses->first()->ended_at))){
   //  			$csv.=$proces->pauses->first()->ended_at->format('d-m-Y').',';
   //          }
   //  		else{
   //  			$csv.='--'.',';
   //          }
			
			
			
			
			// $Total_horas=$proces->pauses->first()->started_at->diffInSeconds($proces->pauses->first()->ended_at);
			// $horas= intval((($Total_horas)/60)/60);
			// $min= intval(($Total_horas - (($horas*60)*60))/60); 
			
			
   //  		$csv.=$horas.':'.$min.'hrs'.',';
   //  		if(!is_null($proces->user_processes->first()->user)){
   //  			$csv.=$proces->user_processes->first()->user->idemployee .' '.$proces->user_processes->first()->user->name .' '.$proces->user_processes->first()->user->firstname.',';
   //          }
   //  		else{ 
   //              $csv.='--'.',';
   //          }
			
			
   //  		if(!is_null($proces->pauses->first()->user)){
   //  			$csv.=$proces->pauses->first()->user->idemployee .' '.$proces->pauses->first()->user->name .' '.$proces->pauses->first()->user->firstname.',';
   //          }
   //  		else{ 
   //  			$csv.='--'.',';
   //          }
			
			
   //  		if(!is_null($proces->pauses)){
   //  			$csv.=str_replace(',',' ',html_entity_decode($proces->pauses->first()->comments));
   //          }
   //  		else{ 
   //              $csv.='--';
				
   //          }
   //          $csv.="\n";
   //  	}
		
   //      $path="mantenimiento/";
   //      $name="mantenimiento_".date('Ymd_His').".csv";
   //      // if (!File::isDirectory($path)) {
   //      //     File::makeDirectory($path, 0664, true);
   //      // }
   //     $remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
   //     $csv=strtr($csv,$remove_acents);
   //     Storage::disk('public')->put("$path$name", $csv);
   //     return Storage::disk('public')->download("$path$name");
	}
}
