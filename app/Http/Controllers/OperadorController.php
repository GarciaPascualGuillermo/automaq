<?php

namespace App\Http\Controllers;

use App\Events\PhaseUpdated;
use Auth;
use App\User;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\LoginOperadorRequest;

class OperadorController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth')->except(['login','glogin']);
        $this->middleware(['setup.redirect'])
			->only(
				[
					'index',
				]
			);
    }
	public function index()
	{
		return view('admin.operador.index');
    }

	public function login(LoginOperadorRequest $request)
	{
		if (auth()->check()) {
			return $this->redirect(auth()->user());
		}

		$user=User::where('idemployee',$request->input('noempleado'))
			->limit(1)
			->get();
		if($user->isNotEmpty()){
			$user=$user->first();
			if (Hash::check($request->input('password'), $user->password)) {
				Auth::login($user, false);
				return $this->redirect($user);
			}
		}
		return redirect()->back()
			->withErrors(['noempleado'=>['EL usuario o contraseña es incorrecto']])
			->withInput()
			;
		// return redirect()->route('jobs.asignados.index');
	}
	private function redirect($user)
	{
		if ($user->hasRoles(['operador'])) {
			if (session('wc')) {
				return redirect()->route('jobs.asignados.index');
			}
		}
		if ($user->hasRoles(['root'])) {
            if (!is_null(session('wc'))) return redirect()->route('jobs.asignados.index');
			else return redirect()->route('maquinas.index');
		}else if ($user->hasRoles(['sup.produccion'])) {
			if (!is_null(session('wc'))) return redirect()->route('jobs.asignados.index');
			else return redirect()->route('maquinas.index');
		}elseif ($user->hasRoles(['insp.calidad'])) {
			Auth::logout();
			return abort(403);
		}else if ($user->hasRoles(['ceo','gte.admin'])) {
			return redirect()->route('indicadores.index');
		}else if ($user->hasRoles(['dep.calidad'])) {
			return redirect()->route('calidad.liberacion');
		}elseif($user->hasRoles(['dep.mantenimiento'])){
			return redirect()->route('mantenimiento.index');
		}elseif ($user->hasRoles(['gte.calidad'])) {
			return redirect()->route('calidad.liberacion');
		}elseif ($user->hasRoles(['sistemas'])) {
			return redirect()->route('usuarios.index');
		}elseif($user->hasRoles(['gte.operacion','jefe.produccion','ceo','sup.produccion'])){
			return redirect()->route('maquinas.index');
		}elseif($user->hasRoles(['ajustador','planeacion','cuenta','jefe.mantenimiento','sup.linea'])){
			return redirect()->route('jobs.index');
		}elseif ($user->hasRoles(['tec.mantenimiento'])) {
            if (session('wc')) {
                return redirect()->route('jobs.asignados.index');
            }
			return redirect()->route('historial.mantenimiento');
		}
		// return redirect()->route('maquinas.index');
		return abort(404);

	}
	public function glogin()
	{
		// $ip="172.31.3.155"; # CT-109
		$ip=request()->ip(); #
		$wc=Workcenter::where('ip_address',$ip)->limit(1)->get() ;
		if ($wc->isNotEmpty()) {
			session()->forget('wc');
			session()->put('wc',$wc);
			session()->save();
			if ( $wc->first()->support ) {
		    	return redirect()->route('mantenimiento.soporte');
			}
		}else  session()->forget('wc');


		if (auth()->check()) return $this->redirect(auth()->user());

		return view('admin.login.index')
			->with('wc',$wc);
	}
	public function cycles(Request $request)
	{
		$process= Process::with(['user_processes'=>function ($queryUP){
				$queryUP->with(['cycles'/*=>function ($queryC){
					$queryC->where('process_status','pr');
				}*/]);
			}])
			->where('id',$request->input('id',session('process_id')))
			->first();

		return getCycles($process);
	}
	public function productividad(Request $request)
	{
		$process = Process::with(['user_processes'=>function ($queryUP){
				$queryUP
				->where('user_id',auth()->user()->id)
				->with(['cycles'=>function ($queryC){
					$queryC
						->orderBy('message_date','ASC')
						;
				}]);
			}])
			->with(['operation'])
			->where('id',$request->input('id',$request->input('process.id')))
            ->first();

		return productivity($process);

	}
	public function scraps(Request $request)
	{

		$process= Process::with(['user_processes'=>function ($queryUP){
				$queryUP
					->where('user_id',auth()->user()->id)
					->where('start_at',Carbon::now())
					->with(['cycles'=>function ($queryC){
							$queryC
								// ->where('process_status','pr')
								->orderBy('message_date','ASC')
								;
						    }
						])
					;
			}])
			->where('id',$request->input('process.id'))
			->first();

		if (is_null($process)) {
			return response()->json(['error' => $process],400);
		}

		// $process->scraps=$request->input('scraps',$process->scraps);
		$user_process = $process->user_processes
			->where('start_at', date('Y-m-d')." 00:00:00")
			->where('user_id', auth()->user()->id)
			->where('log_out', false)
            ->first();

        $getCycles = getCycles($process);

        $total_cycles = $getCycles['pieceTotal'];

		// return response()->json($getCycles,400);
		// $cycles=$user_process->cycles->where('process_status','pr');

        $prod = productivity($process);

		if ($total_cycles == 0) {
			return $prod;
		}

		if (is_null($user_process)) {
			return $prod;
        }

        // $user_process->end=$request->input('pieceCount');

		$user_process->end = $total_cycles;
		$user_process->end_time = $user_process->end_time ?? Carbon::now();
		$user_process->log_out = true;
		$user_process->active = false;
		$user_process->scraps = $request->input('scraps', 0);
		$user_process->productivity = $prod['productividad'];

		// return response()->json($user_process, 400);

		if ($user_process->save()) {

            $process->save();

            $productivity = $user_process->productivities->where('percentage_date',date('Y-m-d')." 00:00:00");

			if ( $productivity->isEmpty() ) {

				$productivity = $user_process->productivities()->create([
					'percentage' => $prod['productividad'],
					'percentage_date' => Carbon::now(),
                ]);

			} else {

                $productivity = $productivity->first();

            }
			return $prod;
		}
		return response()->json("Error ", 500);

	}
	public function getPiece(Request $request)
	{

		$process = Process::with(['user_processes' => function ($query) {
            $query->where('user_id', auth()->user()->id)
                ->where('start_at', Carbon::now())
                ->with(['cycles'])
                ;
			}])
        ->where('id', $request->input('id'))
        ->first();

        $getCycles = getCycles( $process,auth()->user() );

        $total_cycles = $getCycles['pieceTotal'];

        return $total_cycles;

	}
	public function getDibujoTecnico(Request $request)
	{

        $name = '';


		if ($request->input('dibujo')) {
            $pattern = '/' . $request->input('process.operation.piece.part_number') .'\sREV.\s' . $request->input('process.job.rev') . '.pdf/';
			$files = Storage::disk('dibujo')->allFiles("/");
		}elseif ($request->input('mot')) {
            $pattern = '/' . $request->input('process.operation.piece.part_number') .'\sREV.\s' . $request->input('process.job.rev') . '\sMOTD.pdf/';
			$files = Storage::disk('mot')->allFiles("/");
		}else{
            $pattern = '/' . $request->input('process.operation.piece.part_number') .'\sREV.\s' . $request->input('process.job.rev') . '\sPI.pdf/';
			$files = Storage::disk('plan')->allFiles("/");
        }

		foreach ($files as $file) {


            if ( $request->input('dibujo') && preg_match($pattern, $file, $matches) ) {

                return ['url' => Storage::disk('dibujo')->url( $file )];

			} elseif ($request->input('mot') && preg_match($pattern, $file, $matches)) {

                return ['url' => Storage::disk('mot')->url( $file )];

			} elseif ($request->input('plan') && preg_match($pattern, $file, $matches) ) {

                return ['url' => Storage::disk('plan')->url( $file )];

			}
		}
		// dd($request->all('process.job.num_job'));
		return ['url' => '', 'name' => $pattern] ;
	}
	public function logout(Request $request)
	{
		$user=auth()->user();

		$process= Process::with(['user_processes.cycles'])
			->where('id',$request->input('process.id',session('process_id')))
            ->first();


		if (is_null($process)) {

            Auth::logout();
            return response()->json(['redirect' => '/'], 200);

        }

        $total = $process->quantity > 0 ? $process->quantity : $process->job->quantity;

		$getCycles=getCycles($process);
        $total_cycles=$getCycles['pieceTotal'];
		// return response()->json([$total_cycles>=$process->job->quantity,$total_cycles,$process->job->quantity],400);
		if ($total_cycles >= $total) {
			$process->status='cr';
			$process->cr_created_at=Carbon::now();
			$wc=Workcenter::with(['processes'=>function ($q_p){
					$q_p->orderBy('order','asc');
				}])->where('id',$process->workcenter_id)
				->limit(1)
				->first()
				;
			$resta=false;
			foreach ($wc->processes as $p) {
				if ($resta) {
					$p->order--;
				}
				// return response()->json([$p,$process],401);
				if (!is_null($p) && $p->id===$process->id)  {
					$p->active=false;
					$p->order=null;
					$resta=true;
					$p->status='cr';
                    $p->cr_created_at=Carbon::now();
				}
                $p->save();

			}
		} else {
			if ($process->status=='pr') {
                $process->status='crp';
			}
        }



		if ($request->input('finalizar')==true or  $request->input('finalizar')=="true") {
            $process->status='cr';
        }

		$process->active=false;
        $process->save();


        $user_process=UserProcess::where('id', session('user_process_id'))->limit(1)->first();


		if (!is_null($user_process)) {

            $user_process->log_out=true;
			$user_process->active=false;
			$user_process->status_end=$process->status;
			$user_process->end=$total_cycles=$getCycles['pieceTotal'];;
            $user_process->end_time=Carbon::now();

			if($user_process->save()) {

                if (in_array($process->status, ['cr', 'pm', 'pfh', 'pfm'])) {
                    $activity = $process->status;
                } else {
                    $activity = 'crp';
                }

                $process->process_log()->create([
                    'user_id' => auth()->user()->id,
                    'activity' => $activity,
                    'parent_id' => parent_id($process),
                    'user_process_id' => $user_process->id,
                    'workcenter_id' => $process->workcenter_id
                ]);
                session()->forget('user_process_id');
			}
        }
        event(new PhaseUpdated());
		session()->forget('job_id');
		session()->forget('process_id');
		session()->forget('setup_id');
		// session()->forget('wc');
		session()->save();
		Auth::logout();
		return response()->json(['total_cycles'=>$total_cycles,"redirect"=>"/logout"],201);
		return $request->all();
	}
}
