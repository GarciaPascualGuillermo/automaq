<?php

namespace App\Http\Controllers;
use App;
use App\Workcenter;
use App\UserProcess;
use App\Traits\Machines;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class MaquinasController extends Controller
{
    use Machines;
    function __construct()
	{
		$this->middleware(['auth']);
		$this->middleware(['maquina.middleware']);
	}

	//Return view Maquinas

	public function maquinas(Request $request)
	{

        return view('admin.maquinas.index');

    }
    public function updateMachines(Request $request)
    {
        $from = request('from');
        $to = request('to');

		if (!$from || !$to) {
            $from = $to = now();
        }

        $workcenter_section = strtoupper($request->input('section', ''));

		$workcenters = $this->getMachines($from, $to, $workcenter_section);

        return response()->json($workcenters);
    }
	public function datos(Request $request)
	{

        $wc = Workcenter::where('id',$request->input('workcenter.id'))->first();

		if (is_null($wc)) {
			return [
				'machine_times'=>[
					'setup'=>0,
					'prod'=>0,
					'not_used'=> 0
				],
				'efficiency'=> 0,
				'job'=>null,
				'error'=>'No se encontro máquina'
			];
		}

		$times=getTimeMaquinas($wc);
		$process=$wc->processes->where('active',true);

		if ($process->isNotEmpty()) $job='JOB EN CURSO';
		else $job='N/A';

		$efficiency=0;
		$efficiency_count=0;
		$dateTime=Carbon::now();
		$first_turn_start_day=new Carbon(date('Y-m-d')." 07:00:00");
	    $first_turn_end_day=new Carbon(date('Y-m-d')." 14:59:00");
	    $second_turn_start_day=new Carbon(date('Y-m-d')." 15:00:00");
	    $second_turn_end_day=new Carbon(date('Y-m-d')." 23:00:00");
	    $start_day = $first_turn_start_day;

		$user_processes = UserProcess::select('user_processes.*','op.standar_time','op.run_method')
			->join('processes as p','p.id','=','user_processes.process_id')
			->join('operations as op','p.operation_id','=','op.id')
			->with(['cycles'])
			->where('p.workcenter_id',$wc->id)
			->whereBetween('start_time',[$start_day, $dateTime->toDateTimeString()])
			->get();

		// dd($user_processes);

        $time_cycle=0;
		$time_cycle_array=[];
		$time_cycle_eficiencia=[];
		$interaccion=0;
		foreach ($user_processes as $user_process) {
			$cycles_count=0;
			$cycles = $user_process->cycles->sortBy('message_date');
			for ($i=0; $i < $cycles->count()-1 ; $i++) {
				if ($cycles[$i]->message=='START' && (isset($cycles[$i+1]) && $cycles[$i+1]->message=='12345' )) {
					$cycles_count++;
					$time_cycle_array[]=$cycles[$i]->message_date->diffInSeconds($cycles[$i+1]->message_date);
					$i=$i++;

					// if ($interaccion==1) dd($time_cycle_array->toArray(),$cycles_count,$cycles[$i]->toArray(),$cycles[$i+1]->toArray(),$cycles[$i]->message_date->diffInSeconds($cycles[$i+1]->message_date),$user_process->standar_time*60);
				}
			}
			$time_cycle_array=collect($time_cycle_array);
			$time_cycle=round($time_cycle_array->avg(),2);
			if ($cycles_count==0 || floatval($user_process->standar_time)<=0) {
				$time_cycle_eficiencia[]=0;
			}else{
				// $time_cycle_eficiencia[]=(($time_cycle/$cycles_count)/floatval($user_process->standar_time*60)??1)*100;
				$time_cycle_eficiencia[]=((floatval($user_process->standar_time*60))/$time_cycle)*100;
			}

			// if ($interaccion==0) dd($time_cycle_array->toArray(),$time_cycle_eficiencia,$user_process->standar_time*60,$user_process->toArray());
			$interaccion++;
		}

		$time_cycle_eficiencia=collect($time_cycle_eficiencia);
		// dd($user_processes->toArray(),$time_cycle_eficiencia->toArray());


		$now=Carbon::now();

		return [
			'machine_times'=>[
				'setup'=>intval(round(($times['setup']/$times['day_tecmaq'])*100,0,PHP_ROUND_HALF_UP)),
				'prod'=>intval(round(($times['produccion']/$times['day_tecmaq'])*100,0,PHP_ROUND_HALF_UP)),
				'not_used'=> intval(round(($times['sinuso']/$times['day_tecmaq'])*100,0,PHP_ROUND_HALF_UP))
			],
			'efficiency'=> intval(round($time_cycle_eficiencia->avg(),0,PHP_ROUND_HALF_UP)),
			'job'=>$job,
		];
	}
	public function datos_v1(Request $request)
	{

		$wc=Workcenter::where('id',$request->input('workcenter.id'))
			->first();
		if (is_null($wc)) {
			return [
				'machine_times'=>[
					'setup'=>0,
					'prod'=>0,
					'not_used'=> 0
				],
				'efficiency'=> 0,
				'job'=>$job,
				'error'=>'No se encontro máquina'
			];
		}
		// return $wc;
		$setup=0;
		$prod=0;
		$min_setup=0;
		$min_prod=0;
		$efficiency=0;
		$efficiency_count=0;

		$start_day=new Carbon(date('Y-m-d')." 07:00:00");
		$end_day=new Carbon(date('Y-m-d')." 23:00:00");

		$now=Carbon::now();
		$mealtime=new Carbon(date('Y-m-d')." 12:30:00");
		$dinner=new Carbon(date('Y-m-d')." 20:00:00");
		/**
		 * Setup CALCULO
		*/
		foreach ($wc->processes as $process) {
			$start=null;
			$end=null;
			if ( !is_null($process->ih_created_at) && $process->ih_created_at->greaterThanOrEqualTo($start_day) && $process->ih_created_at->lessThanOrEqualTo($end_day)) {
				$start=$process->ih_created_at;
			}else if ( !is_null($process->ia_created_at) && $process->ia_created_at->greaterThanOrEqualTo($start_day) && $process->ia_created_at->lessThanOrEqualTo($end_day)) {
				$start=$process->ia_created_at;
			}else if ( !is_null($process->il_created_at) && $process->il_created_at->greaterThanOrEqualTo($start_day) && $process->il_created_at->lessThanOrEqualTo($end_day)) {
				$start=$process->il_created_at;
			}else if ( !is_null($process->ial_created_at) && $process->ial_created_at->greaterThanOrEqualTo($start_day) && $process->ial_created_at->lessThanOrEqualTo($end_day)) {
				$start=$process->ial_created_at;
			}else if ( !is_null($process->ln_created_at) && $process->ln_created_at->greaterThanOrEqualTo($start_day) && $process->ln_created_at->lessThanOrEqualTo($end_day)) {
				$start=$process->ln_created_at;
			}
			if (!is_null($start)) {
				$end=$process->pr_created_at;
			}
			if (!is_null($end)) {

				$min_setup+=$start->diffInSeconds($end);
				// return $start->toDateTimeString;
			}
			/**
			 * Production
			*/
			$time_cycle=0;
			$time_cycle_array=[];
			$cycles_count=0;
			foreach ($process->user_processes as $user_process) {
				$cycles = $user_process->cycles->sortBy('message_date');
				for ($i=0; $i < $cycles->count()-1 ; $i++) {
					if ($cycles[$i]->message=='START' && (isset($cycles[$i+1]) && $cycles[$i+1]->message=='12345' )) {
						$cycles_count++;
						$time_cycle=$cycles[$i]->message_date->diffInSeconds($cycles[$i+1]->message_date);
						$time_cycle_array[]=$cycles[$i]->message_date->diffInSeconds($cycles[$i+1]->message_date);
						$i=$i++;
					}
				}
				// return response()->json([$cycles,$cycles_count],403);

				$min_prod+= $user_process->start_time->diffInSeconds($user_process->end_time);
			}
			$time_cycle_array=collect($time_cycle_array);
			$time_cycle=round($time_cycle_array->avg(),2);
			// return response()->json([$time_cycle,$time_cycle_array,$cycles_count],403);
			if ($cycles_count==0 || floatval($process->operation->standar_time)<=0 ) {
				$efficiency+=0;
			}else{
				$efficiency+=(($time_cycle/$cycles_count)/floatval($process->operation->standar_time)??1)*100;
			}
			// dd($efficiency,$time_cycle);
			$efficiency_count++;
		}
		$process=$wc->processes->where('active',true);

		if ($process->isNotEmpty()) $job='JOB EN CURSO';
		else $job='N/A';


		$working_time=$start_day->diffInSeconds($now);


		if ( $mealtime->greaterThanOrEqualTo($now) ) {
			$working_time= $working_time - 30*60;
		}
		if ( $dinner->greaterThanOrEqualTo($now) ) {
			$working_time= $working_time - 30*60;
		}

		$setup=($min_setup/($working_time)) * 100;
		$prod=($min_prod/($working_time)) * 100;

		// $process=$wc->processes->first();
		// $prod=40;
		// $setup=30;
		$without_use=100 - $prod - $setup;
		// dd($efficiency_count);
		return [
			'machine_times'=>[
				'setup'=>round($setup,2,PHP_ROUND_HALF_UP),
				'prod'=>round($prod,2,PHP_ROUND_HALF_UP),
				'not_used'=> round(100 - $prod - $setup,2,PHP_ROUND_HALF_UP)
			],
			'efficiency'=> $efficiency_count? round($efficiency/($efficiency_count),1) : 0,
			'job'=>$job,
		];
    }
}
