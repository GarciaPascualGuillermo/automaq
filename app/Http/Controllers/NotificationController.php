<?php

namespace App\Http\Controllers;

use App\Events\PhaseUpdated;
use App\User;
use App\Process;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Notifications\SetupNotification;

class NotificationController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
	}
	public function setup(Request $request)
	{
		$users=User::whereHas('roles',function ($query){
			$query->where('name','setup');
		})->get();
		foreach ($users as $user) {
			$notification = [
				'title' => 'Setup proccess',
				'body' => 'Maquina T101',
				'action_url' => url('/'),
				'icon' => url('/Tunerd_Logo_lentes.png'),
				'created' => Carbon::now()
			];
			$user->notify(new SetupNotification($notification));
		}

		return response()->json('Notification sent.', 201);
	}
	public function herramentaje(Request $request)
	{
		$users=User::whereHas('roles',function ($query){
			$query->where('name','root');
        })->get();
        $process=Process::findOrFail(session('process_id'));
		foreach ($users as $user) {
			// $notification = [
			// 	'title' => 'Setup proccess',
			// 	'body' => 'Maquina T101',
			// 	'action_url' => url('/'),
			// 	'icon' => url('/Tunerd_Logo_lentes.png'),
			// 	'created' => Carbon::now()
			// ];
			$process=Process::findOrFail(session('process_id'));
			$process->status='fh';
			$process->fh_created_at=Carbon::now();
            $process->save();

			// $user->notify(new SetupNotification($notification));
        }

        $process->process_log()->create([
            'activity' => 'fh',
            'user_id' => auth()->user()->id,
            'parent_id' => parent_id($process),
            'user_process_id' => session('user_process_id'),
            'workcenter_id'=>$process->workcenter->id,
        ]);

        event(new PhaseUpdated());

		return  response()->json(['url'=>route('operador.setup.step2')], 201);
	}
}
