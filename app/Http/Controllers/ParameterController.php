<?php

namespace App\Http\Controllers;

use App\Parameter;
use Illuminate\Http\Request;

class ParameterController extends Controller
{


	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('sistemas.middleware');
	}
	
	public function index()
	{
		$parameter= Parameter::all();
		return view('admin.parametros.index')
			->with('parameter',$parameter);
	}

	public function store(Request $request)
	{
		$parameter=Parameter::where('name', 'verde')->first();
		if (is_null($parameter)) { 
			$parameter = Parameter::create([
				'name' => 'verde',
				'value' => $request -> input('verde')
			]);
		}else{
			$parameter->update([
				'value' => $request -> input('verde')
			]);
		}
		$parameter=Parameter::where('name', 'amarillo')->first();
		if (is_null($parameter)) { 
			$parameter = Parameter::create([
				'name' => 'amarillo',
				'value' => $request -> input('amarillo')
			]);
		}else{
			$parameter->update([
				'value' => $request -> input('amarillo')
			]);
		}
		// $parameter=Parameter::where('name', 'Pro_dia')->first();
		// if (is_null($parameter)) { 
		// 	$parameter = Parameter::create([
		// 		'name' => 'Pro_dia',
		// 		'value' => $request -> input('Pro_dia')
		// 	]);
		// }else{
		// 	$parameter->update([
		// 		'value' => $request -> input('Pro_dia')
		// 	]);
		// }
		// $parameter=Parameter::where('name', 'Pro_ca')->first();
		// if (is_null($parameter)) { 
		// 	$parameter = Parameter::create([
		// 		'name' => 'Pro_ca',
		// 		'value' => $request -> input('Pro_ca')
		// 	]);
		// }else{
		// 	$parameter->update([
		// 		'value' => $request -> input('Pro_ca')
		// 	]);
		// }
		$parameter=Parameter::where('name', 'Pro_ca_dia')->first();
		if (is_null($parameter)) { 
			$parameter = Parameter::create([
				'name' => 'Pro_ca_dia',
				'value' => $request -> input('Pro_ca_dia')
			]);
		}else{
			$parameter->update([
				'value' => $request -> input('Pro_ca_dia')
			]);
		}

		return redirect()->route('parametros.index');
	}
}
