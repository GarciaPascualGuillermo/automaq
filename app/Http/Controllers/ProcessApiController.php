<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Process;

class ProcessApiController extends Controller
{
    function __construct()
    {
    	$this->middleware(['auth']);
    }
    public function getProcess(Request $request)
    {
    	$process=Process::with(['operation'=>function ($q2){
					$q2->with(['piece'=>function ($q_piece){
						$q_piece->with('client');
					}]);
				}])
				->with('job')
				->where('id',session('process_id'))
				->limit(1)
				->get();
    	if ($process->isEmpty()) {
    		$process = new  Process();
    	}else{
    		$process=$process->first();
    	}
    	return $process;
    }
}
