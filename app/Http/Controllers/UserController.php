<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Workcenter;

class UserController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('sistemas.middleware');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if (auth()->user()->hasRoles(['root'])) {
			$users = User::orderBy('idemployee','ASC')->with('roles')->get();
			// $users = User::all()->orderBy('username','ASC');
		}else
			// $users = User::where('idemployee',"!=","000")->paginate(10);
			$users = User::where('idemployee',"!=","000")->orderBy('idemployee','ASC')->get();
		// dd($users);
		return view('admin.usuarios.index')
			->with('users',$users)
			;
	}
	public function crear()
	{
		$user=new User();
		if (auth()->user()->hasRoles(['root']))
			$roles=Role::all();
		else
			$roles=Role::where('name','!=','root')->get();

		$workcenters= Workcenter::orderBy('num_machine','ASC')
			->get();

		return view('admin.usuarios.crear')
				->with('user',$user)
				->with('roles',$roles)
				->with('workcenters',$workcenters)
				;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(UserRequest $request)
	{

        $request->validated();

        $user = new User($request->all());

        $user->username="u".$request->input('idemployee');

		$user->save();
		$user->roles()->sync($request->input('roles'));
		$user->workcenters()->sync($request->input('workcenters'));
		if ($fileContents=$request->file('photo')) {

			$path="/".$user->username."_".$user->id."/";
			$name=$user->username.".".$fileContents->getClientOriginalExtension();
			$full=$path.$name;
			Storage::disk('photos')->putFileAs($path, $fileContents,$name);
			$user->photo=$full;
		}
		$user->save();

		return redirect()->route('usuarios.index');


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */

	public function editar($id)
	{
		 $user= User::find($id);
		//$user= User::where('id',$id)->limit(1)->get()->first();

        if (auth()->user()->hasRoles(['root']))

            $roles = Role::all();

        else

			$roles = Role::where('name','!=','root')->get();


		$workcenters = Workcenter::orderBy('num_machine','ASC')
            ->get();

		$this->authorize($user);

		return view('admin.usuarios.editar')
			->with('user', $user)
			->with('roles', $roles)
			->with('workcenters', $workcenters)
			;

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateUserRequest $request, $id)
	{
		$user = User::find($id);
		$user->fill($request->validated());
		$user->roles()->sync($request->input('roles'));
		$user->workcenters()->sync($request->input('workcenters'));
		if ($fileContents=$request->file('photo')) {

			$path="/".$user->username."_".$user->id."/";
			$name=$user->username.".".$fileContents->getClientOriginalExtension();
			$full=$path.$name;
			Storage::disk('photos')->putFileAs($path, $fileContents,$name);
			$user->photo=$full;
		}
		$user->save();
		return redirect()->route('usuarios.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();
		$user->save();

		return redirect()
			->route('usuarios.index')
			->with('message','El usuario: '.$user->name. " ".$user->firstname." ha sido eliminado")
			;
	}
}
