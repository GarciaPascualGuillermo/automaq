<?php

namespace App\Http\Controllers;

use App;
use App\Job;
use App\User;
use App\Piece;
use App\AppLog;
use App\Events\FinishProcessEvent;
use App\Events\ProcessUpdate;
use App\Events\PhaseUpdated;
use App\Process;
use App\Workcenter;
use Illuminate\Http\Request;
use App\Events\WCAsignadosEvent;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AjustadorLoginRequest;
use App\Operation;
use \stdClass;
use App\Traits\OperacionesProductivas;
use Illuminate\Support\Facades\DB;

class JobsApiController extends Controller
{
    use OperacionesProductivas;
	function __construct()
	{
		$this->middleware('auth');
	}
	public function update_job(Request $request)
	{

		$data = $request->validate([
            'job' => 'required|exists:jobs,num_job'
        ]);

        $job = Job::where('num_job', $data['job'])->first();

        // Peticiónal JobBoss
		$ip = env('UPDATE_JOB_HOST', '172.31.2.6');
		$port = env('UPDATE_JOB_PORT', '8080');
		$url = "http://$ip:$port/api/updatejobs?job=" . $request->input('job');
        $ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);

        curl_close($ch);

        $operations = collect(json_decode($output));

        dd($operations);

        $piece = Piece::where('part_number', $operations->first()->part_Number)->limit(1)->first();

        if (!$piece) {
            return response()->json([
                'message' => 'No se ha encontrado una coincidencia para esta pieza'
            ], 405);
        }

        $processes = Process::withTrashed()->whereHas('job', function ($query) use ($data) {
            $query->where('num_job', $data['job']);
        })
        ->with(['operation'])
        ->get()
        ;

        $quantity = 0;
        foreach ($operations as $operation) {

            $process_coincidence = $processes->filter(function ($process) use ($operation) {
                return $process->operation->job_operation == $operation->job_Operation;
            })
            ->first();

            if (!$process_coincidence) {

                $new_operation = $piece->operations()->create([
                    "job_operation"         => $operation->job_Operation,
                    "job"                   => $operation->job,
                    "work_center"           => $operation->work_Center,
                    "operation_service"     => $operation->operation_Service,
                    "run_method"            => $operation->run_Method,
                    "standar_time"          => $operation->run,
                    "sequence"              => $operation->sequence,
                    "description"           => $operation->description,
                ]);
                $new_operation->process()->create([
                    'job_id'                => $job->id,
                    "operation_service"     => $operation->operation_Service,
                    "run_method"            => $operation->run_Method,
                    'quantity'              => $operation->order_Quantity,
                    "standar_time"          => $operation->run,
                ]);

            } else {

                $process_coincidence->update([
                    'job_id'                => $job->id,
                    'operation_service'     => $operation->operation_Service,
                    'run_method'            => $operation->run_Method,
                    'quantity'              => $operation->order_Quantity,
                    'standar_time'          => $operation->run,
                ]);

                $process_coincidence->operation->update([
                    'job_operation'         => $operation->job_Operation,
                    'job'                   => $operation->job,
                    'work_center'           => $operation->work_Center,
                    'operation_service'     => $operation->operation_Service,
                    'run_method'            => $operation->run_Method,
                    'standar_time'          => $operation->run,
                    'sequence'              => $operation->sequence,
                    'description'           => $operation->description,
                ]);

            }

            $quantity = $operation->order_Quantity;

        }

        $job->update([
            'quantity' => $quantity
        ]);

        event(new ProcessUpdate($job));

		return response()->json($operations, 201);
	}
	public function processes(Request $request)
	{
        $num_job = request('num_job');

		$processes = Process::with(['operation.piece.client'])
        ->whereHas('job', function ($query) use ($num_job) {
            $query->where('num_job', 'ILIKE', "%$num_job%");
        })
        ->whereHas('operation', function ($query) {
            $query->whereIn('work_center', $this->obtenerWorkcenterProductivos());
        })
        ->whereNull('workcenter_id')
        ->limit(60)
        ->get()
        ->map(function ($process)
        {
            $process->cycles = getCycles($process);
            return $process;
        })
        ->sortBy(function ($process) {
            return $process->job->num_job;
        });

		return response()->json($processes->toArray(), 200);
	}
	public function workcenters(Request $request)
	{

        $workcenter_section = strtoupper($request->input("section","S"));

		$workCenters = WorkCenter::with(['processes' => function ($query) {
            $query->orderBy('order','asc')
                ->where(function ($subquery){
                    $subquery
                        ->where('status','<>','cr')
                        ->whereNull('deleted_at')
                        ->orWhereNull('status');
                })
                ->with(['operation.piece.client', 'job', 'user_processes.cycles', 'user_processes', 'pauses']);
        }])
        ->where('section','like', $workcenter_section."%")
        ->orderBy('order', 'ASC')
        ->get()
        ->each(function ($workcenter)
        {
            $workcenter->processes->map(function ($process)
            {
                $process->cycles = getCycles($process);
                return $process;
            });
        });

        if (App::environment('local')) {
            $workCenters = $workCenters->take(5);
        }

        $dividirWorkcenters = Workcenter::orderByRaw("SUBSTRING(num_machine FROM '([0-9]+)')::BIGINT ASC, num_machine")
        ->with(['processes' => function ($query) {
            $query->orderBy('order','asc')
                ->where(function ($subquery) {
                    $subquery
                        ->where('status','<>','cr')
                        ->whereNull('deleted_at')
                        ->orWhereNull('status');
                });
        }])
        ->get()
        ;

		foreach ($workCenters as $key => $workcenter) {
			$workCenter["time"] = 0;
			if ($workcenter->processes->isNotEmpty()) {
				foreach ($workcenter->processes as $process) {
					# Tiempo en minutos
					$workcenter["time"]+=$process->job->quantity*$process->standar_time;
				}
			}
			$hour=60; # 1 hour = 60 min
            $day=15; # 1 day = 24hrs

			$workcenter["day"]=intval(($workcenter['time']/$hour)/$day);

			$workcenter['hour']=round(($workcenter['time']/$hour)-($workcenter['day']*$day),2);
			// $workcenter['hour']=intval($workcenter['time']/$hour);
			// $workcenter['min']=$workcenter['time'];
		}

		$all_process    = $workCenters->pluck('processes')->collapse();
		$programados    = $all_process->where('active',false)->count();
        $proceso        = $all_process->where('active',true)->count();

		return [
			'programados'           => $programados,
			'proceso'               => $proceso,
            'workcenters'           => $workCenters,
            'dividirWorkcenters'    => $dividirWorkcenters
        ];

	}
	public function addJobsToWorkcenter(Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){
			$wc=Workcenter::find($request->input('wc_id'));
			$process=null;
			$processes=json_decode($request->input('process'));
            $order=1;
			foreach ($processes as $key => $p) {
				// dd($p);
				$process=Process::where('id',$p->id)
					->with(['operation'])
					->limit(1)
					->first();

				$process->workcenter_id=$wc->id;
				// $process->order=$process->status?0:$order++;
				$process->order=$order++;
				$process->run_method=$process->operation->run_method;
				$process->standar_time=$process->operation->standar_time;
				$process->save();

			}
			event(new WCAsignadosEvent($wc));
			return response()->json($wc,200);
			return $wc;
		}
	}
	public function GetJobOfWC(Request $request)//-----NO---
	{
		return $request->all();
		$wc=Workcenter::find($request->input('wc_id'));
		return $wc->processes;
	}
	public function restoreJob(Request $request)
	{
		// recibo de la peticion post
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])) {
			$process = $request->input('process');
			$process = Process::where('id', $process['id'])
            ->first();

			$process->order = null;
			$process->workcenter_id = null;
            $process->save();

			return $process;
		}
	}
	public function change_job(AjustadorLoginRequest $request)
	{
		$user = User::where('idemployee' ,$request->input('username'))
					->limit(1)
                    ->get();

		$msg = "EL usuario o contraseña es incorrecto";

		if($user->isNotEmpty()) {

			$user=$user->first();
			$roles_access=collect(['root','sup.produccion','sup.linea','jefe.produccion']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);

			if ($intersect->count()) {

				if (Hash::check($request->input('password'), $user->password)) {

					$log = new AppLog();
					$wc = Workcenter::find($request->input('process.workcenter_id'));
					$user->app_logs()->create([
						'description'=>'Cambio de Job en '.$request->input('process.job.num_job')."- ".$request->input('process.operation.operation_service')." en ".$wc->num_machine
                    ]);
                    session()->forget('process_id');

                    $process = Process::find($request->input('process.id'))->update([
                        'status'    => null,
                        'active'    => false,
                    ]);

                    event(new PhaseUpdated());

					return response()->json(['process'=>$request->input('process')], 201);
				}
			} else $msg="No tienes permisos para esta acción, verifica con el administrador.";
		}
		return response()->json(['errors'=>['username'=>[$msg],$request->all()]], 401);
	}
	public function divide(Request $request)
	{

		if (auth()->user()->hasRoles(['root', 'ceo', 'gte.operacion', 'jefe.produccion'])) {


            $workCenter = WorkCenter::find($request->selected_workcenter);

            if (!$workCenter) {
                return response()->json([
                    'errors' => 'Hay un error con el workcenter seleccionado'
                ]);
            }

			$process = Process::where('id', $request->input('process.id'))->limit(1)->first();
            $process->quantity = $request->input('split1');

			if ($process->save()) {

				$process2                   = new Process();
				$process2->job_id           = $process->job_id;
				$process2->operation_id     = $process->operation_id;
				$process2->run_method       = $process->run_method;
				$process2->standar_time     = $process->standar_time;
                $process2->quantity         = $request->input('split2');
                $process2->workcenter_id    = $workCenter->id;

				if ($process2->save()) {

                    return response()->json($process2, 201);

                }

            }
			return response()->json(['errors' => 'No se puede completar esta operación'], 401);
		}
        return response()->json(['errors' => 'No tienes los roles necesarios'], 403);
	}
	public function terminar(Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])) {

            $process = Process::find($request->process_id);

            $process->update([
                'active' => false,
                'order' => null,
                'status' => 'cr',
                'cr_created_at' => now(),
            ]);

            $cycles = getCycles($process);

            $user_process = $process->user_processes()
            ->where('active', true)
            ->where('log_out', false)
            ->orderBy('created_at', 'DESC')
            ->first();

            if (!is_null($user_process)) {
                $user_process->update([
                    'log_out' => true,
                    'active' => false,
                    'end' => $cycles['pieceTotal'],
                    'end_time' => now()
                ]);
            }

            $process->process_log()->create([
                'user_id' => auth()->user()->id,
                'activity' => 'cr',
                'comment' => 'Finalizado por el gerente',
                'parent_id' => parent_id($process),
                'user_process_id' => $user_process ? $user_process->id : null,
                'workcenter_id' => $process->workcenter_id
            ]);


            event(new FinishProcessEvent($process));
            event(new PhaseUpdated());

            if ($request->has('completed')) {
                $process->delete();
            }

			return response()->json([
                'process' => $process
            ], 201);

		} else {

            return response()->json([
                'msg' => 'No tiene los permisos para esta accion'
            ], 402);

        }
	}
	public function charge(Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){
			$wc=Workcenter::findOrFail($request->input('wc_id'));
			$wc->charge=true;
			if ($wc->save()) {
				return response()->json(['wc',$wc],201);
			}
			return abort(405);
		}
	}
	public function chargeterminar(Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){
			$wc=Workcenter::findOrFail($request->input('wc.id'));
			$wc->charge=false;
			if ($wc->save()) {
				return response()->json(['wc',$wc],201);
			}
			return abort(404);
		}
	}

	public function soft_delete(Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){
			$process = Process::findOrFail($request->input('process.id'));
			$process->delete();
			if($process->save()){
				return response()->json(['process'=>$process],201);
			}
			return response()->json(['Error,Page not found'],404);
		}

	}

	public function restoreJobs(Request $request)
	{
        $num_job = $request->input('num_job','');

        $processes = Process::with(['operation.piece.client'])
        ->whereHas('job', function ($query) use ($num_job) {
            $query->where('num_job', 'ILIKE', "%$num_job%");
        })
        ->whereHas('operation', function ($query) {
            $query->whereIn('work_center', $this->obtenerWorkcenterProductivos());
        })
        ->where(function ($query)
        {
            $query
                ->where(function ($subquery)
                {
                    $subquery
                    ->whereNotNull('deleted_at')
                    ->where(function ($subSubquery)
                    {
                        $subSubquery
                            ->whereNull('workcenter_id')
                            ->orWhere(function ($subSubSubquery)
                            {
                                $subSubSubquery->whereNotNull('workcenter_id')
                                ->where('status', 'cr');
                            });
                    })
                    ->orWhere(function ($subSubquery)
                    {
                        $subSubquery
                            ->where('status', 'cr');
                    })
                    ;
                });

        })
        ->withTrashed()
        ->limit(10)
        ->get()
        ->sortBy(function ($process) {
            return $process->job->num_job;
        });

        return $processes;

		// $trasheds = Job::with(['processes' => function($j3) use ($request) {
		// 		$j3->withTrashed(['operation' => function ($j4) {
		// 			$j4->with(['piece' => function($j5) {
		// 				$j5->with('client');
		// 			}]);
		// 		}])
		// 		->with(['operation' => function ($q2){
		// 			$q2->with(['piece' => function ($q_piece){
		// 				$q_piece->with('client');
		// 			}]);
		// 		}])
		// 		->whereNotNull('deleted_at')
		// 	    ->with('job');
		// 	}])
		//     ->where('num_job','ilike','%'.$request->input('num_job','').'%')
		// 	->orderBy('num_job','asc')
		// 	->limit(30)
		// 	->get();

		// $array_trash = array();
		// $contador=0;
		// foreach ($trasheds as $key => $trashed) {
		// 	if (count($trashed->processes)== 0) {
		// 		unset($trasheds[$key]);
		// 		$contador++;
		// 	}elseif(count($trashed->processes)>0){
        //         $array_trash[$key]= $trasheds[$key]->processes;
		// 	    $contador++;
		// 	}
		// }
		// $processesfulltrashed = array();
		// foreach ($array_trash as $jobs) {
		// 	foreach ($jobs as $key => $process) {
		// 	   if (count($processesfulltrashed) == 30) {
		// 	   		break;
		// 	   	}
		// 	   	array_push($processesfulltrashed, $process);
		// 	   	$inspeccion='/MARADO|LIMPIEZA|ALMAC|INSPECCI/';
		// 		if (preg_match($inspeccion, $process->operation->description) ) {
		// 			$jobs->forget($key);
		// 		}
		// 	}
		// }
		// return $processesfulltrashed/*->push($processes->last())*/;
		// }
		// return response()->json([],404);
	}

	public function restoreFinalJobs (Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){

            $process_id = $request->process_id;

            $process = Process::where('id',  $process_id)
                ->withTrashed()
                ->limit(1)
                ->first();

            $process->status = null;
            $process->workcenter_id = null;
            $process->order = null;
            $process->deleted_at = null;
            $process->save();

		}
		return response()->json([
			'msg' => 'Success'
		], 200);
	}
	public function eficiencia(Request $request)
	{
		$process = Process::with(['user_processes'=>function ($queryUP){
				$queryUP
				->where('user_id',auth()->user()->id)
				->with(['cycles'=>function ($queryC){
					$queryC
						->orderBy('message_date','ASC')
						;
				}]);
			}])
			->with(['operation'])
			->where('id',$request->input('id',$request->input('process.id')))
            ->first();
        if ($process->user_processes[0]->cycles->isEmpty()) {
            return ['productividad' => 0];
        }else {
            return productivity($process);
        }

    }
    public function eficienciaCEO (Request $request) {

        $process = Process::with(['user_processes'=>function ($query){
            $query
            ->where('active', 'true')
            ->orderBy('created_at', 'DESC')
            ->with(['cycles' => function ($subquery){
                $subquery
                    ->orderBy('message_date','ASC')
                    ;
            }]);
        }])
        ->with(['operation'])
        ->where('id',$request->input('id',$request->input('process.id')))
        ->first();


        if ($process->user_processes[0]->cycles->isEmpty()) {
            return ['productividad' => 0];
        }else {
            $user = User::find($process->user_processes[0]->user->id);
            return productivity($process, $user);
        }
    }
}
