<?php

namespace App\Http\Controllers;

use Storage;
use App\Job;
use App\User;
use App\Setup;
use App\Process;
use App\UserProcess;
use App\Cycle;
use App\RejectedProcess;
use App\Events\CalidadEvent;
use App\Events\PhaseUpdated;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AjustadorLoginRequest;

class SetupController extends Controller
{
    function __construct()
    {
    	$this->middleware(['auth']);
        $this->middleware(['setup.redirect'])
            ->except(
                [
                    'loginajustador',
                    'loginajustadorproceso',
                    'autoliberacion',
                    'tipoliberacion',
                    'iniciaproduccion',
                    'noiniciaproduccion',
                    'recive',
                    'resetSetup',
                ]
            );
    }
    /**
     * start setup
     *
     * GET petition
     */
    public function index()
    {
    	return view('admin.operador.setup.index');
    }
    /**
     * start setup step2
     *
     * GET petition
     */
    public function step2()
    {
        // $process=Process::findOrFail(session('process_id'));
        // // $process->status='ih';
        // $process->ih_created_at=Carbon::now();
        // $process->save();
        // $dibujo=Storage::disk('dibujo')->allFiles();
        // dd($dibujo);
    	return view('admin.operador.setup.step2');
    }
    /**
     * start setup step2 - login for "ajuste espera"
     *
     * POST petition
     */
    public function loginajustador(AjustadorLoginRequest $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','sup.produccion','sup.linea','ajustador','jefe.produccion']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $process=Process::findOrFail(session('process_id'));
                    $setup= Setup::where('id',session('setup_id'))->first();
                    $process->status='ia';
                    $process->process_log()->create([
                        'activity' => 'ia',
                        'user_id' => $user->id,
                        'parent_id' => parent_id($process),
                        'user_process_id' => session('user_process_id'),
                        'workcenter_id' => $process->workcenter_id
                    ]);
                    event(new PhaseUpdated());
                    $setup->ia_created_at=$process->ia_created_at=Carbon::now();
                    $setup->fh_created_at=$process->fh_created_at=Carbon::now();
                    $setup->ajustador_ia()->associate($user);
                    $process->ajustador_ia()->associate($user);
                    if ($setup->save()) {
                        $process->save();
                    }
                    return response()->json(['url'=>route('operador.setup.step2proceso')], 201);
                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }
    /**
     * start setup step2 - "proceso"
     * Select "calidad" or "autoliberacion"
     *
     * GET petition
     */
    public function step2proceso()
    {
        return view('admin.operador.setup.step2proceso');
    }
    /**
     * start setup step2 - login for "ajuste proceso"
     *
     * POST petition
     */
    public function loginajustadorproceso(AjustadorLoginRequest $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','sup.produccion','sup.linea','ajustador','jefe.produccion']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $process=Process::findOrFail(session('process_id'));
                    $setup= Setup::where('id',session('setup_id'))->first();
                    $process->status='fa';
                    $process->process_log()->create([
                        'activity' => 'fa',
                        'user_id' => $user->id,
                        'parent_id' => parent_id($process),
                        'user_process_id' => session('user_process_id'),
                        'workcenter_id' => $process->workcenter_id
                    ]);
                    $setup->fa_created_at=$process->fa_created_at=Carbon::now();
                    $setup->ajustador_fa()->associate($user);
                    $process->ajustador_fa()->associate($user);
                    if ($setup->save()) {
                        $process->save();
                    }
                    return response()->json(['url'=>""], 201);
                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }
    /**
     * start setup step2 - login for "decide calidad o autoliberacion"
     *
     * POST petition
     */
    public function tipoliberacion(Request $request)
    {

        $process = Process::with('workcenter')
            ->with(['user_processes' => function ($u_p){
                $u_p->where('active',true)->limit(2)->with('user');
            }])
            ->with(['operation'=>function ($q_op){
                $q_op->with(['piece']);
            }])
            ->with(['ajustador_fa'])
            ->where('id',session('process_id'))
            ->first();

        if (is_null($process)) {

            return response()->json(['msg'=>'Proceso no encontrado'],404);

        }

        $process->status = $request->input('tipoliberacion');

        $setup = Setup::where('id',session('setup_id'))->first();

        if ($request->input('tipoliberacion') == 'il') {

            $setup->il_created_at=$process->il_created_at = Carbon::now();
            if ($setup->save()) {
                $process->save();
            }
            $process->process_log()->create([
                'activity' => 'il',
                'user_id' => auth()->user()->id,
                'parent_id' => parent_id($process),
                'workcenter_id' => $process->workcenter_id
            ]);
            event(new PhaseUpdated());
            broadcast( new CalidadEvent( $process ));

        } elseif ($request->input('tipoliberacion')=='ial') {

            $process->process_log()->create([
                'activity' => 'ial',
                'user_id' => auth()->user()->id,
                'parent_id' => parent_id($process),
                'user_process_id' => session('user_process_id'),
                'workcenter_id' => $process->workcenter_id
            ]);

            event(new PhaseUpdated());

            $setup->ial_created_at=$process->ial_created_at = Carbon::now();

        }


        if ($setup->save()) {
            $process->save();
        }


        return response()->json([
            'url' => route('operador.setup.step3.calidad')
        ], 201);
    }
    /**
     * start setup step3 - selected "calidad"
     *
     * GET petition
     */
    public function step3Calidad()
    {
        return view('admin.operador.setup.step3Calidad');
    }
    /**
     *  CALIAD SEND SUCCESS OR NOT
    */
    public function recive(Request $request)
    {
        $process=Process::findOrFail(session('process_id'));
        $setup=Setup::where('id',session('setup_id'))->first();
        if ($request->input('liberada')) {
            $process->status='ls';

            event(new PhaseUpdated());
            $setup->ls_created_at=$process->ls_created_at=$process->ls_created_at??Carbon::now();
            // $setup->ls_created_at=$setup->ls_created_at??Carbon::now();
        }
        else{
            if (!$request->input('calidad',false)) {
                $rejected_process=$process->rejected_processes()->create([
                    'started_at'=>$process->il_created_at,
                    'setup_id'=>$setup->id,
                    'ended_at'=>Carbon::now(),
                ]);
            }
            $process->status='ln';

            event(new PhaseUpdated());
            $setup->ln_created_at=$process->ln_created_at=Carbon::now();
        }
        if ($setup->save()) {
            $process->save();
        }
        return response()->json($request->all(), 201);
    }
    /**
     * start setup step3  selected "autoliberacion"
     *
     * GET petition
     */
    public function step3Autoliberacion()
    {
        return view('admin.operador.setup.step3Autoliberacion');
    }
    /**
     * start setup step3
     *
     * POST petition for login from "ajustador"
     */
    public function autoliberacion(AjustadorLoginRequest $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','sup.produccion','sup.linea','ajustador','jefe.produccion']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {
                    $process=Process::findOrFail(session('process_id'));
                    $setup=Setup::where('id',session('setup_id'))->first();

                    if ($request->input('liberada')=="true") {
                        $process->status='ls';
                        $process->process_log()->create([
                            'activity' => 'ls',
                            'user_id' => auth()->user()->id,
                            'parent_id' => parent_id($process),
                            'workcenter_id' => $process->workcenter_id,
                            'user_process_id' => session('user_process_id')
                        ]);
                        event(new PhaseUpdated());
                        $setup->ls_created_at=$process->ls_created_at=Carbon::now();
                        $setup->ajustador_ial()->associate($user);
                        $process->ajustador_ial()->associate($user);
                    }
                    else{
                        $process->status='ln';
                        $setup->ln_created_at=$process->ln_created_at=Carbon::now();
                        $rejected_process=$process->rejected_processes()->create([
                            'user_id'=>$user->id,
                            'started_at'=>$process->ln_created_at,
                            'setup_id'=>$setup->id,
                            'fullname'=>$user->idemployee.' '.$user->name.' '.$user->firstname,
                            'comment'=>'rechazada por autoliberacion',
                            'ended_at'=>Carbon::now(),
                        ]);
                        $process->process_log()->create([
                            'activity' => 'ln',
                            'user_id' => auth()->user()->id,
                            'parent_id' => parent_id($process),
                            'user_process_id' => session('user_process_id'),
                            'workcenter_id' => $process->workcenter_id

                        ]);
                        event(new PhaseUpdated());
                    }
                    if ($setup->save()) {
                        $process->save();
                    }
                    // return response()->json([$request->all(),$process->status,$request->input('liberada')], 401);
                    return response()->json($request->all(), 201);
                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }

    /**
     * start setup step4 - "Pieza liberada"
     *
     * GET petition
     */
    public function step4liberada(Request $request)
    {
        return view('admin.operador.setup.step4Liberada');
    }
    /**
    *  Save PR (Inicio  de produccion)
    */
    public function iniciaproduccion(Request $request)
    {
        $process=Process::findOrFail(session('process_id'));
        $getCycles = getCycles($process);
        // Se le suma uno por la pieza del ajuste
        $total_cycles = $getCycles['pieceTotal'] + 1;

        $user_process = UserProcess::where('id',session('user_process_id'))->first();
        $user_process->status_end=$process->status;
        $user_process->log_out=true;
        $user_process->active=false;
        $user_process->end = $total_cycles;
        $user_process->end_time=Carbon::now();

        $old_user_process = $user_process;

        if($user_process->save()) {

            $user_process = $process->user_processes()->create([
                'user_id'=>auth()->user()->id,
                'start'=>$total_cycles,
                'start_at'=>Carbon::now(),
                'start_time'=>Carbon::now(),
                'status_start'=>'pr',
                'active'=>true,
            ]);

            Cycle::create([
                'user_process_id' => $old_user_process->id,
                'message' => 'START',
                'message_date' => $old_user_process->start_time,
                'process_status' => 'pr',
            ]);

            Cycle::create([
                'user_process_id' => $old_user_process->id,
                'message' => '12345',
                'message_date' => now(),
                'process_status' => 'pr',
            ]);

            session()->forget('user_process_id');
            session()->put('user_process_id',$user_process->id);
        }

        $process->process_log()->create([
            'activity' => 'pr',
            'user_id' => auth()->user()->id,
            'parent_id' => parent_id($process),
            'user_process_id' => $user_process->id,
            'workcenter_id' => $process->workcenter_id
        ]);

        event(new PhaseUpdated());

        $process->status='pr';
        $process->total_piece=$total_cycles;
        $process->pr_created_at=Carbon::now();
        $process->save();
        return ['url'=>route('operador.index')];
    }
    /**
    *  Save
    */
    public function noiniciaproduccion(Request $request)
    {
        $process=Process::findOrFail(session('process_id'));
        $setup=Setup::where('id',session('setup_id'))->first();

        $process->status='ia';

        event(new PhaseUpdated());

        $process->process_log()->create([
            'activity' => 'ia',
            'user_id' => auth()->user()->id,
            'parent_id' => parent_id($process),
            'user_process_id' => session('user_process_id'),
            'workcenter_id' => $process->workcenter_id
        ]);

        $user_process = $process->user_processes()->where('active', true)->orderBy('created_at', 'DESC')->first();

        Cycle::create([
            'user_process_id' => $user_process->id,
            'message' => 'START',
            'message_date' => $user_process->start_time,
            'process_status' => 'pr',
        ]);

        Cycle::create([
            'user_process_id' => $user_process->id,
            'message' => '12345',
            'message_date' => now(),
            'process_status' => 'pr',
        ]);

        $setup->ia_created_at=$process->ia_created_at=Carbon::now();
        if ($setup->save()) {
            $process->save();
        }
        return ['url'=>route('operador.setup.step2')];
    }
    /**
     * start setup step4 - "Pieza rechazada"
     *
     * GET petition
     */
    public function step4rechazada( )
    {
        return view('admin.operador.setup.step4Rechazada');
    }
    public function resetSetup(Request $request)
    {
        $data = $request->validate([
            'process_id' => 'required|exists:processes,id'
        ]);

        $process = Process::find($data['process_id']);

        $process->update([
            'status'    => 'ih',
            'active'    => true,
        ]);

        event(new PhaseUpdated());

        $process->process_log()->create([
            'activity' => 'ih',
            'user_id' => auth()->user()->id,
            'comment' => 'Reinicio por el operador',
            'parent_id' => parent_id($process),
            'user_process_id' => session('user_process_id'),
            'workcenter_id' => $process->workcenter_id
        ]);

        return response()->json([
            'msg' => 'Setup reiniciado'
        ], 201 );
    }
}
