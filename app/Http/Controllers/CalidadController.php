<?php

namespace App\Http\Controllers;

use App\Events\PhaseUpdated;
use App\User;
use App\Process;
use App\Workcenter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Events\VerifiedQualityEvent;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\LiberaLoginRequest;

class CalidadController extends Controller
{
     function __construct()
    {
    	$this->middleware(['auth']);
        $this->middleware(['deb.calidad.middleware'])->except(['mantenimiento']);
        $this->middleware(['deb.mantenimiento.middleware'])->only(['mantenimiento']);
    }
    public function mantenimiento()
    {

    	return view('admin.solicitudes_control.espera_liberacion')
            ;
    }

    public function liberacion()
    {
        return view('admin.calidad.liberacion.index')
            ;
    }
    public function liberacion4k()
    {
        return view('admin.calidad.layouts.4k.app')
            ;
    }
    public function getProcessLiberacion(Request $request)
    {

        $processes = Process::with('workcenter')
            ->with(['user_processes' => function ($u_p){
                $u_p->where('active', true)
                ->limit(2)
                ->with('user');
            }])
            ->with(['operation'=>function ($q_op) {
                $q_op->with(['piece']);
            }])
            ->with(['job'])
            ->with(['ajustador_fa'])
            ->where('status', 'il')
            ->whereNotNull('workcenter_id')
            ->get();

        return $processes;

    }
    public function liberacionsuccess(Request $request)
    {
        $wc=Workcenter::find($request->input('process.workcenter_id'));
        broadcast( new VerifiedQualityEvent( $wc, $request->input('success') )) ;
        return [$request->input('success')];
    }
    public function liberaLogin(LiberaLoginRequest $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','gte.calidad','insp.calidad']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $process=Process::findOrFail($request->input('process_id'));
                    $setup=$process->setups->where('active',true)->first();
                    $rejected_process=$process->rejected_processes()->create([
                        'user_id'=>$user->id,
                        'setup_id'=>$setup->id,
                        'fullname'=>$user->idemployee.' '.$user->name.' '.$user->firstname,
                        'started_at'=>$process->il_created_at,
                        'ended_at'=>Carbon::now(),
                        'comment'=>$request->input('comment'),
                        'aceptada'=>true,
                    ]);

                    $process->status='ls';

                    $process->process_log()->create([
                        'user_id' => $user->id,
                        'activity' => 'ls',
                        'parent_id' => parent_id($process),
                        'comment' => $request->input('comment'),
                        'workcenter_id' => $process->workcenter_id
                    ]);

                    event(new PhaseUpdated());

                    $setup->ls_created_at=$process->ls_created_at=Carbon::now();
                    $setup->ajustador_il()->associate($user);
                    $process->ajustador_il()->associate($user);
                    if ($setup->save()) $process->save();

                    broadcast( new VerifiedQualityEvent( $process->workcenter, true )) ;
                    return response()->json($process->workcenter, 201);
                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }
    public function rechazaLogin(LiberaLoginRequest $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();
        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','gte.calidad','insp.calidad']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $process= Process::findOrFail($request->input('process_id'));
                    $setup=$process->setups->where('active',true)->first();
                    // return response()->json([$setup],401);
                    $rejected_process=$process->rejected_processes()->create([
                        'user_id'=>$user->id,
                        'setup_id'=>$setup->id,
                        'fullname'=>$user->idemployee.' '.$user->name.' '.$user->firstname,
                        'started_at'=>$process->il_created_at,
                        'ended_at'=>Carbon::now(),
                        'comment'=>$request->input('comment'),
                    ]);

                    $process->process_log()->create([
                        'user_id' => $user->id,
                        'activity' => 'ln',
                        'comment'=>$request->input('comment'),
                        'parent_id' => parent_id($process),
                        'workcenter_id' => $process->workcenter_id
                    ]);
                    event(new PhaseUpdated());

                    $process->status='ln';
                    $process->ln_created_at=Carbon::now();
                    $process->save();
                    broadcast( new VerifiedQualityEvent( $process->workcenter, false )) ;
                    return response()->json($request->all(), 201);
                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }

}
