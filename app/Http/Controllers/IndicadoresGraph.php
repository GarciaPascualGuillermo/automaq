<?php

namespace App\Http\Controllers;

use App\Process;
use App\Job;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class IndicadoresGraph extends Controller
{
    public function machineUse(Request $request)
    {



        $data = [
			'sinuso' 		=> [],
			'herramentaje' 	=> [],
			'ajuste' 		=> [],
			'produccion'  	=> [],
			'paros' 		=> [],
			'mantenimiento' => []
		];

		$properties = [
			'sinuso',
			'herramentaje' ,
			'ajuste' ,
			'produccion' ,
			'paros' ,
			'mantenimiento'
        ];

        $mensual = [];

        $labels = [];

        $day = request('day');

        // $day = now()->subDays(2);

        if (!$day) {

            for ( $i = 1 ; $i < 6 ; $i++ ) {

                $start = now()->startOfMonth()->addWeeks($i - 1);
                $end = now()->startOfMonth()->addWeeks($i);

				$workcenters = $this->getWorkcenter($start, $end);

				$data_time = $this->extractDataTimeWorkcenter($workcenters);

				foreach ($properties as $property) {
					array_push($data[$property], $data_time[$property][0]);
				}

                array_push($labels, $start->format("d M")."  ".$end->format("d M"));

            }

        } else {

            $start = $end = Carbon::parse($day);

			$workcenters = $this->getWorkcenter($start, $end);

			$data_time = $this->extractDataTimeWorkcenter($workcenters);

            array_push($labels, $start->format("d M"));

            foreach ($properties as $property) {
				array_push($data[$property], $data_time[$property][0]);
			}

        }


        foreach ($data as $key => $property) {
            $mensual[$key] = array_sum($property);
        }

        return [
            'label' => $labels,
            'data_semanal' => $data,
            'data_mensual' => $mensual
        ];

    }
    public function getWorkcenter(Carbon $from, Carbon $to)
    {
        $workcenters = Workcenter::with(['processes.process_log' => function ($query) use ($from, $to)
        {
            $query
            ->date($from, $to);
        }])
        ->get()
        ;

        $workcenters->map(function ($workcenter) use ($from, $to)
        {
            $workcenter->tiempos = obtenerTiempoIndicadores($workcenter, $from, $to);
            return $workcenter;
        });

        return $workcenters;
    }

    public function extractDataTimeWorkcenter($workcenters)
    {
        $sinuso = $workcenters->sum('tiempos.sinuso');
        $herramentaje = $workcenters->sum('tiempos.herramentaje');
        $ajuste = $workcenters->sum('tiempos.ajuste');
        $produccion = $workcenters->sum('tiempos.produccion');
        $paros = $workcenters->sum('tiempos.paros');
        $mantenimiento = $workcenters->sum('tiempos.mantenimiento');

        return collect([
			'sinuso' => [$sinuso],
			'herramentaje' => [$herramentaje],
			'ajuste' => [$ajuste],
			'produccion' => [$produccion],
			'paros' => [$paros],
			'mantenimiento' => [$mantenimiento],
		]);
    }
	public function semanal_mensual(Request $request)
	{
		$day = $request->input('day');
		$now=Carbon::now()->endOfWeek();
		$last_week=Carbon::now()->subWeeks(6);
		$resultados=[];
		$resultados_day=[];
		$resultados_name=[];
		$resultados_name_day=[];
		$label=[];
		$setup_semanal=[];
		$prod_semanal=[];
		$sinuso_semanal=[];
		$i = 0;
		// $sinuso_semanal=[];
		// $resultados=getTimeIndicadores2(Carbon::createFromFormat('Y-m-d H:i:s','2019-10-07 00:00:00'),Carbon::createFromFormat('Y-m-d H:i:s','2019-10-07 23:59:59'));
		// dd($resultados['data_name']);
		if ($day) {
			$now=Carbon::now();
			$day = Carbon::parse($day);
			$res_day=getTimeIndicadores2($day->startOfDay(),$day->endOfDay());
			$resultados_day[]=$res_day['data'];
			$resultados_name_day[$i]=[];
			foreach ($res_day['data_name'] as $key => $value) {
				$resultados_name_day[$i][$key]=intval(round(($value/60)/60,0));
			}
			// dd($resultados_name,$res['data_name'] );
			$setup_day[]=intval(round(($res_day['data_name']['setup']/60)/60,0));
			$prod_day[]=intval(round(($res_day['data_name']['produccion']/60)/60,0))+intval(round(($res_day['data_name']['mantenimiento']/60)/60,0))+intval(round(($res_day['data_name']['pausa_otros']/60)/60,0));
			$sinuso_day[]=intval(round(($res_day['data_name']['sinuso']/60)/60,0));
			$label[]=$day->startOfDay()->format("d M");

			$porasignar = Job::with(['processes' => function($q) use($day,$now){
			    $q->with(['operation' => function($d){
			        $d->select('description')
 			        ->where('description','not ilike','%MARADO%')
			        ->orWhere('description','not ilike','%LIMPIEZA%')
			        ->orWhere('description','not ilike','%ALMAC%')
			        ->orWhere('description','not ilike','%INSPECCI%');
			    }])
			    ->where('workcenter_id',null)
		        ->where('status',null)
		        ->whereBetween('updated_at', [$day->startOfDay(), $now->endOfDay()]);
			}])
			->get();
			$forasign = $porasignar->map(function ($item, $key) use($porasignar){
	            if (count($item->processes) != 0) {
	                return count($item->processes);
	            }else {
	            	unset($porasignar[$key]);
	            }

	        });

	        $forasign->all();
			$enproceso = Job::with(['processes' => function($q) use($day,$now){
				    $q->with(['operation' => function($d){
				        $d->select('description')
	 			        ->where('description','not ilike','%MARADO%')
				        ->orWhere('description','not ilike','%LIMPIEZA%')
				        ->orWhere('description','not ilike','%ALMAC%')
				        ->orWhere('description','not ilike','%INSPECCI%');
				    }])
				    ->whereNotIn('status',['cr',null])
				    ->whereBetween('updated_at', [$day->startOfDay(), $now->endOfDay()]);
				}])
				->get();
			$inprogress = $enproceso->map(function ($item, $key) use($enproceso){
	            if (count($item->processes) != 0) {
	                return count($item->processes);
	            }else {
	            	unset($enproceso[$key]);
	            }

	        });

	        $inprogress->all();
			$terminados = Job::with(['processes' => function($q) use($day,$now){
				    $q->with(['operation' => function($d){
				        $d->select('description')
	 			        ->where('description','not ilike','%MARADO%')
				        ->orWhere('description','not ilike','%LIMPIEZA%')
				        ->orWhere('description','not ilike','%ALMAC%')
				        ->orWhere('description','not ilike','%INSPECCI%');
				    }])
				    ->where('status','cr')->where('ih_created_at','>=',$now->subDays(7)->startOfDay())
				    ->whereBetween('updated_at', [$day->startOfDay(), $now->endOfDay()]);
				}])
				->get();
			$finished = $terminados->map(function ($item, $key) use($terminados){
	            if (count($item->processes) != 0) {
	                return count($item->processes);
	            }else {
	            	unset($terminados[$key]);
	            }

	        });

	        $finished->all();
		}else{
			$setup_day = [];
			$prod_day = [];
			$sinuso_day = [];
			$porasignar=array();
			$enproceso=array();
			$terminados=array();
			while ($last_week->lessThanOrEqualTo($now)) {
				$week_start=Carbon::createFromFormat('Y-m-d H:i:s',$last_week)->startOfWeek();
				$weekend=Carbon::createFromFormat('Y-m-d H:i:s',$last_week)->endOfWeek()->subDay();
				// dd($week_start,$weekend,$last_week);
				$res=getTimeIndicadores2($week_start,$weekend);
				$resultados[]=$res['data'];
				$resultados_name[$i]=[];
				foreach ($res['data_name'] as $key => $value) {
					$resultados_name[$i][$key]=intval(round(($value/60)/60,0));
				}
				// dd($resultados_name,$res['data_name'] );
				$setup_semanal[]=intval(round(($res['data_name']['setup']/60)/60,0));
				$prod_semanal[]=intval(round(($res['data_name']['produccion']/60)/60,0))+intval(round(($res['data_name']['mantenimiento']/60)/60,0))+intval(round(($res['data_name']['pausa_otros']/60)/60,0));
				$sinuso_semanal[]=intval(round(($res['data_name']['sinuso']/60)/60,0));
				$label[]=$week_start->format("d M")."  ".$weekend->format("d M");
				$i++;
				$last_week->addWeek();
			}
	    }
		$resultados_name=collect($resultados_name);
		$resultados_name_day=collect($resultados_name_day);

		return [
			'label'=>$label,
			'data_day'=>[
				'setup_day'=>$setup_day,
				'prod_day'=>$prod_day,
				'sinuso_day'=>$sinuso_day,
			],
			'data_semanal'=>[
				'setup_semanal'=>$setup_semanal,
				'prod_semanal'=>$prod_semanal,
				'sinuso_semanal'=>$sinuso_semanal,
			],
			'data_mensual'=>[
				$resultados_name->sum('produccion')+$resultados_name->sum('mantenimiento')+$resultados_name->sum('pausa_otros'),
				$resultados_name->sum('sinuso'),
				$resultados_name->sum('setup'),
			],
			'data_diario'=>[
				$resultados_name_day->sum('produccion')+$resultados_name_day->sum('mantenimiento')+$resultados_name_day->sum('pausa_otros'),
				$resultados_name_day->sum('sinuso'),
				$resultados_name_day->sum('setup'),
			],
			'data_jobs'=>[
			    'porasignar'=>count($porasignar),
			    'enproceso'=>count($enproceso),
			    'terminados'=>count($terminados),
			]
		];

		dd($resultados,$resultados_name,$resultados_name->sum('sinuso'));
	}
	public function semanal(Request $request)
	{
		$now=Carbon::now();
        $last_week=Carbon::now()->subWeek()->startOfDay();


		// dd($now,$last_week);
		$data_name=[];
		$procentajes=[];
		$day = $request->input('day');
		if ($day) {
		    $day = Carbon::parse($day);
			$res=getTimeIndicadores2($day->startOfDay(),$day->endOfDay());
			foreach ($res["data_name"] as $key => $value) {
				$data_name[$key]=intval(round(($value/60)/60,0));
				$procentajes[$key]=intval(round(($value/$res["data_name"]["sinuso_total"])*100,0));
		    }
		}else{
			$res=getTimeIndicadores2($last_week,$now);
			foreach ($res["data_name"] as $key => $value) {
				$data_name[$key]=intval(round(($value/60)/60,0));
				$procentajes[$key]=intval(round(($value/$res["data_name"]["sinuso_total"])*100,0));
			}
		}

		$data_name=collect($data_name);

		return [
			"horas"=>$data_name,
			"porcentajes"=>$procentajes,
		];
	}
	public function productividad(Request $request)
	{
		$day = $request->input('day');
		$now=Carbon::now()->endOfWeek();
		$last_week=Carbon::now()->subWeeks(6)->startOfDay();
		if (!$day) {
			$user_processes = UserProcess::whereBetween('start_time',[$last_week,$now])
				->where('productivity','>',0)
				->get()
				;
			$user_processes_month = UserProcess::whereBetween('start_time',[Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
				->where('productivity','>',0)
				->get()
				;
			$label=[];
			$productivity=[];
			while ($last_week->lessThanOrEqualTo($now)) {
				$week_start=Carbon::createFromFormat('Y-m-d H:i:s',$last_week)->startOfWeek();
				$weekend=Carbon::createFromFormat('Y-m-d H:i:s',$last_week)->endOfWeek()->subDay();
				$label[]=$week_start->format("d M")."  ".$weekend->format("d M");
				$data=$user_processes->whereBetween('start_time',[$week_start,$weekend]);

				$productivity[]=$data->avg('productivity')>100?100:intval(round($data->avg('productivity'),0));

				$last_week->addWeek();
			}
		} else {

			$day = Carbon::parse($day);
            $user_processes = UserProcess::whereDate('start_time', $day)
                ->whereNotNull('productivity')
                ->where('productivity', '>=', 0)
                ->get();

			$label = [];
			$productivity = [];

			$label[] = $day->format("d M");
			$productivity[] = $user_processes->avg('productivity');

		}

		$productivity=collect($productivity);

		return [
			'label' => $label,
			'productivity' => round($productivity[0], 0),
			'prom'=> isset($user_processes_month) ? intval(round($user_processes_month->avg('productivity'), 0)) : round($productivity[0], 0),
		];
	}
}
