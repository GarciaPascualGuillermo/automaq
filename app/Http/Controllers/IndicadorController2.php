<?php

namespace App\Http\Controllers;

use App\Pause;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class IndicadorController2 extends Controller
{
    function __construct()
	{
		$this->middleware('auth');
		$this->middleware('indicador.middleware');
	}
	public function indicadores2()
	{
		// $fech_i=Carbon::create(2019, 9, 26, 14, 0, 0); // 2000-02-04 13:00:00
		// // dd($fech_i);
		// $fech_f=Carbon::create(2019, 9, 25, 14, 0, 0); 
		// fechas_tecmaq($fech_i,$fech_f);
		$mes_herra=0;
		$mes_ajus=0;
		$mes_libe=0;
		$mes_auto=0;
		$mes_setup=0;
		$mes_produccion=0;
		$semana_herra=0;
		$semana_ajus=0;
		$semana_libe=0;
		$semana_auto=0;
		$semana_setup=0;
		$now=Carbon::now();
		$last_month=Carbon::now()->subDays(45)->startOfDay();
		$wcs=Workcenter::all();
	//MENSUAL
				
		//SETUP
		foreach ($wcs as $wc) {
			while ($last_month->lessThanOrEqualTo($now)) {
				foreach ($wc->Process->whereDate($last_month) as $p) {
					$mes_herra+=fechas_tecmaq($p->ih_created_at,$process->fh_created_at);
					$mes_ajus+=fechas_tecmaq($p->ia_created_at,$process->fa_created_at);
					$mes_libe+=fechas_tecmaq($p->il_created_at,$process->ls_created_at);
					$mes_auto+=fechas_tecmaq($p->ial_created_at,$process->ls_created_at);						
				}
			}
			$last_month->addDay();
		}
		$mes_setup=$mes_herra+$mes_ajus+$mes_libe+$mes_auto;

		//PRODUCCION
		foreach ($wcs as $wc) {
			while ($last_month->lessThanOrEqualTo($now)) {
				foreach ($wc->Process->whereDate($last_month) as $p) {
					foreach ($p->UserProcess as $users) {
						$mes_produccion+=fechas_tecmaq($users->start_time,$users->end_time);
					}						
				}
			}
			$last_month->addDay();
		}

		//SIN USO

	//SEMANAL

		//SETUP
		$last_sem=Carbon::now()->subWeek()->startOfDay();
		foreach ($wcs as $wc) {
			while ($last_sem->lessThanOrEqualTo($now)) {
				foreach ($wc->process->whereDate($last_sem) as $p) {
					$semana_herra+=fechas_tecmaq($p->ih_created_at,$process->fh_created_at);
					$semana_ajus+=fechas_tecmaq($p->ia_created_at,$process->fa_created_at);
					$semana_libe+=fechas_tecmaq($p->il_created_at,$process->ls_created_at);
					$semana_auto+=fechas_tecmaq($p->ial_created_at,$process->ls_created_at);						
				}
			}
			$last_sem->addDay();
		}
		$semana_setup=$semana_herra+$semana_ajus+$semana_libe+$semana_auto;

	}
	//PRODUCCION
}
