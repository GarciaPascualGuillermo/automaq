<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\Traits\Schedule as TraitsSchedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    use TraitsSchedule;

    function __construct()
	{
		$this->middleware(['auth']);
    }
    public function index(Request $request)
    {

        $now = now();
        $year = $now->year;
        $weekNumber = $now->weekOfYear;

        $schedules = Schedule::where('year', '>=', $year)
        ->whereDate('start_of_week', '>=', now())
        ->orderBy('id', 'ASC')
        ->get()
        ->map(function ($schedule)
        {
            $schedule->end_of_week = Carbon::parse($schedule->start_of_week)->addWeek()->format('d-m-Y');
            $schedule->start_of_week = Carbon::parse($schedule->start_of_week)->format('d-m-Y');
            return $schedule;

        })
        ;

        return view('admin.schedule.index', compact('schedules'));
    }
    public function alternate(Request $request, Schedule $schedule)
    {
        if ($schedule->first_turn == 'A') {
            $first_turn = 'B';
            $second_turn = 'A';
        } else {
            $first_turn = 'A';
            $second_turn = 'B';
        }

        $schedule->update([
            'first_turn'    => $first_turn,
            'second_turn'   => $second_turn
        ]);

        return response()->json([
            'message' => 'Se ha cambiado el orden de los turnos exitosamente'
        ]);

    }
    public function generate(Request $request)
    {
        $request->validate([
            'to' => 'required'
        ]);

        $success = $this->feedScheduleTable(now(), Carbon::parse($request->to));

        return response()->json([
            'message' => 'Semanas generadas exitosamente'
        ], 201);
    }
}
