<?php

namespace App\Http\Controllers;

use App\User;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ProductividadController extends Controller
{

	function __construct()
	{
		// $this->middleware(['auth']);

	}
    public function index(Request $request)
    {
        $date = Carbon::now()->subDay();
        $fecha = Carbon::parse($date);
        $now = ucwords(dia_esp($fecha->format('l')))." ". $fecha->format('d') . ' de ' . mes_esp($fecha->format('n')) . ' de ' . $fecha->format('Y');
        $count=1;
        if ($request->has('top')) {
            $index=$inicio=6;
            $limite=10;
        }else{
            $index=$inicio=1;
            $limite=5;
        }

        $yesterday=Carbon::now()->subDay()->startOfDay();
        $yesterday_endday= Carbon::now()->subDay()->endOfday();
        $user_processes = UserProcess::with(["user"])
            ->whereBetween('start_time',[$yesterday,$yesterday_endday])->get();
        $user_prom=[];
        foreach ($user_processes->groupBy('user_id') as $user_id => $u_) {
            $user_prom[$user_id]['prom']=$u_->pluck('productivity')->avg();
        }
        $user_prom=collect($user_prom);
        $user_prom=$user_prom->sortByDesc('prom');
        $user_prom=$user_prom->chunk(10)[0];

        return view('admin.productividad.index')
            ->with('date',$now)
            ->with('user_prom',$user_prom)
            ->with('inicio',$inicio)
            ->with('index',$index)
            ->with('count',$count)
            ->with('limite',$limite)
            ;
    }
    public function index2(Request $request, $part = null)
    {
        $date = Carbon::now()->subDay();
        $fecha = Carbon::parse($date);
        $now = ucwords(dia_esp($fecha->format('l')))." ". $fecha->format('d') . ' de ' . mes_esp($fecha->format('n')) . ' de ' . $fecha->format('Y');
        $count=1;
        if ($request->has('top')) {
            $index=$inicio=6;
            $limite=10;
        }else{
            $index=$inicio=1;
            $limite=5;
        }

        $yesterday=Carbon::now()->subDay()->startOfDay();
        $yesterday_endday= Carbon::now()->subDay()->endOfday();
        $user_processes = UserProcess::with(["user"])
            ->whereBetween('start_time',[$yesterday,$yesterday_endday])->get();
        $user_prom=[];
        foreach ($user_processes->groupBy('user_id') as $user_id => $u_) {
            $user_prom[$user_id]['prom']=$u_->pluck('productivity')->avg();
        }
        $user_prom=collect($user_prom);
        $user_prom = $user_prom->sortByDesc('prom');

        if ($part) {
            if ($user_prom->count() > 5)  {
                $user_prom = $user_prom->chunk(5)[1];
            } else {
                $user_prom = [];
            }
        } else {
            $user_prom = $user_prom->take(5);
        }



        return view('admin.productividad.resolucion_mininuc.index2')
            ->with('date',$now)
            ->with('user_prom',number_format($user_prom,2))
            ->with('inicio',$inicio)
            ->with('index',$index)
            ->with('count',$count)
            ->with('limite',$limite)
            ;
    }

    public function top6()
    {
        return view('admin.productividad.top6');
    }
}
