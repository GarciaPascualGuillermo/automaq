<?php

namespace App\Http\Controllers;

use DB;
use App\Role;
use App\User;
use App\Process;
use App\Workcenter;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\MantenimientoCNCEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Events\MantenimientoEsperaEvent;
use App\Events\PhaseUpdated;

class PausaController extends Controller
{
    function __construct()
    {
    	$this->middleware(['auth']);
    }
    public function motivo(Request $request)
    {
        $process=Process::find(session('process_id'));
        $return = null;
    	switch (strtolower($request->input('pausaMotivo'))) {
            case 'mantenimiento':

                $roles = Role::whereIn('name',['jefe.mantenimiento','tec.mantenimiento'])->get();

                $users = DB::table('users as u')
                    ->join('role_user as r_l','u.id','=','r_l.user_id')
                    ->select("u.*")
                    ->whereIn('r_l.role_id',$roles->pluck('id'))
                    ->get();

                dispatch(new SendEmail($users, $process));

                $pause = $process->pauses()->create([
                    'operador_id'       => auth()->user()->id,
                    'motivo'            => 'mantenimiento',
                    'started_at'        => now(),
                    'workcenter_id'     => $process->workcenter->id,
                    'comments'          => $request->comments,
                ]);

                $workcenter = $process->workcenter;
                $workcenter->on_maintenance = true;
                $workcenter->save();

                $process->process_log()->create([
                    'activity' => 'pm',
                    'user_id' => auth()->user()->id,
                    'parent_id' => parent_id($process),
                    'comment' => 'El operador solicitó mantenimiento',
                    'user_process_id' => session('user_process_id'),
                    'workcenter_id'=>$process->workcenter->id,
                    'pause_id' => $pause->id
                ]);

                session()->forget('pause_mantenimiento_id');
                session()->put('pause_mantenimiento_id',$pause->id);
                session()->save();

                broadcast(new MantenimientoEsperaEvent($process->workcenter));
                $return = [
                    'resp'  => 1,
                    'url'   =>route('operador.pausa.login')
                ];
    			break;
    		case 'falta de herramienta':
                $process=Process::findOrFail(session('process_id'));
                $process->status='pfh';
                $process->pfh_created_at=Carbon::now();

                $process->process_log()->create([
                    'activity' => 'pfh',
                    'user_id' => auth()->user()->id,
                    'parent_id' => parent_id($process),
                    'comment' => 'Pausa por herramienta',
                    'user_process_id' => session('user_process_id'),
                    'workcenter_id'=>$process->workcenter->id,
                ]);

                $pause=$process->pauses()->create([
                    'operador_id'=>auth()->user()->id,
                    'motivo'=>'herramienta',
                    'started_at'=>Carbon::now(),
                ]);
                session()->forget('pause_herramienta_id');
                session()->put('pause_herramienta_id',$pause->id);
                session()->save();

                if($pause->save()) $process->save();
                $return = ['resp'=>2,'url'=>route('operador.pausa.herramienta')];

                break;
            case 'falta de material':
                $process=Process::findOrFail(session('process_id'));
                $process->status='pfm';
                $process->pfm_created_at=Carbon::now();

                $process->process_log()->create([
                    'activity' => 'pfm',
                    'user_id' => auth()->user()->id,
                    'parent_id' => parent_id($process),
                    'comment' => 'Pausa por material',
                    'user_process_id' => session('user_process_id'),
                    'workcenter_id'=>$process->workcenter->id
                ]);

                $pause=$process->pauses()->create([
                    'operador_id'=>auth()->user()->id,
                    'motivo'=>'material',
                    'started_at'=>Carbon::now(),
                ]);
                session()->forget('pause_material_id');
                session()->put('pause_material_id',$pause->id);
                session()->save();

                if($pause->save()) $process->save();
                $return = ['resp'=>3,'url'=>route('operador.pausa.material')];
                break;
    		default:
                $return = ['resp'=>0,'url'=>null];
    			break;
        }
        event(new PhaseUpdated());
        $process->save();
    	return $return;
    }
    public function m_finalizar(Request $request)
    {
        $process=Process::findOrFail(session('process_id'));
        $process->status='pr';

        $process->process_log()->create([
            'activity' => 'pr',
            'user_id' => auth()->user()->id,
            'parent_id' => parent_id($process),
            'user_process_id' => session('user_process_id'),
            'workcenter_id' => $process->workcenter_id
        ]);

        if (session('pause_herramienta_id')) {
            $pause=$process->pauses->where('id',session('pause_material_id'))->first();
        }else{
            $pause=$process->pauses->sortByDesc('started_at')->first();
        }


        $pause->ended_at=Carbon::now();
        if ($pause->save()) $process->save();
        return $pause;
    }
    public function h_finalizar(Request $request)
    {
        $process=Process::findOrFail(session('process_id'));
        $process->status='pr';

        $workcenter = $process->workcenter;
        $workcenter->on_maintenance = false;
        $workcenter->save();


        $process->process_log()->create([
            'activity' => 'pr',
            'user_id' => auth()->user()->id,
            'parent_id' => parent_id($process),
            'user_process_id' => session('user_process_id'),
            'workcenter_id' => $process->workcenter_id
        ]);

        if (session('pause_herramienta_id')) {
            $pause=$process->pauses->where('id',session('pause_herramienta_id'))->first();
        }else{
            $pause=$process->pauses->sortByDesc('started_at')->first();
        }
        $pause->ended_at=Carbon::now();
        if ($pause->save()) $process->save();
        return $pause;
    }
    public function login(Request $request)
    {
        return ['resp'=>'ok'];
    }

}
