<?php

namespace App\Http\Controllers;

use App\Pause;
use App\Process;
use App\Job;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Traits\OperacionesProductivas;


class IndicadorController extends Controller
{
    use OperacionesProductivas;

	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('indicador.middleware');
	}
	public function indicadores_v01()
	{
		$prom=collect([10,10,10,10,10,5]);
		$now=Carbon::now()->endOfDay()->subDay();
		$mensual=Carbon::now()->startOfMonth()->startOfDay()->subDays(60);
		$mensual_end_day=Carbon::now()->startOfMonth()->endOfDay()->subDays(60);

		$ago7day=Carbon::now()->subDays(8)->startOfDay();
		$ago7day_end_day=Carbon::now()->subDays(8)->endOfDay();

		$processes=Process::with('user_processes')->where('ih_created_at','>=',$mensual)->whereNotNull('workcenter_id')->get();
		$day=30;
		$day_tecmaq=15; # 15 horas laborales
		$hours=60;
		$minutes=60;
		$seconds=60;
		$horas_no_laboral=(($day-$day_tecmaq)*$minutes*$seconds);

		/*
		*  Uso de maquinas (MENSUAL)
		*/
			$setup_mensual=0;
			$production_mensual=0;
			$sin_uso_mensual=0;

			$wcs=Workcenter::whereNotNull('ip_address')->get();
			$total_horas_laborales_por_dia=($wcs->count() * $day_tecmaq)*$minutes*$seconds; //porque se multiplica por segundos solo rseria por minutos, el dato dte lo da en segundo no?
			//no estas sacando ya el total con tan solo multiplicarlo day_tecmaq, no es necesario multippliar por min y seg
			// dd($total_horas_laborales_por_dia);  //5940000
			while ($mensual->lessThanOrEqualTo($now)) {
				foreach ($processes->whereBetween('il_created_at',[$mensual,$mensual_end_day]) as $key => $process) {
					/*Setup (Herramentaje, ajuste y liberacion)*/
					$setup_mensual+=getTime($process->il_created_at,$process->ls_created_at);
					$setup_mensual+=getTime($process->ial_created_at,$process->ls_created_at);
					$setup_mensual+=getTime($process->ih_created_at,$process->fh_created_at);
					$setup_mensual+=getTime($process->ia_created_at,$process->fa_created_at);
					// dd($setup_mensual); // 0

					/*Producción*/
					foreach ($process->user_processes as $u_process) {
						$production_mensual+=getTime($u_process->start_time,$u_process->end_time,true);
						// dd($production_mensual); //0
					}
					/*Sin uso*/
				}
				$sin_uso_mensual+=$total_horas_laborales_por_dia - $setup_mensual - $production_mensual;
				$mensual->addDay();
				$mensual_end_day->addDay();
			}
			// dd($sin_uso_mensual); //-1305678347
			$sin_uso_mensual=$sin_uso_mensual<=0?0:$sin_uso_mensual;
			// dd($sin_uso_mensual); //0
		/*
		*  Tiempo por fase
		*/
			$herramentaje=$ajuste=$liberacion=$autoliberacion=$produccion=$pausaMantenimiento=$pausa=$sinUso=$totalHorasSemanal=0;
			while ($ago7day->lessThanOrEqualTo($now)) {
				//dd($processes->whereBetween('ih_created_at',[$ago7day,$ago7day_end_day]),Process::whereDate('ih_created_at',$ago7day->format('Y-m-d'))->get());
				foreach ($processes->whereBetween('ih_created_at',[$ago7day,$ago7day_end_day]) as $key => $process) {

					// dd($processes);
					/*Setup (Herramentaje, ajuste y liberacion)*/
					$liberacion+=getTime($process->il_created_at,$process->ls_created_at);
					$autoliberacion+=getTime($process->ial_created_at,$process->ls_created_at);
					$herramentaje+=getTime($process->ih_created_at,$process->fh_created_at);
					$ajuste+=getTime($process->ia_created_at,$process->fa_created_at);
					// dd($liberacion);

					/*Producción*/
					foreach ($process->user_processes as $u_process) {
						$produccion+=getTime($u_process->start_time,$u_process->end_time,true);
						if (getTime($u_process->start_time,$u_process->end_time,true) < 0) dd($u_process,getTime($u_process->start_time,$u_process->end_time,true));
					// dd($produccion); //0
					}
					// Pausa Mantenimiento
					$pausasMantenimiento=$process->pauses->where('motivo','mantenimiento');
					foreach ($pausasMantenimiento as $pMan) {
						$pausaMantenimiento+=getTime($pMan->started_at,$pMan->ended_at);
					}

					/* Pausas otros*/
					$pausasOtros=$process->pauses->where('motivo','!=','mantenimiento');
					foreach ($pausasOtros as $pMan) {
						$pausa+=getTime($pMan->started_at,$pMan->ended_at);
					}
				}
				$ago7day->addDay();
				$ago7day_end_day->addDay();
				// SIN USO
				$sinUso += $total_horas_laborales_por_dia - $liberacion - $autoliberacion - $herramentaje
						- $ajuste - $produccion-$pausaMantenimiento-$pausa;
			}
			$totalHorasSemanal=$sinUso +$liberacion + $autoliberacion + $herramentaje
						+ $ajuste + $produccion + $pausaMantenimiento + $pausa;

			$tiempo_fase=collect([
				'herramentaje'=>round($herramentaje/60/60,0),
				'ajuste'=>round($ajuste/60/60,0),
				'liberacion'=>round($liberacion/60/60,0),
				'autoliberacion'=>round($autoliberacion/60/60,0),
				'produccion'=>round($produccion/60/60,0),
				'pausaMantenimiento'=>round($pausaMantenimiento/60/60,0),
				'pausa'=>round($pausa/60/60,0),
				'sinUso'=>$sinUso<=0?0:round($sinUso/60/60,0),
			]);
			$tiempo_fase_procentaje=collect([
				'herramentaje'=>round(($herramentaje/$totalHorasSemanal)*100,2),
				'ajuste'=>round(($ajuste/$totalHorasSemanal)*100,2),
				'liberacion'=>round(($liberacion/$totalHorasSemanal)*100,2),
				'autoliberacion'=>round(($autoliberacion/$totalHorasSemanal)*100,2),
				'produccion'=>round(($produccion/$totalHorasSemanal)*100,2),
				'pausaMantenimiento'=>round(($pausaMantenimiento/$totalHorasSemanal)*100,2),
				'pausa'=>round(($pausa/$totalHorasSemanal)*100,2),
				'sinUso'=>$sinUso<=0?0:round(($sinUso/$totalHorasSemanal)*100,2),
			]);

		/*Maquinas Semanal
		*/

			$setup_semanal=[];
			$produccion_semanal=[];
			$sin_uso_semanal=[];
			$mensual=Carbon::now()->startOfMonth()->startOfDay()->subDays(30);

			$label=[];
			$semana_inicio=Carbon::createFromFormat('Y-m-d',Carbon::now()->subWeeks(9)->format('Y-m-d'))->startOfWeek();
			$semana_fin=Carbon::createFromFormat('Y-m-d',Carbon::now()->subWeeks(9)->format('Y-m-d'))->endOfWeek();
			while ($semana_inicio->lessThanOrEqualTo($now)) {
				$produccion_semanal_maquina=0;
				$sin_uso_semanal_maquina=0;
				$setup_semanal_maquina=0;
				$label[]=$semana_inicio->format("d M")."  ".$semana_fin->format("d M");
				// dd($label);
				foreach ($processes->whereBetween('ih_created_at',[$semana_inicio,$semana_fin]) as $key => $process) {
					/*Setup (Herramentaje, ajuste y liberacion)*/
					$setup_semanal_maquina+=getTime($process->il_created_at,$process->ls_created_at);
					$setup_semanal_maquina+=getTime($process->ial_created_at,$process->ls_created_at);
					$setup_semanal_maquina+=getTime($process->ih_created_at,$process->fh_created_at);
					$setup_semanal_maquina+=getTime($process->ia_created_at,$process->fa_created_at);

					/*Producción*/
					foreach ($process->user_processes as $u_process) {
						$produccion_semanal_maquina+=getTime($u_process->start_time,$u_process->end_time,true);
					}
					/*Sin uso*/
				}
				$sin_uso_semanal_maquina+=$total_horas_laborales_por_dia - $setup_semanal_maquina - $produccion_semanal_maquina;

				$setup_semanal[]=round($setup_semanal_maquina/60/60,1);
				$sin_uso_semanal[]=$sin_uso_semanal_maquina<=0?0:round($sin_uso_semanal_maquina/60/60,1);
				$produccion_semanal[]=round($produccion_semanal_maquina/60/60,1);

				$semana_inicio->addWeek();
				$semana_fin->addWeek();
			}
			$label=collect($label);

		/*
		*  Status Jobs
		*/
		$process=Process::whereNull('status')->orWhereDate('ih_created_at','>=',Carbon::now()->subDays(8)->startOfDay())->get();

		/*
		*  Productividad mensual
 		*/
 		$user_processes=UserProcess::whereNotNull('end_time')->where('start_at','>=',Carbon::now()->startOfMonth())->get();
 		// dd($user_processes);
 		/*
 		*  Productividad Semanal
 		*/
	 		$productividad_semanal=[];
	 		$mensual=Carbon::now()->startOfMonth()->startOfDay()->subDays(30);

	 		$semana_inicio=Carbon::createFromFormat('Y-m-d',Carbon::now()->subWeeks(7)->format('Y-m-d'))->startOfWeek();
	 		$semana_fin=Carbon::createFromFormat('Y-m-d',Carbon::now()->subWeeks(7)->format('Y-m-d'))->endOfWeek()->subDay();

	 		while ($semana_inicio->greaterThanOrEqualTo($now)) {
	 			$productividad=0;
	 			$u_p=$user_processes->whereBetween('start_time',[$semana_inicio,$semana_fin]);
	 			// if ($u_p->count()) dd( $u_p,$semana_inicio,$semana_fin );
	 			$productividad_semanal[]=round($u_p->avg('productivity'),2);

	 			$semana_inicio->addWeek();
	 			$semana_fin->addWeek();
	 		}
	 		$productividad_semanal=collect($productividad_semanal);

 		// dd($user_processes);
		// dd($totalHorasSemanal);
		return view('admin.indicadores.index')
			->with('setup_mensual',round($setup_mensual/60/60,0))
			->with('production_mensual',round($production_mensual/60/60,0))
			->with('sin_uso_mensual',round($sin_uso_mensual/60/60,0))
			->with('tiempo_fase',$tiempo_fase)
			->with('tiempo_fase_procentaje',$tiempo_fase_procentaje)
			->with('label',$label)
			->with('setup_semanal',collect($setup_semanal))
			->with('produccion_semanal',collect($produccion_semanal))
			->with('sin_uso_semanal',collect($sin_uso_semanal))
			->with('process',$process)
			->with('now',$now)
			->with('user_processes',$user_processes)
			->with('productividad_semanal',$productividad_semanal)
		;
	}
	public function indicadores()
	{
		/*
		*  Status Jobs
		*/
		// '/MARADO|LIMPIEZA|ALMAC|INSPECCI/';
		// $process=Process::select('processes.*','op.description')
		// 	->join('operations as op','op.id','=','processes.operation_id')
		// 	->where(function ($q){
		// 		$q->whereNull('status')
		// 			->orWhere('ih_created_at','>=',Carbon::now()->subDays(7)->startOfDay());
		// 	})
		// 	->where(function($q){
		// 		$q->where('description','not ilike','%MARADO%')
		// 			->orWhere('descriptions','not ilike','%LIMPIEZA%')
		// 			->orWhere('description','not ilike','%ALMAC%')
		// 			->orWhere('description','not ilike','%INSPECCI%');
		// 	})
		// 	->get();
 		/*$process=Job::with(['processes.operation' => function($d){
 			$d->select('description')
 			->where('description','not ilike','%MARADO%')
			->orWhere('description','not ilike','%LIMPIEZA%')
			->orWhere('description','not ilike','%ALMAC%')
			->orWhere('description','not ilike','%INSPECCI%');
 		}])
		->get();*/
		//dd($process[0]);
		$now=Carbon::now();
		$porasignar = Job::with(['processes' => function($q){
			    $q->with(['operation' => function($query){
			        $query->whereIn('work_center', $this->obtenerWorkcenterProductivos());
			    }])
			    ->where('workcenter_id', null)
		        ->where('status', null);
			}])
			->get();
		$forasign = $porasignar->map(function ($item, $key) use($porasignar){
            if (count($item->processes) != 0) {
                return count($item->processes);
            }else {
            	unset($porasignar[$key]);
            }
        });

        $forasign->all();
		$enproceso = Job::with(['processes' => function($q){
			    $q->with(['operation' => function($d){
			        $d->select('description')
 			        ->where('description','not ilike','%MARADO%')
			        ->orWhere('description','not ilike','%LIMPIEZA%')
			        ->orWhere('description','not ilike','%ALMAC%')
			        ->orWhere('description','not ilike','%INSPECCI%');
			    }])
			    ->whereNotIn('status',['cr',null]);
			}])
			->get();
		$inprogress = $enproceso->map(function ($item, $key) use($enproceso){
            if (count($item->processes) != 0) {
                return count($item->processes);
            }else {
            	unset($enproceso[$key]);
            }

        });

        $inprogress->all();
		$terminados = Job::with(['processes' => function($q) use($now){
			    $q->with(['operation' => function($d){
			        $d->select('description')
 			        ->where('description','not ilike','%MARADO%')
			        ->orWhere('description','not ilike','%LIMPIEZA%')
			        ->orWhere('description','not ilike','%ALMAC%')
			        ->orWhere('description','not ilike','%INSPECCI%');
			    }])
			    ->where('status','cr')->where('ih_created_at','>=',$now->subDays(7)->startOfDay());
			}])
			->get();
		$finished = $terminados->map(function ($item, $key) use($terminados){
            if (count($item->processes) != 0) {
                return count($item->processes);
            }else {
            	unset($terminados[$key]);
            }

        });

        $finished->all();
		/*
		*  Productividad mensual
 		*/
 		$user_processes=UserProcess::whereNotNull('end_time')->where('start_at','>=',Carbon::now()->startOfMonth())->get();

		return view('admin.indicadores.v02.index')
			//->with('process',$process)
			->with('porasignar',count($porasignar))
			->with('enproceso',count($enproceso))
			->with('terminados',count($terminados))
			->with('now',$now)
		;
	}
}
