<?php

namespace App\Http\Controllers;
 
use App\User;
use App\Workcenter;
use App\Mail\StaticParams;
use Illuminate\Http\Request;
use App\Events\MaintenanceEvent;
use App\Mail\MantenimientoCNCEmail;
use Illuminate\Support\Facades\Mail;
 
class MailController extends Controller
{
    public function mantenimientoCNCEmail()
    {
        $user = new User([
            'name' => 'Armando',
            'email' => 'armando.hernandez@tunerd.mx'
        ]);
 		$wc=Workcenter::first();
 		broadcast(new MaintenanceEvent($wc));
        Mail::to($user->email, $user->name)
            ->send(new MantenimientoCNCEmail($user,$wc));
    }
}