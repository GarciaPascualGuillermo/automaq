<?php

namespace App\Http\Controllers;

use App;
use App\Events\OperadorAmountPieces;
use Illuminate\Http\Request;
use App\Workcenter;
use App\Cycle;
use App\Events\AmountUpdated;
use App\Events\PhaseUpdated;

class WorkCenterApiController extends Controller
{
    function __construct()
    {
    	// $this->middleware('auth');
    }
    public function recive(Request $request)
    {

        $ip = $request->ip();

    	$wc = Workcenter::with(['processes' => function ($q) {
            $q
                ->where('active', true)
                ->first()
                ->with(['user_processes' => function ($q_UP) {
                    $q_UP->with('cycles')
                        ->where('active',true);
                }]);
	    	}])->where('ip_address', $ip)
            ->get();

        if ($wc->isEmpty()) {
            return response()->json(['msg'=>'no se encontro wc'],403);
        }

        $process=$wc->first()->processes->where('active',true)->first();

        $wc_log=$wc->first()->workcenter_logs()->create([
            'ip' => $request->ip(),
            'description' => $request->input('json_payload.ct-data')." - ".$request->input('json_payload.ct-timestamp').' - '.$process->status.' - '.$process->job->num_job,
        ]);

        $user_processes=$process->user_processes->where('active',true)->first();

        if (is_null($user_processes))
            return response()->json(['msg'=>'no se encontro ningun usuario loggeado'], 403);

        $cycle = null;

    	if ($request->input('json_payload.ct-data')) {

        	$cycle=$user_processes->cycles()->create(
        		[
    	    		'message'=>strtoupper($request->input('json_payload.ct-data')),
    	    		'message_date'=>$request->input('json_payload.ct-timestamp'),
                    'process_status'=>$process->status,
    	    	]
            );

            $cycles=Cycle::where('user_process_id',$user_processes->id)->get();

            $start=$cycles->where('message','START')->where('process_status','pr')->count();

            $end=$cycles->where('message','12345')->where('process_status','pr')->count();

            $total_cycles=0;

            if ($start<=$end) {

                $total_cycles=$start +1;

            } else if ($start>$end) {

                $total_cycles=$end +1;

            }
            $t = ($total_cycles / $process->job->quantity) * 100;

            broadcast(
                new OperadorAmountPieces( $wc->first(), $t > 100 ? 100 : round($t, 2, PHP_ROUND_HALF_UP), $total_cycles)
            );

            event(new PhaseUpdated);

            event(new AmountUpdated);

        	return [
                "msg"=>'recibido.'
        		// $request->input('json_payload.ct-data'),
        		// $process,
        		// $cycle,
                // $user_processes
        	];
        } else {
            return response()->json(['msg'=>'no data payload from '.$wc->num_machine],404);
        }
        return response()->json(['msg'=>'error '], 500);
    }
    public function reorder(Request $request)
    {

        $newPosition = $request->endPosition + 1;
        $startPosition = $request->startPosition + 1;

        $workcenter = Workcenter::find($request->workcenter_id);
        $workcenter->order = $newPosition;
        $workcenter->save();

        if ($newPosition > $startPosition) {
            $order = 'ASC';
        } else {
            $order = 'DESC';
        }

        $workcenters = Workcenter::orderBy('order', 'ASC')->orderBy('updated_at', $order)->get();

        // dd($workcenter_changed, $newPosition, $workcenters->take(5));

        // dd($workcenters->take(5));

        $index = 1;

        foreach ($workcenters as $workcenter) {

            $workcenter->order = $index;
            $workcenter->save();
            $index++;

        }

        return response()->json([
            'message' => 'Actualizado correcto'
        ], 201);


    }
}
