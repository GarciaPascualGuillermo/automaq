<?php

namespace App\Http\Controllers;

use App\Events\VerifiedQualityEvent;
use App\Events\MensajeEvent;
use Illuminate\Http\Request;

class MensajeGlobalController extends Controller
{
    
	public function mensaje_global(Request $request)
    {    	
  		return view('admin.mensaje_global.index');    
    }

    public function mensaje(Request $request)
    {
    	$m=$request->input('m');
        broadcast( new MensajeEvent( $m ));
        
    }
}

