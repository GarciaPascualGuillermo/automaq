<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Role;
use App\Pause;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class HistorialSController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('historial.maquinas')->only(['maquinas_dia','maquinas_semana']);
		$this->middleware('historial.jobs')->only(['jobs']);
		$this->middleware('historial.operadores')->only(['operadores']);
		$this->middleware('historial.calidad')->only(['calidad']);
		$this->middleware('historial.mantenimiento')->only(['mantenimiento']);
	}
	public function maquinas_semana(Request $request)
	{
		$workcenter_section=strtoupper($request->input("section",""));
		$workCenters= WorkCenter::with(['processes'=>function ($q_p){
				$q_p->orderBy('cr_created_at','asc')
					->where(function ($q_where){
						$q_where->where('status','=','cr')
							->whereNotNull('cr_created_at')
							;
					})
					->with('job')
					->with(['pauses'=>function ($q_pause){
						$q_pause
							->whereNotNull('ended_at')
							;
					}])
					->with(['operation'=>function ($q2){
						$q2->with(['piece'=>function ($q_piece){
							$q_piece->with('client');
						}]);
					}])
					->with(['user_processes'=>function ($q_up){
						$q_up->whereNotNull('end_time');
					}])
					;
			}])
			->where('section','like', $workcenter_section."%")
			->orderBy('section','ASC')
			->orderBy('num_machine','ASC')
			->get();
		

		$process_old=Process::where('status','cr')->whereNotNull('cr_created_at')->orderBy('cr_created_at','ASC')->limit(1)->first();
		$limit_start=Carbon::createFromFormat('Y-m-d H:i:s',$process_old->cr_created_at)->startOfWeek();
		$limit_end=Carbon::createFromFormat('Y-m-d H:i:s',$process_old->cr_created_at)->endOfWeek()->subDay();
		$now=carbon::now()->startOfWeek();
		$weekend=carbon::now()->endOfWeek();
		$num_semanas=0;
		while($now->gte($limit_start)){
			$num_semanas++;
			$now->subWeek();
		}
		$now=carbon::now()->startOfWeek();
		// dd($process_old,$limit_start,$limit_end,$now->gte($limit_start));

		return view('admin.historial.maquinas.semana')
			->with('workcenters',$workCenters)
			->with('limit_start',$limit_start)
			->with('limit_end',$limit_end)
			->with('now',$now)
			->with('weekend',$weekend)
			->with('num_semanas',$num_semanas)
			;
	}
	public function maquinas_dia(Request $request)
	{
		$workcenter_section=strtoupper($request->input("section",""));
		$workCenters= WorkCenter::with(['processes'=>function ($q_p){
				$q_p->orderBy('cr_created_at','asc')
					->where(function ($q_where){
						$q_where->where('status','=','cr')
							->whereNotNull('cr_created_at')
							;
					})
					->with('job')
					->with(['pauses'=>function ($q_pause){
						$q_pause
							->whereNotNull('ended_at')
							;
					}])
					->with(['operation'=>function ($q2){
						$q2->with(['piece'=>function ($q_piece){
							$q_piece->with('client');
						}]);
					}])
					->with(['user_processes'=>function ($q_up){
						$q_up->whereNotNull('end_time');
					}])
					;
			}])
			->where('section','like', $workcenter_section."%")
			->orderBy('section','ASC')
			->orderBy('num_machine','ASC')
			->get();
		

		$process_old=Process::where('status','cr')->whereNotNull('cr_created_at')->orderBy('cr_created_at','ASC')->limit(1)->first();
		$limit_start=Carbon::createFromFormat('Y-m-d H:i:s',$process_old->cr_created_at)->startOfWeek();
		$limit_end=Carbon::createFromFormat('Y-m-d H:i:s',$process_old->cr_created_at)->endOfWeek()->subDay();
		$now=carbon::now()->startOfDay();
		$weekend=carbon::now()->endOfDay();
		$num_semanas=0;
		while($now->gte($limit_start)){
			$num_semanas++;
			$now->subWeek();
		}
		$now=carbon::now()->startOfWeek();
		return view('admin.historial.maquinas.dia')
			->with('workcenters',$workCenters)
			->with('limit_start',$limit_start)
			->with('limit_end',$limit_end)
			->with('now',$now)
			->with('weekend',$weekend)
			->with('num_semanas',$num_semanas)
			;
	}
	public function jobs()
	{
		$process= Process::select('j.num_job'
				,'processes.*'/*,'op.*','pc.*','c.*','u_op.*'*/)
			->join('jobs as j','j.id','=','processes.job_id')
			->where('processes.status','cr')
			->whereNotNull('processes.ajustador_ia_id')
			->whereNotNull('processes.ajustador_fa_id')
			->whereNotNull('processes.cr_created_at')
			->withTrashed()
			->orderBy('num_job','DESC')
			->paginate(100);

		// for ($i=0; $i < $cycles->count()-1 ; $i++) { 
		// 	if ($cycles[$i]->message=='START' && (isset($cycles[$i+1]) && $cycles[$i+1]->message=='12345' )) {
		// 		$cycles_count++;
		// 		$time_cycle_array[]=$cycles[$i]->message_date->diffInSeconds($cycles[$i+1]->message_date);
		// 		$i=$i++;

		// 		// if ($interaccion==1) dd($time_cycle_array->toArray(),$cycles_count,$cycles[$i]->toArray(),$cycles[$i+1]->toArray(),$cycles[$i]->message_date->diffInSeconds($cycles[$i+1]->message_date),$user_process->standar_time*60);
		// 	}
		// }
		// dd($process->first()->toArray());
		// $process = Process::where('status','cr')->withTrashed()->get();

		return view('admin.historial.jobs.historial')
		->with('process',$process);

	}

	public function operadores(Request $request)
	{
		
		$name = $request->get('name');
		// ->where('name', 'LIKE', "%name%")

		$roles=Role::select('ru.user_id')
			->join('role_user as ru','ru.role_id','=','roles.id')
			->where('name','operador')
			->get();
		// dd($roles->pluck('user_id'));
		// $users = User::whereIn('id',$roles->pluck('user_id'))->get();
		$users = User::orderBy('id','DESC')->name($name)->get();
		$now = Carbon::now();

		$week_start=Carbon::now()->startOfWeek();
		$weekend=Carbon::now()->endOfWeek();

		$last_day=UserProcess::WhereNotNull('end_time')->orderBy('start_time','ASC')->limit(1)->first();
		// dd($last_day);
		if (!is_null($last_day)) {
			$day=$last_day->start_time->startOfWeek();
		}else {
			$day=Carbon::now()->startOfWeek();
		}
		

		return view('admin.historial.operadores.semana')
			->with('users',$users)
			->with('day',$day)
			->with('week_start',$week_start)
			->with('weekend',$weekend)
			;

	}
	public function calidad(Request $request)
	{
		$num_job = $request->get('num_job');
		$part_number = $request->get('part_number');
		// $inspector = $request->get('ajustador_il');
		// $fecha = $request->get('il_created_at');
		$process = Process::select('processes.*','j.num_job')
			// ->join('rejected_processes as r','r.process_id','=','processes.id')
			// ->join('jobs as j','j.id','=','processes.job_id')
			->with(['rejected_processes'/*=>function ($q_rp){
				$q_rp
					->whereNotNull('started_at');
			}*/])
			->job($num_job)
			->Part_number($part_number)
			// ->Inspector($inspector)
			// ->Fecha($fecha)
			->whereNotNull('ls_created_at')
			->orderBy('j.num_job','DESC')
			
			->paginate(100);

		return view('admin.historial.calidad.historial')
			->with('process',$process);
	}
	public function mantenimiento()
	{
		$pauses=Pause::select("pauses.*","j.num_job",'w.num_machine','w.model', DB::raw("CONCAT(user_mantenimiento.name,' ',user_mantenimiento.firstname) as user_mantenimiento"),'w.order' )
			->join('workcenters as w','w.id','=','pauses.workcenter_id')
			->join('processes as p','pauses.process_id','=','p.id')
			->join('users as user_mantenimiento','user_mantenimiento.id','=','pauses.user_id')
			->join('jobs as j','j.id','=','p.job_id')
			->join('operations as op','op.id','=','p.operation_id')
			->whereNotNull('ended_at')
			->where('motivo','mantenimiento')
			->orderBy('j.num_job')
			->get();
		// dd($pauses->toArray());

		return view('admin.historial.mantenimiento.historial')

		->with('pauses',$pauses)
		;
	}
}
