<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Role;
use App\Pause;
use App\Cycle;
use App\Process;
use App\ProcessLog;
use App\Operation;
use App\MachineDay;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;


class HistorialController2 extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('historial.maquinas')->only(['maquinas_dia','maquinas_semana']);
		$this->middleware('historial.jobs')->only(['jobs']);
		$this->middleware('historial.operadores')->only(['operadores']);
		$this->middleware('historial.calidad')->only(['calidad']);
		$this->middleware('historial.mantenimiento')->only(['mantenimiento']);
	}
	public function maquinas_semana(Request $request)
	{

        $times = new Collection();

        $workcenter_section=strtoupper($request->input("section","S"));

        $workcenters = WorkCenter::with(['processes'])
            ->where('section','like', $workcenter_section."%")
			->orderBy('section','ASC')
			->orderBy('num_machine','ASC')
            ->get();

        $user_procecess = UserProcess::select("created_at")->orderBy('created_at','ASC')->limit(1)->first();

        if (is_null($user_procecess)) {
            return response()->json(['msg'=>'no data'],200);
        }

		$limit = $user_procecess->created_at->startOfWeek();

		foreach ($workcenters as $wc) {
			$week_start=Carbon::now()->subWeek()->startOfWeek();
			$times->put($wc->num_machine,new Collection());
			while ( $limit->lessThanOrEqualTo($week_start)) {
				$times[$wc->num_machine]->push(getTimeSemanalMaquinas($wc,$week_start));
				$week_start->subWeek();
			}
        }

        // dd($times);
		// return response()->json([$workcenters,$times],400);
		return view('admin.historial.maquinas.v02.semana')
			->with('workcenters',$workcenters)
			->with('times',$times)
			->with('limit',$limit)
			->with('week_start',Carbon::now()->subWeek()->startOfWeek())
			;
	}
	public function maquinas_dia(Request $request)
	{
		$workcenter_section=strtoupper($request->input("section","S"));
		$start=Carbon::createFromFormat('Y-m-d','2019-10-28');
		$machine_days=MachineDay::select(
				'machine_days.day','machine_days.ajuste','machine_days.herramentaje','machine_days.liberacion',
				'machine_days.autoliberacion','machine_days.produccion','machine_days.mantenimiento',
				'machine_days.pausa_otros','machine_days.sinuso','wc.num_machine'
			)
			->join('workcenters as wc','wc.id','=','machine_days.workcenter_id')
			->where('wc.section','like', $workcenter_section."%")
			->orderBy('machine_days.day','DESC')
            ->get();

        $workcenters=$machine_days->pluck('num_machine')->unique();
		$limit=Carbon::createFromFormat('Y-m-d H:i:s',$machine_days->last()->day);
		$now=Carbon::now()->subDay()->startOfDay();


		// return response()->json([$workcenters,$times],400);
		return view('admin.historial.maquinas.v02.dia')
			->with('machine_days',$machine_days)
			->with('workcenters',$workcenters)
			->with('limit',$limit)
			->with('now',$now)
			;

	}
	public function jobs()
	{
        $keyword_job_num = request('keyword_job_num');

        $keyword_job_part = request('keyword_job_part');

        $from = request('from');

        $to = request('to');

		$process= Process::select(
            'j.num_job',
            'j.delivered_at',
            'processes.*',
            'processes.id',
            'up.user_id as operador',
            'up.id as u_p_id'
        )
			->join('jobs as j','j.id', '=', 'processes.job_id')
            ->join('user_processes as up', 'up.process_id', '=', 'processes.id')

			->where('processes.status','cr')
			->whereNotNull('processes.ajustador_ia_id')
			->whereNotNull('processes.ajustador_fa_id')
            ->whereNotNull('processes.cr_created_at')
            ->where(function ($q) use ($keyword_job_num, $keyword_job_part, $from, $to)
            {
                $q
                ->jobNum($keyword_job_num)
                ->jobPart($keyword_job_part)
                ->where(function ($subQuery) use ($from, $to)
                {
                    $subQuery->jobDateFrom($from)
                    ->jobDateTo($to);
                });
            })
            ->orderBy('num_job','DESC')
            ->paginate(50)
            ;

		//CONTEO DE LAS PIEZAS
		foreach ($process as $proces) {

			//CONTEO DE LOS CICLOS
			$cycles=Cycle::where('user_process_id',$proces->u_p_id)->get();
            $process->delivered_at=Carbon::createFromFormat('Y-m-d H:i:s',$proces->delivered_at);

			$cycles_count=0;
			$time_cycle_array=[];
            $time_cycle=0;

			for ($i=0; $i < $cycles->count()-1 ; $i++) {
				if ($cycles[$i]->message=='START' && (isset($cycles[$i+1]) && $cycles[$i+1]->message=='12345' )) {
					$cycles_count++;
					$time_cycle_array[]=$cycles[$i]->message_date->diffInSeconds($cycles[++$i]->message_date);
				}
			}
			$time_cycle_array=collect($time_cycle_array);
			$time_cycle=round($time_cycle_array->avg(),2);
			$time_cycle_min=round($time_cycle/60,2);

            if ($time_cycle_min<1)
                $proces->time_cycle= $time_cycle." seg";
            else
                $proces->time_cycle= $time_cycle_min." min";

			$proces->piece=$cycles_count;
		}

		// dd($process);
		// $process = Process::where('status','cr')->withTrashed()->get();

		return view('admin.historial.jobs.historial', compact('process', 'keyword_job_num', 'keyword_job_part', 'to', 'from'));

	}

	public function operadores(Request $request)
	{
		$name = $request->get('name');

		$users = User::select("users.*",'r.name as role_name')
			->join('role_user as r_u','r_u.user_id','=','users.id')
			->join('roles as r','r_u.role_id','=','r.id')
			// ->join('user_processes as u_p','u_p.user_id','=','users.id')
			->where('r.name','operador')
			->orderBy('users.id','ASC')
			->name($name)
			->paginate(15);
		if($users->isEmpty()) return abort(404);
		$user_processes=UserProcess::whereIn('user_id',$users->pluck('id'))
			// ->orderBy('created_at','DESC')
			->get();
		if($user_processes->isEmpty()) return abort(404);
		$limit=Carbon::createFromFormat('Y-m-d H:i:s',UserProcess::select('created_at')->orderBy('created_at','ASC')->first()->created_at);

		$users_headers=new Collection();
		$users_content=new Collection();
		foreach ($users as $user) {
			$name_=$user->idemployee." ".$user->name." ".$user->firstname;
			$week_start = Carbon::now()->startOfWeek()->subWeek();
			$users_headers->push($name_);
			$users_content->put( $name_ , new Collection());
			while ($limit->lessThanOrEqualTo($week_start)) {
				$prom=$user_processes
					->whereBetween('created_at',[$week_start->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString()])
					->where('user_id',$user->id)
					;
				if($prom->isEmpty()) $users_content[$name_]->push(0);
				else $users_content[$name_]->push(round($prom->avg('productivity'),0,PHP_ROUND_HALF_DOWN));

				// return response()->json([$week_start->startOfWeek()->toDateTimeString(),$week_start->endOfWeek()->toDateTimeString(),$prom,$prom->avg('productivity')],200);
				$week_start->startOfWeek()->subWeek();
			}
		}
		return view('admin.historial.operadores.v02.semana')
			->with('users',$users)
			->with('users_headers',$users_headers)
			->with('users_content',$users_content)
			->with('limit',$limit)
			->with('week_start',Carbon::now()->startOfWeek()->subWeek())
			->with('name',$name)
			;

	}
	public function operadores_dia(Request $request)
	{
        $name = $request->name;

        $user_processes_by_date = UserProcess::with(['process.job', 'user'])
        ->whereActive(false)
        ->orderBy('created_at', 'DESC')
        ->get()
        ->groupBy(function ($user_process)
        {
            return Carbon::parse($user_process->created_at)->format('d-M-Y');
        })
        ->map(function ($user_process)
        {
            return $user_process->first();
        });

        return view('admin.historial.operadores.v02.dia', compact('user_processes_by_date'));

	}
	public function calidad(Request $request)
	{
		$num_job = $request->get('num_job');
		$part_number = $request->get('part_number');
		$inspector = $request->get('inspector');
		$fecha = $request->get('fecha');

		$process = Process::select('processes.*','j.num_job',
			DB::raw("CONCAT(insp_il.idemployee,' ',insp_il.name,' ',insp_il.firstname) as inspector"),
			DB::raw("CONCAT(insp_ial.idemployee,' ',insp_ial.name,' ',insp_ial.firstname) as inspector_ail")
			)
			->with(['rejected_processes'=>function ($q_rp){
				$q_rp
					->whereNotNull('started_at');
			}])
			->joinJob()
			->joinOperation()
			->joinPiece()
			->joinInspector()
			->whereNotNull('ls_created_at')
			->orderBy('j.num_job','DESC');
		if(!is_null($num_job)) $process=$process->job($num_job);
		if (!is_null($part_number)) $process=$process->Part_number($part_number);
		if (!is_null($inspector)) {
			$process=$process->where(function ($q_insp)use($inspector){
				$q_insp->where(DB::raw("CONCAT(insp_il.idemployee,' ',insp_il.name,' ',insp_il.firstname)"),'ilike',"%$inspector%")
					->orWhere(DB::raw("CONCAT(insp_ial.idemployee,' ',insp_ial.name,' ',insp_ial.firstname)"),'ilike',"%$inspector%");
			});
		}
		if (!is_null($fecha)) {
			$process=$process->where(function ($q) use ($fecha){
				$q->whereDate('il_created_at',$fecha)
					->orWhereDate('ial_created_at',$fecha);
			});
		}
		$process=$process->paginate(50);

		// dd($process->toArray());
		return view('admin.historial.calidad.historial')
			->with('process',$process)
			->with('num_job',$num_job)
			->with('part_number',$part_number)
			->with('inspector',$inspector)
			->with('fecha',$fecha)
			;
	}
	public function mantenimiento()
	{
		$keyword_workcenter = request('keyword_workcenter');

        $keyword_user = request('keyword_user');

        $day = request('day');

        $from = request('from');

        $to = request('to');

		$pauses=Pause::select("pauses.*","j.num_job",'w.num_machine','w.model', DB::raw("CONCAT(user_mantenimiento.name,' ',user_mantenimiento.firstname) as user_mantenimiento"),'w.order' )
			->join('workcenters as w','w.id','=','pauses.workcenter_id')
			->join('processes as p','pauses.process_id','=','p.id')
			->join('users as user_mantenimiento','user_mantenimiento.id','=','pauses.user_id')
			->join('jobs as j','j.id','=','p.job_id')
			->join('operations as op','op.id','=','p.operation_id')
			->whereNotNull('ended_at')
			->where('motivo','mantenimiento')
			->where(function ($q) use ($keyword_workcenter, $keyword_user, $day, $from, $to)
            {
                $q
                ->MachineNum($keyword_workcenter)
                ->User($keyword_user)
                ->DateMaintenance($day)
                ->where(function ($subQuery) use ($from, $to)
                {
                    $subQuery->DateMaintenanceFrom($from)
                    ->DateMaintenanceTo($to);
                });
            })
			->orderBy('j.num_job')
			->paginate(15);
		//dd($pauses);

		return view('admin.historial.mantenimiento.historial')

		->with('pauses',$pauses)
		;
	}
	public function jobsEliminados()
	{
		$process= Process::select(
            'j.num_job',
            'j.delivered_at',
            'processes.*',
            'processes.id'
        )
			->join('jobs as j','j.id', '=', 'processes.job_id')
            ->withTrashed(['operation'=>function ($j4){
					$j4->with(['piece'=>function($j5){
						$j5->with('client');
					}]);
				}])
			->whereNotNull('processes.deleted_at')
            ->orderBy('deleted_at','DESC')
            ->paginate(20)
            ;
        $process_finished = Process::with(['operation.piece.client', 'job'])
            ->where('status','cr')
            ->whereNotNull('cr_created_at')
            ->where('active', false)
            ->where('order', null)
            ->orderBy('cr_created_at','DESC')
            ->paginate(20)
            ;
        $piece_array = array();
        $piece_last_array = array();
        foreach ($process as $key => $proces) {
            $operation = Operation::with(['piece'=>function($j5){
						$j5->with('client');
					}])
                    ->find($proces->operation_id);
            $piece_array[] = $operation;
        }
        foreach ($process_finished as $key => $proces_f) {
            $operation_f = Operation::with(['piece'=>function($j5){
						$j5->with('client');
					}])
                    ->find($proces_f->operation_id);
            $piece_last_array[] = $operation_f;
        }
 		return view('admin.historial.jobs.historialJobsEliminados')
		->with('process', $process)
		->with('operations', $piece_array)
		->with('process_fin', $process_finished)
		->with('operations_fin', $piece_last_array);
	}
	public function restoreFinalJobs (Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){

            $id = $request->input('id');

			$process_restore = Process::where('id',$id)
				->withTrashed(['operation'])
				->limit(1)
				->restore();
		}
	}
	public function restoreFinalJobsTer (Request $request)
	{
		if(auth()->user()->hasRoles(['root','ceo','gte.operacion','jefe.produccion'])){

            $id = $request->input('id');

			$process_restore = Process::where('id',$id)
				->with(['operation'])
				->limit(1)
				->first();
			$process_log= ProcessLog::where('activity','<>','cr')->where('process_id',$id)->orderBy('id', 'DESC')->first();

			if ($process_log->count() > 0) {
				$process_restore->status = $process_log->activity;
			}else {
                $process_restore->status = '';
            }


			$process_restore->workcenter_id = null;
            $process_restore->cr_created_at = null;
            $process_restore->active = false;
            $process_restore->order = 0;

            $process_restore->save();

            return back()->withSuccess('Job Restaurado');
		}
	}
}
