<?php

namespace App\Http\Controllers;

use App\User;
use App\Pause;
use App\Process;
use App\Workcenter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Events\MaintenanceEvent;
use App\Mail\MantenimientoCNCEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Events\MantenimientoEsperaEvent;
use App\Events\PhaseUpdated;
use App\Http\Requests\MantenimientoLoginRequest;
use App\Http\Requests\MantenimientoProcesoRequest;

class MantenimientoController extends Controller
{
    function __construct()
    {
    	$this->middleware('auth')->except(['send_soporte','finalizar_support']);
    }
    public function espera(MantenimientoLoginRequest $request)
    {

        $user=User::where('idemployee',$request->input('username'))
            ->limit(1)
            ->get();

        $msg = "EL usuario o contraseña es incorrecto";

        if($user->isNotEmpty()){
            $user = $user->first();
            $roles_access = collect(['root', 'jefe.mantenimiento', 'tec.mantenimiento']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $wc = Workcenter::where('id', $request->input('workcenter_id'))
                    ->first();

                    $pause = $wc->pauses()->whereNull('ended_at')->orderBy('created_at', 'DESC')->first();
                    $pause->user_id = $user->id;
                    $pause->started_at = now();
                    $pause->save();

                    $wc->support = true;
                    $wc->save();

                    event(new MaintenanceEvent($wc));

                    broadcast(new MantenimientoEsperaEvent($wc));

                    return response()->json(['resp'=>'ok','user_mantenimiento'=>$user], 201);
                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }
    public function getUserMantenimiento(Request $request)
    {
        $workcenter = Workcenter::find($request->input('workcenter_id'));
        $pause = $workcenter->pauses()->where('ended_at', null)->orderBy('created_at', 'DESC')->first();
        return response()->json([
            'user_mantenimiento' => $pause->user
        ], 201 );

    }
    public function pausa()
    {
    	return view('admin.solicitudes_control.layouts.pausa_mantenimiento');
    }
    public function soporte(MantenimientoProcesoRequest $request)
    {
        return $request->all();
    }

    public function finalizar_valid_credential(MantenimientoProcesoRequest $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','jefe.mantenimiento','tec.mantenimiento']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    // $process=Process::findOrFail(session('process_id'));
                    // $wc=Workcenter::find($process->workcenter_id);
                    // $status=$wc->statusworkcenters()->create([
                    //     'comment'=>$request->input('comment'),
                    //     'user_id'=>$user->id,
                    // ]);
                    // $process->status='pr';
                    // if( boolval($request->input('support')) ){
                    //     $wc->support=true;
                    //     $wc->save();
                    // }else{
                    //     $process->status='pr';
                    //     $pause=$process->pauses->where('id',session('pause_id'))->first();
                    //     if (is_null($pause)) {
                    //         $pause=$process->pauses->where('user_end_id',null)->first();
                    //     }
                    //     // return response()->json([$pause],401);
                    //     $pause->ended_at=Carbon::now();
                    //     $pause->user_end_id=$user->id;
                    //     $pause->comments=$request->input('comment');
                    //     if($pause->save()) $process->save();
                    // }
                    // $process->save();
                    // // return response()->json([$process,$status,$request->input('support'),boolval($request->input('support'))], 401);
                    // broadcast(new MantenimientoEsperaEvent($wc));
                    return response()->json("Usuario valido", 200);


                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }
    public function finalizar(MantenimientoProcesoRequest $request)
    {
        $user = User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg = "EL usuario o contraseña es incorrecto";

        if($user->isNotEmpty()) {

            $user = $user->first();
            $roles_access = collect(['root','jefe.mantenimiento','tec.mantenimiento']);
            $intersect = $user->roles->pluck('name')->intersect($roles_access);

            if ($intersect->count()) {

                if (Hash::check($request->input('password'), $user->password)) {

                    $wc = Workcenter::find($request->workcenter_id);

                    $status = $wc->statusworkcenters()->create([
                        'comment' => $request->input('comment'),
                        'user_id' => $user->id
                    ]);

                    $pause = $wc->pauses()->where('ended_at', null)->orderBy('created_at', 'DESC')->first();

                    $pause->ended_at = now();
                    $pause->user_end_id = $user->id;
                    $pause->comments = $request->comment;
                    $pause->type = $request->newtype;
                    $pause->concept = $request->concepto;
                    $pause->maintenance_type = $request->tipo_mantenimiento;
                    $pause->save();

                    $wc->on_maintenance = false;
                    $wc->support = false;
                    $wc->save();

                    broadcast(new MantenimientoEsperaEvent($wc));
                    event(new PhaseUpdated());

                    return response()->json([
                        $status
                    ], 201);

                }
            } else {
                $msg = "No tienes permisos para esta acción, verifica con el administrador.";
            }
        }

        return response()->json([
            'errors' => [
                'username' => [ $msg ]
            ]
        ], 401);
    }

    public function status(Request $request)
    {

        $on_maintenance_wait = Pause::whereMotivo('mantenimiento')
        ->with(['process.operation.piece', 'process.job', 'user', 'operador', 'workcenter'])
        ->whereNull('user_id')
        ->whereHas('workcenter', function ($query)
        {
            $query->where('on_maintenance', 'true');
        })
        ->has('process_log')
        ->get()
        ;

        $on_maintenance_process = Pause::whereMotivo('mantenimiento')
        ->with(['process.operation.piece', 'process.job', 'user', 'operador', 'workcenter'])
        ->whereNotNull('user_id')
        ->whereNull('ended_at')
        ->whereHas('workcenter', function ($query)
        {
            $query->where('on_maintenance', 'true');
        })
        ->has('process_log')
        ->get()
        ;


        return compact('on_maintenance_wait', 'on_maintenance_process');
    }

    public function send_soporte()
    {
        $ip=request()->ip();
        $wc=Workcenter::where('ip_address',$ip)->limit(1)->get();
        return view('admin.mantenimiento.support')
            ->with('wc',$wc);
        dd($wc);
    }
    public function finalizar_support(Request $request)
    {
        $user=User::where('idemployee',$request->input('username'))
                    ->limit(1)
                    ->get();

        $msg="EL usuario o contraseña es incorrecto";
        if($user->isNotEmpty()){
            $user=$user->first();
            $roles_access=collect(['root','jefe.mantenimiento','tec.mantenimiento']);
            $intersect=$user->roles->pluck('name')->intersect($roles_access);
            if ($intersect->count()) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $wc=Workcenter::find($request->input('wc_id'));
                    $status=$wc->statusworkcenters()->create([
                        'comment'=>$request->input('comment'),
                        'user_id'=>$user->id,
                    ]);
                    $wc->support=$request->input('support');
                    $wc->save();
                    $pause=$wc->pauses->where('ended_at',null)->first();

                    if (!is_null($pause)) {
                        $pause->user_end_id=$user->id;
                        $pause->ended_at=Carbon::now();
                        $pause->comments=$request->input('comment');
                        $pause->save();
                    }

                    broadcast(new MantenimientoEsperaEvent($wc));
                    return response()->json([$pause], 201);


                }
            }else $msg="No tienes permisos para esta acción, verifica con el administrador.";
        }
        return response()->json(['errors'=>['username'=>[$msg]]],401);
    }
}

