<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use App\Cycle;
use App\Process;
use App\Operation;
use App\UserProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CapeandoSupervisorLoginRequest;

class CapeandoController extends Controller
{
	function __construct()
	{
		$this->middleware(['auth']);
	}
	public function login(CapeandoSupervisorLoginRequest $request)
	{
		$user=User::where('idemployee',$request->input('username'))
					->limit(1)
					->get();
		$msg="EL usuario o contraseña es incorrecto";
		if($user->isNotEmpty()){
			$user=$user->first();
			$roles_access=collect(['root','sup.produccion']);
			$intersect=$user->roles->pluck('name')->intersect($roles_access);
			if ($intersect->count()) {
				if (Hash::check($request->input('password'), $user->password)) {
					$process_id=$request->input('process_id');
					$operations = Operation::where('job',$request->input('num_job'))
						->orderBy('operation_service','ASC')
						->get();
					$current_process=Process::where('id',$request->input('process_id'))
						->with(["job"])
						->with(['capeando'])
						->with(['user_processes'])
						->with(['operation'=>function ($q_o) {
							$q_o
							->orderBy('sequence','ASC')
							->with(['piece'=>function ($q_p) {
								$q_p->with(['client']);
							}]);
						}])
						->first()
						;
					$last_process=null;

					// return response()->json([$operations,$current_process,$request->all()],400);
					foreach ($operations as $key => $operation) {
						if ($operation->operation_service==$current_process->operation->operation_service) {
							if ($key==0) {
								$msg="No hay operaciones previas";
							}else{
								$last_process=$operations[$key-1]->process->first();
							}
							break;
						}
					}

					$operation=$last_process->operation;
					// dd($operations);
					$cycles=getCycles($current_process, auth()->user());

					if ($last_process->operation->standar_time<$current_process->operation->standar_time) {
						$msg="La operación ".$last_process->operation->operation_service."  tiene un tiempo menor a la operación actual.";
						return response()->json(['errors'=>['username'=>[$msg]]],401);
					}


					// Guardando el capeando
					$operation->capeandos()->save($current_process);
					// $operation->save();
					// return response()->json([$operation,$current_process,$last_process],400);
					// $current_process->capeando()->save($operation);
					// $current_process->save();

					/*Cerrando session*/
					$user_process=$current_process->user_processes->where('id',session('user_process_id'))->first();
					$user_process->end=$cycles['pieceTotal'];
					$user_process->end_time=Carbon::now();
					$user_process->active=false;

					// Abriendo session con capeando
					$process_capeando=new UserProcess();
					$process_capeando->process_id=$current_process->id;
					$process_capeando->capeando=true;
					$process_capeando->active=true;
					$process_capeando->start=$cycles['pieceTotal'];
					$process_capeando->start_at=date('Y-m-d');
					$process_capeando->start_time=Carbon::now();
					$process_capeando->user_id=auth()->user()->id;
					if ($user_process->save()) {
						$process_capeando->save();

						session()->forget('user_process_id');
						session()->put('user_process_id',$process_capeando->id);
						session()->save();

						session()->save();
					}

					return response()->json([
						'process'=>$current_process,
						'process_user'=>$process_capeando,
						'operation'=>$operation
					], 201);
				}
			}else $msg="No tienes permisos para esta acción, verifica con el administrador.";
		}
		return response()->json(['errors'=>['username'=>[$msg]]],401);
	}
	public function getprocess(Request $request)
	{
		$process_id=$request->input('process.id');
		
		$process = Process::where('id',$process_id)
			->with(["job"])
			->with(['operation'=>function ($q_o) {
				$q_o->with(['piece'=>function ($q_p) {
					$q_p->with(['client']);
				}]);
			}])
			->first()
            ;
        $operations = Operation::where('job', $process->operation->job)
			->orderBy('operation_service','ASC')
            ->get(); 
		$last_process=null;
        $current_process=null;


		foreach ($operations as $key => $operation) {

			if ($operation->operation_service==$process->operation->operation_service) {
				if ($key == 0) {

                    $msg = "No hay operaciones previas";

				} else {

					$last_process=$operations[$key-1]->process->first();
					$current_process=$process;
				}
				break;
            }

		}
		$result=getCycles($last_process);
		$result['process']=Process::where('id',$last_process->id)
			->with(["job"])
			->with(['operation'=>function ($q_o) {
				$q_o->with(['piece'=>function ($q_p) {
					$q_p->with(['client']);
				}]);
			}])
			->first()
			;;
		return response()->json($result,200);
	}
}
