<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workcenter;
use App\Http\Requests\WCRequest;

class WCController extends Controller
{


	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('sistemas.middleware');
	}

    public function index()
	{
		$workcenters = Workcenter::all();

		return view('admin.centro_trabajo.index')
		->with('workcenters',$workcenters)
		;
	}
	public function crear()
	{
		$workcenter=new Workcenter();

		return view('admin.centro_trabajo.crear')
			->with('workcenter',$workcenter)
			;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(WCRequest $request)
	{

		$workcenter = Workcenter::create($request->validated());


		$workcenter->save();

		return redirect()->route('centro_trabajo.index');


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */

	public function editar($id)
	{
		$workcenter= Workcenter::find($id);
		$workcenter= Workcenter::where('id',$id)->limit(1)->get()->first();


		return view('admin.centro_trabajo.editar')
			->with('workcenter',$workcenter);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(WCRequest $request, $id)
	{
		$workcenter = Workcenter::find($id);
		$workcenter->fill($request->validated());

		$workcenter->save();
		return redirect()->route('centro_trabajo.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$workcenter = Workcenter::find($id);
		$workcenter->delete();
		$workcenter->save();



		return redirect()
			->route('centro_trabajo.index')
			->with('message','El centro de trabajo: '.$workcenter->num_machine." ha sido eliminado")
			;
	}
}
