<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Productivity extends Model
{
    use Uuids;

    protected $keyType = 'string';
    
    protected $fillable=[
    	'user_process_id',
    	'percentage',
    	'percentage_date',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'percentage_date' => 'datetime',
    ];

    public function user_process()
    {
    	return $this->belongsTo(UserProcess::class);
    }
    public function getCreatedAtAttribute($date)
    {
        return new Date($date);
    }
}
