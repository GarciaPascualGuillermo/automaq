<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\Uuids;

class Pause extends Model
{
    use Uuids;

    protected $keyType = 'string';

    protected $fillable=[
    	'process_id','user_id','user_end_id', 'motivo', 'type', 'concept', 'comments','started_at','ended_at','workcenter_id','operador_id', 'maintenance_type'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'started_at'=>'datetime',
        'ended_at'=>'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'started_at','ended_at',
    ];

    /**
    *  RELATIONS
    */
    public function process()
    {
    	return $this->belongsTo(Process::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function user_end()
    {
        return $this->belongsTo(User::class, 'id', 'user_end_id');
    }
    public function process_log()
    {
        return $this->hasOne(ProcessLog::class, 'pause_id');
    }
    public function operador()
    {
        return $this->belongsTo(User::class,'operador_id','id');
    }
    public function workcenter()
    {
        return $this->belongsTo(Workcenter::class);
    }
    public function scopeDateMaintenanceTo($query, $keyword = null)
    {

        if ($keyword) {
            try {
                $keyword = Carbon::parse($keyword);
            } catch (\Throwable $th) {
                $keyword = null;
            }
            return $query->where('pauses.ended_at', '<=', $keyword);
        }

    }
    public function scopeDateMaintenanceFrom($query, $keyword = null)
    {

        if ($keyword) {
            try {
                $keyword = Carbon::parse($keyword);
            } catch (\Throwable $th) {
                $keyword = null;
            }
            return $query->where('pauses.ended_at', '>=', $keyword);
        }

    }
    public function scopeDateMaintenance($query, $keyword = null)
    {

        if ($keyword) {
            try {
                $keyword = Carbon::parse($keyword)->toDateString();
            } catch (\Throwable $th) {
                $keyword = null;
            }
            return $query->where('pauses.ended_at', 'LIKE', "%$keyword%");
        }

    }
    public function scopeMachineNum($query, $keyword = null)
    {
        if ($keyword) {
            return $query->orWhere('w.num_machine', 'LIKE', "%$keyword%");
        }
    }
    public function scopeUser($query, $keyword = null)
    {
        if ($keyword) {
            return $query->orWhere('user_mantenimiento.firstname', 'LIKE', "%$keyword%");
        }
    }
}
