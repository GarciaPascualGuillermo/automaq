<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Operation extends Model
{
    use Uuids;

    protected $keyType = 'string';
    protected $fillable=[
		'piece_id',
        'job_operation',
		'job',
		'work_center',
		'operation_service',
        'run_method',
        'standar_time',
        'sequence',
		'description',
    ];

    // relations
    public function piece()
    {
    	return $this->belongsTo(Piece::class);
    }
    public function jobs()
    {
    	return $this->hasMany(Job::class);
    }
    public function process()
    {
        return $this->hasMany(Process::class);
    }
    public function capeandos()
    {
        return $this->hasMany(Process::class,'capeando_id','id');
    }

}
