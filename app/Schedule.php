<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['year', 'week_number', 'first_turn', 'second_turn', 'group', 'start_of_week'];
}
