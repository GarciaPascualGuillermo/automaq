<?php

namespace App\Observers;

use App\ProcessLog;
use App\Schedule;
use Carbon\Carbon;

class ProcessLogObserver
{
    /**
     * Handle the process log "created" event.
     *
     * @param  \App\ProcessLog  $processLog
     * @return void
     */
    public function created(ProcessLog $process_log)
    {

        $now = now();
        $year = $now->year;
        $weekNumber = $now->weekOfYear;

        $schedule = Schedule::where('year', $year)
        ->where('week_number', $weekNumber)
        ->first();

        if (!$schedule) {
            return;
        }

        $middle = Carbon::createFromFormat('Y-m-d H:i:s', now()->format('Y-m-d') . ' 15:00:00');

        $start_middle = Carbon::createFromFormat('Y-m-d H:i:s', now()->format('Y-m-d') . ' 14:50:00');
        $end_middle = Carbon::createFromFormat('Y-m-d H:i:s', now()->format('Y-m-d') . ' 15:20:00');

        if (
            $process_log->parent_log &&
            $now->greaterThanOrEqualTo($start_middle) &&
            $now->lessThan($end_middle)
            )
        {

            if ($process_log->parent_log->created_at->format('Y-m-d') == now()->format('Y-m-d')) {

                $turn = $process_log->parent_log->turn;

            } else {

                if ($now->greaterThanOrEqualTo($middle)) {
                    $turn = $schedule->second_turn;
                } else {
                    $turn = $schedule->first_turn;
                }

            }

        } else {

            if ($now->greaterThanOrEqualTo($middle)) {
                $turn = $schedule->second_turn;
            } else {
                $turn = $schedule->first_turn;
            }

        }



        $process_log->turn = $turn;
        $process_log->save();
    }
}
