<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkcenterLog extends Model
{
    protected $fillable=[
    	'workcenter_id','description','ip'
    ];

    /*Relaciones*/
    public function workcenter()
    {
    	return $this->belongsTo(Workcenter::class);
    }
}
