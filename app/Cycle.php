<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Cycle extends Model
{
    use Uuids;

    protected $keyType = 'string';
    
    protected $fillable=[
        // 'user_id',
    	'user_process_id',
        'message','message_date',
        'process_status',
    ];
    protected $casts = [
        'message_date'=>'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'message_date',
    ];
    /**
    *  RELATIONS
    */
    public function user_process()
    {
    	return $this->belongsTo(UserProcess::class,'id','user_process_id');
    }
    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }
    
}
