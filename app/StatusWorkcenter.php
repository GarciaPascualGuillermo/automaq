<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusWorkcenter extends Model
{
    protected $fillable=[
    	'workcenter_id','user_id',
    	'comment',
    ];

    public function workcenter()
    {
    	$this->belongsTo(Workcenter::class);
    }
    public function user()
    {
    	$this->belongsTo(User::class);
    }
}
