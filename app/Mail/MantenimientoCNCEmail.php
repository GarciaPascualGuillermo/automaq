<?php

namespace App\Mail;

use App\User;
use App\Workcenter;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MantenimientoCNCEmail extends Mailable/* implements ShouldQueue*/
{
    public $user;
    public $workcenter;
    use Queueable, SerializesModels;

    // public $workcenter;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Workcenter $workcenter)
    {
        // $this->user = $user;
        $this->workcenter = $workcenter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                    // ->from('admin@tunerd.mx')
                    ->view('mails.Mantenimiento')
                    ->subject('Máquina en Mantenimiento');
        // return $this->from('admin@tunerd.mx')
        //             ->view('mails.Mantenimiento');
    }
}
