<?php

namespace App;

use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process extends Model
{
	use Uuids;
    use SoftDeletes;

    protected $keyType = 'string';

    // Legend
    // ih = Inicio de herramentaje
    // fh = Fin de herramentaje
    // ia = Fin de ajuste
    // fa = Fin de ajuste
    // fa = Fin de ajuste

    // il = Inicio de liberacion


	protected $fillable=[
		'job_id','operation_id','workcenter_id','capeando_id',
		'order','active',
		'operation_service','run_method','standar_time','quantity',
		'order','scraps','total_piece','status',
		'ajustador_ia_id','ajustador_fa_id','ajustador_il_id','ajustador_ial_id','ajustador_pm_id',
		'ih_created_at','fh_created_at','ia_created_at','fa_created_at','il_created_at',
		'ial_created_at','ls_created_at','ln_created_at','pr_created_at','pm_created_at', 'pmp_created_at',
		'pfh_created_at','pfm_created_at','cr_created_at','total_piece'
	];

	// protected $with = ['job','operation'];
	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'ih_created_at'=>'datetime',
		'fh_created_at'=>'datetime',
		'ia_created_at'=>'datetime',
		'fa_created_at'=>'datetime',
		'il_created_at'=>'datetime',
		'ial_created_at'=>'datetime',
		'ls_created_at'=>'datetime',
		'ln_created_at'=>'datetime',
		'pr_created_at'=>'datetime',
		'pm_created_at'=>'datetime',
		'pmp_created_at'=>'datetime',
		'pfh_created_at'=>'datetime',
		'pfm_created_at'=>'datetime',
		'cr_created_at'=>'datetime',
		'delivered_at'=>'date',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'ih_created_at','fh_created_at','ia_created_at','fa_created_at','il_created_at',
		'ial_created_at','ls_created_at','ln_created_at','pr_created_at','pm_created_at',
		'pmp_created_at','pfh_created_at','pfm_created_at','cr_created_at',
	];

	/**
	*  RELATIONS
	*/
	public function job()
	{
		return $this->belongsTo(Job::class);
	}
	public function setups()
	{
		return $this->hasMany(Setup::class);
	}
	public function operation()
	{
		return $this->belongsTo(Operation::class);
	}
	public function capeando()
	{
		return $this->belongsTo(Operation::class,'capeando_id','id');
	}

	public function workcenter()
	{
		return $this->belongsTo(Workcenter::class);
	}
    public function process_log()
    {
        return $this->hasMany(ProcessLog::class);
    }
	// public function cycles()
	// {
	// 	return $this->hasMany(Cycle::class);
	// }
	public function pauses()
	{
		return $this->hasMany(Pause::class);
	}
	public function user_processes()
	{
		return $this->hasMany(UserProcess::class);
	}

	public function ajustador_ia()
	{
		return $this->belongsTo(User::class);
	}
	public function ajustador_fa()
	{
		return $this->belongsTo(User::class);
	}
	public function ajustador_il()
	{
	  return $this->belongsTo(User::class);
	}
	public function ajustador_ial()
	{
		return $this->belongsTo(User::class);
	}
	public function ajustador_pm()
	{
		return $this->belongsTo(User::class);
	}
	public function rejected_processes()
	{
		return $this->hasMany(RejectedProcess::class);
	}


	/*
	*  QUERY SCOPES
	*/
	public function scopeJoinJob($query)
	{
		return $query->join('jobs as j','j.id','=','processes.job_id');
	}
	public function scopeJoinOperation($query)
	{
		return $query->join('operations as p','p.id','=','processes.operation_id');
	}
	public function scopeJoinPiece($query)
	{
		return $query->join('pieces as pi','pi.id','=','p.piece_id');
	}
	public function scopeJoinRechazadas($query)
	{
		return $query->join('rejected_processes as r','r.process_id','=','processes.id');
	}
	// public function scopeJoinInspector($query)
	// {
	// 	return $query->join('users as inspector','inspector.id','=','processes.ajustador_il_id');
	// }
	public function scopeJoinInspector($query)
	{
		return $query->leftJoin('users as insp_il','insp_il.id','=','processes.ajustador_il_id')
			->leftJoin('users as insp_ial','insp_ial.id','=','processes.ajustador_ial_id')
			;
	}

	public function scopeJob($query, $num_job='')
	{
		return $query->where('j.num_job','ilike',"%$num_job%");
	}
	public function scopePart_number($query, $part_number='')
	{
		return $query->where('pi.part_number','ILIKE',"%$part_number%");
	}
	// public function scopeInspector($query, $ajustador_il='')
	// {
	// 	return $query->join('jobs as j','j.id','=','processes.job_id')->where('j.num_job','ilike',"%$num_job%");
	// }
	// public function scopeJob($query, $num_job='')
	// {
	//     return $query->join('jobs as j','j.id','=','processes.job_id')->where('j.num_job','ilike',"%$num_job%");
    // }
    public function scopeJobDateTo($query, $keyword = null)
    {

        if ($keyword) {
            try {
                $keyword = Carbon::parse($keyword);
            } catch (\Throwable $th) {
                $keyword = null;
            }
            return $query->where('j.delivered_at', '<=', $keyword);
        }

    }
    public function scopeJobDateFrom($query, $keyword = null)
    {

        if ($keyword) {
            try {
                $keyword = Carbon::parse($keyword);
            } catch (\Throwable $th) {
                $keyword = null;
            }
            return $query->where('j.delivered_at', '>=', $keyword);
        }

    }
    public function scopeJobNum($query, $keyword = null)
    {
        if ($keyword) {
            return $query->orWhere('j.num_job', 'LIKE', "%$keyword%");
        }
    }
    public function scopeJobPart($query, $keyword = null)
    {
        if ($keyword) {
            return $query->orWhere('j.num_part', 'LIKE', "%$keyword%");
        }
    }
    public function scopeForDay($query, $day)
    {

        try {
            $day = Carbon::parse($day)->format('Y-m-d');
        } catch (\Throwable $th) {
            $day = null;
        }

        if ($day) {
            $query->whereHas('processes', function ($subquery) use ($day)
            {
                $subquery
                ->whereDate('ih_created_at', $day)
                ->orWhereDate('ih_created_at', $day)
                ->orWhereDate('ia_created_at', $day)
                ->orWhereDate('fa_created_at', $day)
                ->orWhereDate('il_created_at', $day)
                ->orWhereDate('ial_created_at', $day)
                ->orWhereDate('ln_created_at', $day)
                ->orWhereDate('ls_created_at', $day)
                ->orWhereDate('pr_created_at', $day)
                ->orWhereDate('cr_created_at', $day)
                    ;
            }, '>', 0);
        }

    }
}
