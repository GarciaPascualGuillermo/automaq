<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class MachineDay extends Model
{
	use Uuids;

    protected $fillable = [
    	'workcenter_id',
		'day',
		'ajuste',
		'herramentaje',
		'liberacion',
		'autoliberacion',
		'produccion',
		'mantenimiento',
		'pausa_otros',
		'sinuso',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    	'day'=>'date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
    	'day'
    ];


    /*Relaciones*/

    public function workcenter()
    {
    	return $this->belongsTo(Workcenter::class);
    }
}
