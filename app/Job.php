<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Job extends Model
{
	use Uuids;

	protected $keyType = 'string';
	/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable=[
			'num_job','quantity','delivered_at','status', 'release_date'

		];

		protected $casts = [
			'delivered_at'=>'date',
        ];

		/**
		*  Relations
		*/
		public function operation()
		{
			return $this->belongsTo(Operation::class);
		}

		public function processes()
		{
			return $this->hasMany(Process::class);
        }

        public function onlyTrashedProcesses()
        {
            return $this->hasMany(Process::class)->onlyTrashed();
        }
		public function scopeJob($query, $num_job='')
		{
		    return $query->where('num_job','ILIKE',"%$num_job%");
        }
}
