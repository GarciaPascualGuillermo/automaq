<?php

namespace App\Events;

use App\Workcenter;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class OperadorAmountPieces implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $wc;
    public $porcent;
    public $pieceTotal;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Workcenter $wc, $porcent, $pieceTotal)
    {
        $this->wc = $wc;
        $this->porcent = intval($porcent);
        $this->pieceTotal = $pieceTotal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('amount_piece-' . $this->wc->id);
    }
}
