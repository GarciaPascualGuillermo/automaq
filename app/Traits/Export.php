<?php

namespace App\Traits;

use App\Process;
use Carbon\Carbon;
use App\Workcenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait Export {

    public function exportexcelto(Request $request)
    {
        $day = request('day');
        $active = false;

        // Transformacion de la fecha
        if ($day) {
            try {
                $day = Carbon::parse($day)->format('d-M-Y');
            } catch (\Throwable $th) {
                $day = null;
            }
        } else {
            $day = null;
            $active = true;
        }

		if (auth()->user()->hasRoles(['sup.produccion'])) {
			$user = auth()->user();
            $workcenters = $user->workcenters;
		} else {
            $workcenters = Workcenter::all();
		}

        $workcenter_section = strtoupper($request->input("section", ""));

		$workCenters = WorkCenter::with([
            'processes', 'processes.job', 'processes.operation.piece.client', 'processes' => function ($subquery) use ($active)
            {
                $subquery->where('active', $active);
            }
        ])
		->where('section', 'ILIKE', "%$workcenter_section%")
        ->whereIn('id', $workcenters->pluck('id'))
        ->forDay($day)
        ->orderBy('section','ASC')
        ->get();

        // dd($workCenters);

        $csv = '- ,JOB, HERRAMENTAJE, AJUSTE, LIBERACION, PRODUCCION \n';

		foreach ($workCenters as $workcenter) {

            if (!$day) {
                $process = $workcenter->processes->where('active',true)->first();
            } else {
                $process = $workcenter->processes->first();
            }



			$workcenter['total_herramentaje']=0;
			$workcenter['total_ajuste']=0;
			$workcenter['total_liberacion']=0;
			$workcenter['total_produccion']=0;


			if (!is_null($process) && !is_null($process->ih_created_at)) {
				$workcenter['total_herramentaje']=$process->ih_created_at->diffInSeconds($process->fh_created_at);
			}
			if (!is_null($process) && !is_null($process->ia_created_at)) {
				$workcenter['total_ajuste']=$process->ia_created_at->diffInSeconds($process->fa_created_at);
			}
			if (!is_null($process) && !is_null($process->ial_created_at) ) {
				$workcenter['total_liberacion']=$process->ial_created_at->diffInSeconds($process->ls_created_at);
			}elseif(!is_null($process) && !is_null($process->il_created_at) ){
				$workcenter['total_liberacion']=$process->il_created_at->diffInSeconds($process->ls_created_at);
			}
			if (!is_null($process) && !is_null($process->pr_created_at) ) {
				$workcenter['total_produccion']=$process->pr_created_at->diffInSeconds($process->cr_created_at);
            }

			$workcenter['total_herramentaje']=round(($workcenter['total_herramentaje']/60),0);
			$workcenter['total_ajuste']=round(($workcenter['total_ajuste']/60),0);
			$workcenter['total_liberacion']=round(($workcenter['total_liberacion']/60),0);
			$workcenter['total_produccion']=round(($workcenter['total_produccion']/60),0);
			$workcenter['total_cycles']=0;

			$getCycles=["piecePercent" => 0];

			if (!is_null($process)) {

				$user_processes = $process->user_processes->where('active',true)->first();
				$getCycles = getCycles($process);

			}

			if (!is_null($process)) {
				$total=$process->job->quantity;
			}else
			 $total=1;
			$workcenter['progreso']=$getCycles['piecePercent'];
			$workCenter["time"]=0;

            $processes = Process::with('user_processes')
                ->where('workcenter_id', $workcenter->id)
				->select('processes.*', 'up.status_start', 'up.start_time', 'up.end_time')
				->join('user_processes as up', 'up.process_id', '=', 'processes.id')
				->whereIn('status_start', ['pr', 'crp'])
				->where('processes.active', true)
				->get();

            $time = [];

			foreach ($processes as $process) {

				$start_tim = Carbon::createFromFormat('Y-m-d H:i:s',$process->start_time);
                $end_time = null;

				if (!is_null($process->end_time)) {
					$end_time = Carbon::createFromFormat('Y-m-d H:i:s',$process->end_time);
                }

				$time[] = fechas_tecmaq($start_tim, $end_time);
			}
            $time = collect($time);

            $workcenter->total_produccion = intval($time->avg()/60);

            $csv .= $workcenter->num_machine . ",";
            if ($workcenter->processes->count() > 0) {
                $csv .= $workcenter->processes->first()->job->num_job . ",";
            } else {
                $csv .= "- ,";

            }

            $csv .= $workcenter['total_herramentaje'] . ",";
            $csv .= $workcenter['total_ajuste'] . ",";
            $csv .= $workcenter['total_liberacion'] . ",";
            $csv .= $workcenter['total_produccion'] . ", \n";
            $csv .= 'Operador, ';

            if ($workcenter->processes->count() > 0) {

                $process = $workcenter->processes->first();

                $user = $process->user_processes->where('active',true)->first();
                if ( !is_null($user) ) {
                    $user = $user->user;
                    $csv .= $user->name . " " . $user->firstname . ",";
                } else {
                    $csv .= "- ,";
                }

                if ($process->ajustador_ia) {
                    $csv .=  $process->ajustador_ia->name . " " . $process->ajustador_ia->firstname . ",";
                } else {
                    $csv .= "- ,";
                }

                if ($process->ajustador_il) {
                    $csv .=  $process->ajustador_il->name . " " . $process->ajustador_il->firstname . ",";
                } else {
                    if ($process->ajustador_ial) {
                        $csv .=  $process->ajustador_ial->name . " " . $process->ajustador_ial->firstname . ",";
                    } else {
                        $csv .= "- ,";
                    }
                }

                $user_processes = $process->user_processes->where('active', true);

                if ($user_processes->count() > 0) {
                    $csv .= $workcenter['progreso'] . "\n";
                } else {
                    $csv .= "- \n";
                }

            } else {
                $csv .= "-,-,-,- \n";
            }

        }

        $path="supervisor_dia/";
		$name="supervisor_dia" . date('y_m_d') . ".csv";
		$remove_acents=["Á"=>"A","É"=>"E","Í"=>"I","Ó"=>"O","Ú"=>"U","á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u"];
		$csv = strtr($csv,$remove_acents);
		Storage::disk('public')->put("$path$name", $csv);
		return Storage::disk('public')->download("$path$name");

    }

}
