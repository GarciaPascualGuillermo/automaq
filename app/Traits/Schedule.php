<?php

namespace App\Traits;

use App\Schedule as AppSchedule;
use Carbon\Carbon;
use Schema;

trait Schedule {

    /**
     * Alimenta la tabla schedule para un periodo dado, basado en semanas pares e impares.
     * Devuelve true si fue exitoso, false si falla el task
     *
     * @param Carbon\Carbon $from
     * @param Carbon\Carbon $to
     * @return boolean
     */
    public function feedScheduleTable(Carbon $from, Carbon $to = null)
    {

        if (!$to) {
            $to = Carbon::createFromDate(now()->format('Y'), 12, 31);
            $end_day = $to->startOfWeek();
        } else {
            $end_day = $to->startOfWeek();
        }

        $start = Carbon::parse($from);

        $start_of_week = $start->startOfWeek();

        $flag = false;

        $on_cycle = true;

        do {

            $week_number = $start_of_week->weekOfYear;
            $year = $start_of_week->year;

            if ($week_number % 2 == 0) {
                $group_in_morning = 'B';
                $group_in_afternoon = 'A';
            } else {
                $group_in_morning = 'A';
                $group_in_afternoon = 'B';
            }

            $match = AppSchedule::firstOrCreate([
                'year'          => $year,
                'week_number'   => $week_number,
                'start_of_week' => $start_of_week->format("Y-m-d"),
            ], [
                'first_turn'    => $group_in_morning,
                'second_turn'   => $group_in_afternoon,
            ]);

            $start_of_week->addWeek();

            if ($flag) {
                $on_cycle = false;
            }

            if ($start_of_week->isSameDay($end_day)) {
                $flag = true;
            }

        } while ($on_cycle);

        return true;
    }
}
