<?php

namespace App\Traits;

use App\Workcenter;
use Illuminate\Support\Facades\App;

trait Machines {

    function getMachines($from, $to, $workcenter_section)
    {
        // dd($from, $to);
        $workcenters = Workcenter::with(['processes' => function ($query) use ($from, $to) {
            $query
                ->orderBy('order','asc')
                ->whereHas('process_log', function ($subquery) use ($from, $to)
                {
                    $subquery->date($from, $to);
                })
                ->with(['job', 'operation.piece.client', 'process_log' => function ($subquery) use ($from, $to) {
                    $subquery
                        ->with(['process.job', 'process.operation.piece', 'user', 'user_process', 'parent_log'])
                        ->date($from, $to)
                        ->orderBy('created_at', 'DESC')
                        ;
                }])
                ;
        }, 'process_logs' => function ($query) use ($from, $to)
        {

            $query
                ->with(['process.job', 'process.operation.piece', 'process.user_processes', 'user', 'user_process', 'parent_log'])
                ->orderBy('created_at', 'DESC')
                ->date($from, $to)
                ;
        }])
        ->where('section','LIKE',  "%$workcenter_section%")
        ->orderBy('order','ASC')
        ->orderBy('section','ASC')
        ->get()
        ->map(function ($workcenter) use ($from, $to) {

            if ($workcenter->process_logs->count() > 0) {

                $log = $workcenter->process_logs->first();
                $cycles = getCycles($log->process);

                $standar_time = $log->process->operation->standar_time;
                $quantity = $cycles['pieceTotal'];

                if ($log->process->pr_created_at) {
                    $eficiencia = productivity($log->process, $log->user)['productividad'];
                } else {
                    $eficiencia = 0;
                }


                $workcenter->total_pieces = $cycles['pieceTotal'];
                $workcenter->pieces_percent = $cycles['piecePercent'];

            } else {
                $eficiencia = 0;
            }

            $workcenter->eficiencia = $eficiencia;

            $workcenter->tiempos = porcentajes_maquina($workcenter, $from, $to);
            return $workcenter;
        });

        if (App::environment('local')) {
            $workcenters = $workcenters->take(5);
        }

        return $workcenters;
    }
}
