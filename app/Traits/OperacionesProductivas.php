<?php

namespace App\Traits;

trait OperacionesProductivas {

    public function obtenerWorkcenterProductivos()
    {
        return [
            '5 EJES',
            'CMHORIZONT',
            'CMVERTCH',
            'CMVERTGR',
            'CMVERTICAL',
            'DHP HORIZ',
            'DMU 80',
            'HP6300',
            'LNX200/220',
            'LYNX 200',
            'LYNX 220',
            'MAZAK I600',
            'PMA240/280',
            'PUMA 300',
            'PUMA 400',
            'PUMA 480',
            'TORNOVERT',
            'TRNOSUIZO',
            'I700',
            'MAZAKVC500',
            '108 PUMATW'
        ];
    }

}
