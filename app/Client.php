<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable =[
    	'name',
    ];

    // Relations
    public function pieces()
    {
    	return $this->hasMany(Piece::class);
    }
}
