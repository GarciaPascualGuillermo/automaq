<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    protected $fillable = [
    	'client_id','part_number','mot','technical_drawing','inspection_plan',
    ];

    // Relations
    public function client()
    {
    	return $this->belongsTo(Client::class);
    }

    public function operations()
    {
    	return $this->hasMany(Operation::class);
    }

    public function scopePart_number($query, $part_number='')
    {
        return $query->where('part_number','ILIKE',"%$part_number%");
    }
}
