<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProcessLog extends Model
{

    protected $fillable = ['activity', 'process_id', 'user_id', 'comment', 'parent_id', 'user_process_id', 'workcenter_id', 'pause_id', 'turn'];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
    public function process()
    {
        return $this->belongsTo(Process::class)->withTrashed();
    }
    public function parent_log()
    {
        return $this->belongsTo(ProcessLog::class, 'parent_id');
    }
    public function child_log()
    {
        return $this->hasOne(ProcessLog::class, 'parent_id');
    }
    public function user_process()
    {
        return $this->belongsTo(UserProcess::class);
    }
    public function workcenter()
    {
        return $this->belongsTo(Workcenter::class);
    }
    public function setTurnAttribute($value)
    {
        $this->attributes['turn'] = $value;
    }
    public function getColorAttribute()
    {
        if ( in_array($this->activity, ['cr', 'crp']) ) {
            $activity = $this->parent_log->activity;
        } else {
            $activity = $this->activity;
        }

        if ( in_array($activity, ['ih', 'fh', 'ia', 'fa']) ) {
            return 'orange';
        }
        if ( in_array($activity, ['il', 'ls', 'ial', 'ln']) ) {
            return 'yellow';
        }
        if ( $activity == 'pr' ) {
            return 'green';
        }
        if ( in_array($activity, ['pfh', 'pfm']) ) {
            return 'red';
        }
        if ( $activity == 'pm' ) {
            return 'purple';
        }
    }
    public function getTiempoMuertoAttribute()
    {
        $act = $this->activity;
        if ( $act == 'ia' || ( $act == 'pr' && $this->parent_log->activity == 'ls' )) {
            try {
                $this->created_at->diff($this->parent_log->created_at);
            } catch (\Throwable $th) {
                return '';
            }
            return $this->created_at->diff($this->parent_log->created_at)->format('%H:%I:%S');
        } else {
            return '';
        }
    }
    public function getConceptAttribute()
    {
        switch ($this->activity) {
            case 'pr':
                return 'GAP en espera de operador';
                break;

            case 'ia':
                return 'GAP en espera de ajustador';
                break;

            case 'ln':
                return 'GAP en espera de ajustador';
                break;

            default:
                return '';
                break;
        }
    }
    public function turnoTime($turno)
    {
        $time = null;

        if($turno == 'a') {
            $time['start'] = '6:45:00';
            $time['end'] = '15:00:00';
        } elseif ($turno == 'b') {
            $time['start'] = '15:00:00';
            $time['end'] = '23:00:00';
        } else {
            return null;
        }

        return $time;
    }
    public function specificLog($activities)
    {

        if (!empty($activities)) {

            if (!is_array($activities)) {
                $activities = [$activities];
            }

            $parent = $this->parent_log;

            while ($parent) {

                if (in_array($parent->activity, $activities)) {
                    return $parent;
                }

                $parent = $parent->parent_log;

            }
        }
    }
    /**
     * Query Scopes
     */
    public function scopeEmployee($query, $keyword)
    {
        if (!empty($keyword)) {

            if (!is_array($keyword)) {
                $keyword = [$keyword];
            }

            $query->whereHas('user', function (Builder $subquery) use ($keyword)
            {
                $subquery->whereIn('idemployee', $keyword);
            });
        }
    }
    public function scopeJob($query, $keyword)
    {
        if ($keyword) {
            $query->whereHas('process.job', function (Builder $subquery) use ($keyword)
            {
                $subquery->where('num_job', 'ILIKE', "%$keyword%");
            });
        }
    }
    public function scopePartNumber($query, $keyword)
    {
        if ($keyword) {
            $query->whereHas('process.operation.piece', function (Builder $subquery) use ($keyword)
            {
                $subquery->where('part_number', 'ILIKE', "%$keyword%");
            });
        }
    }
    public function scopeCustomer($query, $keyword)
    {
        if ($keyword) {
            $query->whereHas('process.operation.piece.client', function (Builder $subquery) use ($keyword)
            {
                $subquery
                ->where('customer', 'ILIKE', "%$keyword%")
                ->orWhere('name', 'ILIKE', "%$keyword%")
                ;
            });
        }
    }
    public function scopeDate($query, $from, $to, $turno = null)
    {
        if ($from || $to) {

            $timeTurno = $this->turnoTime($turno);
            $query
            ->where(function ($query) use ($from, $to, $timeTurno)
            {
                if ($from) {
                    try {
                        $from = Carbon::parse($from);
                    } catch (\Throwable $th) {
                        $from = null;
                    }
                    $query
                    ->whereDate('created_at', '>=', $from);
                }
                if ($to) {
                    try {
                        $to = Carbon::parse($to);
                    } catch (\Throwable $th) {
                        $to = null;
                    }
                    $query
                    ->whereDate('created_at', '<=', $to);
                }
                // if (!empty(($timeTurno))) {

                //     $query
                //     ->whereTime('created_at', '>=', $timeTurno['start'])
                //     ->whereTime('created_at', '<=', $timeTurno['end'])
                //     ;

                // }
            });
        }
    }
    public function scopeWorkcenterId($query, $workcenters_ids)
    {

        if (!is_array($workcenters_ids)) {
            $workcenters_ids = [$workcenters_ids];
        }

        if (!empty($workcenters_ids)) {
            $query->whereIn('workcenter_id', $workcenters_ids);
        }

    }
    public function scopeType($query, $keyword)
    {
        if ($keyword) {
            $query->whereHas('process.operation',function ($subquery) use ($keyword)
            {
                $subquery->where('work_center', 'LIKE', "%$keyword%");
            });
        }
    }
    public function scopeProductivas($query)
    {
        $operaciones_productivas = $this->obtenerWorkcenterNoProductivos();

        $query->whereHas('process.operation', function ($subquery) use ($operaciones_productivas)
        {
            $subquery->whereIn('work_center', $operaciones_productivas);
        });
    }
    public function scopeTechnician($query, $keyword)
    {
        if (!empty($keyword)) {
            $query->whereHas('pause', function ($subquery) use ($keyword)
            {
                $subquery->where('user_id', $keyword);
            });
        }
    }
}
