<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Mail\MantenimientoCNCEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $users;
    public $process;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users,$process)
    {
        $this->users=$users;
        $this->process=$process;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->users->pluck('email'), "Mantenimiento")
            ->send(new MantenimientoCNCEmail($this->process->workcenter));
        Mail::to(['andrea.gallastegui@tunerd.mx'], "Mantenimiento")
            ->send(new MantenimientoCNCEmail($this->process->workcenter));
    }
}
