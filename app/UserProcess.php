<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class UserProcess extends Model
{
    use Uuids;

    protected $keyType = 'string';

    protected $fillable=[
    	'user_id','process_id','active',
        'log_out',
    	'start','end','start_at',
        'start_time','end_time',
        'capeando',
        'status_start','status_end',
    ];

    protected $casts = [
        'start_time'=>'datetime',
        'end_time'=>'datetime',
        'start_at'=>'date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_time','end_time',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function process()
    {
    	return $this->belongsTo(Process::class);
    }
    public function cycles()
    {
        return $this->hasMany(Cycle::class,'user_process_id','id');
    }
    public function productivities()
    {
        return $this->hasMany(Productivity::class);
    }
    public function getDonePiecesAttribute()
    {
        if ($this->end)
            return $this->end - $this->start;
        return 0;
    }
    public function getConformedPiecesAttribute()
    {
        if ($this->done_pieces && $this->scraps > 0)
            return $this->done_pieces - $this->scraps;
        return 0;
    }
    public function getSessionTimeAttribute()
    {
        $time = new \DateTime('00:00');

        $process = $this->process;

        if (!$this->start_time || !$this->end_time) {
            return 'En produccion';
        }

        $time->add($this->start_time->diff($this->end_time));

        return $time->diff(new \DateTime('00:00'))->format('%H:%I:%S');
    }
}
