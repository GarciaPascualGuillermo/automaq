<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class RejectedProcess extends Model
{
    use Uuids;
    
    protected $keyType = 'string';
    
    protected $fillable=[
    	'process_id',
        'comment',
    	'ended_at','started_at',
        'setup_id',
        'user_id',
        'fullname',
        'aceptada'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'ended_at' => 'datetime',
        'started_at' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['started_at','ended_at'];

    public function process()
    {
    	return $this->belongsTo(Process::class);
    }
    public function setup()
    {
        return $this->belongsTo(Setup::class);
    }
}
