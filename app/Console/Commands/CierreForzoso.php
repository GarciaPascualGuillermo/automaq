<?php

namespace App\Console\Commands;

use App\{UserProcess, ProcessLog, Process, User};
use App\Events\FinishProcessEvent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CierreForzoso extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cierre:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando tiene la funcionalidad de hacer un cierre forzoso a los operadores que olvidan cerrar sesión de sus procesos. Será ejecutado por un Cron Job a diario a las 11:05 pm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $user_processes = UserProcess::whereNull('end')
        ->where('log_out', false)
        ->whereNull('end_time')
        ->whereDate('start_at', now())
        // ->where('active', true)
        ->get();

        foreach ($user_processes as $user_process) {

            try {

                $process = Process::find($user_process->process_id);
                $user = User::find($user_process->user_id);
                $cycles = getCycles($process);
                $productivity = productivity($process, $user);

                $process->status = 'crp';
                $process->active = false;

                $process->save();

                $user_process->log_out = true;
                $user_process->active = false;
                $user_process->status_end = 'crp';
                $user_process->end = $cycles['pieceTotal'];
                $user_process->end_time = now();
                $user_process->productivity = $productivity['productividad'];
                $user_process->save();

                // Datos para el log
                $parent_id = parent_id($process);

                $process_log = ProcessLog::find($parent_id);

                ProcessLog::create([
                    'user_process_id'   => $user_process->id,
                    'process_id'        => $process->id,
                    'comment'           => 'Cierre forzoso',
                    'user_id'           => $user->id,
                    'parent_id'         => $parent_id,
                    'activity'          => 'crp',
                    'workcenter_id'     => $process_log->workcenter_id
                ]);

                event(new FinishProcessEvent($process));


            } catch (\Throwable $th) {



            }


        }

    }
}
