<?php

use App\Process;
use Illuminate\Support\Carbon;

function fechas_tecmaq ($fech_i,$fech_f=null)
{
	$day=24;
	$day_tecmaq=15; # 15 horas laborales
	$hours=60;
	$minutes=60;
	$seconds=60;
	$horas_no_laboral= (9*60)*60;
	$no_day=0;
	$now=Carbon::now();
	if (is_null($fech_f)){
		$fech_f=Carbon::now();	
	}
	if ($fech_i->greaterThan($fech_f) || is_null($fech_i)){
		return -1;
	}else{
		$fech_i_sd = Carbon::createFromFormat('Y-m-d H:i:s', $fech_i->toDateTimeString())->startOfDay();
		// dd($fech_i_sd);
		$fech_f_sd = Carbon::createFromFormat('Y-m-d H:i:s', $fech_f->toDateTimeString())->startOfDay();
		// dd($fech_f_sd);
		$dif= $fech_f->diffInSeconds($fech_i);
		// dd($dif);
		$no_day= $fech_f_sd->diffInDays($fech_i_sd);
		// dd($no_day,$dif);
		if ($no_day>0) {
			$dif = $dif-(($horas_no_laboral)*$no_day);
			// dd($dif);
		}

		return $dif;
	}
}
	
function fechas_tecmaq_mas2 ($fech_i,$fech_f)
{
	$day=24;
	$day_tecmaq=15; # 15 horas laborales
	$hours=60;
	$minutes=60;
	$seconds=60;
	$horas_no_laboral= (9*60)*60;
	$no_day=0;
	$now=Carbon::now();
	
	if ($fech_i->greaterThan($fech_f)){
		return -1;
	}
	else{
	
		if (is_null($fech_f)){
			$fech_f=Carbon::now();
			$fech_i->addHours(2);  
		}
		
		$fech_i_sd = Carbon::createFromFormat('Y-m-d H:i:s', $fech_i->toDateTimeString())->startOfDay();
		// dd($fech_i_sd);
		$fech_f_sd = Carbon::createFromFormat('Y-m-d H:i:s', $fech_f->toDateTimeString())->startOfDay();
		// dd($fech_f_sd);
		$dif= $fech_f->diffInSeconds($fech_i);
		// dd($dif);
		$no_day= $fech_f_sd->diffInDays($fech_i_sd);
		// dd($no_day,$dif);
		if ($no_day>0) {
			$dif = $dif-(($horas_no_laboral)*$no_day);
			// dd($dif);
		}
		return $dif;
	}
}

function fechas_tecmaq_sub_mealtime (Carbon $fech_i,Carbon $fech_f=null)
{
	$day=24;
	$day_tecmaq=15; # 15 horas laborales
	$hours=60;
	$minutes=60;
	$seconds=60;
	$horas_no_laboral= (9*60)*60;
	$no_day=0;
	$now=Carbon::now();
	$mealtime_start=new Carbon(date('Y-m-d')." 12:00:00");
	$mealtime_end=new Carbon(date('Y-m-d')." 12:30:00");
	$dinner_start=new Carbon(date('Y-m-d')." 19:30:00");
	$dinner_end=new Carbon(date('Y-m-d')." 20:00:00");

	if ($fech_i->greaterThan($fech_f) || is_null($fech_i)){
		return 0;
	}else{
	
		if (is_null($fech_f)){
			$fech_f=Carbon::now();	
		}
		$fech_i_sd = Carbon::createFromFormat('Y-m-d H:i:s', $fech_i->toDateTimeString())->startOfDay();
		$fech_f_sd = Carbon::createFromFormat('Y-m-d H:i:s', $fech_f->toDateTimeString())->startOfDay();
		$dif= $fech_f->diffInSeconds($fech_i);
		$no_day= $fech_f_sd->diffInDays($fech_i_sd);
		// dd($fech_i,$fech_f,$dif,$no_day);

		if ($no_day>0 && $dif>54000) {
			$dif = $dif-(($horas_no_laboral)*$no_day);
		}
		if ($fech_i->lessThanOrEqualTo($mealtime_start) && $fech_f->greaterThanOrEqualTo($mealtime_end)) {
			$dif=$dif-(30*60); //Resta de la comida o cena
		}
		if($fech_i->lessThanOrEqualTo($dinner_start) && $fech_f->greaterThanOrEqualTo($dinner_end)) {
			$dif=$dif-(30*60); //Resta cena
		}
		return $dif;
	}
}