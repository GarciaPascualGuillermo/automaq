<?php

use App\{Process, UserProcess};
use Illuminate\Support\Carbon;

function productivity(Process $process, $user = null, $user_process_active = true)
{
	if (is_null($user)) {
		$user=auth()->user();
	}

	$mealtime_start = Carbon::createFromFormat('Y-m-d H:i:s',date('Y-m-d')." ".$user->mealtime);
	$mealtime_end = Carbon::createFromFormat('Y-m-d H:i:s',date('Y-m-d')." ".$user->mealtime)->addMinutes(30);

	$mealtime2_start = Carbon::createFromFormat('Y-m-d H:i:s',date('Y-m-d')." ".$user->mealtime2);
	$mealtime2_end = Carbon::createFromFormat('Y-m-d H:i:s',date('Y-m-d')." ".$user->mealtime2)->addMinutes(30);



	$tiempo_standar = $process->standar_time * 60;
	$tiempo_real_prod = 0;
	$ciclos = 0;
	$productividad = 0;
	$pausa = 0;
    $now = Carbon::now();

	if ($user_process_active) {

		$user_processes = $process->user_processes->where('active',true);
        $pauses = $process->pauses->where('workcenter_id', '!=', null)->where('started_at', '>=', date('Y-m-d')." 07:30:00");

    } else {

		$pauses = $process->pauses->where('workcenter_id','!=',null);
        $user_processes = $process->user_processes;

    }

    $pauses = $pauses->where('motivo', '<>', 'mantenimiento');

	$user_processes = collect([$process->user_processes->sortBy('created_at')->last()]);

    foreach ($pauses as $pause) {
		if (is_null($pause->ended_at)) {
			$pause->ended_at=Carbon::now();
		}
		$pausa += $pause->started_at->diffInSeconds($pause->ended_at);
	}

	foreach ($user_processes as $user_process) {

        if (is_null($user_process->cycles))
            continue;

		$cycles = $user_process->cycles->where('process_status','pr')->whereIn('message', ['START','12345'] )->sortBy('message_date');

		if ($process->pr_created_at && $process->pr_created_at->greaterThanOrEqualTo($user_process->start_time)) {
			$start_production = $process->pr_created_at;
        } else {
            $start_production = $user_process->start_time;
        }

		if ( is_null($user_process->end_time)) {
			$end_production = $now;
		} else {
			$end_production = $user_process->end_time;
        }

        $tiempo_real_prod += $start_production->diffInSeconds($end_production);
		//quita hora de comida matutino
		if($start_production->lte($mealtime_start) && $end_production->gte($mealtime_end)){
			if ($now->gte($mealtime_start)) {
				if ($now->lte($mealtime_end)) {
					$tiempo_real_prod-= $mealtime_start->diffInSeconds($now);
				}else{
					$tiempo_real_prod-= (30*60);
				}
			}
		}
		// Quita hora de comida vespertino
		if($start_production->lte($mealtime2_start) && $end_production->gte($mealtime2_end)){
			if ($now->gte($mealtime2_start)) {
				if ($now->lte($mealtime2_end)) {
					$tiempo_real_prod-= $mealtime2_start->diffInSeconds($now);
				}else{
					$tiempo_real_prod-= (30*60);
				}
			}
		}
		$getCycles = getCycles($process, $user);
        $ciclos = $getCycles["pieceTotal"];

    }


    $tiempo_real_prod = $tiempo_real_prod - $pausa;

	if ($ciclos != 0) {
		$productividad = $tiempo_standar / ($tiempo_real_prod / $ciclos);
	}
	// dd($end_production,$start_production,$tiempo_real_prod,$tiempo_standar,$ciclos,$productividad*100);
	// return response()->json([$tiempo_real_prod,$tiempo_standar,$ciclos,$productividad],401);
    $productividad = round($productividad*100, 0);

	return [
        'productividad' => $productividad < 0 ? 0 : $productividad, 'pausas' => $pausa
    ];
}
function getCycles(Process $process, $user = null)
{


	if(is_null($user)) {

        $cycles = collect($process->user_processes->pluck('cycles'))->collapse();
        $pauses = $process->pauses->where('workcenter_id','!=',null)->where('started_at','>=',date('Y-m-d')." 07:30:00");

	} else {

        $cycles = collect($process->user_processes->where('user_id', $user->id)->where('active', true)->pluck('cycles'))->collapse();
        $pauses = $process->pauses->where('workcenter_id','!=',null);

    }

	if ($cycles->where('message','START')->count() == 0) {
        return [
            "piecePercent"  => intval((0 / $process->job->quantity) * 100),
            'pieceTotal'    => 0
        ];
    }

    $total_cycles = 0;

	$cycles = $cycles
			->where('process_status','!=','pm')->where('process_status','!=','pmp')
			->where('process_status','!=','pfh')->where('process_status','!=','pfm')
            ->whereIn('message',['START','12345']);

	foreach ($cycles as $key => $cycle) {

        $contar_ciclo = true;

		if ($cycle->message=='START') {

			if ( isset($cycles[$key + 1])) {

				foreach ($pauses as $pause) {
					if (is_null($pause->ended_at)) {
						$pause->ended_at = Carbon::now();
					}

					if ($cycle->message_date->gte($pause->started_at) && $cycle->message_date->lte($pause->ended_at) && $pause->motivo != 'mantenimiento' ) {
						$contar_ciclo = false;
						break;
					}
                }

				if ($cycles[$key + 1]->message == '12345' && $contar_ciclo) {
					//$tiempo_real_prod+=$cycle->message_date->diffInSeconds($cycles[$key+1]->message_date);
					$total_cycles++;
				}
			}
		}
	}

	//if ($cycles->where('process_status','!=','pr')->count() && ($process->status=='pr' ||  $process->status=='crp')) $total_cycles++;
    $total = $process->quantity > 0 ? $process->quantity : $process->job->quantity;
    $t = ($total_cycles / $total) * 100;
	// dd($cycles->sortBy('message_date')->toArray(),$total_cycles,$t);

	return [
        "piecePercent"  => $t > 100 ? 100 : intval($t),
        "pieceTotal"    => $total_cycles
    ];
}
function getCyclesByUserProcess(UserProcess $user_process)
{


    $cycles = $user_process->cycles;
    $pauses = $user_process->process->pauses->where('workcenter_id', '!=', null);


	if ($cycles->where('message','START')->count() == 0) {
        return [
            'pieceTotal'    => 0
        ];
    }

    $total_cycles = 0;

	$cycles = $cycles
			->where('process_status','!=','pm')->where('process_status','!=','pmp')
			->where('process_status','!=','pfh')->where('process_status','!=','pfm')
            ->whereIn('message',['START','12345']);



	foreach ($cycles as $key => $cycle) {

        $contar_ciclo = true;

		if ($cycle->message == 'START') {

			if ( isset($cycles[$key + 1])) {

				foreach ($pauses as $pause) {
					if (is_null($pause->ended_at)) {
						$pause->ended_at = Carbon::now();
					}

					if ($cycle->message_date->gte($pause->started_at) && $cycle->message_date->lte($pause->ended_at) ) {
						$contar_ciclo = false;
						break;
					}
                }

				if ($cycles[$key + 1]->message == '12345' && $contar_ciclo) {
					//$tiempo_real_prod+=$cycle->message_date->diffInSeconds($cycles[$key+1]->message_date);
					$total_cycles++;
				}
			}
		}
    }
    if ($user_process->process->quantity > 0) {

        $quantity = $user_process->process->quantity;

    } else {

        $quantity = $user_process->process->job->quantity;
    }


    $t = ($total_cycles / $quantity) * 100;

	return [
        "piecePercent"  => intval($t),
        "pieceTotal"    => $total_cycles
    ];
}
