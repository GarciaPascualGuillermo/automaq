<?php

use App\Pause;
use App\Process;
use App\MachineDay;
use App\ProcessLog;
use App\Workcenter;
use App\UserProcess;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

function porcentajes_maquina(Workcenter $workcenter, $desde = null, $hasta = null, $turno = null)
{

    // Primero se define el total de tiempo que una máquina está operativa

    $startDay = '06:45:00';
    $endDay = '23:00:00';

    $intervalo_total_en_segundos = Carbon::createFromFormat('H:i:s', $startDay)->diffInSeconds(Carbon::createFromFormat('H:i:s', $endDay));
    $segundos_en_un_dia = $intervalo_total_en_segundos;

    // Variables finales para ser calculadas durante el desarrollo del algoritmo
    $sinuso = $produccion = $ajuste = $herramentaje = $liberacion = $autoliberacion = $mantenimiento = $paros = $tiempo_total_produccion = 0;
    $eficiencia = collect();
    $ponderada = collect();

    // Parseado
    $desde = Carbon::parse($desde);
    $hasta = Carbon::parse($hasta);

    if ($hasta->greaterThan(now())) {
        $hasta = now();
    }

    if ($desde->greaterThan(now())) {
        $desde = now();
    }

    if ($desde->greaterThan($hasta)) {
        $desde = $hasta;
    }

    $total_dias = $desde->diffInDaysFiltered( function ($date) {
        return !$date->isDayOfWeek('Sunday');
    }, $hasta);

    $intervalo_total_en_segundos *= ($total_dias + 1);

    if (
        $desde->format('d-m-Y') == now()->format('d-m-Y') ||
        $hasta->format('d-m-Y') == now()->format('d-m-Y')
    ) {

        $time_today = Carbon::createFromFormat('H:i:s', $endDay)->diffInSeconds(now());

        $intervalo_total_en_segundos -= $time_today;

    }


    $logs_produccion = collect();

    foreach($workcenter->processes as $process) {

        foreach ($process->process_log as $log) {

            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'ih' && $log->child_log->activity == 'fh') ||
                        ($log->activity == 'ih' && $log->child_log->activity == 'crp')
                    )
                )
                    ||
                (
                    !$log->child_log && $log->activity == 'ih'
                )
            ) {
                if ($log->child_log) {

                    $herramentaje += $log->created_at->diffInSeconds($log->child_log->created_at) - meal_time_calc_to_remove($log, $log->child_log);


                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {

                        $herramentaje += $log->created_at->diffInSeconds(now());
                    }
                }

            }

            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'ia' && $log->child_log->activity == 'fa') ||
                        ($log->activity == 'ia' && $log->child_log->activity == 'crp')
                    )
                )
                    ||
                (
                    !$log->child_log && $log->activity == 'ia'
                )
            ) {
                if ($log->child_log) {
                    $ajuste += $log->created_at->diffInSeconds($log->child_log->created_at) - meal_time_calc_to_remove($log, $log->child_log);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $ajuste += $log->created_at->diffInSeconds(now());
                    }
                }
            }

            if (
                (
                    $log->activity == 'pr' && !$log->child_log
                )
                    ||
                $log->child_log &&
                (
                    ($log->child_log->activity == 'crp' && $log->activity == 'pr') ||
                    ($log->child_log->activity == 'cr' && $log->activity == 'pr')
                )
            ) {

                if ( $log->parent_log && $log->activity == 'pr' && in_array($log->parent_log->activity, ['pfm', 'pfh']) ) {

                    $real_prod_start = $log->specificLog('pr');
                    $tiempo_total_produccion = $log->child_log->created_at->diffInSeconds($real_prod_start->created_at);

                } else {

                    if ($log->child_log) {
                        $tiempo_total_produccion = $log->created_at->diffInSeconds($log->child_log->created_at) - meal_time_calc_to_remove($log, $log->child_log);
                    } else {
                        if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                            $tiempo_total_produccion += $log->created_at->diffInSeconds(now());
                        }
                    }

                }

                if ($log->user_process) {


                    if ($log->child_log) {
                        $logs_produccion->push( $log );
                    }

                    $total_cycles = 0;

                    $cycles = $log->user_process->cycles;

                    for ( $i = 0 ; $i < $cycles->count() ; $i++ ) {

                        if ( $cycles[$i]->message == 'START' && ( isset( $cycles[$i + 1] ) && $cycles[$i+1]->message == '12345' )) {

                            $total_cycles++;

                        }

                    }

                    if ($total_cycles != 0 && $log->process->operation->standar_time != 0) {

                        $eficiencia->push(
                            [
                                'eficiencia' => (($log->process->operation->standar_time * 60) * $total_cycles ) / $tiempo_total_produccion,
                                'tiempo' => $tiempo_total_produccion
                            ]
                        );

                    } else {

                        $eficiencia->push( 0 );

                    }

                }
            }


            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'pfm' && $log->child_log->activity == 'pr') ||
                        ($log->activity == 'pfh' && $log->child_log->activity == 'pr')
                    )
                )
                    ||
                (
                    !$log->child_log && ($log->activity == 'pfm'  || $log->activity == 'pfh')
                )
            ) {
                if ($log->child_log) {
                    $paros += $log->created_at->diffInSeconds($log->child_log->created_at) - meal_time_calc_to_remove($log, $log->child_log);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $paros += $log->created_at->diffInSeconds(now());
                    }
                }
            }

            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'pm' && $log->child_log->activity == 'pr')
                    )
                )
                    ||
                (
                    !$log->child_log && $log->activity == 'pm'
                )
            ) {
                if ($log->child_log) {
                    $mantenimiento += $log->created_at->diffInSeconds($log->child_log->created_at) - meal_time_calc_to_remove($log, $log->child_log);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $mantenimiento += $log->created_at->diffInSeconds(now());
                    }
                }
            }

        }

    }

    $total = $eficiencia->sum('tiempo');


    $eficiencia = $eficiencia->map(function ($el) use ($total)
    {
        if (!isset($el['tiempo'])) {
            return [
                'porcentaje_tiempo' => 0,
                'porcentaje_eficiencia' => 0
            ];
        }
        if ($total != 0) {
            $el['porcentaje_tiempo'] = $el['tiempo'] / $total;
        } else {
            $el['porcentaje_tiempo'] = 0;
        }
        $el['porcentaje_eficiencia'] = $el['porcentaje_tiempo'] * ($el['eficiencia']);
        return $el;
    });

    $sinuso = $intervalo_total_en_segundos - ($herramentaje + $ajuste + $tiempo_total_produccion + $paros + $mantenimiento);


    $sinuso = round(($sinuso * 100) / $intervalo_total_en_segundos, 2);
    $herramentaje = round(($herramentaje * 100) / $intervalo_total_en_segundos, 2);
    $ajuste = round(($ajuste * 100) / $intervalo_total_en_segundos, 2);
    $produccion = round(($tiempo_total_produccion * 100) / $intervalo_total_en_segundos, 2);
    $paros = round(($paros * 100) / $intervalo_total_en_segundos, 2);
    $mantenimiento = round(($mantenimiento * 100) / $intervalo_total_en_segundos, 2);
    $eficiencia = round($eficiencia->avg(), 1);
    $eficiencia_ponderada = round($ponderada->avg() * $eficiencia, 1);

    $results = collect(compact(
        'sinuso',
        'eficiencia',
        'eficiencia_ponderada',
        'herramentaje',
        'ajuste',
        'produccion',
        'paros',
        'mantenimiento'
    ));

    return $results;

}
function getTimeMaquinas(Workcenter $wc,Carbon $dateTime=null)
{
	if (is_null($dateTime)) {
		$dateTime=Carbon::now();
	}

    $first_turn_start_day=new Carbon(date('Y-m-d')." 07:00:00");
	$first_turn_end_day=new Carbon(date('Y-m-d')." 14:59:00");
	$second_turn_start_day=new Carbon(date('Y-m-d')." 15:00:00");
	$second_turn_end_day=new Carbon(date('Y-m-d')." 23:00:00");
	$day_tecmaq=7.5*60*60; // 7 y 30 horas en segundos
	$days=1;
	$sinuso=$produccion=$ajuste=$herramentaje=$liberacion=$autoliberacion=0;
	$mantenimiento=$pausas_otros=0;
	$sinuso_total=$day_tecmaq*$days;

	$sinuso=0;
	$last_day= $first_turn_start_day;
    $last_day2=$first_turn_end_day;



	$wc_setup = DB::table('setup')
		->select('setup.*','p.status','p.ih_created_at as ih_created','p.fh_created_at as fh_created'
			,'p.ia_created_at as ia_created','p.fa_created_at as fa_created')
		->join("processes as p","p.id","=","setup.process_id")
		->join("workcenters as wc","wc.id","=","setup.workcenter_id")
		->whereBetween('setup.ih_created_at',[$last_day,$dateTime->toDateTimeString()])
		->whereNotNull('setup.ih_created_at')
		->where('setup.workcenter_id',$wc->id)
		// ->whereIn('p.status',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])
		->get();
	$wc_prod=UserProcess::whereBetween('start_time',[$last_day,$dateTime->toDateTimeString()])
		->select('user_processes.*','p.workcenter_id')
		->join('processes as p','p.id','=','user_processes.process_id')
		->where('p.workcenter_id',$wc->id)
		->where(function($q_up){
			$q_up->whereIn('status_start',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp']);
		})
		->get();
	$pausas=Pause::whereBetween('started_at',[$last_day,$dateTime->toDateTimeString()])
		->where('workcenter_id',$wc->id)
		->get();
	// dd($wc_prod->toArray());
	$sinuso_day=$produccion_day=$ajuste_day=$herramentaje_day=$liberacion_day=$autoliberacion_day=0;
	$mantenimiento_day=$pausas_otros_day=0;
	$data=$wc_setup->whereBetween('ih_created_at',[$last_day,$dateTime->toDateTimeString()]);
	// SETUP
	// DB::beginTransaction();
	foreach ($data as $set) {
		if(!is_null($set->ih_created_at)){
			$fh_created_at=null;
			if (!is_null($set->fh_created_at)) {
				$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fh_created_at);
			}elseif (is_null($set->fh_created_at) && !is_null($set->ia_created_at)) {
				$set->fh_created_at=$set->ia_created_at;
				// DB::table('setup')->where('id',$set->id)
				// 	->update(['fh_created_at'=>$set->ia_created_at]);
			}elseif (is_null($set->fh_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at)->addHours(2);
			}
			$ih_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at);
			$herramentaje_day+=fechas_tecmaq_sub_mealtime($ih_created_at,$fh_created_at);
		}
	}
	$data=$wc_setup->whereBetween('ia_created_at',[$last_day,$dateTime->toDateTimeString()]);
	foreach ($data as $set) {
		if(!is_null($set->ia_created_at)){
			$fa_created_at=null;
			if (!is_null($set->fa_created_at)) {
				$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fa_created_at);
			}elseif (is_null($set->fa_created_at) && (!is_null($set->il_created_at) || !is_null($set->ial_created_at) )) {
				$set->fa_created_at=is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at;
				// DB::table('setup')->where('id',$set->id)
				// 	->update(['fa_created_at'=>is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at]);
			}elseif (is_null($set->fa_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at)->addHours(2);
			}
			$ia_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at);
			$ajuste_day+=fechas_tecmaq_sub_mealtime($ia_created_at,$fa_created_at);
		}
	}
	$data=$wc_setup->whereBetween('il_created_at',[$last_day,$dateTime->toDateTimeString()]);
	foreach ($data as $set) {
		if(!is_null($set->il_created_at)){
			$ls_created_at=null;
			if (!is_null($set->ls_created_at)) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
			}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at)->addHours(2);
			}
			$il_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at);
			$liberacion_day+=fechas_tecmaq_sub_mealtime($il_created_at,$ls_created_at);
		}
	}
	$data=$wc_setup->whereBetween('ial_created_at',[$last_day,$dateTime->toDateTimeString()]);
	foreach ($data as $set) {
		if(!is_null($set->ial_created_at)){
			$ls_created_at=null;
			if (!is_null($set->ls_created_at)) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
			}elseif (is_null($set->ls_created_at) && isset($set->pr_created_at) && !is_null($set->pr_created_at)) {
				$set->ls_created_at=$set->pr_created_at;
				// DB::table('setup')->where('id',$set->id)
				// 	->update(['ls_created_at'=>$set->pr_created_at]);
			}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at)->addHours(2);
			}
			$ial_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at);
			$autoliberacion_day+=fechas_tecmaq_sub_mealtime($ial_created_at,$ls_created_at);
		}
	}
	/*PRoducciion*/
	$data_prod=$wc_prod->whereBetween('start_time',[$last_day,$dateTime->toDateTimeString()]);
	foreach ($data_prod as $prod) {
		if (!is_null($prod->start_time)) {
			if (!is_null($prod->end_time)) {
				$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->end_time);
			}else
				$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$dateTime);

			// if (is_null($prod->end_time) && ($prod->status_end!='cr' && !is_null($prod->status_end))) {
			// 	$end_time=Carbon::now();
			// }elseif(is_null($prod->end_time) && $prod->status_end=='cr'){
			// 	$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time)->addHours(2);
			// }
			$start_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time);

			$produccion_day+=fechas_tecmaq_sub_mealtime($start_time,$end_time);
		}
	}
	/*Pausas*/
	$data=$pausas->where('motivo','mantenimiento');
	foreach ($data as $pause) {
		if (!is_null($pause->started_at)) {
			$started_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->started_at);
			$ended_at=null;
			if (!is_null($pause->ended_at)) {
				$ended_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->ended_at);
			}
			$mantenimiento_day+=fechas_tecmaq_sub_mealtime($started_at,$ended_at);
		}
	}
	$data=$pausas->where('motivo','!=','mantenimiento');
	foreach ($data as $pause) {
		if (!is_null($pause->started_at)) {
			$started_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->started_at);
			$ended_at=null;
			if (!is_null($pause->ended_at)) {
				$ended_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->ended_at);
			}
			$pausas_otros_day+=fechas_tecmaq_sub_mealtime($started_at,$ended_at);
		}
	}

	$setup_day=$ajuste_day+$herramentaje_day+$liberacion_day+$autoliberacion_day;
	$pausas_day=$mantenimiento_day+$pausas_otros_day;

	// if (($setup_day+$produccion_day+$pausas_day)<=$sinuso_total) {
	// 	$sinuso=$sinuso-$setup_day-$produccion_day-$pausas_day;
	// }


	$ajuste+=$ajuste_day;
	$herramentaje+=$herramentaje_day;
	$liberacion+=$liberacion_day;
	$autoliberacion+=$autoliberacion_day;
	$produccion+=$produccion_day;
	$mantenimiento+=$mantenimiento_day;
	$pausas_otros+=$pausas_otros_day;
	// DB::commit();
	$setup=$ajuste+$herramentaje+$liberacion+$autoliberacion;
	if (($setup+$produccion+$mantenimiento+$pausas_otros)<=$sinuso_total) {
		$sinuso=$sinuso_total-($setup+$produccion+$mantenimiento+$pausas_otros);
	}
	return collect([
		'ajuste'=>$ajuste,
		'herramentaje'=>$herramentaje,
		'liberacion'=>$liberacion,
		'autoliberacion'=>$autoliberacion,
		'produccion'=>$produccion,
		'mantenimiento'=>$mantenimiento,
		'pausa_otros'=>$pausas_otros,
		'sinuso'=>$sinuso,

		'sinuso_total'=>$sinuso_total,
		'day_tecmaq'=>$day_tecmaq,
		'setup'=>$setup,
	]);
}
function getTimeSemanalMaquinas(Workcenter $wc,Carbon $dateTime=null)
{
	if (is_null($dateTime)) {
		$dateTime=Carbon::now();
	}
	$days=6;
	$sinuso=$produccion=$ajuste=$herramentaje=$liberacion=$autoliberacion=0;
	$mantenimiento=$pausas_otros=0;

	$day_tecmaq=15*60*60; // 15 horas en segundos

	$sinuso_total=$day_tecmaq*$days;

	$sinuso=0;

	$last_day=Carbon::createFromFormat('Y-m-d H:i:s',$dateTime)->startOfWeek();
	$last_day2=Carbon::createFromFormat('Y-m-d H:i:s',$dateTime)->endOfWeek()->subDay();

	$wc_setup = DB::table('setup')
		->select('setup.*','p.status','p.ih_created_at as ih_created','p.fh_created_at as fh_created'
			,'p.ia_created_at as ia_created','p.fa_created_at as fa_created')
		->join("processes as p","p.id","=","setup.process_id")
		->join("workcenters as wc","wc.id","=","setup.workcenter_id")
		->whereBetween('setup.ih_created_at',[$last_day,$last_day2])
		->whereNotNull('setup.ih_created_at')
		->where('setup.workcenter_id',$wc->id)
		// ->whereIn('p.status',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])
		->get();
	$wc_prod=UserProcess::whereBetween('start_time',[$last_day,$last_day2])
		->select('user_processes.*','p.workcenter_id')
		->join('processes as p','p.id','=','user_processes.process_id')
		->where('p.workcenter_id',$wc->id)
		->where(function($q_up){
			$q_up->whereIn('status_start',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp']);
		})
		->get();
	$pausas=Pause::whereBetween('started_at',[$last_day,$last_day2])
		->where('workcenter_id',$wc->id)
		->get();
	// dd($wc_prod->toArray());
	$sinuso_day=$produccion_day=$ajuste_day=$herramentaje_day=$liberacion_day=$autoliberacion_day=0;
	$mantenimiento_day=$pausas_otros_day=0;
	$data=$wc_setup->whereBetween('ih_created_at',[$last_day,$last_day2]);
	// SETUP
	// DB::beginTransaction();
	$i=0;
	foreach ($data as $set) {
		if(!is_null($set->ih_created_at)){
			$fh_created_at=null;
			if (!is_null($set->fh_created_at)) {
				$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fh_created_at);
			}elseif (is_null($set->fh_created_at) && !is_null($set->ia_created_at)) {
				$set->fh_created_at=$set->ia_created_at;
				// DB::table('setup')->where('id',$set->id)
				// 	->update(['fh_created_at'=>$set->ia_created_at]);
			}elseif (is_null($set->fh_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at)->addHours(2);
			}
			$ih_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at);
			$herramentaje_day+=fechas_tecmaq_sub_mealtime($ih_created_at,$fh_created_at);
			// $i==2?dd($herramentaje_day,$ih_created_at,$fh_created_at,fechas_tecmaq_sub_mealtime($ih_created_at,$fh_created_at)):'';
			$i++;
		}
	}
	$data=$wc_setup->whereBetween('ia_created_at',[$last_day,$last_day2]);
	foreach ($data as $set) {
		if(!is_null($set->ia_created_at)){
			$fa_created_at=null;
			if (!is_null($set->fa_created_at)) {
				$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fa_created_at);
			}elseif (is_null($set->fa_created_at) && (!is_null($set->il_created_at) || !is_null($set->ial_created_at) )) {
				$set->fa_created_at=is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at;
				// DB::table('setup')->where('id',$set->id)
				// 	->update(['fa_created_at'=>is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at]);
			}elseif (is_null($set->fa_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at)->addHours(1);
			}
			$ia_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at);
			$ajuste_day+=fechas_tecmaq_sub_mealtime($ia_created_at,$fa_created_at);
		}
	}
	$data=$wc_setup->whereBetween('il_created_at',[$last_day,$last_day2]);
	foreach ($data as $set) {
		if(!is_null($set->il_created_at)){
			$ls_created_at=null;
			if (!is_null($set->ls_created_at)) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
			}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at)->addHours(1);
			}
			$il_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at);
			$liberacion_day+=fechas_tecmaq_sub_mealtime($il_created_at,$ls_created_at);
		}
	}
	$data=$wc_setup->whereBetween('ial_created_at',[$last_day,$last_day2]);
	foreach ($data as $set) {
		if(!is_null($set->ial_created_at)){
			$ls_created_at=null;
			if (!is_null($set->ls_created_at)) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
			}elseif (is_null($set->ls_created_at) && isset($set->pr_created_at) && !is_null($set->pr_created_at)) {
				$set->ls_created_at=$set->pr_created_at;
				// DB::table('setup')->where('id',$set->id)
				// 	->update(['ls_created_at'=>$set->pr_created_at]);
			}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
				$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at)->addHours(1);
			}
			$ial_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at);
			$autoliberacion_day+=fechas_tecmaq_sub_mealtime($ial_created_at,$ls_created_at);
		}
	}
	/*PRoducciion*/
	$data_prod=$wc_prod->whereBetween('start_time',[$last_day,$last_day2]);
	foreach ($data_prod as $prod) {
		if (!is_null($prod->start_time)) {
			if (!is_null($prod->end_time)) {
				$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->end_time);
			}else
				$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$dateTime);

			// if (is_null($prod->end_time) && ($prod->status_end!='cr' && !is_null($prod->status_end))) {
			// 	$end_time=Carbon::now();
			// }elseif(is_null($prod->end_time) && $prod->status_end=='cr'){
			// 	$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time)->addHours(2);
			// }
			$start_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time);

			$produccion_day+=fechas_tecmaq_sub_mealtime($start_time,$end_time);
		}
	}
	/*Pausas*/
	$data=$pausas->where('motivo','mantenimiento');
	foreach ($data as $pause) {
		if (!is_null($pause->started_at)) {
			$started_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->started_at);
			$ended_at=null;
			if (!is_null($pause->ended_at)) {
				$ended_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->ended_at);
			}
			$mantenimiento_day+=fechas_tecmaq_sub_mealtime($started_at,$ended_at);
		}
	}
	$data=$pausas->where('motivo','!=','mantenimiento');
	foreach ($data as $pause) {
		if (!is_null($pause->started_at)) {
			$started_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->started_at);
			$ended_at=null;
			if (!is_null($pause->ended_at)) {
				$ended_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->ended_at);
			}
			$pausas_otros_day+=fechas_tecmaq_sub_mealtime($started_at,$ended_at);
		}
	}

	$setup_day=$ajuste_day+$herramentaje_day+$liberacion_day+$autoliberacion_day;
	$pausas_day=$mantenimiento_day+$pausas_otros_day;

	// if (($setup_day+$produccion_day+$pausas_day)<=$sinuso_total) {
	// 	$sinuso=$sinuso-$setup_day-$produccion_day-$pausas_day;
	// }


	$ajuste+=$ajuste_day;
	$herramentaje+=$herramentaje_day;
	$liberacion+=$liberacion_day;
	$autoliberacion+=$autoliberacion_day;
	$produccion+=$produccion_day;
	$mantenimiento+=$mantenimiento_day;
	$pausas_otros+=$pausas_otros_day;
	// DB::commit();
	$setup=$ajuste+$herramentaje+$liberacion+$autoliberacion;
	if (($setup+$produccion+$mantenimiento+$pausas_otros)<=$sinuso_total) {
		$sinuso=$sinuso_total-($setup+$produccion+$mantenimiento+$pausas_otros);
	}
	return collect([
		'ajuste'=>$ajuste,
		'herramentaje'=>$herramentaje,
		'liberacion'=>$liberacion,
		'autoliberacion'=>$autoliberacion,
		'produccion'=>$produccion,
		'mantenimiento'=>$mantenimiento,
		'pausa_otros'=>$pausas_otros,
		'sinuso'=>$sinuso,

		'sinuso_total'=>$sinuso_total,
		'day_tecmaq'=>$day_tecmaq,
		'setup'=>$setup,
		'start_day'=>$last_day,
		'end_day'=>$last_day2,
	]);
}
function getAllTimeToDB()
{
	$times = new Collection();
	$workcenter_section="S";
	$workcenters= WorkCenter::whereNotNull('ip_address')
		->orderBy('id','ASC')
		// ->limit(1)
		->get();

	$user_process = UserProcess::select("created_at")->orderBy('created_at','ASC')->limit(1)->first();
	if (is_null($user_process)) return json_encode(['error']);
	$limit = $user_process->created_at->startOfDay();
	// $limit=Carbon::now()->subDay()->subMonths(1);
	$machine_days=new Collection();
	foreach ($workcenters as $wc) {
		echo "inciando: ".$wc->num_machine."....";
		$day_start=Carbon::now()->subDay()->startOfDay();
		while ( $limit->lessThanOrEqualTo($day_start)) {
			if ($day_start->dayOfWeek===Carbon::SUNDAY) {
				$day_start->startOfDay()->subday();
				continue;
			}

			$machine_day=MachineDay::where('day',$day_start)
				->where('workcenter_id',$wc->id)
				// ->limit(1)
				->get();
			if ($machine_day->isNotEmpty()) {
				echo "\t Ya existe un registro del dia ".$day_start->toDateString()." del ".$wc->num_machine."\n";
			}else{
				$machine_day=$machine_day->first();
				$times=getTimeMaquinas($wc,$day_start);
				$times->put('workcenter_id',$wc->id);
				$times->put('day',$day_start->toDateString());

				$machine_day=MachineDay::create($times->toArray());
				$machine_days->push( $machine_day );
			}

			$day_start->subDay();
		}
		echo "OK..\n";
	}
	return $machine_days->toJson();
}
function getDayTimeToDB()
{
	$times = new Collection();
	$workcenter_section="S";
	$workcenters= WorkCenter::whereNotNull('ip_address')
		->orderBy('id','ASC')
		// ->limit(1)
		->get();

	$user_process = UserProcess::select("created_at")->orderBy('created_at','ASC')->limit(1)->first();
	if (is_null($user_process)) return json_encode(['error']);
	$machine_days=new Collection();
	foreach ($workcenters as $wc) {
		echo "inciando: ".$wc->num_machine."....";
		$day_start=Carbon::now()->startOfDay();
		if ($day_start->dayOfWeek===Carbon::SUNDAY) {
			$day_start->startOfDay()->subday();
			echo "\n";
			continue;
		}

		$machine_day=MachineDay::where('day',$day_start)
			->where('workcenter_id',$wc->id)
			// ->limit(1)
			->get();
		if ($machine_day->isNotEmpty()) {
			echo "\n\t Ya existe un registro del dia ".$day_start->toDateString()." del ".$wc->num_machine."\n";
		}else{
			$machine_day=$machine_day->first();
			$times=getTimeMaquinas($wc,$day_start);
			$times->put('workcenter_id',$wc->id);
			$times->put('day',$day_start->toDateString());

			$machine_day=MachineDay::create($times->toArray());
			$machine_days->push( $machine_day );
		}
		echo "Finalizando ".$wc->num_machine."....\n\n";
	}
	return $machine_days->toJson();
}
function meal_time_calc_to_remove(ProcessLog $process_log_start, ProcessLog $process_log_end)
{

    $meal_time_start = Carbon::createFromFormat('d-m-Y H:i:s', $process_log_start->created_at->format('d-m-Y') . ' ' . $process_log_start->user->mealtime);
    $meal_time_end = Carbon::createFromFormat('d-m-Y H:i:s', $process_log_start->created_at->format('d-m-Y') . ' ' . $process_log_start->user->mealtime)->addMinutes(30);

    $meal_time_start2 = Carbon::createFromFormat('d-m-Y H:i:s', $process_log_start->created_at->format('d-m-Y') . ' ' . $process_log_start->user->mealtime2);
    $meal_time_end2 = Carbon::createFromFormat('d-m-Y H:i:s', $process_log_start->created_at->format('d-m-Y') . ' ' . $process_log_start->user->mealtime2)->addMinutes(30);

    $tiempo = 0;

    if ($process_log_start->created_at->lte($meal_time_start) && $process_log_end->created_at->gte($meal_time_end)) {
        if ($process_log_end->created_at->lte($meal_time_end)) {
            $tiempo = $meal_time_start->diffInSeconds($process_log_end->created_at);
        }else{
            $tiempo = (30*60);
        }
    } elseif ($process_log_start->created_at->between($meal_time_start, $meal_time_end)) {

        $tiempo = $process_log_start->created_at->diffInSeconds($meal_time_end);

    }

    if ($process_log_start->created_at->lte($meal_time_start2) && $process_log_end->created_at->gte($meal_time_end2)) {
        if ($process_log_end->created_at->lte($meal_time_end2)) {
            $tiempo = $meal_time_start2->diffInSeconds($process_log_end->created_at);
        }else{
            $tiempo = (30*60);
        }
    } elseif ($process_log_start->created_at->between($meal_time_start2, $meal_time_end2)) {

        $tiempo = $process_log_start->created_at->diffInSeconds($meal_time_end2);

    }

    return $tiempo;

}
