<?php

use App\Process;
use Illuminate\Support\Carbon;

function getTime($startDate,$endDate,$prod=false,$dd=false){

	$day=24;
	$day_tecmaq=15; # 15 horas laborales
	$hours=60;
	$minutes=60;
	$seconds=60;
	$horas_no_laboral=(($day-$day_tecmaq)*$minutes*$seconds);
	$tiempo_en_segundos=0;

	if ( !is_null($startDate) ) {
		$end_day_tecmaq=Carbon::createFromFormat('Y-m-d H:i:s',$startDate->format('Y-m-d')." 23:30:00");
		$start_day_tecmaq=Carbon::createFromFormat('Y-m-d H:i:s',$startDate->format('Y-m-d')." 07:30:00");
		$turno_tecmaq=Carbon::createFromFormat('Y-m-d H:i:s',$startDate->format('Y-m-d')." 15:30:00");

		if (is_null($endDate))  {
			if ($prod) {
				if ($startDate->gt($start_day_tecmaq) && $startDate->lt($turno_tecmaq)) {
					$endDate=Carbon::createFromFormat('Y-m-d H:i:s',$turno_tecmaq->format('Y-m-d H:i:s') );
				}else{
					$endDate=Carbon::createFromFormat('Y-m-d H:i:s',$end_day_tecmaq->format('Y-m-d H:i:s') );
				}
			}else $endDate=Carbon::now();

		}

		if ($endDate->greaterThan( $end_day_tecmaq )) {
			// seteo el dia al inicio 00:00:00
			$inicio_process=Carbon::createFromFormat('Y-m-d H:i:s',$startDate)->startOfDay();
			$fin_operacion=Carbon::createFromFormat('Y-m-d H:i:s',$endDate)->startOfDay();

			$diff_day = $inicio_process->diffInDays( $fin_operacion );
			if($dd) dd($startDate,$endDate,$diff_day);
			$diff_second=$startDate->diffInSeconds( $endDate );

			$tiempo_en_segundos+=$diff_second-($diff_day*$horas_no_laboral);
			// dd( $startDate,$endDate, $diff_day,$diff_second,$tiempo_en_segundos,$horas_no_laboral);
		}
	}
	return $tiempo_en_segundos;
}

function mes_esp($mes=null)
{
	if (is_null($mes)) {
		$mes=intval(date('m'));
	}
    $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    return $meses[$mes-1];
}

function mes_esp2($mes=null)
{
	if (is_null($mes)) {
		$mes=intval(date('m'));
	}
    $meses = ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"];
    return $meses[$mes-1];
}

function dia_esp($dia=null)
{
	if (is_null($dia)) {
		$dia=Carbon::now()->format('l');
	}
    $dias = ["Monday"=>"lunes","Tuesday"=>"martes","Wednesday"=>"miércoles","Thursday"=>"jueves","Friday"=>"viernes","Saturday"=>"sábado","Sunday"=>"Domingo"];
    return $dias[$dia];
}
/**
 * @param Process
 *
 * @return null|int
 */
function parent_id(Process $process)
{
    $parent = $process->process_log()->orderBy('id', 'DESC')->first();

    if ($parent) {
        return $parent->id;
    }
    return null;
}
