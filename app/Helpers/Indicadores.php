<?php

use App\Pause;
use App\Process;
use App\Workcenter;
use App\UserProcess;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


function obtenerTiempoIndicadores(Workcenter $workcenter, Carbon $desde, Carbon $hasta)
{
    // Primero se define el total de tiempo que una máquina está operativa

    $startDay = '06:45:00';
    $endDay = '23:00:00';

    $intervalo_total_en_segundos = Carbon::createFromFormat('H:i:s', $startDay)->diffInSeconds(Carbon::createFromFormat('H:i:s', $endDay));
    $segundos_en_un_dia = $intervalo_total_en_segundos;

    // Variables finales para ser calculadas durante el desarrollo del algoritmo
    $sinuso = $produccion = $ajuste = $herramentaje = $liberacion = $autoliberacion = $mantenimiento = $paros = $tiempo_total_produccion = 0;
    $eficiencia = collect();
    $ponderada = collect();

    // Parseado
    $desde = Carbon::parse($desde);
    $hasta = Carbon::parse($hasta);

    if ($hasta->greaterThan(now())) {
        $hasta = now();
    }

    if ($desde->greaterThan(now())) {
        $desde = now();
    }

    if ($desde->greaterThan($hasta)) {
        $desde = $hasta;
    }

    $total_dias = $desde->diffInDaysFiltered( function ($date) {
        return !$date->isDayOfWeek('Sunday');
    }, $hasta);

    $intervalo_total_en_segundos *= ($total_dias + 1);

    if (
        $desde->format('d-m-Y') == now()->format('d-m-Y') ||
        $hasta->format('d-m-Y') == now()->format('d-m-Y')
    ) {

        $time_today = Carbon::createFromFormat('H:i:s', $endDay)->diffInSeconds(now());

        $intervalo_total_en_segundos -= $time_today;

    }


    $logs_produccion = collect();

    foreach($workcenter->processes as $process) {

        foreach ($process->process_log as $log) {



            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'ih' && $log->child_log->activity == 'fh') ||
                        ($log->activity == 'ih' && $log->child_log->activity == 'crp')
                    )
                )
                    ||
                (
                    !$log->child_log && $log->activity == 'ih'
                )
            ) {
                if ($log->child_log) {

                    $herramentaje += $log->created_at->diffInSeconds($log->child_log->created_at);


                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {

                        $herramentaje += $log->created_at->diffInSeconds(now());
                    }
                }

            }

            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'ia' && $log->child_log->activity == 'fa') ||
                        ($log->activity == 'ia' && $log->child_log->activity == 'crp')
                    )
                )
                    ||
                (
                    !$log->child_log && $log->activity == 'ia'
                )
            ) {
                if ($log->child_log) {
                    $ajuste += $log->created_at->diffInSeconds($log->child_log->created_at);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $ajuste += $log->created_at->diffInSeconds(now());
                    }
                }
            }

            if (
                (
                    $log->activity == 'pr' && !$log->child_log
                )
                    ||
                $log->child_log &&
                (
                    ($log->child_log->activity == 'crp' && $log->activity == 'pr') ||
                    ($log->child_log->activity == 'cr' && $log->activity == 'pr')
                )
            ) {

                if ($log->child_log) {
                    $tiempo_total_produccion += $log->created_at->diffInSeconds($log->child_log->created_at);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $tiempo_total_produccion += $log->created_at->diffInSeconds(now());
                    }
                }

                if ($log->user_process) {


                    if ($log->child_log) {
                        $logs_produccion->push( $log );
                    }


                    $total_cycles = 0;

                    $cycles = $log->user_process->cycles;

                    for ( $i = 0 ; $i < $cycles->count() ; $i++ ) {

                        if ( $cycles[$i]->message == 'START' && ( isset( $cycles[$i + 1] ) && $cycles[$i+1]->message == '12345' )) {

                            $total_cycles = $cycles[$i]->message_date->diffInSeconds( $cycles[$i + 1]->message_date );

                        }

                    }

                    if ($total_cycles != 0 && $log->process->operation->standar_time != 0) {

                        $eficiencia->push( $total_cycles / (floatval($log->process->operation->standar_time * 60)) );

                    } else {

                        $eficiencia->push( 0 );

                    }

                }
            }

            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'pfm' && $log->child_log->activity == 'pr') ||
                        ($log->activity == 'pfh' && $log->child_log->activity == 'pr')
                    )
                )
                    ||
                (
                    !$log->child_log && ($log->activity == 'pfm'  || $log->activity == 'pfh')
                )
            ) {
                if ($log->child_log) {
                    $paros += $log->created_at->diffInSeconds($log->child_log->created_at);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $paros += $log->created_at->diffInSeconds(now());
                    }
                }
            }

            if (
                (
                    $log->child_log &&
                    (
                        ($log->activity == 'pm' && $log->child_log->activity == 'pr')
                    )
                )
                    ||
                (
                    !$log->child_log && $log->activity == 'pm'
                )
            ) {
                if ($log->child_log) {
                    $mantenimiento += $log->created_at->diffInSeconds($log->child_log->created_at);
                } else {
                    if ($log->created_at->format('d-m-Y') == now()->format('d-m-Y')) {
                        $mantenimiento += $log->created_at->diffInSeconds(now());
                    }
                }
            }

        }

    }

    $sinuso = $intervalo_total_en_segundos - ($herramentaje + $ajuste + $tiempo_total_produccion + $paros + $mantenimiento);

    $sinuso         = floor($sinuso / 3600);
    $herramentaje   = floor($herramentaje / 3600);
    $ajuste         = floor($ajuste / 3600);
    $produccion     = floor($tiempo_total_produccion / 3600);
    $paros          = floor($paros / 3600);
    $mantenimiento  = floor($mantenimiento / 3600);

    return collect(compact(
        'sinuso',
        'herramentaje',
        'ajuste',
        'produccion',
        'paros',
        'mantenimiento'
    ));

}

function getTimeIndicadores(Carbon $dateTimeEnd,Carbon $dateTimeStart)
{
	$wcs=Workcenter::all();
	// $wcs=Workcenter::where('id',8)->get();
	$sinuso=$produccion=$ajuste=$herramentaje=$liberacion=$autoliberacion=0;
	$day_tecmaq=15*60*60; // 15 horas en segundos
	foreach ($wcs as $wc) {
		$sinuso_day=$produccion_day=$ajuste_day=$herramentaje_day=$liberacion_day=$autoliberacion_day=0;
		$now=Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeStart)->endOfDay();
		$last_day=Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeEnd)->startOfDay();
		$last_day2=Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeEnd)->endOfDay();

		$wc_setup = DB::table('setup')
			->select('setup.*','p.status','p.ih_created_at as ih_created','p.fh_created_at as fh_created'
				,'p.ia_created_at as ia_created','p.fa_created_at as fa_created')
			->join("processes as p","p.id","=","setup.process_id")
			->join("workcenters as wc","wc.id","=","setup.workcenter_id")
			->where('wc.id',$wc->id)
			->whereBetween('setup.ih_created_at',[$last_day,$now])
			->whereNotNull('setup.ih_created_at')
			->whereIn('p.status',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])
			->get();
		$wc_prod=UserProcess::whereBetween('start_time',[$last_day,$now])
			->where(function($q_up){
				$q_up->whereIn('status_start',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp']);
			})
			->get();
		// dd($wc_prod->toArray());
		while ($last_day->lessThanOrEqualTo($now)) {
			$data=$wc_setup->whereBetween('ih_created_at',[$last_day,$last_day2]);
			// SETUP
			DB::beginTransaction();
			foreach ($data as $set) {
				if(!is_null($set->ih_created_at)){
					if (!is_null($set->fh_created_at)) {
						$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fh_created_at);
					}elseif (is_null($set->fh_created_at) && !is_null($set->ia_created_at)) {
						$set->fh_created_at=$set->ia_created_at;
						DB::table('setup')->where('id',$set->id)
							->update(['fh_created_at'=>$set->ia_created_at]);
					}elseif (is_null($set->fh_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at)->addHours(2);
					}
					$ih_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at);
					$herramentaje_day+=fechas_tecmaq($ih_created_at,$fh_created_at);
				}
			}
			$data=$wc_setup->whereBetween('ia_created_at',[$last_day,$last_day2]);
			foreach ($data as $set) {
				if(!is_null($set->ia_created_at)){
					if (!is_null($set->fa_created_at)) {
						$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fa_created_at);
					}elseif (is_null($set->fa_created_at) && (!is_null($set->il_created_at) || !is_null($set->ial_created_at) )) {
						$set->fa_created_at=is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at;
						DB::table('setup')->where('id',$set->id)
							->update(['fa_created_at'=>is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at]);
					}elseif (is_null($set->fa_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at)->addHours(2);
					}
					$ia_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at);
					$ajuste_day+=fechas_tecmaq($ia_created_at,$fa_created_at);
				}
			}
			$data=$wc_setup->whereBetween('il_created_at',[$last_day,$last_day2]);
			foreach ($data as $set) {
				if(!is_null($set->il_created_at)){
					if (!is_null($set->ls_created_at)) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
					}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at)->addHours(2);
					}
					$il_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at);
					$liberacion_day+=fechas_tecmaq($il_created_at,$ls_created_at);
				}
			}
			$data=$wc_setup->whereBetween('ial_created_at',[$last_day,$last_day2]);
			foreach ($data as $set) {
				if(!is_null($set->ial_created_at)){
					if (!is_null($set->ls_created_at)) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
					}elseif (is_null($set->ls_created_at) && !is_null($set->pr_created_at)) {
						$set->ls_created_at=$set->pr_created_at;
						DB::table('setup')->where('id',$set->id)
							->update(['ls_created_at'=>$set->pr_created_at]);
					}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at)->addHours(2);
					}
					$ial_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at);
					$autoliberacion_day+=fechas_tecmaq($ial_created_at,$ls_created_at);
				}
			}
			/*PRoducciion*/
			$data_prod=$wc_prod->whereBetween('start_time',[$last_day,$last_day2]);
			foreach ($data_prod as $prod) {
				if (!is_null($prod->start_time)) {
					$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time)->addMinutes(1);
					if (is_null($prod->end_time) && ($prod->status_end!='cr' && !is_null($prod->status_end))) {
						$end_time=Carbon::now();
					}elseif(is_null($prod->end_time) && $prod->status_end=='cr'){
						$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time)->addHours(2);
					}
					$start_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time);

					$produccion_day+=fechas_tecmaq($start_time,$end_time);
				}
			}
			$setup_day=$ajuste_day+$herramentaje_day+$liberacion_day+$autoliberacion_day;
			if (($setup_day+$produccion_day)<=$day_tecmaq) {
				$sinuso+=$day_tecmaq-$setup_day-$produccion_day;
			}
			$ajuste+=$ajuste_day;
			$herramentaje+=$herramentaje_day;
			$liberacion+=$liberacion_day;
			$autoliberacion+=$autoliberacion_day;
			$produccion+=$produccion_day;

			// dd($day_tecmaq,$setup,$produccion,$data_prod->toArray());
			DB::commit();
			$last_day->addDay();
			$last_day2->addDay();
		}
	}
	return collect([
			'data_name'=>collect([
				'ajuste'=>$ajuste,
				'herramentaje'=>$herramentaje,
				'liberacion'=>$liberacion,
				'autoliberacion'=>$autoliberacion,
				'produccion'=>$produccion,
				'sinuso'=>$sinuso,
				'setup'=>$ajuste+$herramentaje+$liberacion+$autoliberacion,
			]),
			'data'=>collect([
				$ajuste,
				$herramentaje,
				$liberacion,
				$autoliberacion,
				$produccion,
				$sinuso,
			]),
		]);

}
/*
* $dataTimeEnd -> fecha de pasado
* $dateTimeStart -> fecha actual (presente)
*/
function getTimeIndicadores2(Carbon $dateTimeEnd,Carbon $dateTimeStart)
{
	$wc=DB::table('workcenters')
		->select(DB::raw('count(id) as total'))
		->whereNotNull('ip_address')
		->first();
	$days=$dateTimeEnd->diffInDays($dateTimeStart) + 1;
	// dd($days);
	// if ($days<=0) {
	// 	$days=1;
	// }
	$produccion=$ajuste=$herramentaje=$liberacion=$autoliberacion=0;
	$mantenimiento=$pausas_otros=0;

	$day_tecmaq=15*60*60; // 15 horas en segundos
	$sinuso_total=$day_tecmaq*$wc->total*$days;
	$sinuso=$day_tecmaq*$wc->total*$days;
	$now=Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeStart)->endOfDay();
	$last_day=Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeEnd)->startOfDay();
	$last_day2=Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeEnd)->endOfDay();
	$wc_setup = DB::table('setup')
		->select('setup.*','p.status','p.ih_created_at as ih_created','p.fh_created_at as fh_created'
			,'p.ia_created_at as ia_created','p.fa_created_at as fa_created')
		->join("processes as p","p.id","=","setup.process_id")
		->join("workcenters as wc","wc.id","=","setup.workcenter_id")
		->whereBetween('setup.ih_created_at',[$last_day,$now])
		->whereNotNull('setup.ih_created_at')
		->whereIn('p.status',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])
		->get();
	$wc_prod=UserProcess::whereBetween('start_time',[$last_day,$now])
		->where(function($q_up){
			$q_up->whereIn('status_start',['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp']);
		})
		->get();
	$pausas=Pause::whereBetween('started_at',[$last_day,$now])
		->get();
	// dd($wc_prod->toArray());
	while ($last_day->lessThanOrEqualTo($now)) {
		if ($last_day->dayOfWeek === Carbon::SUNDAY) {
			$sinuso=$sinuso-($day_tecmaq*$wc->total);
		}else{
			$sinuso_day=$produccion_day=$ajuste_day=$herramentaje_day=$liberacion_day=$autoliberacion_day=0;
			$mantenimiento_day=$pausas_otros_day=0;
			$data=$wc_setup->whereBetween('ih_created_at',[$last_day,$last_day2]);
			// SETUP
			// DB::beginTransaction();
			foreach ($data as $set) {
				if(!is_null($set->ih_created_at)){
					if (!is_null($set->fh_created_at)) {
						$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fh_created_at);
					}elseif (is_null($set->fh_created_at) && !is_null($set->ia_created_at)) {
						$set->fh_created_at=$set->ia_created_at;
						// DB::table('setup')->where('id',$set->id)
						// 	->update(['fh_created_at'=>$set->ia_created_at]);
					}elseif (is_null($set->fh_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$fh_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at)->addHours(2);
					}
					$ih_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ih_created_at);
					$herramentaje_day+=fechas_tecmaq($ih_created_at,$fh_created_at);
				}
			}
			$data=$wc_setup->whereBetween('ia_created_at',[$last_day,$last_day2]);
			foreach ($data as $set) {
				if(!is_null($set->ia_created_at)){
					if (!is_null($set->fa_created_at)) {
						$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->fa_created_at);
					}elseif (is_null($set->fa_created_at) && (!is_null($set->il_created_at) || !is_null($set->ial_created_at) )) {
						$set->fa_created_at=is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at;
						// DB::table('setup')->where('id',$set->id)
						// 	->update(['fa_created_at'=>is_null($set->ial_created_at)?$set->il_created_at:$set->ial_created_at]);
					}elseif (is_null($set->fa_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$fa_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at)->addHours(2);
					}
					$ia_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ia_created_at);
					$ajuste_day+=fechas_tecmaq($ia_created_at,$fa_created_at);
				}
			}
			$data=$wc_setup->whereBetween('il_created_at',[$last_day,$last_day2]);
			foreach ($data as $set) {
				if(!is_null($set->il_created_at)){
					if (!is_null($set->ls_created_at)) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
					}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at)->addHours(2);
					}
					$il_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->il_created_at);
					$liberacion_day+=fechas_tecmaq($il_created_at,$ls_created_at);
				}
			}
			$data=$wc_setup->whereBetween('ial_created_at',[$last_day,$last_day2]);
			foreach ($data as $set) {
				if(!is_null($set->ial_created_at)){
					if (!is_null($set->ls_created_at)) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ls_created_at);
					}elseif (is_null($set->ls_created_at) && isset($set->pr_created_at) && !is_null($set->pr_created_at)) {
						$set->ls_created_at=$set->pr_created_at;
						// DB::table('setup')->where('id',$set->id)
						// 	->update(['ls_created_at'=>$set->pr_created_at]);
					}elseif (is_null($set->ls_created_at) && in_array($set->status,['pr', 'pm', 'pmp', 'pfh', 'pfm', 'cr', 'crp'])) {
						$ls_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at)->addHours(2);
					}
					$ial_created_at=Carbon::createFromFormat('Y-m-d H:i:s',$set->ial_created_at);
					$autoliberacion_day+=fechas_tecmaq($ial_created_at,$ls_created_at);
				}
			}
			/*PRoducciion*/
			$data_prod=$wc_prod->whereBetween('start_time',[$last_day,$last_day2]);
			foreach ($data_prod as $prod) {
				if (!is_null($prod->start_time)) {
					if (!is_null($prod->end_time)) {
						$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->end_time);
					}else
						$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time)->addMinutes(120);

					// if (is_null($prod->end_time) && ($prod->status_end!='cr' && !is_null($prod->status_end))) {
					// 	$end_time=Carbon::now();
					// }elseif(is_null($prod->end_time) && $prod->status_end=='cr'){
					// 	$end_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time)->addHours(2);
					// }
					$start_time=Carbon::createFromFormat('Y-m-d H:i:s',$prod->start_time);

					$produccion_day+=fechas_tecmaq($start_time,$end_time);
				}
			}

			/*Pausas*/
			$data=$pausas->where('motivo','mantenimiento');
			foreach ($data as $pause) {
				if (!is_null($pause->started_at)) {
					$started_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->started_at);
					$ended_at=null;
					if (!is_null($pause->ended_at)) {
						$ended_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->ended_at);
					}
					$mantenimiento_day+=fechas_tecmaq($started_at,$ended_at);
				}
			}
			$data=$pausas->where('motivo','!=','mantenimiento');
			foreach ($data as $pause) {
				if (!is_null($pause->started_at)) {
					$started_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->started_at);
					$ended_at=null;
					if (!is_null($pause->ended_at)) {
						$ended_at=Carbon::createFromFormat('Y-m-d H:i:s',$pause->ended_at);
					}
					$pausas_otros_day+=fechas_tecmaq($started_at,$ended_at);
				}
			}

			$setup_day=$ajuste_day+$herramentaje_day+$liberacion_day+$autoliberacion_day;
			$pausas_day=$mantenimiento_day+$pausas_otros_day;

			// if (($setup_day+$produccion_day+$pausas_day)<=$sinuso_total) {
			// 	$sinuso=$sinuso-$setup_day-$produccion_day-$pausas_day;
			// }


			$ajuste+=$ajuste_day;
			$herramentaje+=$herramentaje_day;
			$liberacion+=$liberacion_day;
			$autoliberacion+=$autoliberacion_day;
			$produccion+=$produccion_day;
			$mantenimiento+=$mantenimiento_day;
			$pausas_otros+=$pausas_otros_day;
			// DB::commit();
		}
		// dd($day_tecmaq,$setup,$produccion,$data_prod->toArray());
		$last_day->addDay();
		$last_day2->addDay();
	}
	$setup=$ajuste+$herramentaje+$liberacion+$autoliberacion;
	if (($setup+$produccion+$mantenimiento+$pausas_otros)<=$sinuso_total) {
		$sinuso=$sinuso_total-($setup+$produccion+$mantenimiento+$pausas_otros);
	}
	return collect([
		'data_name'=>collect([
			'ajuste'=>$ajuste,
			'herramentaje'=>$herramentaje,
			'liberacion'=>$liberacion,
			'autoliberacion'=>$autoliberacion,
			'produccion'=>$produccion,
			'mantenimiento'=>$mantenimiento,
			'pausa_otros'=>$pausas_otros,
			'sinuso'=>$sinuso,

			'sinuso_total'=>$sinuso_total,
			'day_tecmaq'=>$day_tecmaq,
			'setup'=>$setup,
		]),
		'data'=>collect([
			$ajuste,
			$herramentaje,
			$liberacion,
			$autoliberacion,
			$produccion,
			$sinuso,
		]),
	]);
}
