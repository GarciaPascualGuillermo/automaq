<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Setup extends Model
{
	use Uuids;

	protected $keyType = 'string';
	protected $table = 'setup';

	protected $fillable=[
		'user_id','process_id','workcenter_id',
        'ajustador_ia_id','ajustador_fa_id',
        'ajustador_il_id','ajustador_ial_id','ajustador_pm_id',
        'ih_created_at','fh_created_at',
        'ia_created_at','fa_created_at',
        'il_created_at','ial_created_at',
        'ls_created_at','ln_created_at',
	];

	// protected $with = ['job','operation'];
	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'ih_created_at' => 'datetime',
		'fh_created_at' => 'datetime',
		'ia_created_at' => 'datetime',
		'fa_created_at' => 'datetime',
		'il_created_at' => 'datetime',
		'ial_created_at' => 'datetime',
		'ls_created_at' => 'datetime',
		'ln_created_at' => 'datetime',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'ih_created_at','fh_created_at','ia_created_at','fa_created_at','il_created_at',
		'ial_created_at','ls_created_at','ln_created_at',
	];

	/*relaciones*/
	public function process()
	{
		return $this->belongsTo(Process::class);
	}
	public function workcenter()
	{
		return $this->belongsTo(Workcenter::class);
	}
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function ajustador_ia()
	{
		return $this->belongsTo(User::class);
	}
	public function ajustador_fa()
	{
		return $this->belongsTo(User::class);
	}
	public function ajustador_il()
	{
	  return $this->belongsTo(User::class);
	}
	public function ajustador_ial()
	{
		return $this->belongsTo(User::class);
	}

	public function rejected_processes()
	{
		return $this->hasMany(RejectedProcess::class);
	}
}
