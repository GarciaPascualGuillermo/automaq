<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Cycle;
use App\Events\AmountUpdated;
use App\Events\DesasignarEvent;
use App\Events\FinishProcessEvent;
use App\Events\OperadorAmountPieces;
use App\Events\PhaseUpdated;
use App\Process;
use App\ProcessLog;
use App\Traits\Schedule;
use App\User;
use App\UserProcess;
use App\Workcenter;
use Illuminate\Support\Facades\Schema;

Route::get('/', "OperadorController@glogin")->name('root');

Route::get('/reporteador/{routeRedirect?}', 'HomeController@reporteador')->name('reporteador');

Route::get('/open-session/{token}', function (String $token) {

    if (Auth::check()) {
        return redirect()->route('root');
    }

    if (!$token) {
        return redirect()->route('root');
    }

    $user = User::whereToken($token)->first();

    if (!$user) {
        return redirect()->route('root');
    }

    Auth::login($user, true);

    auth()->user()->update([
        'token' => ''
    ]);

    return redirect()->route('root');

});

Auth::routes();

Route::prefix('admin/schedule')->group(function ()
{
    Route::get('/', 'ScheduleController@index')->name('admin.schedule.index');
    Route::put('/alternate/{schedule}', 'ScheduleController@alternate')->name('admin.schedule.alternate');
    Route::post('/generate', 'ScheduleController@generate')->name('admin.schedule.generate');
});

Route::group(['prefix' => 'operador'], function () {
    Route::post('logout','OperadorController@logout')->name('operador.logout');

    Route::group(['prefix'=>'pausa'],function (){
        Route::post('motivo','PausaController@motivo')->name('operador.pausa.motivo');
        Route::post('login','PausaController@login')->name('operador.pausa.login');
        Route::group(['prefix'=>'herramienta'],function (){
            Route::post('/','PausaController@herramienta')->name('operador.pausa.herramienta');
            Route::post('finalizar','PausaController@h_finalizar')->name('operador.pausa.herramienta.finalizar');
        });
        Route::group(['prefix'=>'material'],function (){
            Route::post('/','PausaController@material')->name('operador.pausa.material');
            Route::post('finalizar','PausaController@m_finalizar')->name('operador.pausa.material.finalizar');
        });
        Route::post('materialproceso','PausaController@login')->name('operador.pausa.materialproceso');
        Route::get('historial_semana', 'HistorialSController@index')->name('historial_semana.index');

        /*Mantenimiento*/
        Route::group(['prefix'=>'mantenimiento'],function (){
            Route::post('espera','MantenimientoController@espera')->name('operador.pausa.mantenimiento.espera');
            Route::post('getUserMantenimiento','MantenimientoController@getUserMantenimiento')->name('operador.pausa.mantenimiento.getUserMantenimiento');
            Route::post('finalizar_valid_credential','MantenimientoController@finalizar_valid_credential')->name('operador.pausa.mantenimiento.finalizar_valid_credential');
            Route::post('finalizar','MantenimientoController@finalizar')->name('operador.pausa.mantenimiento.finalizar');
            Route::post('finalizar_support','MantenimientoController@finalizar_support')->name('operador.pausa.mantenimiento.finalizar_support');
        });
    });

    Route::post('setup/reset', 'SetupController@resetSetup')->name('operador.setup.reset');

    Route::get('/', 'OperadorController@index')->name('operador.index');
    Route::group(['prefix'=>'scraps'],function (){
       Route::post('/', 'OperadorController@scraps')->name('operador.scraps');
       Route::post('/getPiece', 'OperadorController@getPiece')->name('operador.getPiece');
    });

    // Route::get('/', 'OperadorController@index')->name('operador.index');
    Route::group(['prefix' => 'setup'], function () {
        Route::get('/', 'SetupController@index')->name('operador.setup.index');
        Route::group(['prefix' => 'step2'], function () {
            Route::get('/', 'SetupController@step2')->name('operador.setup.step2');
            Route::get('/proceso', 'SetupController@step2proceso')->name('operador.setup.step2proceso');
        });
        Route::group(['prefix' => 'step3'], function () {
            Route::get('/calidad', 'SetupController@step3Calidad')->name('operador.setup.step3.calidad');
            Route::get('/autoliberacion', 'SetupController@step3Autoliberacion')->name('operador.setup.step3.autoliberacion');
            Route::post('/autoliberacion','SetupController@autoliberacion')->name('operador.setup.step3.autoliberacion');
        });
        Route::group(['prefix' => 'step4'], function () {
            Route::get('/liberada', 'SetupController@step4liberada')->name('operador.setup.step4.liberada');
            Route::post('/iniciaproduccion', 'SetupController@iniciaproduccion')->name('operador.setup.step4.iniciaproduccion');

            Route::get('/rechazada', 'SetupController@step4rechazada')->name('operador.setup.step4.rechazada');
            Route::post('/noiniciaproduccion', 'SetupController@noiniciaproduccion')->name('operador.setup.step4.noiniciaproduccion');
        });
        Route::post('/loginajustador', 'SetupController@loginajustador')->name('operador.setup.loginajustador');
        Route::post('/loginajustadorproceso', 'SetupController@loginajustadorproceso')->name('operador.setup.loginajustadorproceso');
        Route::post('/tipoliberacion', 'SetupController@tipoliberacion')->name('operador.setup.tipoliberacion');
        Route::post('/calidad/recive', 'SetupController@recive')->name('operador.setup.recive');
    });
    Route::post('/getDibujoTecnico','OperadorController@getDibujoTecnico')->name('operador.getDibujoTecnico');

    Route::post('/login', 'OperadorController@login')->name('operador.login');
    Route::group(['prefix'=>'capeando'],function (){
        Route::post('/login', 'CapeandoController@login')->name('operador.capeando.login');
        Route::post('/getprocess', 'CapeandoController@getprocess')->name('operador.capeando.getprocess');
    });

    Route::post('/cycles','OperadorController@cycles')->name('operador.cycles');
    Route::post('/productividad','OperadorController@productividad')->name('operador.productividad');
    Route::get('/productividad','OperadorController@productividad')->name('operador.productividad');

    Route::group(['prefix'=>'pausa'],function (){
        Route::group(['prefix'=>'mantenimiento'],function (){
            Route::post('espera','MantenimientoController@espera')->name('operador.pausa.mantenimiento.espera');
            Route::group(['prefix'=>'proceso'],function (){
                Route::post('/soporte','MantenimientoController@soporte')->name('operador.pausa.mantenimiento.soporte');
                Route::post('/finalizar','MantenimientoController@finalizar')->name('operador.pausa.mantenimiento.finalizar');
            });
        });
    });
});
//Maquinas
Route::group(['prefix'=>'maquinas'],function (){
    Route::get('/', 'MaquinasController@maquinas')->name('maquinas.index');
    Route::post('/updateMachines', 'MaquinasController@updateMachines')->name('maquinas.update');
    Route::post('/datos','MaquinasController@datos')->name('maquinas.datos');
    Route::get('/datos','MaquinasController@datos')->name('maquinas.datos');
});

//Jobs
Route::group(['prefix' => 'jobs'], function () {

    Route::get('/', 'JobsController@index')->name('jobs.index');
    Route::post('/desasignar', 'JobsController@desasignar')->name('jobs.asignados.desasignar');
    Route::post('/desasignar/readed', 'JobsController@desasignarReaded')->name('jobs.asignados.readed');
    Route::post('/help/system', 'JobsController@solicitarAyuda')->name('jobs.asignados.ayuda');

    route::group(['prefix'=>'asignados'],function (){

        Route::get('/', 'JobsController@asignados')->name('jobs.asignados.index');
        Route::post('/selecciona', 'JobsController@selecciona')->name('jobs.asignados.selecciona');


    });
    //Solo de prueba eliminar al final maquinas 1
    Route::get('/maquinas1', 'JobsController@maquinas1')->name('maquinas1.index');
    Route::post('/update_job', 'JobsApiController@update_job')->name('jobs.update_job');
    Route::get('/update_job', 'JobsApiController@update_job')->name('jobs.update_job');

});

//Supervisores
Route::get('/supervisores', 'SupervisoresController@supervisores')->name('supervisores.index');
//Indicadores
Route::group(['prefix'=>'indicadores'],function(){
    Route::get('/', 'IndicadorController@indicadores')->name('indicadores.index');
    Route::post('/semanal_mensual', 'IndicadoresGraph@semanal_mensual')->name('indicadores.semanal_mensual');
    Route::get('/semanal_mensual', 'IndicadoresGraph@semanal_mensual')->name('indicadores.semanal_mensual');

    Route::post('/semanal', 'IndicadoresGraph@semanal')->name('indicadores.semanal');
    Route::get('/semanal', 'IndicadoresGraph@semanal')->name('indicadores.semanal');

    Route::post('/productividad', 'IndicadoresGraph@productividad')->name('indicadores.productividad');
    Route::get('/productividad', 'IndicadoresGraph@productividad')->name('indicadores.productividad');

    Route::post('/uso_maquinas', 'IndicadoresGraph@machineUse')->name('indicadores.uso_maquinas');

});

Route::get('/indicadores2', 'IndicadorController2@indicadores2')->name('indicadores2.index');

//Mantenimiento
Route::group(['prefix' => 'mantenimiento'], function () {
    Route::get('/','MantenimientoController@pausa')->name('mantenimiento.index');
    Route::get('/espera','CalidadController@mantenimiento')->name('mantenimiento.index');
    Route::get('/soporte','MantenimientoController@send_soporte')->name('mantenimiento.soporte');
});


//Productividad
Route::group(['prefix' => 'productividad'], function () {
    Route::get('/', 'ProductividadController@index')->name('productividad.index');
    Route::get('/2/{part?}', 'ProductividadController@index2')->name('productividad.index2');
    Route::get('/top6', 'ProductividadController@top6')->name('top6.index');
});

//Calidad

Route::group(['prefix' => 'calidad'], function () {
    Route::group(['prefix'=>'mantenimiento'],function (){
        Route::post('/status','MantenimientoController@status')->name('calidad.mantenimiento.status');
    });
    Route::post('/liberacion/success','CalidadController@liberacionsuccess')->name('calidad.liberacionsuccess');
    Route::get('/historial','CalidadController@historial')->name('calidad.historial');
    Route::post('/calidad/liberaLogin','CalidadController@liberaLogin')->name('calidad.liberaLogin');
    Route::post('/rechazaLogin','CalidadController@rechazaLogin')->name('calidad.rechazaLogin');

    //Calidad, 4k liberacion
    Route::get('/liberacion','CalidadController@liberacion')->name('calidad.liberacion');
    Route::get('/liberacion4k','CalidadController@liberacion4k')->name('calidad.liberacion4k');
    Route::post('/calidad/getProcessLiberacion','CalidadController@getProcessLiberacion')->name('calidad.getProcessLiberacion');
});

//Reportes
Route::get('/reportes', 'ReporteController@reporte')->name('reporte.index');
Route::get('/eficiencia', 'ReporteController@eficiencia')->name('eficiencia.index');

//Mensaje-Global
Route::get('/mensaje', 'MensajeGlobalController@mensaje_global')->name('mensaje_global.index');
Route::post('/mensaje/getMensajeGlobal', 'MensajeGlobalController@mensaje')->name('mensaje.index');

/**/
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'notifications'], function () {
    Route::get('/setup', 'NotificationController@setup')->name('setup');
    Route::post('/herramentaje', 'NotificationController@herramentaje')->name('notification.operador.herramentaje');
    // Route::get('/herramentaje', 'NotificationController@herramentaje')->name('notification.operador.herramentaje');
});

// Push Subscriptions
Route::post('subscriptions', 'PushSubscriptionController@update');
Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');


Route::group(['prefix'=>'api'],function (){
    // JOBS
    Route::get('/processes','JobsApiController@processes')->name('api.processes');
    Route::post('/processes','JobsApiController@processes')->name('api.processes');
    Route::post('/terminar','JobsApiController@terminar')->name('api.terminar');
    Route::post('/charge/terminar','JobsApiController@chargeterminar')->name('api.charge.terminar');
    Route::post('/eficiencia','JobsApiController@eficiencia')->name('api.eficiencia');
    Route::post('/eficiencia/ceo','JobsApiController@eficienciaCEO')->name('api.eficiencia.ceo');
    Route::post('/charge/','JobsApiController@charge')->name('api.charge');
    Route::post('jobs/soft_delete','JobsApiController@soft_delete')->name('api.jobs.soft_delete');

    Route::get('/workcenters','JobsApiController@workcenters')->name('api.workcenters');
    Route::post('/workcenters','JobsApiController@workcenters')->name('api.workcenters');

    Route::group(['prefix'=>'jobs'],function (){
        Route::post('/divide', 'JobsApiController@divide')->name('api.jobs.divide');
    });

    Route::post('/addJobsToWorkcenter','JobsApiController@addJobsToWorkcenter')->name('api.addJobsToWorkcenter');
    Route::post('/GetJobOfWC','JobsApiController@GetJobOfWC')->name('api.GetJobOfWC');
    Route::post('/restoreJob','JobsApiController@restoreJob')->name('api.restoreJob');

    Route::post('/restoreJobs','JobsApiController@restoreJobs')->name('api.restoreJobs');

    Route::post('/restoreFinalJobs','JobsApiController@restoreFinalJobs')->name('api.restoreFinalJobs');


    Route::group(['prefix'=>'process'],function (){
        Route::post('/getProcess','ProcessApiController@getProcess')->name('api.getProcess');
    });
    Route::group(['prefix'=>'workcenter'],function (){
        Route::post('/recive','workCenterApiController@recive')->name('api.recive');
    });
});

Route::post('/workcenter/reorder', 'WorkCenterApiController@reorder')->name('workcenter.reorder');

//Rutas de historial para Sidebar

Route::group(['prefix'=>'historial'],function (){
    Route::group(['prefix'=>'jobs'],function (){
        Route::get('/', 'HistorialController2@jobs')->name('historial.jobs')->where('keyword', '.*');;
        Route::get('/eliminados', 'HistorialController2@jobsEliminados')->name('historial.jobs.eliminados');
        Route::post('/restaurados', 'HistorialController2@restoreFinalJobs')->name('historial.jobs.restaurados');
        Route::put('/restaurados_t', 'HistorialController2@restoreFinalJobsTer')->name('historial.jobs.restaurados_t');
    });
    Route::group(['prefix'=>'maquinas'],function (){
        Route::get('/dia', 'HistorialController2@maquinas_dia')->name('historial_dia.maquinas');
        Route::get('/semana', 'HistorialController2@maquinas_semana')->name('historial_semana.maquinas');
    });
    Route::group(['prefix'=>'operadores'],function (){
        Route::get('/semana', 'HistorialController2@operadores')->name('historial_semana.operadores');
        Route::get('/dia', 'HistorialController2@operadores_dia')->name('historial_dia.operadores');

    });
    Route::group(['prefix'=>'calidad'],function (){
        Route::get('/', 'HistorialController2@calidad')->name('historial.calidad');
    });
    Route::group(['prefix'=>'mantenimiento'],function (){
        Route::get('/', 'HistorialController2@mantenimiento')->name('historial.mantenimiento')->where('keyword', '.*');;;
    });
    Route::group(['prefix'=>'api'],function (){
        Route::group(['prefix'=>'maquinas'],function (){
            Route::get('/dia', 'HistorialApiController@maquinas_dia')->name('historial.api.maquinas.dia');
            Route::post('/dia', 'HistorialApiController@maquinas_dia')->name('historial.api.maquinas.dia');
            Route::get('/semanal', 'HistorialApiController@maquinas_semanal')->name('historial.api.maquinas.semanal');
            Route::post('/semanal', 'HistorialApiController@maquinas_semanal')->name('historial.api.maquinas.semanal');
        });
        Route::group(['prefix'=>'operadores'],function (){
            Route::get('/dia', 'HistorialApiController@operadores_dia')->name('historial.api.operadores.dia');
            Route::post('/dia', 'HistorialApiController@operadores_dia')->name('historial.api.operadores.dia');
            Route::get('/semanal', 'HistorialApiController@operadores_semanal')->name('historial.api.operadores.semanal');
            Route::post('/semanal', 'HistorialApiController@operadores_semanal')->name('historial.api.operadores.semanal');
        });
    });
});


//Ruta para exportar CSV
Route::group(['prefix'=>'export'],function (){
    Route::get('/job', 'ExportController@jobs')->name('export.jobs');
    Route::get('/calidad', 'ExportController@calidad')->name('export.calidad');
    Route::get('/mantenimiento', 'ExportController@mantenimiento')->name('export.mantenimiento');
    Route::get('/operadores', 'ExportController@operadores')->name('export.operadres');
    Route::get('/maquinas_semana', 'ExportController@maquinas_semana')->name('export.maquina_semanas');
    Route::get('/maquinas_dia', 'ExportController@maquinas_dia')->name('export.maquinas_dias');
});


Route::get('/logout','Auth\LoginController@logout')->name('logout');




Route::get('/export/supervisores/pdf', 'SupervisoresController@exportpdf')->name('supervisores.export.pdf');
Route::get('/export/supervisores/excel', 'SupervisoresController@exportexcel')->name('supervisores.export.excel');

Route::get('/flush-session', 'JobsController@flushSession')->name('flush.session');


Route::get('/petition/{id?}/{quantity?}', function ($id = null, $quantity = null)
{



    // $process = Process::where('id', $id)->with(['user_processes.cycles'])->first();


    // $cycles = $process->user_processes->pluck('cycles')->collapse()
    // ->sortBy('message_date')
    // ->map(function ($cycle)
    // {
    //     return $cycle->message . '  ' . $cycle->message_date . '  ' . $cycle->process_status;
    // });

    // dd($cycles);


    // $process_logs =  ProcessLog::whereNull('workcenter_id')->with('process')->get();

    // dd($process_logs);
    // $processes = Process::where('job_id', 'd9aced7f-ae7e-4081-99b7-363bfe4c2882')->whereHas('operation', function ($query) {
    //     $query->where('operation_service', 20);
    // })
    // ->with('operation')
    // ->get();



    // $workcenter = Workcenter::with(['processes' => function ($query) {
    //     $query->orderBy('order', 'ASC');
    // }])
    // ->where('num_machine', 'CT-29')
    // ->first()
    // ->processes
    // ->map(function ($process) {
    //     return $process->job->num_job . " => " . $process->order . ' => OPERACION ' . $process->operation->operation_service;
    // });
    //     ;

    // dd($workcenter);
    // $user_process = App\UserProcess::where('id', "95cd3e99-1942-4b82-9843-c31934787a34")->first();
    $user_process = App\UserProcess::where('process_id', $id)->orderBy('created_at', 'DESC')->first();


    for ($i = 0; $i < $quantity; $i++) {

        Cycle::create([
            'user_process_id' => $user_process->id,
            'message' => 'START',
            'message_date' => now()->addSeconds($i + 0),
            'process_status' => 'pr',
        ]);

        Cycle::create([
            'user_process_id' => $user_process->id,
            'message' => '12345',
            'message_date' => now()->addSeconds($i + 1),
            'process_status' => 'pr',
        ]);

    }

    event(new PhaseUpdated);
    $process = $user_process->process;
    broadcast(
        new OperadorAmountPieces($process->workcenter, getCycles($process, $user_process->user)['piecePercent'], getCycles($process, $user_process->user)['pieceTotal'])
    );
    // event(new AmountUpdated);

    // $wc = Workcenter::where('ip_address', '127.0.0.1')->first();

    // broadcast(
    //     new OperadorAmountPieces( $wc, '20', '70') );

    // event(new PhaseUpdated());

    // dd(App::environment('local'));

    // $user_processes = UserProcess::where('process_id', 'a32cfdd8-908b-499e-9ca0-8ece09f0b922')->get();

    // foreach ($user_processes as $user_process) {
    //     foreach ($user_process->process_logs as $log) {
    //         $log->delete();
    //     }

    //     foreach ($user_process->cycles as $cycle) {
    //         $cycle->delete();
    //     }
    //     $user_process->delete();
    // }

    return 'Success';

});
