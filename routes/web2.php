<?php 

Route::group(['prefix' => 'admin'], function () {
	Route::group(['prefix' => 'usuarios'], function () {
    	Route::get('/', 'UserController@index')->name('usuarios.index');
    	Route::get('/crear', 'UserController@crear')->name('usuarios.crear');
    	Route::post('/usuarios/store','UserController@store')->name('usuarios.store');
    	Route::get('/{user_id}/actualizar', 'UserController@editar')->name('usuarios.editar');
    	Route::post('/{user_id}/update', 'UserController@update')->name('usuarios.update');
    	Route::post('/{user_id}/destroy', 'UserController@destroy')->name('usuarios.destroy');
	});

	Route::group(['prefix' => 'centro_trabajo'], function () {
    	Route::get('/', 'WCController@index')->name('centro_trabajo.index');
    	Route::get('/crear', 'WCController@crear')->name('centro_trabajo.crear');
    	Route::post('/centro_trabajo/store','WCController@store')->name('centro_trabajo.store');
    	Route::get('/{ct_id}/editar', 'WCController@editar')->name('centro_trabajo.editar');
    	Route::post('/{ct_id}/update', 'WCController@update')->name('centro_trabajo.update');
    	Route::post('/{ct_id}/destroy', 'WCController@destroy')->name('centro_trabajo.destroy');
	});


    Route::get('/parametros', 'ParameterController@index')->name('parametros.index');
    Route::post('/parametros/store', 'ParameterController@store')->name('parametros.store');
});
//Rutas para el envio de EMAILS
Route::get('/send_dynamic', 'MailController@MantenimientoCNCEmail');
Route::get('/prueba_ind', 'PauseController@index');


Route::group(['prefix'=>'operadores'],function (){
    // Route::redirect('/dia', '/historial/operadores/dia')->name('historial_dia.operadores');
});