<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
use Illuminate\Support\Facades\Log;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('amount_piece-{id}', function ($wc, $id) {
	// Log::debug($wc->toArray());
    return true;
});
Broadcast::channel('quality-{id}', function ($wc, $id) {
	// Log::debug($wc->toArray());
    return true;
});
Broadcast::channel('mantenimiento-{id}', function ($wc, $id) {
	// Log::debug($wc->toArray());
    return true;
});

Broadcast::channel('mantenimiento', function ($wc) {
	// Log::debug($wc->toArray());
    return true;
});
Broadcast::channel('calidad', function ($wc) {
	// Log::debug($wc->toArray());
    return true;
});

Broadcast::channel('wc_asignados-{id}', function ($wc) {
    return true;
});
Broadcast::channel('MensajeGlobal', function ($wc) {
    // Log::debug($wc->toArray());
    return true;
});

Broadcast::channel('process-{id}', function ($process) {
    return true;
});

Broadcast::channel('finish-process-{id}', function ($process) {
    return true;
});

Broadcast::channel('phase-updated', function () {
    return true;
});

Broadcast::channel('amount-updated', function () {
    return true;
});

Broadcast::channel('process-update-{job}', function ($job) {
    return true;
});
